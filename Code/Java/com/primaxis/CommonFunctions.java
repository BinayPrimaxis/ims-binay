package com.primaxis;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class CommonFunctions {

	public static TreeMap<String, Double> SortByValue(Map<String, Double> map) {
		ValueComparator bvc = new ValueComparator(map);
		TreeMap<String, Double> sorted_map = new TreeMap<String, Double>(bvc);
		sorted_map.putAll(map);
		return sorted_map;
	}

	public static TreeMap<String, Map<String, Double>> SortInterpreterMap(Map<String, Map<String, Double>> map) {
		ValueComparator_1 bvc = new ValueComparator_1(map);
		TreeMap<String, Map<String, Double>> sorted_map = new TreeMap<String, Map<String, Double>>(bvc);
		sorted_map.putAll(map);
		return sorted_map;
	}
	
}

class ValueComparator_1 implements Comparator<String> {

	Map<String, Map<String, Double>> base;

	public ValueComparator_1(Map<String, Map<String, Double>> base) {
		this.base = base;
	}
	
	public int compare(String a, String b) {
		
		if (base.get(a).get("cntTot") >= base.get(b).get("cntTot")) {
			return -1;
		} else {
			return 1;
		}
	}
	
}

class ValueComparator implements Comparator<String> {

	Map<String, Double> base;

	public ValueComparator(Map<String, Double> base) {
		this.base = base;
	}

	public int compare(String a, String b) {
		if (base.get(a) >= base.get(b)) {
			return -1;
		} else {
			return 1;
		}
	}
}
