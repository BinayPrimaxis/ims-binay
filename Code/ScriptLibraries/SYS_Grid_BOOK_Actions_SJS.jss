function BOOK_GridAction_StatusChange(){
	
	var fldName = 'selUNIDs';
	var vIDs = getComponent(fldName).getValue().split('~');
	getComponent(fldName).setValue('');
	if(IsNullorBlank(vIDs)){
		view.postScript("Notify_Warning('Error Encountered: Please contact IT Support. Error: NO value in selUNIDs')");
		return
	}
	var strStatus = getComponent("StatusChange").getValue();
	getComponent("StatusChange").setValue('');
	if(IsNullorBlank(strStatus)){
		view.postScript("Notify_Warning('Error Encountered: Please contact IT Support. Error: Status is blank')");
		return
	}
		
	var ndb:NotesDatabase = GetDB_Database('BOOK'); var vMsg = [];

	for(var i=0; i<vIDs.length; i++){		
		
		var ndoc:NotesDocument = ndb.getDocumentByUNID(vIDs[i]);
		var sprefix = ndoc.getItemValueString("Form");
		var bSave = true;

		
		if(ndoc == null){
			vMsg.push('ERROR: Booking document not found with ID: ' + vIDs[i] + ', Please contact IT Support');
			bSave = false;
			
		} else {
			//Do not allow to change to any status for OnCall amended bookings
			if (isEnabledOnCallAmendAPI() && @IsMember(ndoc.getItemValueString(sprefix+'_Status'), ['updated','api_amend_pending','api_create_error'])
			&& !ndoc.getItemValueString(sprefix + '_Agency_ID').equals("") 
			&& ndoc.getItemValueString(sprefix + '_Interpreter_FullName').equalsIgnoreCase ("OnCall")) { //If amended for oncall and pending to send to oncall, user should deal booking individually first
				if (ndoc.getItemValueString(sprefix+'_Status') == 'api_amend_pending') {
					vMsg.push('ERROR: Amendments are pending to send to OnCall. Booking is not allowed to change status.');	
				} else {
					vMsg.push('ERROR: Amendments are pending to accept for OnCall bookings.');	
				}
			} else if(strStatus == 'Booked'){
				if(ndoc.getItemValueString(sprefix+'_Status') != 'updated' && ndoc.getItemValueString(sprefix+'_Status') != 'submitted_agency') {
					if (sprefix=="BOOK")
					{
						vMsg.push('ERROR: Booking document patient URN: ' + ndoc.getItemValueString('BOOK_PatientUR') + ', current status can only be '+GetKeyword('BOOK_Status_updated') + " or "+ GetKeyword('BOOK_Status_submitted_agency'));
						
					}
					else
					{
						vMsg.push('ERROR: Booking document #: ' + ndoc.getItemValueString('DocKey') + ', current status can only be '+GetKeyword('BOOK_Status_updated') + " or "+ GetKeyword('BOOK_Status_submitted_agency'));
							
					}
					bSave = false;
				} else {
					var strSD = BOOK_Status(ndoc, 'booked');
					BOOK_LogAction(ndoc, 'Status changed to ' + strSD, true);				
				}
				
			//} else if(@IsMember(strStatus, ['Unmet', 'Other', 'Booking Error'])){
			} else if(strStatus == 'Unmet' | strStatus == 'Other' | strStatus == 'Booking Error'){	

				var statusCode = @LowerCase(strStatus);
				if(statusCode == 'booking error'){statusCode = 'error'}
				var strSD = BOOK_Status(ndoc, statusCode);	
				ndoc.replaceItemValue('BOOK_Completed', 'No');
				ndoc.replaceItemValue('BOOK_NC_Reason', statusCode);	
				BOOK_LogAction(ndoc, 'Booking marked as ' + strSD , true);
			
			} else if(strStatus == 'Rejected By Agency'){

				var strSD = BOOK_Status(ndoc, 'unmet_pending');
				BOOK_LogAction(ndoc, 'Status changed to ' + strSD, true);
								
			} else if(strStatus == 'New'){

				var strSD = BOOK_Status(ndoc, 'requested');
				ndoc.replaceItemValue(sprefix+'_Interpreter_Type', '');
				ndoc.replaceItemValue(sprefix+'_Interpreter_DocKey', '');
				ndoc.replaceItemValue(sprefix+'_Interpreter_FullName','');
				ndoc.replaceItemValue(sprefix+'_Agency_SentDate', '');
				ndoc.replaceItemValue(sprefix+'_Agency_SentBy', '');
				BOOK_LogAction(ndoc, 'Status changed to ' + strSD, true);
				
			} else if(strStatus == 'Completed'){

				if (sprefix=="CBN")
				{
						vMsg.push('ERROR: Combined Bookings (#'+ ndoc.getItemValueString('DocKey') +') cannot be set to completed, you must complete the bookings inside the combined booking');
						
					bSave = false;
				}
				else
				{
					var strSD = BOOK_Status(ndoc, 'completed');;
					ndoc.replaceItemValue('BOOK_Completed', 'Yes');
					BOOK_LogAction(ndoc, 'Booking marked as ' + strSD , true);
				}
				
			} else {				
				view.postScript("Notify_Warning('Error Encountered: Please contact IT Support. Error: Status not handled')");
				return
			}
			
			if(bSave){
				ndoc.computeWithForm(true, false)
				ndoc.save(true, false);
				CBN_Status_Change(ndoc)
			}			
		}
	}
	
	if(vMsg.length == vIDs.length){
		view.postScript("Dialog_Message('Bookings status not changed to " + strStatus + "', '" + vMsg.join('<br/>') + "');"); 
		return
	} 
	
	var vCJS_Function = [];	
		
	if(vMsg.length == 0){
		vCJS_Function.push("Notify_Info('SUCCESS: All selected records marked as \"" + @UpperCase(strStatus) + "\"')");
	} else {
		var strMsg = vMsg.length + " out of \"" + vIDs.length + "\" records are not changed to " + @UpperCase(strStatus);		
		vCJS_Function.push("Notify_Warning('" + strMsg + "')");
	}
	vCJS_Function.push("Grid_Generate_DateRange()");
	vCJS_Function.push("UpdateFTIndex('xFTIndex_Agent.xsp')");		
	view.postScript(vCJS_Function.join("; "));
}

function BOOK_GridAction_StatusToCancel(){

	var vIDs = getComponent('selUNIDs').getValue().split('~');
	getComponent('selUNIDs').setValue('');
	if(IsNullorBlank(vIDs)){print('ERROR: Some Serious Error, NO value in selUNIDs'); return}

	if (typeof cancellationReasonsObject == 'undefined' || cancellationReasonsObject == null) {
		var cancellationReasonsObject = {};
		cancellationReasonsObject.fldCancelReasonDD = getComponent('fldCancelReasonDD').getValue();
		cancellationReasonsObject.fldCancelOtherReason = getComponent('fldCancelOtherReason').getValue();
		cancellationReasonsObject.fldCancelNoticePeriod = getComponent('fldCancelNoticePeriod').getValue();
		cancellationReasonsObject.fldCancelReason = getComponent('fldCancelReason').getValue();
		cancellationReasonsObject.fldCancelINRReason = getComponent('fldCancelINRReason').getValue();
		cancellationReasonsObject.fldCancelIFTAReason = getComponent('fldCancelIFTAReason').getValue();
		cancellationReasonsObject.fldCancelIFTAOtherReason = getComponent('fldCancelIFTAOtherReason').getValue();
	}
	for(var i=0; i<vIDs.length; i++){		
		if(BOOK_CancelBooking(vIDs[i], cancellationReasonsObject) == false){
			view.postScript("Notify_Warning('ERROR: One or more documents are not updated, Please contact IT Support')")
			//clear out the selected ids as there was a problem
			getComponent('selUNIDs').setValue('');
			return
		}
	}
	//We will be putting some ids into the selUNID's field if we hit documents that need more confirmation (ie ones that will
	//incur a charge for canceling.
	var confirmIDs = getComponent('selUNIDs').getValue();

	if (confirmIDs.equals('')){
		view.postScript("Notify_Info('SUCCESS: All selected documents marked as Cancelled'); Grid_Generate_DateRange(); UpdateFTIndex('xFTIndex_Agent.xsp')")

	}else{
		//display a dialog with the relevant info so they can confirm
		var message = "Please note: The following charges will be incured if you cancel these documents.<br><br>";
		confirmIDs = confirmIDs.split ("~");

		for (var i=0; i<confirmIDs.length; i++)
		{
			message += confirmIDs[i].slice(confirmIDs[i].indexOf('|') + 1) +"<br>" ;
		}
		view.postScript("ConfirmAction('" + message +  "','btnBOOKCancelOnCallBooking_SJS')");
	}
}

function BOOK_GridAction_AssignAgency(){
	var vIDs = getComponent('selUNIDs').getValue().split('~');
	getComponent('selUNIDs').setValue('');
	if(IsNullorBlank(vIDs)){print('ERROR: Some Serious Error, NO value in selUNIDs'); return}
	
	var strIDK = getComponent('fldAgency').getValue();
	var strInterpreter = @DbLookup(GetDB_FilePath('AGN'), "all_byDocKey", strIDK, "AGN_Name", "[FAILSILENT]");	
	if(IsNullorBlank(strInterpreter)){
		view.postScript("Notify_Warning('ERROR: Couldn't find Agency details, Please contact IT Support')")
		return
	}
	
	var ndb:NotesDatabase = GetDB_Database('BOOK'); var vMsg = [];
	
	for(var i=0; i<vIDs.length; i++){		
		var ndoc:NotesDocument = ndb.getDocumentByUNID(vIDs[i]);

		if(ndoc == null){
			vMsg.push('ERROR: Booking document not found with ID: ' + vIDs[i] + ', Please contact IT Support');			
		} else {
			var prefix = ndoc.getItemValueString("Form");
			 if(isEnabledOnCallAmendAPI() && @IsMember(ndoc.getItemValueString(prefix+'_Status'), ['updated','api_amend_pending','api_create_error'])  
						&& !ndoc.getItemValueString(prefix + '_Agency_ID').equals("") 
						&& ndoc.getItemValueString(prefix + '_Interpreter_FullName').equalsIgnoreCase ("OnCall")) { //If amended for oncall, let it be handled by API in background
			 	if (ndoc.getItemValueString(prefix+'_Status') == 'api_amend_pending') {
					vMsg.push('ERROR: Amendments are pending to send to OnCall. Booking is not allowed to change Agency.');	
				} else {
					vMsg.push('ERROR: Amendments are pending to accept for OnCall bookings.');	
				}
			} else if(@IsNotMember(ndoc.getItemValueString(prefix+'_Status'), ['requested', 'booked', 'updated'])){
				vMsg.push('ERROR: Booking Status is already marked as ' + ndoc.getItemValueString(prefix+'_Status_Display'));	
			} else {	
				BOOK_AssignToAgency_UpdateDoc(strIDK, strInterpreter, true, null, ndoc,false);
			//	ndoc.computeWithForm(true, false)
			//	ndoc.save(true, false);
			}	
		}			
	}
	
	if(vMsg.length == vIDs.length){
		view.postScript("Dialog_Message('Selected Bookings are not assigned to Agency', '" + vMsg.join('<br/>') + "');"); 
		return
	} 
	
	var vCJS_Function = [];	
	
	if(vMsg.length == 0){
		vCJS_Function.push("Notify_Info('SUCCESS: All selected Bookings are assigned to \"" + strInterpreter + "\"')");
	} else {
		var strMsg = vMsg.length + " out of \"" + vIDs.length + "\" Bookings are not assigned to \"" + strInterpreter + "\ due to following reason/s.<br/><br/>";
		strMsg += vMsg.join('<br/>');
		vCJS_Function.push("Dialog_Message('Interpreter not assigned', '" + strMsg + "')");
	}
	
	vCJS_Function.push("Grid_Generate_DateRange(); UpdateFTIndex('xFTIndex_Agent.xsp');");
	view.postScript(vCJS_Function.join("; "))	
}

function BOOK_GridAction_AssignInterpreter(fldName){
	
	var vIDs = getComponent(fldName).getValue().split('~');
	getComponent(fldName).setValue('');
	if(IsNullorBlank(vIDs)){print('ERROR: Some Serious Error, NO value in selUNIDs'); return}
	
	var strIDK = getComponent('fldInterpreter').getValue();
	var strInterpreter = @DbLookup(GetDB_FilePath('USER'), "all_byDocKey", strIDK, "USER_FullName", "[FAILSILENT]");
	if(IsNullorBlank(strInterpreter)){
		view.postScript("Notify_Warning('ERROR: Couldn't find Interpreter details, Please contact IT Support')")
		return
	}
	
	var ndb:NotesDatabase = GetDB_Database('BOOK'); var vMsg = []; var strMsg;
	var vData_ChangeHours = []; var strRow; var strCheckDate = ''; var bSameDate = true;
	var bModifyBooking = (IsUser_Manager() | IsUser_Coordinator() | IsUser_DBAdmin());
	
	var strCheckAvailMessage = "";
	
	for(var i=0; i<vIDs.length; i++){		
		
		var ndoc:NotesDocument = ndb.getDocumentByUNID(vIDs[i]);	
	
		if(ndoc == null){
			strMsg = 'ERROR: Booking document not found with ID: ' + vIDs[i] + ', Please contact IT Support';
		} else {
			var prefix = ndoc.getItemValueString("Form");
			
			if(isEnabledOnCallAmendAPI() && @IsMember(ndoc.getItemValueString(prefix+'_Status'), ['updated','api_amend_pending','api_create_error']) 
					&& !ndoc.getItemValueString(prefix + '_Agency_ID').equals("") 
					&& ndoc.getItemValueString(prefix + '_Interpreter_FullName').equalsIgnoreCase ("OnCall")) { //If amended for oncall, let it be handled by API in background
				if (ndoc.getItemValueString(prefix+'_Status') == 'api_amend_pending') {
					strMsg = 'ERROR: Amendments are pending to send to OnCall. Booking is not allowed to change Agency.';	
				} else {
					strMsg = 'ERROR: Amendments are pending to accept for OnCall bookings.';	
				}
			} else if(@IsNotMember(ndoc.getItemValueString(prefix+'_Status'), ['requested', 'booked', 'updated'])){
				strMsg = 'ERROR: Booking Status is already marked as ' + ndoc.getItemValueString(prefix+'_Status_Display');				
			}  else {
				strMsg = BOOK_AssignInterpreter_UpdateDoc(ndoc, strIDK, strInterpreter, true);				
				//Make Data for Change Hours Dialog
				if(bModifyBooking & strMsg != "UPDATED" & strMsg.indexOf('LEAVE')<0){					
					strRow = '{"unid" : "' + vIDs[i] + '", ';					
					strRow += '"bTime" : "' + ndoc.getItemValueString(prefix+'_ApptStartTime_Text') + '", ';
					strRow += '"bDuration" : "' + @Text(ndoc.getItemValueDouble(prefix+'_ApptDuration')) + '"}';
					vData_ChangeHours.push(strRow);
					strCheckAvailMessage = strMsg;	// This Message would be same for all records since we are allowing to change hours for single booking date					
					if(strCheckDate == ''){
						strCheckDate = ndoc.getItemValueString(prefix+'_ApptStartDate_Text');
					} else {
						if(strCheckDate != ndoc.getItemValueString(prefix+'_ApptStartDate_Text')){bSameDate = false;}
					}
				}
			}
		}
		if(strMsg != "UPDATED"){
			if(strMsg.indexOf('~')>=0){strMsg = @Left(strMsg, '~')}
			vMsg.push("Selected Record # " + (i+1) + '. ' + strMsg)	//The meaningful Message only
		}				
	}
	
	//Check if Change Hours can be allowed, if all are same date, go ahead, otherwise add to message
	var strMsg_ChangeHours = ""; var strExecuteFunction = "";
	
	if(vData_ChangeHours.length > 0){
		
		strMsg_ChangeHours = "<br/><b><u>NOTE:</u></b> ";
		
		if(bSameDate == false){
			strMsg_ChangeHours += "You cannot change hours because the selected records are of different dates."
			vData_ChangeHours = []; //Kill the data	
		} else {
			
			strMsg_ChangeHours += "You will be prompted for option to Change Hours on close of this notification.";
			
			//Building First Row of JSON Data to be header fields like Interpreter, DocKey, Date, Existing Time,
			var vJSON = [];
			vJSON.push('"iName" : "' + strInterpreter + '"');
			vJSON.push('"iDocKey" : "' + strIDK + '"');
			vJSON.push('"bDate" : "' + strCheckDate + '"');
			
			//Need to add existing hours of Interpreter, refer to Check Availability Message Function
			
			//Received strMsg is - Code ~ BookingDateTime ~ DateTimes(different types for different Codes
			//Return strMsg - MeaningFull Message ~ strIName ~ Original above Received strMsg
			
			var vCode = strCheckAvailMessage.split('~');
			//Length of 5 means no working day for Intepreter
			if(vCode.length > 5){
				vJSON.push('"startTime_1" : "' + vCode[4] + '"');
				vJSON.push('"endTime_1" : "' + vCode[5] + '"');
			}
			if(vCode.length > 6){
				vJSON.push('"startTime_2" : "' + vCode[6] + '"');
				vJSON.push('"endTime_2" : "' + vCode[7] + '"');
			}
			
			//Full Json to be passed to ClientJS Function
			var strJSON = '[{' + vJSON.join(', ') + '}, ' + vData_ChangeHours.join(', ') + ']';
			
			strExecuteFunction = "Grid_Interpreter_EditHours_Show(" + strJSON + ")";
		}
	}	
		
			
	if(vMsg.length > 0){
		strMsg = vMsg.join('<br/>');
		if(strMsg_ChangeHours != ""){strMsg += '<br/>' + strMsg_ChangeHours;}
	}
	
	var vCJS_Function = [];
	
	if(vMsg.length == vIDs.length){		
		vCJS_Function.push("Dialog_Message('Interpreter NOT Assigned', '" + strMsg + "', '" + strExecuteFunction + "')");
	} else {
		
		if(vMsg.length == 0){
			vCJS_Function.push("Notify_Info('SUCCESS: All selected Bookings assigned to \"" + strInterpreter + "\"')");
		} else {			
			var strMsg_Pre = vMsg.length + " out of \"" + vIDs.length + "\" Bookings are not assigned to \"" + strInterpreter + "\ due to following reason/s.<br/><br/>";
			strMsg = strMsg_Pre + strMsg;
			vCJS_Function.push("Dialog_Message('Interpreter NOT Assigned', '" + strMsg + "', '" + strExecuteFunction + "')");
		}
		
		if(fldName.indexOf("_")>=0){
			vCJS_Function.push("Grid_Generate('" + @Right(fldName, "_") + "')");
		} else {
			vCJS_Function.push("Grid_Generate_DateRange()");
		}
		
		vCJS_Function.push("UpdateFTIndex('xFTIndex_Agent.xsp')");
	}
		
	view.postScript(vCJS_Function.join("; "))
}

function BOOK_GridAction_SendToAgency(){	
	var vIDs = getComponent('selUNIDs').getValue().split('~');
	getComponent('selUNIDs').setValue('');
		
	if(IsNullorBlank(vIDs)){print('ERROR: Some Serious Error, NO value in selUNIDs'); return}
	var ndb:NotesDatabase = GetDB_Database('BOOK');
	
	var vCols = GetKeyword("BOOK_AgencyFile");
	var vColHeaders = @Right(vCols, "~");
	var vFields = @Left(vCols, "~");
	var vRows = []; var vRow = [];
	
	//vRows.push(vColHeaders);
	
	for(var i=0; i<vIDs.length; i++){
		
		var ndoc:NotesDocument = ndb.getDocumentByUNID(vIDs[i]);
		if(ndoc == null){
			print('ERROR: BOOK Document not found, UNID: ' + vIDs[i]);
			view.postScript("Notify_Warning('ERROR: Bookings not updated, Please contact IT Support')")
			return false
		}
		//if the agency is 'oncall' then we need to mark it ready for sending through
		var sprefix = ndoc.getItemValueString("Form"); //accomodate CBN
		if(isEnabledOnCallAmendAPI() && @IsMember(ndoc.getItemValueString(sprefix+'_Status'), ['updated','api_amend_pending','api_create_error']) 
				&& !ndoc.getItemValueString(sprefix + '_Agency_ID').equals("") 
				&& ndoc.getItemValueString(sprefix + '_Interpreter_FullName').equalsIgnoreCase ("OnCall")) { //If amended for oncall, let it be handled by API in background
			strMsg = 'ERROR: Amendments are pending to send to OnCall. Booking is not allowed to send to OnCall from grid.';	
		} else if (ndoc.getItemValueString (sprefix+ "_Interpreter_FullName").equalsIgnoreCase("ONCALL")) {
			ndoc.replaceItemValue (sprefix+ "_PendingSend", 1);
		}else{
			BOOK_Status(ndoc, 'submitted_agency');		
			ndoc.replaceItemValue(sprefix+ "_Agency_SentDate", session.createDateTime(@Now()));
		}
		ndoc.replaceItemValue(sprefix+ "_Agency_SentBy", General_GetUser_FullName());
		BOOK_LogAction(ndoc, 'Sent to Agency: ' + ndoc.getItemValueString (sprefix+ "_Interpreter_FullName"), true);
		ndoc.computeWithForm(true, false)
		ndoc.save(true, false);	
		CBN_Status_Change(ndoc);
	}	
}