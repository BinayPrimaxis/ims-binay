var strGenderPre;
var vComms = [];
function CBN_BeforePageLoad(){
	var docX:NotesXspDocument = document1;	
}

function CBN_BOOK_Add(){
	//print("CBN_BOOK_Add()")
	var strID = getComponent('selUNIDs_GRID_CBN_BOOK_Search').getValue()
	//print(strID)
	getComponent('selUNIDs_GRID_CBN_BOOK_Search').setValue('');
	if(IsNullorBlank(strID)){view.postScript("Dialog_Message('ERROR', 'NO Values selected. Please contact IT Support')"); return}
	
	var ndb:NotesDatabase = GetDB_Database('BOOK'); var vMsg = [];
	var docBOOK:NotesDocument = ndb.getDocumentByUNID(strID);
	if(IsNullorBlank(strID)){view.postScript("Dialog_Message('ERROR', 'Booking Document not found. Please contact IT Support')"); return}
	
	var docCBN:NotesDocument = document1.getDocument();
	var intMaxTimeDiff = parseInt(GetKeyword("CBN_MaximumTimeDifference"));
	var sform = docCBN.getItemValueString("Form");
	//print("form="+sform);
	/*Check Time either can be added as first or last
	To add as first, BOOK end time to be greater than CBN Start Time and check difference
	To add as Last, BOOK start time to be greater than CBN End Time and Check difference*/
	var dtStart_BOOK:NotesDateTime = docBOOK.getItemValueDateTimeArray('BOOK_Appt_Start').elementAt(0);
	var dtEnd_BOOK:NotesDateTime = docBOOK.getItemValueDateTimeArray('BOOK_Appt_End').elementAt(0);
	var dtStart_CBN:NotesDateTime = docCBN.getItemValueDateTimeArray(sform+'_Appt_Start').elementAt(0);
	var dtEnd_CBN:NotesDateTime = docCBN.getItemValueDateTimeArray(sform+'_Appt_End').elementAt(0);
	
	var intDiff_Start = dtStart_CBN.timeDifference(dtEnd_BOOK)/60;
	var intDiff_End = dtStart_BOOK.timeDifference(dtEnd_CBN)/60;
	
	//Can it be Added as First ?
	var bAddasFirst = intDiff_Start<=intMaxTimeDiff & intDiff_Start >= 0;
	//print('intDiff_Start: ' + intDiff_Start + '~~~~~~~~~'); print('bAddasFirst: ' + bAddasFirst + ' ~~~~~~~~~');
	
	//Can it be Added as Last ?
	var bAddasLast = intDiff_End<=intMaxTimeDiff & intDiff_End >= 0;
	//print('intDiff_End: ' + intDiff_End + ' ~~~~~~~~~'); print('bAddasLast: ' + bAddasLast + ' ~~~~~~~~~');
	
	if(bAddasFirst == false & bAddasLast == false){
		//Check if overlapping bookings, either book start<cbn end or cbn start<book end
		if ((dtEnd_CBN.timeDifference(dtStart_BOOK)>0 && dtStart_BOOK.timeDifference(dtStart_CBN)>0 ) || (dtEnd_BOOK.timeDifference(dtStart_CBN)>0 && dtStart_CBN.timeDifference(dtStart_BOOK)>0))
		{
			view.postScript("Dialog_Message('ERROR', 'Selected Booking Document has over-lapping time')"); 
			
		}
		else
		{
			view.postScript("Dialog_Message('ERROR', 'Selected Booking Document Time exceeds the allowed Time Difference between bookings')"); 
			
		}
		return		
	}
	if(bAddasFirst & bAddasLast){
		view.postScript("Dialog_Message('ERROR', 'Error adding Booking document, Please contact IT Support')"); 
		return		
	}
	//*** should we instead now just launch into create process with an array of unids, i think this
	//*** would be better
	//print("pre-vUNIDs")
	var vUNIDs = CBN_Get_UNIDS();
	//print("vUNIDS="+vUNIDs);
	vUNIDs.push(strID);
	if (sform=="BOOK")
	{
		vUNIDs.push(docCBN.getUniversalID());
	}
	//print("vUNIDS="+vUNIDs.join(","));
	var vMsg=[];
	var ndc:NotesDocumentCollection = ndb.getProfileDocCollection('BLANK_Document_Collection');
	var ndc
	var vData = CBN_Validate_Collection(vUNIDs,vMsg,ndb,ndc)
	if(vMsg.length > 0){
		view.postScript("Dialog_Message('Bookings cannot be reallocated', '" + vMsg.join('<br/>') + "')"); 
		return
		}
	//print(vData);
	if (sform=="BOOK")
	{
		CBN_Create_New(vUNIDs,"BOOK")
		
	}
	else
	{
		CBN_Reallocate_Process(document1,vUNIDs)
	}
	
	return
	
	
	//Update BOOking document
	var strDocKey = docCBN.getItemValueString('DocKey');
	//888
	var title=docBOOK.getItemValueString("JSON_PFirstName")+" "+docBOOK.getItemValueString("JSON_PLastName") + ": " + docBOOK.getItemValueString("BOOK_ApptStartTime_Text")
	
	var strStatus = docBOOK.getItemValueString('BOOK_Status');
	if (@IsMember(strStatus, ['requested'])){
		docBOOK.replaceItemValue('CBN_DocKey', strDocKey);
		BOOK_Status (docBOOK, "requested"); //set new record to requested
	//vDocKeys.push(docBOOK.getItemValueString("DocKey"));
	//	urlList.push("<li class='gridli'><a title='"+title+"' onclick='IFrame_OpenDoc(\""+docBOOK.getUniversalID()+"\", \"BOOK\")'>"+docBOOK.getItemValueString("DocKey")+"</a></li>");
	//	docBOOK.replaceItemValue("View_CBN","<div style='font-weight:700'>"+docBOOK.getItemValueString("DocKey")+"</div>"+ cbnlink)
		//need to computed View_CBN
		var cbnlink = "<li class='gridli'><a title='Combined Booking Link' onclick='IFrame_OpenDoc(\""+docCBN.getUniversalID()+"\", \"CBN\")'>"+docCBN.getItemValueString("DocKey")+"</a></li>";
		docBOOK.replaceItemValue("View_CBN","<div style='font-weight:700'>"+docBOOK.getItemValueString("DocKey")+"</div>"+ cbnlink)
		BOOK_LogAction(docBOOK, "Added to Combine Booking - " + BOOK_Linkify(docCBN), true);
		var strDocKey_New = docBOOK.getItemValueString('DocKey');	
		var url = "<li class='gridli'><a title='"+title+"' onclick='IFrame_OpenDoc(\""+docBOOK.getUniversalID()+"\", \"BOOK\")'>"+docBOOK.getItemValueString("DocKey")+"</a></li>";

	}
	else
	{
		var docCLONE:NotesDocument = ndb.createDocument();
		docCLONE.replaceItemValue("Form","BOOK")
		docCLONE.replaceItemValue("BOOK_Appt_End",docBOOK.getItemValue("BOOK_Appt_End"))
		BOOK_Create_Cloned_Booking(docCLONE,docBOOK)
		//docCLONE.replaceItemValue("BOOK_Duration",docBOOK.getItemValue("BOOK_Duration"))//function above converts to text, so re-write this value as a double
		docBOOK.replaceItemValue("BOOK_Reallocated","Yes")
		docCLONE.replaceItemValue('CBN_DocKey', strDocKey);
		//	docBOOK.replaceItemValue('BOOK_Interpreter_Type', 'Agency');
		docBOOK.replaceItemValue("FlagCancelledForCBN","true")
		BOOK_Status (docBOOK, "cancellation_req"); //cancel orignal record
		BOOK_Status (docCLONE, "requested"); //set new record to requested
		BOOK_LogAction(docCLONE, "Cloned for Combine Booking - " + BOOK_Linkify(docCBN), true);
		
		docCLONE.replaceItemValue("View_CBN","<div style='font-weight:700'>"+docCLONE.getItemValueString("DocKey")+"</div>"+ cbnlink)
		docCLONE.computeWithForm(true, false);
		docCLONE.save(true, false);	
		var cbnlink = "<li class='gridli'><a title='Combined Booking Link' onclick='IFrame_OpenDoc(\""+docCBN.getUniversalID()+"\", \"CBN\")'>"+docCBN.getItemValueString("DocKey")+"</a></li>";
		docCLONE.replaceItemValue("View_CBN","<div style='font-weight:700'>"+docCLONE.getItemValueString("DocKey")+"</div>"+ cbnlink)

		var strDocKey_New = docCLONE.getItemValueString('DocKey');
		var url = "<li class='gridli'><a title='"+title+"' onclick='IFrame_OpenDoc(\""+docCLONE.getUniversalID()+"\", \"BOOK\")'>"+docCLONE.getItemValueString("DocKey")+"</a></li>";

	//	urlList.push("<li class='gridli'><a title='"+title+"' onclick='IFrame_OpenDoc(\""+docCLONE.getUniversalID()+"\", \"BOOK\")'>"+docCLONE.getItemValueString("DocKey")+"</a></li>");
	//	vDocKeys.push(docCLONE.getItemValueString("DocKey"));
	}
	//8888
//	docBOOK.replaceItemValue('CBN_DocKey', strDocKey);
//	docBOOK.replaceItemValue('BOOK_Interpreter_Type', 'Agency');
//	//need to computed View_CBN
//	var cbnlink = "<li class='gridli'><a title='Combined Booking Link' onclick='IFrame_OpenDoc(\""+docCBN.getUniversalID()+"\", \"CBN\")'>"+docCBN.getItemValueString("DocKey")+"</a></li>";
//	docBOOK.replaceItemValue("View_CBN","<div style='font-weight:700'>"+docBOOK.getItemValueString("DocKey")+"</div>"+ cbnlink)
	docBOOK.computeWithForm(true, false);
	docBOOK.save(true, false);
	
	//Update Combine booking Document
	var strDocKeys_Existing = docCBN.getItemValue('CBN_BOOK_DocKeys').join('~');
	
	var strComments_Existing = docCBN.getItemValue('CBN_Comments').join('|~~|');
	var strComments_New = @Trim(docBOOK.getItemValueString('BOOK_CommentsInterpreter'));
	
	var patient = "<li class='gridli'>" +docBOOK.getItemValueString("JSON_PFirstName")+" "+docBOOK.getItemValueString("JSON_PLastName")+"</li>";
	var loc = "<li class='gridli'>" +docBOOK.getItemValueString("BOOK_Location")+"</li>";
	var clinic = "<li class='gridli'>" +docBOOK.getItemValueString("BOOK_Clinic")+"</li>";
	var URNo = "<li class='gridli'>" +docBOOK.getItemValueString("BOOK_PatientUR")+"</li>";
	//Now check if CBN status neeeds to be recreated, if CBN is live then need to cancel req and create a new booking
	//with an incremented DocKey, otherwise add in place
	var sStatus = docCBN.getItemValueString("CBN_Status");
	if(@IsMember(sStatus, ['requested'])){//add to current document
		
	}
	else
	{
		//cancellation requested and create new
		
	}
	
	if(bAddasFirst){
		strDocKeys_Existing = strDocKey_New + '~' + strDocKeys_Existing;
		if(!IsNullorBlank(strComments_New)){strComments_Existing = strComments_New + '|~~|' + strComments_Existing}
		dtStart_CBN = dtStart_BOOK;
		docCBN.replaceItemValue("CBN_Appt_Start", dtStart_CBN);
		docCBN.replaceItemValue("CBN_ApptStartTime_Text", I18n.toString(dtStart_CBN.toJavaDate(), "HH:mm"));
		var vFlds = GetKeyword('CBN_Fields_CopyFirstDocument');
		for(var i=0; i<vFlds.length; i++){docCBN.replaceItemValue('CBN_' + @Right(vFlds[i], 'BOOK_'), docBOOK.getItemValue(vFlds[i]))}
		//calc view fields
		docCBN.replaceItemValue("View_Patients", patient + docCBN.getItemValueString("View_Patients"));
		docCBN.replaceItemValue("View_Locations", loc + docCBN.getItemValueString("View_Locations"));
		docCBN.replaceItemValue("View_Clinics", clinic + docCBN.getItemValueString("View_Clinics"));
		docCBN.replaceItemValue("View_URNos", URNo + docCBN.getItemValueString("View_URNos"));
		var urltemp = docCBN.replaceItemValueString("View_DocKeys").split("</div>");
		urltemp[1]=url+urltemp[1];
		docCBN.replaceItemValue("View_DocKeys", urltemp.join("</div>"));

	}
	if(bAddasLast){
		strDocKeys_Existing = strDocKeys_Existing + '~' + strDocKey_New;
		if(!IsNullorBlank(strComments_New)){strComments_Existing = strComments_Existing + '|~~|' + strComments_New}
		dtEnd_CBN = dtEnd_BOOK;
		docCBN.replaceItemValue("CBN_Appt_End", dtEnd_CBN);
		docCBN.replaceItemValue("CBN_ApptEndTime_Text", I18n.toString(dtEnd_CBN.toJavaDate(), "HH:mm"));
		//calc view fields
		docCBN.replaceItemValue("View_Patients", docCBN.getItemValueString("View_Patients")+patient);
		docCBN.replaceItemValue("View_Locations", docCBN.getItemValueString("View_Locations")+loc);
		docCBN.replaceItemValue("View_Clinics", docCBN.getItemValueString("View_Clinics")+clinic);
		docCBN.replaceItemValue("View_URNos", docCBN.getItemValueString("View_URNos")+URNo);
		docCBN.replaceItemValue("View_DocKeys", docCBN.getItemValueString("View_DocKeys")+url);
	}
	
	var vDocKeys = strDocKeys_Existing.split('~');

	docCBN.replaceItemValue("CBN_BOOK_DocKeys", vDocKeys);

	docCBN.replaceItemValue("CBN_BOOK_Count", vDocKeys.length);
	docCBN.replaceItemValue('CBN_Comments', strComments_Existing.split('|~~|'));	
	docCBN.replaceItemValue("CBN_ApptDuration", dtEnd_CBN.timeDifference(dtStart_CBN)/60);
	if (@IsMember(strStatus, ['requested'])){
		BOOK_LogAction(docCBN, "Added a new Booking (" + BOOK_Linkify(docBOOK) + ") to Combine booking", true);
	}
	else
	{
		BOOK_LogAction(docCBN, "Added a new Booking (" + BOOK_Linkify(docCLONE) + ") to Combine booking", true);
	}
	
	docCBN.computeWithForm(true, false);
	docCBN.save(true, false);
	
	view.postScript("Dialog_Message('SUCCESS', 'Booking successfully added to Combine Booking', 'Grid_CBN_AddBooking_OnComplete()')")	
}

function CBN_BOOK_Collection(bThrowError,ndoc:NotesDocument){
	if (ndoc==null){
		var docX:NotesXspDocument = document1;
		ndoc = docX.getDocument()
}
	
	var ndc = ndoc.getParentDatabase().getView('GRID_CBN_BOOK').getAllDocumentsByKey(ndoc.getItemValueString('DocKey'), true);
	if(ndc.getCount() == 0){
		//Not sure we need thuuis ...
		//view.postScript("Notify_Warning('ERROR: No Booking documents found for this Combine Booking')")
	}
	return ndc;
}

function CBN_Can_Cancel(ndoc,sBtnClick){
	/*
	 * Place holder function, should check cancelation charge and return false if there is a charge
	 */
	var strStatus = ndoc.getItemValueString('CBN_Status');
	if (sBtnClick==null){sBtnClick = "btnCBNCancelOnCallBooking_SJS"}
	if (@IsMember(strStatus, ['requested'])){
		return true
	}
	/**
	 * Keep BOOK_ instead of CBN for BOOK_Agency_FlowComplete as we are not setting CBN_Agency_FlowComplete anywhere. - Change by Baljit
	 */
	if (ndoc.getItemValueString ("CBN_Interpreter_Type").equalsIgnoreCase("Agency") && !ndoc.getItemValueString("BOOK_Agency_FlowComplete").equalsIgnoreCase("Yes") && !ndoc.getItemValueString ("CBN_Status").equalsIgnoreCase("api_create_error"))
	{
		//spin off to the agency function
		if (ndoc.getItemValueString ("CBN_Interpreter_FullName").equalsIgnoreCase("ONCALL")) {
			sBtnClick = "btnCBNCancelOnCallBooking_SJS"
		}
		var errorReason = BOOK_CheckCancelAgencyBooking (ndoc,"CBN");
		//print("CBN_Can_Cancel errorReason="+errorReason)
		if ( errorReason != true)
		{
			if (errorReason == false ){
				if (ndoc.getItemValueString ("CBN_Interpreter_FullName").equalsIgnoreCase("ONCALL")) {
					view.postScript("Notify_Warning('ERROR: There was an error requesting cancellation from ONCALL, if the problem persists, please contact IT Support')");
					return false;
				}else{
					view.postScript("Notify_Warning('ERROR: There was an error cancelling the document, please contact IT Support.')");
					return false;
				}
				
				return false;
			};
			if (!errorReason.equals (""))
			{
				//just do the single check about charges.
				errorReason = "Please note: The following bookings will incur charges if you cancel them.<br><br>" + errorReason;
				//print("btn to click = " + sBtnClick);
				view.postScript("ConfirmAction('" + errorReason +  "','"+sBtnClick+"')");

				return false;
			}else{
				return false;
			}
		}
	}
	return true
}

function CBN_CancelSingleAgency (docX:NotesXspDocument, strReason)
{

	if (!docX){
		var docX  = document1;
	}
	var ndoc:NotesDocument = docX.getDocument();
	if (!strReason )
	{
		strReason = getComponent('fldCancelReason').getValue();
	}
	if (ndoc.getItemValueString ("CBN_Interpreter_FullName").equalsIgnoreCase("ONCALL") && !ndoc.getItemValueString('CBN_Agency_ID').equals("")) {
		if (BOOK_SendCancelToOnCall (ndoc))
		{
			//success
			//print("success of cancel")
			CBN_Process_Delete(docX)
		} else {
		//there was an error cancelling through the api - right now do nothing
		}
	} else {
		CBN_Process_Delete(docX)	
	}
}

function CBN_Complete(docX)
{
	if (!docX.hasItem("CBN_DocKey")){return}
	var docCBN:NotesDocument = docX.getDocument().getParentDatabase().getView('vwAllByDocKey').getDocumentByKey(docX.getItemValueString("CBN_DocKey"), true);
	if (docCBN!=null){
		BOOK_Status(docCBN, "completed");//set to completed regardless of what is happening to linked child doc booking
		docCBN.save(true, false);	
	}
	
}
function CBN_Create_CBN(vData,ndb,ndc,oldDocKey,oldLog){
	
	var ndt_Start:NotesDateTime = session.createDateTime(vData[0].split('~')[2]);
	var ndt_Last:NotesDateTime = session.createDateTime(vData[vData.length-1].split('~')[3]);
	var docCBN:NotesDocument = ndb.createDocument();
	
	var vDocKeys = [];
	docCBN.replaceItemValue('Form', 'CBN');
	if (oldDocKey!=null)
	{
		
		var objDocKey = BOOK_Increment_DocKey(oldDocKey)
		//print(objDocKey.DocKey)
		var strDocKey = objDocKey.DocKey

		docCBN.replaceItemValue ("CBN_Amended_Number", objDocKey.Num)
		
	}
	else
	{
		var strDocKey = AppForm_DocKey('CBN');
	}
	docCBN.replaceItemValue('DocKey', strDocKey);
	docCBN.replaceItemValue("DocKey_CreatedBy", General_GetUser_DocKey());
	docCBN.replaceItemValue("CreatedDate", session.createDateTime(@Today()).getDateOnly());
	docCBN.replaceItemValue("CreatedBy", General_GetUser_FullName());
	BOOK_Status(docCBN, 'requested');
	
	
	docCBN.replaceItemValue("CBN_BOOK_Count", ndc.getCount());
	
	strGenderPre = @Trim(@ReplaceSubstring(strGenderPre.split("|OR|").join(''), 'SAME', ''));
	strGenderPre = @Trim(@ReplaceSubstring(strGenderPre, 'NULL', ''));
	docCBN.replaceItemValue("CBN_PatientGenderPreference", strGenderPre);
	
	docCBN.replaceItemValue("CBN_Appt_Start", ndt_Start);
	docCBN.replaceItemValue("CBN_Appt_End", ndt_Last);
	docCBN.replaceItemValue("CBN_ApptDuration", ndt_Last.timeDifference(ndt_Start)/60);
	docCBN.replaceItemValue("CBN_ApptStartDate_Text", I18n.toString(ndt_Start.toJavaDate(), "dd MMM yyyy"));
	docCBN.replaceItemValue("CBN_ApptStartTime_Text", I18n.toString(ndt_Start.toJavaDate(), "HH:mm"));
	docCBN.replaceItemValue("CBN_ApptEndTime_Text", I18n.toString(ndt_Last.toJavaDate(), "HH:mm"));
	
	//Site Location Clinic CostCenter from First Appointment, Get the First Document
	var docBOOK:NotesDocument = ndb.getView("vwAllByDocKey").getDocumentByKey(vData[0].split('~')[1], true);
	if(docBOOK == null){
		view.postScript("Dialog_Message('Bookings not Combined - ERROR', 'Error encountered, first booking document not found')"); 
		return null;
	}
	var vFlds = GetKeyword('CBN_Fields_CopyFirstDocument');
	for(var i=0; i<vFlds.length; i++){docCBN.replaceItemValue('CBN_' + @Right(vFlds[i], 'BOOK_'), docBOOK.getItemValue(vFlds[i]))}
		
	//All Comments
	docCBN.replaceItemValue('CBN_Comments', @Implode(vComms,","));
	//docCBN.replaceItemValue('CBN_Interpreter_Type', 'Agency');
	if (oldLog)
	{
		docCBN.replaceItemValue ("CBN_Log", oldLog)
		BOOK_LogAction(docCBN, '-------------------------------------------------------------------------');
	}
	BOOK_LogAction(docCBN, "Combine booking created with " + @Text(ndc.getCount()) + " booking requests", true);
	docCBN.computeWithForm(true, false);
	
	/*
	 * Added by Johan 15 Sep 2018
	 * Due to nature of system, need to follow superceded/reallocation practise of cancelling old booking and creating a new record
	 * This prevents double bookings
	 */	
	//Update Booking Documents
	var patientlist=[];
	var loclist=[];
	var cliniclist =[];
	var urlList=[];
	var unidList=[];
	var URNoList = [];
	var cbnlink = "<li class='gridli'><a title='Combined Booking Link' onclick='IFrame_OpenDoc(\""+docCBN.getUniversalID()+"\", \"CBN\")'>"+docCBN.getItemValueString("DocKey")+"</a></li>";
	
	var docBOOK:NotesDocument = ndc.getFirstDocument();
	while (docBOOK != null){		
		//Now create a new record
		/*
		 * If docBOOK status is updated or new, then there is no need to cancel and recreate
		 * Otherwise if is another status, ie BOOKED, Submitted to agency, then we need to clone the record and set the
		 * old record to cancellation requested
		 */
		var strStatus = docBOOK.getItemValueString('BOOK_Status');
		if (@IsMember(strStatus, ['requested', 'unmet_pending','api_create_error'])){
			docBOOK.replaceItemValue('CBN_DocKey', strDocKey);
			BOOK_Status (docBOOK, "requested"); //set new record to requested
			vDocKeys.push(docBOOK.getItemValueString("DocKey"));
			urlList.push("<li class='gridli'><a title='"+title+"' onclick='IFrame_OpenDoc(\""+docBOOK.getUniversalID()+"\", \"BOOK\")'>"+docBOOK.getItemValueString("DocKey")+"</a></li>");
			docBOOK.replaceItemValue("View_CBN","<div style='font-weight:700'>"+docBOOK.getItemValueString("DocKey")+"</div>"+ cbnlink)
					
		}
		else
		{
			var docCLONE:NotesDocument = ndb.createDocument();
			docCLONE.replaceItemValue("Form","BOOK")
			docCLONE.replaceItemValue("BOOK_Appt_End",docBOOK.getItemValue("BOOK_Appt_End"))
			BOOK_Create_Cloned_Booking(docCLONE,docBOOK)
			docBOOK.replaceItemValue("BOOK_Reallocated","Yes")
			//docCLONE.removeItem("BOOK_Duration");
			docCLONE.replaceItemValue("BOOK_ApptDuration",docBOOK.getItemValue("BOOK_ApptDuration"))//function above converts to text, so re-write this value as a double
			docCLONE.replaceItemValue ("BOOK_Log", docBOOK.getItemValue ("Book_Log"))
			BOOK_LogAction(docCLONE, '-------------------------------------------------------------------------');
			docCLONE.replaceItemValue('CBN_DocKey', strDocKey);
			//	docBOOK.replaceItemValue('BOOK_Interpreter_Type', 'Agency');
			docBOOK.replaceItemValue("FlagCancelledForCBN","true")

			var sStat = BOOK_Status (docBOOK, "cancellation_req"); //cancel orignal record
			BOOK_Status (docCLONE, "requested"); //set new record to requested
			BOOK_LogAction(docCLONE, "Cloned and added to Combine Booking - " + strDocKey, true);
			BOOK_LogAction(docBOOK, "Booking set to " + sStat, true);
			docCLONE.replaceItemValue("View_CBN","<div style='font-weight:700'>"+docCLONE.getItemValueString("DocKey")+"</div>"+ cbnlink)
			docCLONE.computeWithForm(true, false);
			docCLONE.save(true, false);	
			urlList.push("<li class='gridli'><a title='"+title+"' onclick='IFrame_OpenDoc(\""+docCLONE.getUniversalID()+"\", \"BOOK\")'>"+docCLONE.getItemValueString("DocKey")+"</a></li>");
			vDocKeys.push(docCLONE.getItemValueString("DocKey"));
		}
		BOOK_LogAction(docBOOK, "Added to Combine Booking - " + strDocKey, true);
		docBOOK.computeWithForm(true, false);
		docBOOK.save(true, false);	
		//These array will be used to fill some View_ row data on the combined booking (which will then be displayed in the grids)
		patientlist.push("<li class='gridli'>" +docBOOK.getItemValueString("JSON_PFirstName")+" "+docBOOK.getItemValueString("JSON_PLastName")+"</li>");
		loclist.push("<li class='gridli'>" +docBOOK.getItemValueString("BOOK_Location")+"</li>");
		cliniclist.push("<li class='gridli'>" +docBOOK.getItemValueString("BOOK_Clinic")+"</li>");
		unidList.push(docBOOK.getUniversalID());
		URNoList.push("<li class='gridli'>" +docBOOK.getItemValueString("BOOK_PatientUR")+"</li>");
		var title=docBOOK.getItemValueString("JSON_PFirstName")+" "+docBOOK.getItemValueString("JSON_PLastName") + ": " + docBOOK.getItemValueString("BOOK_ApptStartTime_Text")
		
		docBOOK = ndc.getNextDocument(docBOOK);
	}
	docCBN.replaceItemValue("CBN_BOOK_DocKeys", vDocKeys);
	docCBN.replaceItemValue("View_Patients", patientlist.join(""));
	docCBN.replaceItemValue("View_Locations", loclist.join(""));
	docCBN.replaceItemValue("View_Clinics", cliniclist.join(""));
	docCBN.replaceItemValue("View_URNos", URNoList.join(""));
	docCBN.replaceItemValue("View_DocKeys", "<div style='font-weight:700'>"+strDocKey+"</div>" + urlList.join(""));
	docCBN.save(true, false);
	return docCBN;
}

function CBN_Create_New(vIDs,oldDocKey,oldLog){
	//print("CBN_Create_New")
	strGenderPre="";
	vComms = [];
	if (vIDs==null)
	{
		var vIDs = getComponent('selUNIDs').getValue().split('~');
		getComponent('selUNIDs').setValue('');
		if(IsNullorBlank(vIDs)){view.postScript("Dialog_Message('ERROR', 'NO Values selected. Please contact IT Support')"); return}
	}
	var ndb:NotesDatabase = GetDB_Database('BOOK');
	var ndc:NotesDocumentCollection = ndb.getProfileDocCollection('BLANK_Document_Collection');
	var vMsg=[];
	var vData=CBN_Validate_Collection(vIDs,vMsg,ndb,ndc,false);
	//Check Keyword to get which Fields to be matching in order to Create this Combine Booking
	//print(vComms)
	//print(vMsg);
	if(vMsg.length > 0){view.postScript("Dialog_Message('Bookings not Combined', '" + vMsg.join('<br/>') + "')"); return}
	//print("strGenderPre="+strGenderPre);
	var docCBN:NotesDocument = null;
	if (oldDocKey==null || oldDocKey=="BOOK"){
		docCBN = CBN_Create_CBN(vData,ndb,ndc)
	}
	else
	{
		docCBN = CBN_Create_CBN(vData,ndb,ndc,oldDocKey,oldLog)
	}
	
	//print('new docCBN='+docCBN)
	if (docCBN==null){return}
	
	
	var vCJS_Function = [];
	
	vCJS_Function.push("Notify_Info('SUCCESS: All selected records are combined now.')");
	vCJS_Function.push("UpdateFTIndex('xFTIndex_Agent.xsp')");
	
	if (oldDocKey==null){
		vCJS_Function.push("Grid_CRD_Open('GRID_CBN_All')");
		vCJS_Function.push("IFrame_OpenDoc('" + docCBN.getUniversalID() + "', 'CBN')");
		}
	else{
		vCJS_Function.push("window.location.replace('xCBN.xsp?documentId="+docCBN.getUniversalID()+"&action=openDocument')");
		}
	view.postScript(vCJS_Function.join("; "));
}

function CBN_Delete(sBtnClick){
	var docX:NotesXspDocument = document1;
	var ndoc:NotesDocument = docX.getDocument();

	if (!CBN_Can_Cancel(ndoc,sBtnClick))
	{
		//view.postScript("Dialog_Message('SUCCESS', 'Cannot cancel Combined booking', 'Form_Delete_OnComplete()')");
		return true
	}
	CBN_Process_Delete(docX)
}

function CBN_Delete_Force(){
	//print("CBN_Delete_Force");
	var docX:NotesXspDocument = document1;
	CBN_Process_Delete(docX)
}

function CBN_Dockey_Links(docX)
{
	var ids = docX.getItemValue('CBN_BOOK_DocKeys');
	var html="";
	try {
		for (var k=0;k<ids.length;k++){
			//html+="<div><a title='Booking' onclick='IFrame_OpenDoc(\""+vids[k]+"\", \"BOOK\")'>"+ids[k]+"</a></div>";
			var unid = docX.getDocument().getParentDatabase().getView('vwAllByDocKey').getDocumentByKey(ids[k], true).getUniversalID();
			html+="<div><a title='Booking' href='xBOOK.xsp?documentId="+unid+"&action=openDocument'>"+ids[k]+"</a></div>"
		}
		return html
	}
	catch(err)
	{
		return document1.getItemValue('CBN_BOOK_DocKeys').join(",")
	}
}
function CBN_FieldIsEditable(strField,strSection){	
	
	var docX:NotesXspDocument = document1;
	if(docX.isEditable() == false){return false}
	if(IsUser_Requestor() | IsUser_Interpreter()){return false}
	
	var strStatus = GetValue('CBN_Status');	
	//If Booking is marked as Cancelled only comments are allowed to edit any time
	
	if(strStatus == 'requested' & (strField == 'CBN_Interpreter_DocKey' || strField == 'CBN_Comments' || strField=='CBN_Interpreter_Type' || strField=='CBN_Interpreter_DocKey')){
		return true
	}

	if(strField == 'CBN_Comments_Internal'){return true}
	

	if((strSection == 'ProviderDetails') && !IsUser_Requestor()) {
			if (GetValue("CBN_Interpreter_FullName").equalsIgnoreCase ("OnCall")){ return false;}
			return true;
		} 

	if (docX.getItemValueString('CBN_Completed') != "" || docX.getItemValueString('CBN_Status') == "completed") {
		if((strSection == 'PostBooking' || strSection == 'ProviderDetails') && !IsUser_Requestor()) {
			if (GetValue("CBN_Interpreter_FullName").equalsIgnoreCase ("OnCall")){ return false;}
			return true;
		} else {
			return false;
		}
	}
	
	//var bReturn = BOOK_FieldIsEditable_Check(strField, strSection,'CBN');	
	return false
}

function CBN_Get_UNIDS(ndoc)
{
	//print("CBN_Get_UNIDS for " + ndoc)
	var bookDC:NotesDocumentCollection = CBN_BOOK_Collection(false,ndoc);
//loop through bookings and get UNIDs
	var vUNIDs = [];
	var bookDoc:NotesDocument = bookDC.getFirstDocument()
	while (bookDoc!=null)
	{
		vUNIDs.push(bookDoc.getUniversalID())
		bookDoc = bookDC.getNextDocument(bookDoc)
	}
	return vUNIDs;
}

function CBN_Interpreter_List(strType){
	var strLang = document1.getItemValueString('CBN_Language')
	if(!strType){strType = document1.getItemValueString('CBN_Interpreter_Type')}	
	if(IsNullorBlank(strType)){return null}	
	if(strType == 'In-House'){
		if(strLang.indexOf('~')<0){
			var vList = @DbLookup(GetDB_FilePath('USER'), "USER_byLanguage", strLang, 2, "[FAILSILENT]");
			if(IsNullorBlank(vList)){return null} else {return vList}
		} else {
			//Multiple Languages
			return InterpreterList_ByMultipleLanguage(strLang.split("~"))
		}
	} else {
		var vListAll = @DbLookup(GetDB_FilePath('AGN'), "AGN_byLanguage", "ALL", 2, "[FAILSILENT]");
		var vListAgency = "";
		if(strLang != "ALL"){vListAgency = @DbLookup(GetDB_FilePath('AGN'), "AGN_byLanguage", strLang, 2, "[FAILSILENT]")}	
		var vRet = @Trim(@Explode(@Implode(vListAgency,"~") + "~" + @Implode(vListAll,"~"), "~"));		
		if(IsNullorBlank(vRet)){return null} else {return vRet}
	}
}

function CBN_MarkAsUnmet(){
	var docX:NotesXspDocument = document1;
	var ndoc:NotesDocument = docX.getDocument();
	var strSD = BOOK_Status(ndoc, 'unmet');	
	ndoc.replaceItemValue('CBN_Completed', 'No');
	ndoc.replaceItemValue('CBN_NC_Reason', 'Unmet');	
	BOOK_LogAction(ndoc, 'Booking marked as ' + strSD , true);
	CBN_Status_Change(ndoc,'unmet','Booking marked as ' + strSD + ' from the Combined Booking ' + BOOK_Linkify(ndoc));	
	ndoc.save(true,false)
	CBN_Open(ndoc.getUniversalID())
}

function CBN_Open(sUNID){
	var strURL = 'xCBN.xsp?documentId=' + sUNID + '&action=openDocument'
	context.redirectToPage(strURL);
}

function CBN_Process_Delete(docX,sStatus)
{
	
	var strDocKey = GetValue('DocKey');
	//print("CBN Process Delete for " + strDocKey)
	var nvw:NotesView = GetDB_Database('BOOK').getView('GRID_CBN_BOOK');
	var ndc:NotesDocumentCollection = nvw.getAllDocumentsByKey(strDocKey, true);
	//print("Found booking -> " + ndc.getCount())
	if(ndc.getCount() > 0){	
		var docBOOK:NotesDocument = ndc.getFirstDocument();
		while (docBOOK != null){		
			docBOOK.removeItem('CBN_DocKey');
			docBOOK.removeItem('View_CBN');		
			if (docBOOK.getItemValueString("BOOK_Status")=="cancellation_req"){
				BOOK_Status(docBOOK, 'cancelled')
				BOOK_LogAction(docBOOK, "Cancelled and removed from Combine booking " + BOOK_Linkify(docX.getDocument()), true);
			}
/*			else if (docBOOK.getItemValueString("BOOK_Status")=="updated"){
				BOOK_Status(docBOOK, 'updated')
				BOOK_LogAction(docBOOK, "Cancelled and removed from Combine booking " + BOOK_Linkify(docX.getDocument()), true);
				docBOOK.replaceItemValue('BOOK_Interpreter_Type', '');
				docBOOK.replaceItemValue('BOOK_Interpreter_FullName', '');

			}*/
			else if (docBOOK.getItemValueString("BOOK_Status")=="cancelled"){
				//BOOK_Status(docBOOK, 'cancelled')
				BOOK_LogAction(docBOOK, "Removed from Combine booking " + BOOK_Linkify(docX.getDocument()), true);
			}
			else
			{
				BOOK_Status(docBOOK, 'requested')
				docBOOK.replaceItemValue('BOOK_Interpreter_Type', '');
				docBOOK.replaceItemValue('BOOK_Interpreter_FullName', '');
				BOOK_LogAction(docBOOK, "Removed from Combined booking " + BOOK_Linkify(docX.getDocument()), true);
			}
			

			
			docBOOK.computeWithForm(true, false);
			docBOOK.save(true, false);		
			docBOOK = ndc.getNextDocument(docBOOK);
		}
	}
	//Global_RemoveDocument(docX.getDocument());
	if (sStatus==null){sStatus="cancelled"}
	
	if (docX.getItemValueString("CBN_Interpreter_FullName").equalsIgnoreCase("OnCall")) {
		docX.replaceItemValue ("BOOK_Agency_FlowComplete", "Yes");
		docX.removeItem('CBN_PendingSend');
	}
	BOOK_Status(docX, sStatus);
	BOOK_LogAction(docX, 'Combined Booking set to ' + docX.getItemValueString("CBN_Status_Display") , true);
	docX.save()
	view.postScript("Dialog_Message('SUCCESS', 'Combine Booking Document Removed Successfully !!', 'Form_Delete_OnComplete()')");
}

function CBN_PropagateAgencyDetails(doc)
{
	//print("CBN_Prop")
	if (!doc.hasItem("CBN_Status")){return}
	var ndc:NotesDocumentCollection = CBN_BOOK_Collection(true,doc);
	//print("count="+ndc.getCount())
	if(ndc.getCount() == 0){return false}
	var docBOOK:NotesDocument = ndc.getFirstDocument();
	while (docBOOK != null){
		//BOOK_AssignToAgency_UpdateDoc(strIDK, strInterpreter, false, null, docBOOK);
		/*
		 * These 'child' records are informational
		 */
		CBN_PropagateAgencyFields(doc,docBOOK)
		
		//BOOK_Status(docBOOK, docX.getItemValueString("CBN_Status"));	
		docBOOK.computeWithForm(true, false);
		docBOOK.save(true, false);		
		docBOOK = ndc.getNextDocument(docBOOK);
	}
}

function CBN_PropagateAgencyFields(doc,docBOOK)
{

		docBOOK.replaceItemValue("BOOK_Interpreter_Type",doc.getItemValueString("CBN_Interpreter_Type"));
		docBOOK.replaceItemValue("BOOK_Interpreter_FullName",doc.getItemValueString("CBN_Interpreter_FullName"));
		docBOOK.replaceItemValue("BOOK_Interpreter_DocKey",doc.getItemValueString("CBN_Interpreter_DocKey"));
		docBOOK.replaceItemValue("BOOK_Agency_ID",doc.getItemValueString("CBN_Agency_ID"));
		docBOOK.replaceItemValue("BOOK_Agency_URL",doc.getItemValueString("CBN_Agency_URL"));
		docBOOK.replaceItemValue("BOOK_Agency_Interpreter",doc.getItemValueString("CBN_Agency_Interpreter"));
		docBOOK.replaceItemValue("BOOK_Agency_Interpreter_Level",doc.getItemValueString("CBN_Agency_Interpreter_Level"));
		docBOOK.replaceItemValue("BOOK_Agency_Interpreter_Gender",doc.getItemValueString("CBN_Agency_Interpreter_Gender"));
		docBOOK.replaceItemValue("BOOK_Agency_Last_Updated",doc.getItemValue("CBN_Agency_Last_Updated"));

}

function CBN_QuerySave(){
	
	var docX:NotesXspDocument = document1;
	var ndoc:NotesDocument = docX.getDocument();
	var strStatus = GetValue('CBN_Status');
	var strIDK = GetValue("CBN_Interpreter_DocKey");
	var strIType = GetValue('CBN_Interpreter_Type');
	var bAllow = @IsMember(strStatus, ['draft', 'requested', 'booked', 'updated']);
	if(!IsNullorBlank(strIType) &&!IsNullorBlank(strIDK) ){
		if(strIType == 'Agency'){		
				var strInterpreter = @DbLookup(GetDB_FilePath('AGN'), "all_byDocKey", strIDK, "AGN_Name", "[FAILSILENT]");
	
			} else {
				var strInterpreter = @DbLookup(GetDB_FilePath('USER'), "all_byDocKey", strIDK, "USER_FullName", "[FAILSILENT]");
				if(BOOK_DatePassed_Value("CBN") == ""){
					//print("checking interpreter availability");
					if(bAllow & getComponent('CBN_Interpreter_DocKey').isRendered()){
						
						var strMsg = BOOK_CheckAvailability(docX, strIDK,"CBN");
					//print("BOOK_CheckAvailability result="+strMsg);
						var msgObj = getComponent('BOOK_CheckAvailability');
						//print (msgObj)
						if(strMsg == 'OK'){
							msgObj.setValue('');
						} else {			
							msgObj.setValue(BOOK_CheckAvailability_Message(strMsg, strIDK, strInterpreter));
							//print("msgObj set");
							return false
						}
					}
				}	
			}
			
			//print("check if interpreter has change")
			if(ndoc.getItemValueString('CBN_Interpreter_FullName') != strInterpreter){
				docX.replaceItemValue('CBN_Interpreter_FullName', strInterpreter);
				if (strIType=='In-House')
				{
					BOOK_Status(docX, 'booked');
					BOOK_LogAction(docX, 'Assigned to "' + strInterpreter + '"' , true);
				}
				else
				{
					BOOK_AssignToAgency_UpdateDoc(strIDK, strInterpreter, false, null, docX)
				}
			}
				var ndc:NotesDocumentCollection = CBN_BOOK_Collection(true);
				if(ndc.getCount() == 0){return false}
				var docBOOK:NotesDocument = ndc.getFirstDocument();
				while (docBOOK != null){
					//BOOK_AssignToAgency_UpdateDoc(strIDK, strInterpreter, false, null, docBOOK);
					/*
					 * These 'child' records are informational
					 */
					docBOOK.replaceItemValue("BOOK_Interpreter_Type",strIType);
					docBOOK.replaceItemValue("BOOK_Interpreter_FullName",strInterpreter);
					docBOOK.replaceItemValue("BOOK_Interpreter_DocKey",strIDK);
					docBOOK.replaceItemValue("BOOK_Agency_ID",docX.getItemValueString("CBN_Agency_ID"));
					docBOOK.replaceItemValue("BOOK_Agency_URL",docX.getItemValueString("CBN_Agency_URL"));
					docBOOK.replaceItemValue("BOOK_Agency_Interpreter",docX.getItemValueString("CBN_Agency_Interpreter"));
					docBOOK.replaceItemValue("BOOK_Agency_Interpreter_Level",docX.getItemValueString("CBN_Agency_Interpreter_Level"));
					docBOOK.replaceItemValue("BOOK_Agency_Interpreter_Gender",docX.getItemValueString("CBN_Agency_Interpreter_Gender"));
					docBOOK.replaceItemValue("BOOK_Agency_Last_Updated",docX.getItemValue("CBN_Agency_Last_Updated"));

					BOOK_Status(docBOOK, docX.getItemValueString("CBN_Status"));	
					docBOOK.computeWithForm(true, false);
					docBOOK.save(true, false);		
					docBOOK = ndc.getNextDocument(docBOOK);
				}
				
				
				/* - not sure the point of this ...
				if(strStatus != "requested"){
					var strSD = BOOK_Status(docX, 'requested');		
					BOOK_LogAction(docX, 'Status changed to ' + strSD, true);
				}*/
				
	
		//	CBN_PropagateAgencyDetails(docX);
		}		
}

function CBN_Reallocate(noCancelCheck)
{
	/*
	 * 
	 */
	if (noCancelCheck==null){noCancelCheck=false}
	strGenderPre="";
	vComms=[];
	var vMsg = [];
	var docX:NotesXspDocument = document1;
	var ndoc:NotesDocument = docX.getDocument();
	var ndb:NotesDatabase = GetDB_Database('BOOK');
	var ndc:NotesDocumentCollection = ndb.getProfileDocCollection('BLANK_Document_Collection');

	//Prior to cancelling booking - get handle on linked bookings
	var bookDC:NotesDocumentCollection = CBN_BOOK_Collection(false,ndoc);
	//loop through bookings and get UNIDs
	var vUNIDs = [];
	var bookDoc:NotesDocument = bookDC.getFirstDocument()
	while (bookDoc!=null)
	{
		vUNIDs.push(bookDoc.getUniversalID())
		bookDoc = bookDC.getNextDocument(bookDoc)
	}
	//validate data;
	
	var vData=CBN_Validate_Collection(vUNIDs,vMsg,ndb,ndc)
	if(vMsg.length > 0){view.postScript("Dialog_Message('Bookings cannot be reallocated', '" + vMsg.join('<br/>') + "')"); return}
//	print("number of bookings = " + vData.length+1);
	//get current status of CBN, this will determine future workflow (because CBN_Delete will change status to cancelled)
/*	var sStatus = ndoc.getItemValueString("CBN_Status");
	var newStatus = "cancellation_req"
	//Cancel booking and reform combined booking
	if(@IsMember(sStatus, ['requested']))
	{
		//No need to request a cancellation on the old record
		newStatus = "cancelled"
	}
	var sBtnClick = "btnCBN_Reallocate_Oncall_SJS"
	if (!noCancelCheck)
	{
		if (!CBN_Can_Cancel(ndoc,sBtnClick))
		{
			//view.postScript("Dialog_Message('SUCCESS', 'Cannot cancel Combined booking', 'Form_Delete_OnComplete()')");
			return false
		}
	}

	CBN_Process_Delete(docX,newStatus)
	var newCBN:NotesDocument = CBN_Create_New(vUNIDs,ndoc.getItemValueString("DocKey"))
	*/
	CBN_Reallocate_Process(docX,vUNIDs)
}

function CBN_Reallocate_Process(docX,vUNIDs,sBtnClick,noCancelCheck){
	var ndoc:NotesDocument = docX.getDocument();
	var sStatus = ndoc.getItemValueString("CBN_Status");
	var newStatus = "cancellation_req"
	//Cancel booking and reform combined booking
	if(@IsMember(sStatus, ['requested','unmet_pending']))
	{
		//No need to request a cancellation on the old record
		newStatus = "cancelled"
	}
	if (sBtnClick==null){sBtnClick = "btnCBN_Reallocate_Oncall_SJS"}
	if (noCancelCheck==null){noCancelCheck=false}
	if (!noCancelCheck)
	{
		if (!CBN_Can_Cancel(ndoc,sBtnClick))
		{
			//view.postScript("Dialog_Message('SUCCESS', 'Cannot cancel Combined booking', 'Form_Delete_OnComplete()')");
			return false
		}
	}
	docX.replaceItemValue("BOOK_Reallocated","Yes")
	CBN_Process_Delete(docX,newStatus)
	var newCBN:NotesDocument = CBN_Create_New(vUNIDs,ndoc.getItemValueString("DocKey"),docX.getItemValue("CBN_Log"))
	return newCBN
}

function CBN_ShowBtn(strID){
	
	var docX:NotesXspDocument = document1;
		
	var bModifyBooking = (IsUser_Manager() | IsUser_Coordinator() | IsUser_DBAdmin());
	//If Not in any role, then return false
	if(!bModifyBooking){return false}
	
	//All below buttons are for read mode only
	if((docX.isEditable() | docX.isNewNote()) && strID != "btnCBN_BOOK_Add"){return false}
	var sform = docX.getItemValueString("Form")||"CBN"
	var strStatus = GetValue(sform+'_Status');
	var IsPast = (BOOK_DatePassed_Value() == 'Yes');
	
	//This is for dropdown buttons
	if(strID == 'btnDropDown'){
		var vChildBtns = ['btnCBN_Delete', 'btnCBN_Cancel'];
		for(var i=0; i<vChildBtns.length; i++){if(CBN_ShowBtn(vChildBtns[i])){return true}}
		return false
	}
	
	//Edit button is available to Coordinators	
	if(strID == 'btnCBN_Edit'){
		if(@IsMember(strStatus, ['requested', 'booked', 'updated', 'submitted_agency', 'service_delivered']))
			{return true}
	}
	
	//Delete Button allowed if requested
	if(strID == 'btnCBN_Delete'){//return strStatus == 'requested';
		if(@IsNotMember(strStatus, ['cancellation_req','cancelled','Reallocated']) && !IsPast)
		{return true}
	}
	
	if(strID == 'btnCBN_MarkUnMet'){//return strStatus == 'requested';
		if(@IsNotMember(strStatus, ['cancelled','Reallocated','unmet']) && IsPast)
		{return true}
	}
	//Need to review this one later
	if(strID == 'btnCBN_AcceptCancel'){
		if(@IsMember(strStatus, ['cancellation_req'])  && !IsPast )
		{return true}
		}
	
	if(strID == 'btnCBN_Reallocate'){
		if(@IsMember(strStatus, ['updated','booked',"unmet_pending","submitted_agency","api_create_error","error"]) && !IsPast)
		{return true}
		}
	
	if(strID == "btnCBN_BOOK_Add"){
		if(@IsMember(strStatus, ['requested','updated','booked','submitted_agency']) && !IsPast)
		{
			return true
		}
		}
	if(strID == "btnCBN_Refresh"){
		if(@IsMember(strStatus, ['submitted_agency', 'booked', 'updated']) && !IsPast && docX.getItemValueString('CBN_Interpreter_FullName').equalsIgnoreCase ("ONCALL"))
		{
			return true
		}	
	}
	return false;	
}
function CBN_Status_Change(ndoc:NotesDocument,sStatus,sLog){
	//print("CBN Status Change");
	if (ndoc==null)
	{
		var docX:NotesXspDocument = document1;
		ndoc = docX.getDocument();
	}
	if (ndoc.getItemValueString("Form")=="CBN"){
		//print("get linked child bookings")
		if (sStatus==null){sStatus=ndoc.getItemValueString("CBN_Status")}
		//Propogate status to child records
		var ndc:NotesDocumentCollection = CBN_BOOK_Collection(true,ndoc);
		//print("post get linked child bookings")
		if(ndc.getCount() == 0){return false}
			var docBOOK:NotesDocument = ndc.getFirstDocument();
			while (docBOOK != null){
				var strSD = BOOK_Status(docBOOK, sStatus);	
				if (sLog){
					BOOK_LogAction(docBOOK, sLog , true);
				}
				if (sStatus=="unmet"){
					docBOOK.replaceItemValue('BOOK_Completed', 'No');
					docBOOK.replaceItemValue('BOOK_NC_Reason', strSD);	
					BOOK_LogAction(docBOOK, 'Booking marked as ' + strSD , true);
				}
				docBOOK.computeWithForm(true, false);
				docBOOK.save(true, false);		
				docBOOK = ndc.getNextDocument(docBOOK);
			}
	}
	else
	{

		if (ndoc.hasItem("CBN_DocKey")){
			//Propogate Status to parent
			
			var unid = ndoc.getUniversalID();
			if (sStatus==null){sStatus=ndoc.getItemValueString("BOOK_Status")}
			if (sStatus=="unmet"){return}
			var docCBN:NotesDocument = ndoc.getParentDatabase().getView('vwAllByDocKey').getDocumentByKey(ndoc.getItemValueString("CBN_DocKey"), true);
			if (docCBN!=null){
				if (docCBN.getItemValueString("CBN_Status")!=sStatus)
				{
					//Log the change
					BOOK_LogAction(docCBN,"Status changed to " + BOOK_Status(docCBN, sStatus), true);
				}
				else
				{
					BOOK_Status(docCBN, sStatus);
				}
				
				docCBN.save(true, false);	
				//Now propogate status change to linked records
				/*var ndc:NotesDocumentCollection = CBN_BOOK_Collection(true,docCBN);
				print("post get linked child bookings")
				if(ndc.getCount() == 0){return false}
					var docBOOK:NotesDocument = ndc.getFirstDocument();
					while (docBOOK != null){
						if (docBOOK.getUniversalID()!=unid)
						{
							BOOK_Status(docBOOK, sStatus);	
							docBOOK.computeWithForm(true, false);
							docBOOK.save(true, false);	
						}
						docBOOK = ndc.getNextDocument(docBOOK);
					}
					*/
			}
			
		}
	}
}

function CBN_Suggesstion(){
	
	var strSSKey = 'GRID_CBN_BOOK_Search';
	var sform=currentDocument.getItemValueString("Form")||"CBN"
	var unid = currentDocument.getDocument().getUniversalID();
	if(viewScope.containsKey(strSSKey) == false){	
		var docX:NotesXspDocument = document1;
		var vControlKW = GetKeyword("CBN_Fields_Matching");
		var vTemp; var strFldName; var fldValue; var vQuery = [];
		
		for(var j=0; j<vControlKW.length; j++){
			
			vTemp = vControlKW[j].split("~");
			strFldName = vTemp[1];
			if (strFldName=="BOOK_PatientGenderPreference")
			{
				//do nothing, ie exclude
			} else if(vTemp.length == 3){
				fldValue = vTemp[2];
				//Fixed Single Value - Match from KW
				if(fldValue.indexOf("|OR|")<0){				
					vQuery.push('[' + strFldName + '] = "' + fldValue + '"');
				} else {
					var vKWVals = fldValue.split("|OR|");
					var vFldVals = []; //Store all fldValues to Match
					for(var i=0; i<vKWVals.length; i++){
						//If 'SAME' then get FldValue of CBN Document
						if(vKWVals[i] == "SAME"){
							if (GetValue(sform+"_" + @Right(strFldName, "BOOK_"))=="") //full text search can't have blank string
							{
								vFldVals.push('NOT [' + strFldName + '] IS PRESENT');
							}
							else
							{
								vFldVals.push('[' + strFldName + '] = "' + GetValue(sform+"_" + @Right(strFldName, "BOOK_")) + '"');
							}
						
						} else if(vKWVals[i] == "NULL"){
							//If 'NULL' then Add Blank
							vFldVals.push('NOT [' + strFldName + '] IS PRESENT');
						} else {
							
							if (vKWVals[i]=="") //full text search can't have blank string
							{
								vFldVals.push('NOT [' + strFldName + '] IS PRESENT');
							}
							else
							{
								vFldVals.push('[' + strFldName + '] = "' + vKWVals[i] + '"');
							}
							
						}
					}
					//BOOK_PatientGender = "M" : "F"
					vQuery.push('(' + vFldVals.join(' | ') + ')');
				}
				
			} else if (vTemp.length == 4){
				//NOT scenario
				//var vNot = vTemp[3].split("|")
			}else {
		
				//No FldValue in KW, so match with CBN Field
				fldValue = GetValue(sform+"_" + @Right(strFldName, "BOOK_"));
				vQuery.push('[' + strFldName + '] = "' + fldValue + '"');
			}			
		}
		if (unid!="" && unid!=null){
			vQuery.push('NOT "' + unid + '"');
		}
		viewScope.put(strSSKey, vQuery.join(' & '));
		//print(vQuery.join(' & '))
	}	
	//SessionScope same as View Name, this is removed after used by GridSJS
	//Keeping in View Scope, so that on Edit and save action, this doesn't need to calculate again
	sessionScope.put(strSSKey, viewScope.get(strSSKey));	
}

function CBN_Validate_Booking(ndoc:NotesDocument,vMsg,vCompareVals,vFldLabels,vControlKW,ignore)
{
	/*
	 * Maybe shouldn't have broken into seperate function, nevertheless it is done
	 */
	if (ignore==null){ignore=true;}
	var strCompare="";
	var vTemp; var strFldValue;
	if (ndoc.hasItem("CBN_DocKey") && !ignore)
	{
		vMsg.push('Booking ('+ndoc.getItemValueString("DocKey")+') is already part of another Combined Booking ('+ndoc.getItemValueString("CBN_DocKey")+')');

	} else if (ndoc.getItemValueString("Form")=="CBN"){
		vMsg.push('Booking ('+ndoc.getItemValueString("DocKey")+'): Combined booking cannot be combined into a new combined booking');
	} else if (@IsMember(ndoc.getItemValueString("BOOK_Status"), ['cancelled','superceded','completed','cancellation_req'])){
		vMsg.push('Booking ('+ndoc.getItemValueString("DocKey")+'): Bookings at "'+ndoc.getItemValueString("BOOK_Status_Display")+'" status cannot be combined');
	}
	else
	{
	for(var j=0; j<vControlKW.length; j++){
		
		vTemp = vControlKW[j].split("~");
		
		strFldValue = @Trim(ndoc.getFirstItem(vTemp[1]).getText());
	//	print(vTemp[1] + '~~~~' + strFldValue);
		//Check for NOT scenario
		if(vTemp.length == 4){
	//		print("checking NOT")
			if (vTemp[2]=="NOT")
			{
				var notValues = vTemp[3].split("|")
				for (var z=0;z<notValues.length;z++)
				{
					if (notValues[z]==strFldValue){
						vMsg.push(vTemp[0] + " - cannot have a value of " + strFldValue)
						break;
					}
				}
			}
			
		}
	//	print("what does i=?" + i)
		if(i==0){
			vFldLabels.push(vTemp[0]);
		
			if(vTemp.length == 3){
				//If fldValue is not blank and 'SAME' check exists, then replace 'SAME' with fldValue
				if(vTemp[2].indexOf("SAME")>=0 & strFldValue != ''){
					strFldValue = @ReplaceSubstring(vTemp[2], "SAME", strFldValue);						
				} else {
					strFldValue = vTemp[2];
				}	
				if(strFldValue.indexOf("|OR|")>=0){
					strFldValue = strFldValue.split("|OR|")
					
					}
				
			}
			else if (vTemp.length==4)
			{
				strFldValue="*IGNORE*"
			}
			vCompareVals.push(strFldValue);
			
		} else {
			
			try{
			//Compare Values of Document
			strCompare = vCompareVals[j];
		//	print("strCompare="+strCompare);
			if (vTemp[1]=="BOOK_PatientGenderPreference"){//sorry...hardcoded check until better solution implemented
				var hasN = false
				var hasGender = false
				for (var z=0;z<strCompare.length;z++)
				{
					if (strCompare[z]=="N"){hasN=true;}
					if (strCompare[z]=="M" || strCompare[z]=="F"){hasGender=true}
				}
				if (!hasN && strFldValue=="N")
				{
					strCompare.push("N")
				}
				if (!hasGender && (strFldValue=="M" || strFldValue=="F"))
				{
					strCompare.push(strFldValue)
				}
				
			}
			if(isString(strCompare)){
				//Value not matching with first record
				if(strCompare != strFldValue && strCompare!="*IGNORE*"){ vMsg.push(vFldLabels[j] + " - is not matching with other selected booking records.")}
				
			} else {
				
				//IF 'SAME' still exists in Compare and fldValue is not null, then replace SAME with fldValue
				if(strCompare.indexOf("SAME")>=0 & strFldValue != ''){						
					strCompare = @ReplaceSubstring(strCompare.join("|OR|"), "SAME", strFldValue).split("|OR|");
					vCompareVals[j] = strCompare;
				}
				
				//Blank not allowed
				if(strFldValue == ""){
					if(strCompare.indexOf("NULL")<0){vMsg.push(vFldLabels[j] + " - is not filled in, blank value")}
				} else {
					if(strCompare.indexOf(strFldValue)<0){vMsg.push(vFldLabels[j] + " - is not matching with allowed values")}
				}
				
				if(vTemp[1] == 'BOOK_PatientGenderPreference'){strGenderPre = strCompare.join("|OR|")}
			}		
			}
			catch(err)
			{
				if (vMsg.lenth==0)
				{
					vMsg("Unexpected error: " + err.toString())
				}
				
			}
		}			
		
	} // End of For Loop J

	
}
//	print("strGenderPre="+strGenderPre)
	return strGenderPre
}

function CBN_Validate_Collection(vIDs,vMsg,ndb,ndc,ignore)
{
	/*
	 * 
	 */
	//print("CBN_Validate_Collection")

	//Check Keyword to get which Fields to be matching in order to Create this Combine Booking
	var vControlKW = GetKeyword("CBN_Fields_Matching");
	var intMaxTimeDiff = parseInt(GetKeyword("CBN_MaximumTimeDifference"));
	var vCompareVals = [];
	var vFldLabels = [];
	// var vFldLabels = []; var vTemp; var strFldValue;

	var vData = []; var dt_StartTime:Date; var dt_EndTime:Date; var strTemp;
	strGenderPre = ""
	
	for(var i=0; i<vIDs.length; i++){
		
		var ndoc:NotesDocument = ndb.getDocumentByUNID(vIDs[i]);
	//	print("doc check #" + i)
		CBN_Validate_Booking(ndoc,vMsg,vCompareVals,vFldLabels,vControlKW,ignore);
		//print("outside strGenderPre="+strGenderPre)
		if (vMsg.length==0)
		{
			ndc.addDocument(ndoc);
			dt_StartTime = ndoc.getItemValueDateTimeArray('BOOK_Appt_Start').elementAt(0).toJavaDate();
			dt_EndTime = ndoc.getItemValueDateTimeArray('BOOK_Appt_End')[0].toJavaDate();
			//Fields to copy to vData
			strTemp = I18n.toString(dt_StartTime, "HHmm") + '~'; //StartTime, for sorting
			strTemp += ndoc.getItemValueString('DocKey') + '~'; //DocKey
			strTemp += dt_StartTime.toString() + '~'; //StartDateFull
			strTemp += dt_EndTime.toString() + '~'; //EndDateFull
			strTemp += ndoc.getItemValueString('BOOK_Comments'); //Comments
			vData.push(strTemp);
		}

	}

	
	vData = vData.sort();// Sort to Compare on Dates
	var dt_First_Start:Date; var dt_Last_End:Date; var intDiff;  	
	for(var i=0; i<vData.length; i++){
		var vTemp = vData[i].split('~');
		if(i>0){
			dt_First_Start = new Date(vTemp[2]);
		//	print('dt_Last_End -> ' + dt_Last_End); print('dt_First_Start -> ' + dt_First_Start);
			intDiff = (dt_First_Start - dt_Last_End)/60000;
		//	print('intDiff -> ' + intDiff);
			if(intDiff < 0){
		//		print("overlap")
				vMsg.push('You have selected bookings with over-lapping time');
			} else if(intDiff > intMaxTimeDiff){
		//		print("time difference")
				vMsg.push('You have selected bookings with Time Difference of more than ' + intMaxTimeDiff.toString() + ' minutes.');
			}
		}
		dt_Last_End = new Date(vTemp[3]);
		
		if(!IsNullorBlank(vTemp[4]) != ''){
				//print("adding comment:"+vTemp[4]);
				vComms.push(vTemp[4])}; //Comments Combine to add to CBN Document
	}
//	print("end strGenderPre="+strGenderPre)
//	print("vCOmms="+vComms)
//	print("vData="+vData)
	return vData
	
}
