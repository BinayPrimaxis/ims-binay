function Grid_JSON_Java(){
	
	var externalContext = facesContext.getExternalContext();
	var writer = facesContext.getResponseWriter();
	var response = externalContext.getResponse();
 
	// Set content type
	response.setContentType("application/json");
	response.setHeader("Cache-Control", "no-cache");
	
	var strViewName = param.VK;
	var strForm = param.Frm;
	var nvw:NotesView = GetDB_Database(strForm).getView(strViewName);	
	var out:com.primaxis.ViewColumn = new com.primaxis.ViewColumn(session);
	var dateRange = param.dtRange;
	var strCat = param.cat;
	var strFTSearch = param.ftkey;	
	var strFTSearch_PreDefined = null;
	
	//This is used by Combine Booking to Search new Suggestion list
	if(sessionScope.containsKey(strViewName)){
		strFTSearch_PreDefined = sessionScope.get(strViewName);
		sessionScope.remove(strViewName);
	}
		
	//Calendar JSON
	var strStart = param.start;	var strEnd = param.end; var CalKey = param.CalKey; //Used by
		
	if(!IsNullorBlank(strFTSearch) | !IsNullorBlank(strFTSearch_PreDefined)){
		
		var strQuery;
		if(!IsNullorBlank(strFTSearch)){ 
			strQuery = searchQuery(@Right(strFTSearch, "~"));
			//This is to restrict search for interpreters to their Language only
			if(strViewName == 'GRID_INT_SearchURN'){
				var strQApp = @Implode(ssGetVariable('ssUser_LanguageList'), ' | ');				
				strQuery += " AND ( [BOOK_Language] = "  + strQApp + " )"				
			}
		} else if(!IsNullorBlank(strFTSearch_PreDefined)){
			strQuery = strFTSearch_PreDefined
		}
		writer.write("{\"rows\":" + out.getViewColumnValueJSON_FTSearch(nvw, strQuery, -1, true) + "}");
		
	} else if(!IsNullorBlank(dateRange)){
		
		dateRange = dateRange.split('_');
			
		//This is used by Interpreter Language All & Requestor All Grid
		if(IsNullorBlank(strCat) == false){			
			var vRows = []; var vCat = strCat.split('~');
			for(var i=0; i<vCat.length; i++){
				var vRet = View_GetEntryPositions(nvw, vCat[i] + '~' + dateRange[0], vCat[i] + '~' + dateRange[1]);
				if(vRet != null){
					var strTemp = out.getViewColumnValueJSON_Positions(nvw, vRet[0], vRet[1]);
					if(strTemp != "[]"){
						strTemp = @RightBack(@LeftBack(strTemp,1),1)	
						vRows.push(strTemp);
					}					
				}					
			}
			if(vRows.join('') == ''){
				writer.write("{\"rows\":[]}");	
			} else {
				writer.write("{\"rows\":[" + JSON_SortData(vRows) + "]}");				
			}			
				
		} else {
			var vRet = View_GetEntryPositions(nvw, dateRange[0], dateRange[1]);		
			if(vRet == null){
				writer.write("{\"rows\":[]}");	
			} else {	
				writer.write("{\"rows\":" + out.getViewColumnValueJSON_Positions(nvw, vRet[0], vRet[1]) + "}");
			}
		}
		
	} else if(!IsNullorBlank(strCat)){
		var vRows = out.getViewColumnValueJSON_Category(nvw, @Explode(strCat,"~"));
		if(vRows == "[]"){writer.write("{\"rows\":[]}");return}
		writer.write("{\"rows\":[" + JSON_SortData(vRows) + "]}");		
		
		
	} else if(!IsNullorBlank(strStart) & !IsNullorBlank(strEnd)){
		//JSON call by Calendar
		if(CalKey == 'null'){
			writer.write("[]")
		} else {
			strStart = CalKey + '~' + @ReplaceSubstring(strStart, "-", "");
			strEnd = CalKey + '~' + @ReplaceSubstring(strEnd, "-", "");
			var vRet = View_GetEntryPositions(nvw, strStart, strEnd);			
			if(vRet == null){
				writer.write("[]");	
			} else {
				writer.write(out.getViewColumnValueJSON_Positions_Calendar(nvw, vRet[0], vRet[1]));
			}
		}	
	
	} else {
		
		var strJSON = out.getViewColumnValueJSON(nvw);
		
		if(@Contains(strJSON, "~EVAL~")){
			var vJSON = strJSON.split('~EVAL~'); 
			for(var i=1; i<vJSON.length; i++){
				if(vJSON[i].indexOf('FS~')>=0){					
					vJSON[i] = eval(@Left(@Right(vJSON[i], "FS~"), "~FE"));
				}				
			}
			strJSON = vJSON.join('');
		}
		writer.write("{\"rows\":" + strJSON + "}");	
	}
	
	writer.endDocument();
	
}

function JSON_SortData(vRows){	
	//For Sorting the Data		
	if(typeof vRows == 'string'){
		vRows = vRows.split("],[");	
	} else {
		vRows = vRows.join(',').split("],[");	
	}	
	var count = vRows.length;
	vRows[0] = vRows[0].replace('[','');
	vRows[count-1] = vRows[count-1].replace(']','');
	return "[" + vRows.sort().join('],[') + "]";
}

function View_GetEntryPositions(nvw, strStartKey, strEndKey){
	
	//Start Date is Greater than End Date
	if(View_GetEntryPosition_Continue(strStartKey, strEndKey) == false){return null}
	
	var posStart = 0; var bFirstPosition = true; var iCount = 0;
	
	while (posStart == 0){		
		posStart = View_GetEntryPosition_GetPosition(nvw, strStartKey, bFirstPosition)
		if(posStart == 0){
			strStartKey = View_GetEntryPosition_AdjustSearchString(strStartKey, bFirstPosition);			
			//Start Date is Greater than End Date
			if(View_GetEntryPosition_Continue(strStartKey, strEndKey) == false){
				return null
			}			
		}
		iCount++;
		if(iCount > 50){return null}
	}
		
	var posEnd = 0; bFirstPosition = false;
	while (posEnd == 0){		
		posEnd = View_GetEntryPosition_GetPosition(nvw, strEndKey, bFirstPosition)
		if(posEnd == 0){
			strEndKey = View_GetEntryPosition_AdjustSearchString(strEndKey, bFirstPosition);			
			//Start Date is Greater than End Date
			if(View_GetEntryPosition_Continue(strStartKey, strEndKey) == false){
				return null
			}			
		}
	}
	
	return [posStart, posEnd];	
}

function View_GetEntryPosition_Continue(strStartKey, strEndKey){
	
	if(strStartKey.indexOf('~')>=0){
		var intStart = parseInt(strStartKey.split('~')[1]); var intEnd = parseInt(strEndKey.split('~')[1]);		
	} else {
		var intStart = parseInt(strStartKey); var intEnd = parseInt(strEndKey);
	}	
	//if(intStart>intEnd){print("ERROR: Date Range End Date should be greater than Start Date")}
	return intStart <= intEnd;
}

function View_GetEntryPosition_AdjustSearchString(strSearchString, bFirstPosition){
	
	var strApp = ''; var strDtString = strSearchString;	
	if(strSearchString.indexOf('~')>=0){
		strDtString = strSearchString.split('~')[1]; strApp = strSearchString.split('~')[0] + '~';					
	}
	
	var dt:Date = @Date(strDtString.substring(0,4), strDtString.substring(4,6), strDtString.substring(6));
	//if(bFirstPosition){dt = @Adjust(dt, 0, 0, 1, 0, 0, 0)} else {dt = @Adjust(dt, 0, 0, -1, 0, 0, 0)}
	
	if(bFirstPosition){dt.setDate(dt.getDate() + 1)} else {dt.setDate(dt.getDate() - 1)}
	var strReturn = strApp + I18n.toString(dt, "yyyyMMdd");
	//print('Adjusted ' + (bFirstPosition ? 'StartDate' : 'EndDate') + ' ~~~~~ ' + strSearchString + ' to ' + strReturn);	
	return strReturn;
}

function View_GetEntryPosition_GetPosition(nvw, strSearchString, bFirstPosition){
	//print(strSearchString)
	var nve:NotesViewEntryCollection = nvw.getAllEntriesByKey(strSearchString, true);
	if(nve.getCount() == 0){return 0}	
	//print('~~~~~~~ Position Found for ' + (bFirstPosition ? 'StartDate' : 'EndDate'));
	var intPosition = nve.getCount();	
	if(bFirstPosition){intPosition = 1}
	return parseInt(nve.getNthEntry(intPosition).getPosition("."));	
}

function Grid_SaveColumnOrder_SJS(strKey){	
	if(!strKey){
		strKey = getComponent('gridViewName').getValue();
		var fld = getComponent("saveColumnOrder");
	} else {
		var fld = getComponent("saveColumnOrder_" + strKey)
	}
	
	var strForm = (IsUser_Requestor() ? 'REQU' : 'USER');	
	var doc:NotesDocument = GetDB_Database(strForm).getDocumentByUNID(General_GetUser_UNID());	
		
	if(doc != null){
		doc.replaceItemValue(strForm + '_' + strKey, fld.getValue().split("|@|"));
		doc.save(true, false);
		fld.setValue(null);
		return
	}		
}	

function Grid_DateString(strKey, strViewName){
	
	var dt = @Today();	//Default
	var bAdjustToWeekDay = false;
	
	if(strKey == "Tomorrow"){
		dt = @Tomorrow();
		bAdjustToWeekDay = true;
	}
	
	if(strKey == "Yesterday"){dt = @Yesterday()}
	
	if(strKey != "Today" & strKey != "Tomorrow" & (strKey.indexOf("Today")>=0 | strKey.indexOf("Tomorrow")>=0)){
		var intAdjust = (strKey.indexOf("Today")>=0 ? parseInt(@Right(strKey, "Today")) : parseInt(@Right(strKey, "Tomorrow")));
		dt = @Adjust(dt, 0, 0, intAdjust, 0, 0, 0);
		bAdjustToWeekDay = true;
	}	
	
	if(@LowerCase(strKey) == 'all-start'){
		var nvw:NotesView = GetDB_Database('BOOK').getView(strViewName);
		var viewNav:NotesViewNavigator = nvw.createViewNav();
		return viewNav.getFirst() ? nvw.createViewNav().getFirst().getColumnValues().elementAt(0) : null
	}
	
	if(@LowerCase(strKey) == 'all-end'){
		var nvw:NotesView = GetDB_Database('BOOK').getView(strViewName);
		var viewNav:NotesViewNavigator = nvw.createViewNav();
		return viewNav.getLast() ? nvw.createViewNav().getLast().getColumnValues().elementAt(0) : null
	}
	
	if(strKey == 'LastMonth_Start'){
		var intMonth = @Month(dt); var intYear = @Year(dt);
		//Change Month year if present month is Janurary
		if(intMonth == 1){intMonth = 12; intYear--;} else {intMonth--;}
		dt = @Date(intYear, intMonth, 1);
	}
	
	//Create Date first of present Month and then adjust -1 day to get last day of previous month
	if(strKey == 'LastMonth_End'){
		var intMonth = @Month(dt);
		var intYear = @Year(dt);
		dt = @Date(@Year(dt), intMonth, 1);	
		dt = @Adjust(dt, 0, 0, -1, 0, 0, 0);
	}
	
	//Create Date first of present Month and then adjust -1 day to get last day of previous month
	if(strKey == 'CurrentMonth_End'){
		var intMonth = @Month(dt);
		var intYear = @Year(dt);
		dt = @Date(@Year(dt), intMonth+1, 1);	//Next Month First Day
		dt = @Adjust(dt, 0, 0, -1, 0, 0, 0);	//Go back one day
	}
	
	if(bAdjustToWeekDay){
		var intWeekDay = @Weekday(dt);
		if(intWeekDay == 1){dt = @Adjust(dt, 0, 0, 1, 0, 0, 0)}
		if(intWeekDay == 7){dt = @Adjust(dt, 0, 0, 2, 0, 0, 0)}		
	}
	
	//print('Key - ' + strKey + "   , value - " + I18n.toString(dt, "yyyyMMdd"))
	
	return I18n.toString(dt, "yyyyMMdd");
	
}

function Grid_Model(strType){
	//print('Grid Model Called for ' + strType)
	var strReturn = '';	
	var gList = GetKeyword("GRID_List_" + strType);	
	if(isString(gList)){gList = [gList]}
	for(var i=0; i<gList.length; i++){		
		strReturn += Grid_Model_GenerateMember(gList[i]);		
		strReturn += "gridObject['" + gList[i] + "'] = " + gList[i] + ";\n";
	}		
	return strReturn
}

function Grid_Model_GenerateMember(gridName){	
	/*
	 * This is called by itself in case of Interpreter Grid 'cos they are loaded together and need
	 * the grid member object independently
	 * while in case of coordinator it's only a single grid which changes thru menu items
	 */	
	var gridModel, vTemp, varName, varValue;	
	var strReturn = "var " + gridName + " = {};\n";	
	var gridModel = GetKeyword(gridName);
	
	for(var j=0; j<gridModel.length; j++){		
	
		vTemp = gridModel[j].split(":");
		varName = @Trim(vTemp[0]);
		varValue = @Trim(vTemp[1]);
		
		if(varName == 'ColumnModel'){			
			varValue = Grid_ColumnModel(gridName, false, @Trim(varValue.split(",")));
			
		} else if(varValue.indexOf('EVAL')>=0){
			
			var strKey = @Right(varValue, 'EVAL~');
						
			if(varName == 'DateRangeStart' | varName == 'DateRangeEnd'){
				varValue = "'" + Grid_DateString(strKey, gridName) + "'";
			} else if (varName == 'MultiSelect' | varName == 'RowCount'){
				varValue = eval(strKey);
			}
			
			
		} else if(varName == 'ViewCategory'){			
			varValue = "'" + escape(eval(varValue)) + "'";
			
		} else if(varName == 'ExportColumnModel'){
			//This value has lots of ':', so needs to eval differently
			varValue = @Right(gridModel[j], varName);	
			varValue = @Trim(@Right(gridModel[j], ':'));			
		}
		
		strReturn += gridName + "['" + varName + "'] = " + varValue + ";\n";		
	}
	return strReturn
	

}

function Grid_ColumnModel(strKey, bReset, vColList){
	
	var bModifyName = (@Left(strKey,8) == 'GRID_INT');
		
	//If Column list is not sent in function, get the Grid Keyword and get column list
	if(!vColList){
		var vTemp1; var gridModel = GetKeyword(strKey);
		for(var j=0; j<gridModel.length; j++){	
			vTemp1 = gridModel[j].split(":");
			if( @Trim(vTemp1[0]) == 'ColumnModel'){
				var vColList = @Trim(vTemp1[1].split(","));				
			}
		}
	}	
	//Get Column Attributed for column list
	var bHidden; var strColName; var strColModel;
	for(var k=0; k<vColList.length; k++){
		strColName = vColList[k];
		
		bHidden = strColName.indexOf('*')>=0;
		if(bHidden){strColName = @Left(vColList[k], '*');}
		
		strColModel = GetKeyword("Grid_Column_" + strColName);
		if(strColModel.indexOf('~EVAL_START~')>=0){
			var strFormula = @Middle(strColModel, '~EVAL_START~', '~EVAL_END~');
			strColModel = @ReplaceSubstring(strColModel, '~EVAL_START~' + strFormula + '~EVAL_END~', eval(strFormula));			
		}
		
		if(bModifyName){
		//	if(strColName == '_@unid'){strColName = '@unid'}
		//	strColModel = @ReplaceSubstring(strColModel, 'name:"' + strColName + '"', 'name:"' + strKey + '_' + strColName + '"');
		}
		
		if(bHidden){
			if(strColModel.indexOf('hidden:true')<0){strColModel += ', hidden:true'}				
		}
		vColList[k] = strColModel + ', jsonmap: ' + (k+1);
		//print(strColModel + ', jsonmap: ' + (k+1))
	}


	
	
	var retVal = "[{" + vColList.join("},{") + "}]";
	if(bReset == true){return retVal}
	if(viewScope.containsKey('resetGridColumn')){if(viewScope.resetGridColumn == strKey){return retVal}}
	
	if(General_GetUser_UNID() == ''){return retVal}
	
	var strForm = (IsUser_Requestor() ? 'REQU' : 'USER');	
	var doc:NotesDocument = GetDB_Database(strForm).getDocumentByUNID(General_GetUser_UNID());
	if(doc == null){return retVal}
	if(doc.hasItem(strForm + '_' + strKey) == false){return retVal}
	
	//Sample value - kwdName~227@kwdCategory~227@kwdDescription~471
	var vPref_ColOrder = @Left(doc.getItemValue(strForm + '_' + strKey), "~");
	var vPref_ColWidth = @Right(doc.getItemValue(strForm + '_' + strKey), "~");
	//print(vColOrder);
	
	var strColAtt; var vColAtt; var strColName; var bHidden; var index; var vColAtt; var strWidth; 
	var vColModel_Show = []; var vColModel_Hidden = []; var bHiddenAttPresent;
	
	for(var i=0; i<vColList.length; i++){
		
		//Sample value - name:"kwdName", label:"Name", width:120
		strColAtt = vColList[i];
		strColName = @Left(@Right(@Trim(strColAtt), 'name:"'), '"');
		
		bHiddenAttPresent = (strColAtt.indexOf('hidden') >=0);
		
		//Column found in Prefrences, so set the width and if default is hidden, then unhide it				
		index = vPref_ColOrder.indexOf(strColName);
		if(index >= 0){strWidth = vPref_ColWidth[index]}
		
		vColAtt = vColList[i].split(","); //Column Attributes into Array
		
		for(var j=0; j<vColAtt.length; j++){
			// Show the column and set width
			if(index >= 0){
				//Set Width
				if(@Trim(@Left(vColAtt[j], ":")) == 'width'){
					vColAtt[j] = "width: " + strWidth;
				}				
			}
			if(bHiddenAttPresent){
				if(@Trim(@Left(vColAtt[j], ":")) == 'hidden'){
					if(index >= 0){					
						vColAtt[j] = "hidden: false";
					} else {
						vColAtt[j] = "hidden: true";
					}
				}	
			}			
		}
		strColAtt = vColAtt.join(", ");
		//Add Hidden Attaribute if not present and needs to be hidden 
		if(bHiddenAttPresent == false & index == -1){strColAtt += ", hidden: true";}
				
		if(index == -1){
			//print(strColAtt)
			vColModel_Hidden.push(strColAtt);
		} else {
			vColModel_Show[index] = strColAtt;
		}
		//print(strColName + " OriginalPos: " + i + ", NewPos: " + (index + hideCounter))				
	}
	//print(vColModel_Show)
	//print(vColModel_Hidden)
	var strReturn = "{" + vColModel_Show.join("},{") + "}";
	if(vColModel_Hidden.length > 0){
		strReturn += ", {" + vColModel_Hidden.join("},{") + "}"
	}
	return "[" + strReturn + "]";	
}

function USER_Grid_Hours(strDocKey){
	
	var vHM:java.util.HashMap = USER_Hours_NonStandard();
	if(vHM.containsKey(strDocKey) == false){return 'Standard Hours'}
	
	var vDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var vValues:java.util.Vector = vHM.get(strDocKey);
	var strReturn = '';
	for(var i=0; i<vValues.size(); i++){
		if(vValues[i] != 0){
			strReturn += vDays[i] + " " + @Right('0' + @Text(vValues[i][0]),4);
			strReturn += " " + @Right('0' + @Text(vValues[i][1]),4);
			strReturn += '<br/>';
		}
	}
	return strReturn	
}

function USER_Grid_Leaves(strDocKey){
	
	var vHM:java.util.HashMap = USER_Leaves();
	if(vHM.containsKey(strDocKey) == false){return '--'}
	
	var vValues:java.util.Vector = vHM.get(strDocKey);
	var vReturn = [];
	for(var i=0; i<vValues.size(); i++){
		vReturn.push(ConvDt(@Text(vValues[i][0])) + ' till ' + ConvDt(@Text(vValues[i][1])))
	}
	return vReturn.join('<br/>')
}

function searchQuery(val){
	
	var v:String = val;
	var checkAlert = Alerts_GetSearchString(val);
	if(checkAlert != val){return checkAlert}

	if ((v != null) || (v != "")) {
    	v = v.toLowerCase();
    	v = v.replace("\"", "");
    	v = v.replace("(", "");
    	v = v.replace(")", "");
    	v = v.replace("[", "");
    	v = v.replace("]", "");
  
	    v = v.replace("$", "");    
	    v = v.replace(" and ", " ");
     	v = v.replace("{", "");
    	v = v.replace("}", "");
    	
    	var a = v.split(" ");
    	v = a.join(" and ");    
    	v = v.replace(" not ", "\"not\"");
    	v = v.replace(" or ", "\"or\"");
    	
    	return v    		
	}
	
	return val
}

function View_ColumnValue(strColumn){
	var colField = 'coll.' + @Right(collNames, '~');	
	if(colField == 'coll.modified' | colField.indexOf('coll.date') >=0){colField = 'View_ColumnValue_DateFormat(' + colField + ')'}
	return eval(colField)
}

function Grid_Status_Color(pos){
	var vList = GetKeyword('BOOK_Status_Colors');
	if(isString(vList)){
		if(vList == 'NA'){return '<span class="statusColor txt_Hide">NA</span>'}
		vList = [vList]
	}
	
	var strCSS = "td:nth-child(" + pos + "){background-color:";
	var strStyle = '<style>';
	for(var i=0; i<vList.length; i++){
		strStyle += ('.status_' + @LowerCase(@Left(vList[i], '~')) + ' ' + strCSS + @Right(vList[i], '~') + ' !important} ');
	}
	return strStyle + '</style>';
}

function Grid_Interpreter_Color(){
	
	var nv:NotesView = GetDB_Database("USER").getView("USER_Colors");
	var nvec:NotesViewEntryCollection = nv.getAllEntries();
	var nve:NotesViewEntry; var strStyle = null;
	if(nvec.getCount() > 0){
		nve = nvec.getFirstEntry();
		strStyle = '<style>';
		while(nve != null){
			strStyle += nve.getColumnValues()[0];
			nve = nvec.getNextEntry(nve);
		}	
		strStyle += '</style>';
	}
	
	nv = GetDB_Database("AGN").getView("AGN_Colors");
	nvec = nv.getAllEntries();
	if(nvec.getCount() > 0){
		nve = nvec.getFirstEntry();
		strStyle += '<style>';
		while(nve != null){
			strStyle += nve.getColumnValues()[0];
			nve = nvec.getNextEntry(nve);
		}	
		strStyle += '</style>';
	}

	return strStyle
}

function Alerts_Enabled(){return GetKeyword('Alert_Enabled') == 'YES'}

function Alerts_Data(){
	
	var intCount = 1;
	var vData = GetKeyword('Alert_' + intCount);
	var strHTML_Template = GetKeyword('Alert_HTML'); var strCSS_Template = GetKeyword('Alert_CSS');
	var strHTML_Return = ''; var strCSS_Return = ''; var strTemp;
	
	var out:com.primaxis.ViewColumn = new com.primaxis.ViewColumn(session);
	var nvw:NotesView = GetDB_Database('BOOK').getView('GRID_CRD_Search');
		
	while (vData != ''){
		
		var vDocList = out.getViewColumnValueJSON_FTSearch(nvw, Alerts_Evaluate(vData[1]), 3, false)
		var docCount = 0; var vUNID = [];
		if(vDocList != '[]'){
			vDocList = @Left(@Right(vDocList, '[["'), '"]]');	
			vUNID = vDocList.split('"],["');
			docCount = vUNID.length;
		}
		
		//HTML
		strTemp = strHTML_Template;		
		strTemp = @ReplaceSubstring(strTemp, '@COLOUR@', vData[3]);
		strTemp = @ReplaceSubstring(strTemp, '@NAME@', vData[0]);
		strTemp = @ReplaceSubstring(strTemp, '@COUNT@', docCount);
		strTemp = @ReplaceSubstring(strTemp, '@ID@', 'Alert_' + intCount);
		strTemp = @ReplaceSubstring(strTemp, '@DESCRIPTION@', vData[2]);
		strHTML_Return += strTemp;
		
		//CSS
		strTemp = strCSS_Template;
		strTemp = @ReplaceSubstring(strTemp, '@COLOUR@', vData[3]);
		var vTemp = strTemp.split('{'); var strFirstChar;
		
		/*
		 Sample Output
		 .CA2580A300320648CA257F4F001C150A td:nth-child(2), 
		 .\32 9CCEE9AC18563B1CA2580A6001BF8A9 td:nth-child(2), 
		 dummy {background-color:#d9534f} 
		 */
		
		for(var i=0; i<vUNID.length; i++){
			
			strFirstChar = @Left(vUNID[i],1);			
			
			strCSS_Return += '.';
			
			if(isNaN(parseInt(strFirstChar))){
				strCSS_Return += vUNID[i];
			} else {
				strCSS_Return += '\\3' + strFirstChar + ' ' + @Right(vUNID[i],31);
			}
			
			strCSS_Return += ' ' + vTemp[0] + ', ';			
		}
			
		strCSS_Return += 'dummy {' + vTemp[1];
		
		intCount ++;
		vData = GetKeyword('Alert_' + intCount);
	}
	
	return '<h4>' + strHTML_Return + '</h4><style>' + strCSS_Return + '</style>';
}

function Alerts_GetSearchString(strSearch){
	
	var intCount = 1; var vData = GetKeyword('Alert_' + intCount);			
	while (vData != ''){		
		if(vData[0] == strSearch){return Alerts_Evaluate(vData[1])}			
		intCount ++;
		vData = GetKeyword('Alert_' + intCount);
	}	
	return strSearch;
}

function Alerts_Evaluate(strSearch){

	if(strSearch.indexOf('EvalStart')>=0){	
		var vTemp = strSearch.split('EvalStart');
		var strEval; var strFormula;
		for(var i=0; i<vTemp.length; i++){
			
			if(vTemp[i].indexOf('EvalEnd')>=0){
				strFormula = @Left(vTemp[i], 'EvalEnd');
				
				if(strFormula.indexOf('@')>=0){				
					strEval = session.evaluate(strFormula).join('');
					
					//Check business Days
					if(strFormula.indexOf('@Adjust')>=0){
						
						var dt:Date = I18n.parseDate(strEval);
						if(@Weekday(dt) == 1){dt = @Adjust(dt,0,0,1,0,0,0)}
						if(@Weekday(dt) == 7){dt = @Adjust(dt,0,0,2,0,0,0)}
						strEval = I18n.toString(dt, "dd/MM/yyyy");		
					}
				}	
							
				strSearch = @ReplaceSubstring(strSearch, 'EvalStart' + strFormula + 'EvalEnd', strEval);
			}	
		}
	}
	return strSearch
}

function Grid_InterpretList(){
	//print('Grid_InterpretList() - Called with id - ' + param.Id)
	var strID = param.Id;
	if(IsNullorBlank(strID)){
		print('ERROR: NO ID');
		return null
	}
	
	var docBook:NotesDocument = GetDB_Database("BOOK").getDocumentByUNID(strID);
	if(docBook == null){
		print('ERROR: NO Document found');
		return null
	}
	
	var strExistInterpreter = docBook.getItemValueString('BOOK_Interpreter_DocKey');
	
	var strLang = docBook.getItemValueString('BOOK_Language');
	if(IsNullorBlank(strLang)){
		print('ERROR: NO Language');
		return null
	}
	
	//Interpreter List
	var vIntList = @DbLookup(GetDB_FilePath('USER'), "USER_byLanguage", strLang, 2, "[FAILSILENT]");
	
	//Agency List
	var vListAll = @DbLookup(GetDB_FilePath('AGN'), "AGN_byLanguage", "ALL", 2, "[FAILSILENT]");
	var vListAgency = @DbLookup(GetDB_FilePath('AGN'), "AGN_byLanguage", strLang, 2, "[FAILSILENT]")
	
	var vRet = @Trim(@Explode(@Implode(vIntList,"~") + "~" + @Implode(vListAgency,"~") + "~" + @Implode(vListAll,"~"), "~"));		
	if(IsNullorBlank(vRet)){
		print('ERROR: NO List');
		return null
	} 
	if(isString(vRet)){vRet = [vRet]}
	
	var externalContext = facesContext.getExternalContext();
	var writer = facesContext.getResponseWriter();
	var response = externalContext.getResponse();
	response.setContentType("application/json");
	response.setHeader("Cache-Control", "no-cache");
	
	var vJSON = []; var strIDocKey; var vItem;
	for(var i=0;i<vRet.length; i++){		
		vItem = [];
		strIDocKey = @Right(vRet[i], "|");
		vItem.push('"value" : "' + strIDocKey + '"');
		vItem.push('"text" : "' + @Left(vRet[i], "|") + '"');
		if(strIDocKey == strExistInterpreter){vItem.push('"selected" : true');}
		vJSON.push('{' + vItem.join(', ') + '}');
	}	
		
	writer.write('[' + vJSON.join(',') + ']');	
	writer.endDocument();			
}

function Grid_Update_EditData(){
	
	var exCon = facesContext.getExternalContext();
	var reader = exCon.getRequest().getReader();	
	var line = null;
	var jb:java.lang.StringBuffer = new java.lang.StringBuffer();	
	while ((line = reader.readLine()) != null){jb.append(line)}	
	reader.close();
	var sDB=param.DB;
	if (sDB==null){sDB="BOOK"}
	
	//print(jb.toString());
	var jsonData = fromJson(jb.toString());
	var vMsg = [];
	for(var i=0; i<jsonData.length; i++){
		//print("UNID-" + jsonData[i].ge_unid + ", rowID-" + jsonData[i].ge_rowID + ", flds-" + jsonData[i].ge_flds);
		if (sDB=="RCN_UPLOAD")
		{

				vMsg.push(RCN_Grid_UpdateDocument(jsonData[i].ge_unid, jsonData[i].ge_rowID, jsonData[i].ge_flds));
		
		}
		else
		{
			vMsg.push(BOOK_Grid_UpdateDocument(jsonData[i].ge_unid, jsonData[i].ge_rowID, jsonData[i].ge_flds));		
			
		}
	}
	
	var response = facesContext.getExternalContext().getResponse();
	var writer = response.getWriter();
	response.setContentType("application/json"); 
	response.setHeader("Cache-Control", "no-cache");
	writer.write('[{' + vMsg.join('},{') + '}]');	
	facesContext.responseComplete();
}

function RCN_Grid_UpdateDocument(strUNID, rowID, fldObject)
{
try {
		
		var ndb:NotesDatabase = GetDB_Database('RCN_UPLOAD'); var strMsg;
		
		//Get Document
		var ndoc:NotesDocument = ndb.getDocumentByUNID(strUNID);
		if(ndoc == null){
			strMsg = 'ERROR: Booking document not found with ID: ' + strUNID + '. Please contact IT Support';
			return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
		}
		if((ndoc.getItemValueString("StatusRemediation")=="Accepted" || ndoc.getItemValueString("StatusRemediation")=="Remediated") && param.accept=="true"){
			strMsg = 'Row ' + rowID + " Has already been accepted";
			//return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
			//vGridUpdateCols.push({rowID:rowID,error:'Row ' + rowID + " Has already been accepted"})
			return '"rowID":'+rowID+',"error":"'+strMsg+'"'
		}
		//Need to revisit to handle multiple fields sent for same UNID, Save should happend when all fields are updated properly
		var vMsg = []; var vGridUpdateCols = [];var sStatus="";
		if (param.accept=="true")
		{
			if (ndoc.getItemValueString("StatusRemediation")=="Missing" && ndoc.getItemValueString("Flag_Site_Selected")!="OK"){
				strMsg = 'Row ' + rowID + ": You must verify Location details before accepting";
				return '"rowID":'+rowID+',"error":"'+strMsg+'"'
			}
			RCN_Accept(ndoc)
			if (RCN_Check_Remediation(ndoc))
			{
				//print("pushing status remedial = remediated to return json")
				fldObject.push({ge_fldName:'rcnStatusRemedial',ge_fldVal:"Remediated"})
			}	
			//print("saving ndoc")
			ndoc.save(true, false);
		}
		else if (param.recalc=="true"){
			RCN_Recalc(ndoc);
			ndoc.recycle();
			//print("re-getting document");
			var ndoc:NotesDocument = ndb.getDocumentByUNID(strUNID);
			//print("ndoc="+ ndoc.getUniversalID());
		//	print(ndoc.getItemValue("IMSCost"));
			fldObject.push({ge_fldName:'IMSCost',ge_fldVal:ndoc.getItemValueDouble("IMSCost")})
			fldObject.push({ge_fldName:'RCN_CostDescription',ge_fldVal:ndoc.getItemValueString("RCN_COSTDESCRIPTION")})
			fldObject.push({ge_fldName:'RCN_Status',ge_fldVal:ndoc.getItemValueString("STATUS")})
			fldObject.push({ge_fldName:'rcnStatusRemedial',ge_fldVal:ndoc.getItemValueString("StatusRemediation")})
			fldObject.push({ge_fldName:'RCN_Difference',ge_fldVal:ndoc.getItemValueDouble("DIFFERENCE")})
		//	print(fldObject)
		}
		else
		{
			for(var i=0; i < fldObject.length; i++){
				
				var strFieldName = fldObject[i].ge_fldName; var strFieldValue = fldObject[i].ge_fldVal;
				strFieldValue = unescape(fldObject[i].ge_fldVal);
										
				//May be we allow field to be blank from Grid
				if(IsNullorBlank(strFieldValue)){
					strMsg = 'ERROR: Field Value cannot be blank';
					return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
					
				} else { 
					//<<Another field come here>>, make sure each field update is returning error or 'UPDATED'
					//if(strFieldName == 'BOOK_Comments'){ndoc.replaceItemValue(strFieldName, strFieldValue)}
					//if(strFieldName == 'BOOK_CommentsInterpreter'){ndoc.replaceItemValue(strFieldName, strFieldValue)}
					if (strFieldName=="RemedialCost" || strFieldName=="DIFFERENCE" || strFieldName=="IMSCost") {
						//print("convert remedial cost to number")
						ndoc.replaceItemValue(strFieldName, strFieldValue*1)
					}
					else
					{
						ndoc.replaceItemValue(strFieldName, strFieldValue)
					}
					
				}			
			}
			//print("saving ndoc")
			ndoc.save(true, false);
		}
		
		
		
		//If above loop is finished it means all updated, loop again to get Grid Update Column details
		for(var i=0; i < fldObject.length; i++){
			var vList = GetKeyword("GE_UpdateCols_" + fldObject[i].ge_fldName)
			//print(vList)
			for(var j=0; j<vList.length; j++){		
				//print(@Right(vList[j], '~'));
				//print(toJson(@Left(vList[j], '~')) + ': ' + toJson(ndoc.getFirstItem(@Right(vList[j], '~')).getText()));
				vGridUpdateCols.push(toJson(@Left(vList[j], '~')) + ': ' + toJson(ndoc.getFirstItem(@Right(vList[j], '~')).getText()))		
			}
		}
		
		return BOOK_Grid_UpdateDocument_Return("UPDATED", rowID, vGridUpdateCols);
		
	} catch(e) {
		strMsg = "ERROR Encountered: " + e.toString();
		return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
	}
}
function BOOK_Grid_UpdateDocument(strUNID, rowID, fldObject){
	
	print("BOOK_Grid_UpdateDocument - START - " + strUNID)	
	// fldObject JSON Format Description. [{ge_fldName : <<FieldName>>, ge_fldVal : <<FieldValue>>}]    	

	try {
		
		var ndb:NotesDatabase = GetDB_Database('BOOK'); var strMsg;
		
		//Get Document
		var ndoc:NotesDocument = ndb.getDocumentByUNID(strUNID);
		if(ndoc == null){
			strMsg = 'ERROR: Booking document not found with ID: ' + strUNID + '. Please contact IT Support';
			return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
		}
		
		//Need to revisit to handle multiple fields sent for same UNID, Save should happend when all fields are updated properly
		var vMsg = []; var vGridUpdateCols = [];
		for(var i=0; i < fldObject.length; i++){
			
			var strFieldName = fldObject[i].ge_fldName; var strFieldValue = fldObject[i].ge_fldVal;
			strFieldValue = unescape(fldObject[i].ge_fldVal);
									
			//May be we allow field to be blank from Grid
			if(IsNullorBlank(strFieldValue)){
				strMsg = 'ERROR: Field Value cannot be blank';
				return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
				
			} else { 
			
				if(strFieldName == 'BOOK_Interpreter_DocKey'){
					
					print('current status inBOOK_Grid_UpdateDocument  == ' + ndoc.getItemValueString('BOOK_Status') + ' and strFieldName == ' + strFieldName);
					if(@IsNotMember(ndoc.getItemValueString('BOOK_Status'), ['requested', 'booked', 'updated'])){
						strMsg = 'ERROR: Booking Status is already marked as ' + ndoc.getItemValueString('BOOK_Status_Display');
						return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
					}	
					
					//Check if it's Interpreter or Agency DocKey
					var strInterpreter = @DbLookup(GetDB_FilePath('USER'), "all_byDocKey", strFieldValue, "USER_FullName", "[FAILSILENT]");
					//It's Interpreter
					if(IsNullorBlank(strInterpreter) == false){
						
						strMsg = BOOK_AssignInterpreter_UpdateDoc(ndoc, strFieldValue, strInterpreter, false);
						if(strMsg.indexOf('~')>=0){	//We don't need check availablity codes handled here, so just need first part of message
							strMsg = @Left(strMsg, '~');
						}
						
					} else {
						//Check if its Agency
						strInterpreter = @DbLookup(GetDB_FilePath('AGN'), "all_byDocKey", strFieldValue, "AGN_Name", "[FAILSILENT]");					
						if(IsNullorBlank(strInterpreter)){
							strMsg = 'Interpreter/Agency not found with ID: ' + strFieldValue + '. Please contact IT Support';
							return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
						} else {
							BOOK_AssignToAgency_UpdateDoc(strFieldValue, strInterpreter, false, null, ndoc);
							strMsg = "UPDATED"
						}					
					}
				
					//Check this after each field update, if error no need to procced further
					if(strMsg != 'UPDATED'){return BOOK_Grid_UpdateDocument_Return(strMsg, rowID)}
					
				}
				
				//<<Another field come here>>, make sure each field update is returning error or 'UPDATED'
				if(strFieldName == 'BOOK_Comments'){ndoc.replaceItemValue(strFieldName, strFieldValue)}
				if(strFieldName == 'BOOK_CommentsInterpreter'){ndoc.replaceItemValue(strFieldName, strFieldValue)}
			}			
		}
		
		ndoc.computeWithForm(true, false); ndoc.save(true, false);
		
		//If above loop is finished it means all updated, loop again to get Grid Update Column details
		for(var i=0; i < fldObject.length; i++){
			var vList = GetKeyword("GE_UpdateCols_" + fldObject[i].ge_fldName)
			for(var j=0; j<vList.length; j++){		
				//print(toJson(@Left(vList[j], '~')) + ': ' + toJson(ndoc.getFirstItem(@Right(vList[j], '~')).getText()));
				vGridUpdateCols.push(toJson(@Left(vList[j], '~')) + ': ' + toJson(ndoc.getFirstItem(@Right(vList[j], '~')).getText()))		
			}
		}
		
		return BOOK_Grid_UpdateDocument_Return("UPDATED", rowID, vGridUpdateCols);
		
	} catch(e) {
		strMsg = "ERROR Encountered: " + e.toString();
		return BOOK_Grid_UpdateDocument_Return(strMsg, rowID);
	}
	
}

function BOOK_Grid_UpdateDocument_Return(strMsg, rowID, vGridUpdateCols){	
	var strReturn = '"rowID": "' + rowID + '",';	
	if(strMsg != 'UPDATED'){
		strReturn += '"error": "' + strMsg + '"'
	} else {
		strReturn += '"data": {' + vGridUpdateCols.join(',') + '}';
	}
	return strReturn
}

function Grid_JSON_Search_SavedList(intPos, strJoin){
	
	var strKey = "gridSavedSearches";
	
	if(sessionScope.containsKey(strKey) == false){
	
		//print('FUNCTIOn CALLED for intPos -> ' + intPos)
		var vw:NotesView = GetDB_Database(IsUser_Requestor() ? 'REQU' : 'USER').getView('JSON_Search_byDocKey');
		vw.refresh();
		var nvec:NotesViewEntryCollection = vw.getAllEntriesByKey(General_GetUser_DocKey());
		//print('COUNT ->' + nvec.getCount())
		
		if(nvec.getCount() == 0){
			sessionScope.put(strKey, 0);
		} else {
		
			var nve:NotesViewEntry = nvec.getFirstEntry(); 
			var vList_1 = []; var vList_2 = []; var vList_3 = []; 
			var vColVals:java.util.Vector = new java.util.Vector();
			
			while(nve != null){
				vColVals = nve.getColumnValues();
				vList_1.push(vColVals.elementAt(1));
				vList_2.push(vColVals.elementAt(2));
				vList_3.push(vColVals.elementAt(3));						
				nve = nvec.getNextEntry(nve);
			}
			
			sessionScope.put(strKey, [vList_1, vList_2, vList_3]);
		}		
	}
	
	var vList = sessionScope.get(strKey);
	if(vList == 0){return null}
	if(strJoin){return vList[intPos].join(strJoin)}
	return vList[intPos];
}

function Grid_JSON_Search_Save(){	
	
	var strSearchName = getComponent('fld_qb_Search_Name').getValue();
	var strForm = (IsUser_Requestor() ? 'REQU' : 'USER');
	var ndb:NotesDatabase = GetDB_Database(strForm);
	var vw:NotesView = ndb.getView('JSON_Search_byDocKey');	
	var strDocKey = General_GetUser_DocKey();
	var vKeys:java.util.Vector = new java.util.Vector(); vKeys.add(strDocKey); vKeys.add(strSearchName);  
	
	var doc:NotesDocument = vw.getDocumentByKey(vKeys, true);
	if(doc == null){
		doc = ndb.createDocument();
		doc.replaceItemValue('Form', 'JSON_Search');
		doc.replaceItemValue('DocKey_Parent', strDocKey);
		doc.replaceItemValue('JSON_Search_Name', strSearchName);
	}
	
	doc.replaceItemValue('JSON_Search_QB', getComponent('fld_qb_SearchQuery').getValue());
	doc.replaceItemValue('JSON_Search_Grid', getComponent('fld_grid_SearchQuery').getValue());
	doc.save(true, false);
	
	sessionScope.remove('gridSavedSearches');
	getComponent('fld_qb_SavedSearchList').setValue(strSearchName);
	
}

function Grid_JSON_Search_Delete(){	
	var strSearchName = getComponent('fld_qb_SavedSearchList').getValue();
	var vw:NotesView = GetDB_Database(IsUser_Requestor() ? 'REQU' : 'USER').getView('JSON_Search_byDocKey');	
	var vKeys:java.util.Vector = new java.util.Vector(); vKeys.add(General_GetUser_DocKey()); vKeys.add(strSearchName);  
	var doc:NotesDocument = vw.getDocumentByKey(vKeys, true);
	if(doc != null){Global_RemoveDocument(doc)}
	sessionScope.remove('gridSavedSearches');
}

function Grid_JSON_Search_ShowQuickSearch(){
	if(Grid_JSON_Search_SavedList(0) == null){return false}
	return true
}