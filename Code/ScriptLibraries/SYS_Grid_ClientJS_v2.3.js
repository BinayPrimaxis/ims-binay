var gbl_strKey = null; var appGrid = null; var gbl_FTKey = null;
var gbl_dateRangeStart = null; var gbl_dateRangeEnd = null; 
var gbl_NoRecords = "<div style='padding:6px;color: #a94442; background-color:#f2dede;font-weight:bold;' class='div_grid_norecords'>No records found</div>";
var gridObject = {}; var gbl_ViewCategory = null; var gbl_bRemoveSorting = null;

function Grid_Generate_DateRange(){
	
	var tmp_gbl_strKey = gbl_strKey
	if (gridObject[gbl_strKey].gridKey) //in case of send to agency want to use the same grid key, but different grid model
		{
		tmp_gbl_strKey = gridObject[gbl_strKey].gridKey
		}

	var strURL = './xJson_Data.xsp?Frm=' + gridObject[gbl_strKey].Form + '&VK=' + tmp_gbl_strKey;
	$('.gridViewName').val(tmp_gbl_strKey);
	
	if(gbl_dateRangeStart != null && gbl_dateRangeEnd != null){
		strURL += '&dtRange=' + gbl_dateRangeStart + '_' + gbl_dateRangeEnd;
	}
	
	if(gbl_FTKey != null){strURL += '&ftkey=' + gbl_FTKey}
	
	//This is used by Searching Patient URN
	if(gbl_ViewCategory != null){
		strURL += "&cat=" + gbl_ViewCategory;
		gbl_ViewCategory = null
	} else {
		var viewCategory = gridObject[gbl_strKey].ViewCategory;
		if(typeof(viewCategory) != "undefined"){strURL += "&cat=" + viewCategory}
	}
	
	//cLog("GRID URL - " + strURL);
	//if($('.gridALL_Heading').html() == ''){$('.gridALL_Heading').html(gridObject[gbl_strKey].Heading)}
	$('.gridALL_Heading').html(gridObject[gbl_strKey].Heading);
	
	var linkCurrSC = 'link_' + gbl_strKey;
	$('.gridViewMenu').removeClass('disabled');
	$('.' + linkCurrSC).addClass('disabled');
	JQ_HideSC($(".btnGridActions"), !gridObject[gbl_strKey].btnChooseAgency);
	JQ_HideSC($(".btnSearchAll"), !gridObject[gbl_strKey].btnChooseAgency);
	JQ_HideSC($(".btnSendAgency"), gridObject[gbl_strKey].btnSendAgency);
	JQ_HideSC($(".btnChooseAgency"), gridObject[gbl_strKey].btnChooseAgency);
		
	if(appGrid != null){
		//cLog('Grid Reloading ....')
		appGrid.jqGrid('setGridParam', {url: strURL, datatype:'json', page:1});
		appGrid.trigger('reloadGrid');
        return
	}
	
	$.jgrid.styleUI.Bootstrap.base.headerTable = "table table-bordered table-condensed";
	$.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-condensed";
	$.jgrid.styleUI.Bootstrap.base.footerTable = "table table-bordered table-condensed";
	$.jgrid.styleUI.Bootstrap.base.pagerTable = "table table-condensed";
	
	appGrid = $("#listxGrid");
	var xPager = "#pagerxGrid";
	//cLog(gridObject[gbl_strKey].ColumnModel);	
	appGrid.jqGrid({
		url:strURL		
		,datatype: "json"
		,colModel: gridObject[gbl_strKey].ColumnModel
		,height: gridObject[gbl_strKey].Height
		,rowNum: gridObject[gbl_strKey].RowCount
		,multiselect: gridObject[gbl_strKey].MultiSelect
		,jsonReader: {
			repeatitems: false,
			cell: "", 
			id: "@unid",
			root: function (obj) { return obj.rows; },
			records: function (obj) { return obj.rows.length; },
			beforeRequest: function() {if (pages.length === 0) {appGrid[0].p.page = 0}},
			page: function () { return 1; },			
			total: function () { return 1; }
		}
		,emptyrecords: "No records found"
		,ignoreCase: true
		,styleUI: "Bootstrap"
		,responsive: true
		,loadonce: true
		,navOptions: { reloadGridOptions: { fromServer: true } }
		,viewrecords: true
		,sortable: true	
        ,autowidth: true
		,shrinkToFit : true       
		,rowList : [20,50,100,500,1000,100000]
		,rownumWidth : 35
		,gridComplete: function(){			
			var $this = $(this);			
			//Alerts Function			
			if(('.Alert_Refresh_Interval_Seconds').length == 0){return}
			var strSec = $('.Alert_Refresh_Interval_Seconds').html();
			if(strSec == "NA"){return}
			if($this.jqGrid("getGridParam", "datatype") === "json"){Alerts_Update()}
        }		
        ,rownumbers: true
        ,multiSort: true
		,colMenu : true		
        ,pager: xPager
        ,groupingView : {		
			groupDataSorted : true, groupCollapse : true, groupText : ['<b>{0}</b> ({1})']	
		}
        ,ondblClickRow: function(ids) {        	
        	if ($("tr#"+ids).attr("editable") == "1") {return false}
			var rowObject = appGrid.getRowData(ids);
			if (rowObject['form']!=null){
	
				IFrame_OpenDoc(rowObject['@unid'], rowObject['form']);
			}
			else
				{
				IFrame_OpenDoc(rowObject['@unid'], gridObject[gbl_strKey].Form);
				}
			
							
		}
		,rowattr: function (rowId) {
			var strClass = rowId['@unid'];
			if(rowId['bookStatus'] != undefined){strClass += (' status_' + rowId['bookStatus'].toLowerCase())}
			return { "class": strClass};
        }
		,beforeSelectRow: function (ids) {
        	if ($("tr#"+ids).attr("editable") == "1") {return false}
        	return true;
        }
		,loadComplete: function() {
			
			if ($(this).getGridParam('records') === 0) {
				if($('.div_grid_norecords').length == 0){$('#listxGrid').after(gbl_NoRecords)}
				return
			}
			
			$('.div_grid_norecords').remove();
			$(xPager + " option[value=100000]").text('All');
							
			//This process is to retain filter, sorts, page and grouping after reload of data from server				
			
			//var post_page = postdata.page;			
			var postdata = appGrid.jqGrid('getGridParam','postData');	
			var post_filt = postdata.filters;	
			if (gridObject[gbl_strKey].Agency)
			{
				filters =  {groupOp:"OR",rules:[]};
				filters.rules.push({field:"bookInterpreter",op:"cn","data":gridObject[gbl_strKey].Agency})
				post_filt = XSP.toJson(filters);
				console.log(post_filt)
			}
			var post_sord = postdata.sord;
			var post_sidx = postdata.sidx;
			var post_gcol = appGrid.jqGrid("getGridParam", "groupingView").groupField;
			
			//cLog('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
			//cLog('post_filt -> ' + post_filt); cLog('post_sord -> ' + post_sord); cLog('post_sidx -> ' + post_sidx); cLog('post_gcol -> ' + post_gcol);
			
			//No need to continue if nothing is available to reload in local data
			if((post_filt === undefined || post_filt == '{"groupOp":"AND","rules":[]}') && post_sidx == '' && post_gcol == ''){return}
			
			if (appGrid.jqGrid("getGridParam", "datatype") === "json") {
				
				//cLog('GRID Data is JSON.. needs reloading');
				Notify_Loading(true);
				
				setTimeout(function () {
					
					if (appGrid.jqGrid("getGridParam", "datatype") === "local"){
						
						//cLog('post_sord-> ' + post_sord); cLog('post_sidx-> ' + post_sidx);
						
						appGrid.jqGrid("setGridParam", {
							postData: {filters: post_filt, sord: post_sord, sidx: post_sidx},
							search: true
						});
						
						//appGrid.trigger("reloadGrid", [{page: post_page}]);
						appGrid.trigger("reloadGrid");
						
						if(post_gcol.length > 0){
							appGrid.jqGrid('groupingRemove', true);
							appGrid.jqGrid('groupingGroupBy', post_gcol);
						}
						
						Notify_Loading(false);
					}
				}, 50);
			}
			
			//To load all the results
			//$(xPager + " select").val("100000");
			//appGrid.jqGrid('setGridParam' , {rowNum: appGrid.jqGrid('getGridParam' , 'records')});			
		} 
	});
	
	appGrid.filterToolbar({stringResult: false, defaultSearch: 'cn', searchOnEnter: false});		
	appGrid.jqGrid('navGrid', xPager, 
			{search:false, cloneToTop:false, del:false, edit:false, refresh: false, add:false}
			//,{ multipleSearch: true, multipleGroup:true} // search options - define multiple search
	);
	
	//Search Button
	appGrid.jqGrid('navButtonAdd', xPager, {
	    caption: "", title: "Search Templates", buttonicon: 'glyphicon-search', onClickButton: function() {Grid_JSON_Search()}
	});
	
	//Refresh Option
	appGrid.jqGrid('navButtonAdd', xPager, {
        caption: "", buttonicon: "glyphicon-refresh", title: "Reload Data", onClickButton: function () {Grid_ReloadDataFromServer();}
    });
	
	//Refresh Option & Clear
	appGrid.jqGrid('navButtonAdd', xPager, {
        caption: "", buttonicon: "glyphicon-repeat", title: "Clear Search Filter/Grouping/Sorting & Reload Data",
        onClickButton: function () {Grid_Unload_Load()}
    });
	
	//Save Column Order
	appGrid.jqGrid('navButtonAdd', xPager, {
	    caption: "", title: "Save Grid Column Order", buttonicon: 'glyphicon-floppy-disk', onClickButton: function() {Grid_SaveColumnOrder_CJS(appGrid, "")}
	});
		
	//Reset Column Order
	appGrid.jqGrid('navButtonAdd', xPager, {
	    caption: "", title: "Reset to Default Column Order", buttonicon: 'glyphicon-flash', onClickButton: function() {$('.btnResetColumnOrder').click()}
	});
	
	//Excel Button
	appGrid.jqGrid('navButtonAdd', xPager,{
		caption:"", title: "Export Current Page to Spreadsheet", buttonicon: "glyphicon-download-alt", onClickButton: function(){Grid_Export_Excel(appGrid);}
	});
	
	//Print Grid
	appGrid.jqGrid('navButtonAdd', xPager, {
	    caption: "", title: "Print", buttonicon: 'glyphicon-print', onClickButton: function() {Grid_Print('listxGrid')}
	});
	
	//New Booking
	appGrid.jqGrid('navButtonAdd', xPager, {
	    caption: "", title: "Add New Booking", position: "last", buttonicon: 'glyphicon-plus', onClickButton: function() {BOOK_New_URNSearch();}
	});
	
	$(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
	$(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
	$(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
	$(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
	$(".ui-jqgrid-pager").removeClass("ui-state-default");
	$(".ui-jqgrid").removeClass("ui-widget-content");	
}

function Grid_Unload_Load(){
	$.jgrid.gridUnload("listxGrid");
	appGrid = null;
	Grid_Generate_DateRange();
}

/*function Grid_Clear_SearchFilterGroupSort(){	
	cLog('Grid_Clear_SearchFilterGroupSort -- Function')	
	if(appGrid == null){cLog('Error, Grid is null');return}
	//appGrid[0].clearToolbar();	
	cLog('Before ->' + appGrid.jqGrid('getGridParam','postData').sidx)	
	appGrid.jqGrid("setGridParam", {
		postData: {filters: undefined, sord: "asc", sidx: ""},
		search: true
	});	
	cLog('After ->' + appGrid.jqGrid('getGridParam','postData').sidx)	
	appGrid.jqGrid("getGridParam", "groupingView").groupField = [];
	appGrid.jqGrid('groupingRemove', true);	
}*/

function Grid_ReloadDataFromServer(){
	if(appGrid == null){cLog('Error, Grid is null');return}
	appGrid.jqGrid('setGridParam', {datatype:'json'});
	appGrid.trigger('reloadGrid', [{page:1}]);
}

function Grid_DateRange(){
	
	var dtStart = moment(gbl_dateRangeStart,'YYYYMMDD');
	var dtEnd = moment(gbl_dateRangeEnd,'YYYYMMDD');
		
    $('#reportrange').daterangepicker({
        startDate: dtStart,
        endDate: dtEnd,        
        ranges: {
           'Today': [moment(), moment()],
           'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
           'Next 7 Days': [moment(), moment().add(6, 'days')],
           //'Next 30 Days': [moment(), moment().add(29, 'days')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Previous Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')]
        },
        locale: {format: "YYYY-MM-DD", cancelLabel: 'Clear'}
    }, Grid_DateRange_CallBack);

    $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
    	$('#reportrange span').html('');
        gbl_dateRangeStart = null; gbl_dateRangeEnd = null; Grid_Generate_DateRange();
    });
    
    Grid_DateRange_CallBack(dtStart, dtEnd);
}

function Grid_DateRange_CallBack(dtStart, dtEnd, bSetControl) {
	
	//cLog('Grid_DateRange_CallBack called with ' + dtStart + '  ' + dtEnd)
	
	if(dtStart == null && dtEnd == null){
		$('#reportrange span').html(''); 
		gbl_dateRangeStart = null; 
		gbl_dateRangeEnd = null;				
	} else {		
		$('#reportrange span').html(dtStart.format('DD MMM YYYY') + '  -  ' + dtEnd.format('DD MMM YYYY'));
	    gbl_dateRangeStart = dtStart.format('YYYYMMDD'); 
	    gbl_dateRangeEnd = dtEnd.format('YYYYMMDD');	    
	}
	console.log(gbl_strKey)
	//Change the Grid Object Dates
	gridObject[gbl_strKey].DateRangeStart = gbl_dateRangeStart; gridObject[gbl_strKey].DateRangeEnd = gbl_dateRangeEnd;
	
	if(bSetControl){
		$('#reportrange').data('daterangepicker').setStartDate(dtStart);
		$('#reportrange').data('daterangepicker').setEndDate(dtEnd);
	}
	
	//Disable Enable btnDate_Today Button
	if($('.btnDate_Today').length > 0){
		var bDisable = false;
		if(dtStart != null && dtEnd != null){
			var strToday = moment().format('YYYYMMDD');
			bDisable = (dtStart.format('YYYYMMDD') == strToday && dtEnd.format('YYYYMMDD') == strToday)
		}
		$('.btnDate_Today').prop('disabled', bDisable);
	}	
	
	Grid_Generate_DateRange();   
}

function Grid_Export_Excel(obj,filename){
	
	/* For Grid Agency Pending, we use different column model to export and only the selected records  */
	//if(gbl_strKey == 'GRID_CRD_Agency_Pending'){Grid_Export_GRID_CRD_Agency_Pending(obj); return}
if (gridObject[gbl_strKey]){
	if(gridObject[gbl_strKey].ExportColumnModel){Grid_Export_GRID_CRD_Agency_Pending(obj); return}
}
	//If header checkbox is selected, then export all recrods
	if($('#cb_listxGrid').prop('checked')){
		var f = $(".ui-search-input>input")
		console.log(f)
		var bAll = true
		for (var i=0;i<f.length;i++)
			{
			if (f[i].value!="")
				{
				bAll = false
				}
			}

		if (bAll){Grid_Export_Excel_ALL(obj);return}
		}
	
	//Check if any rows are selected
	var rowID = obj.jqGrid('getGridParam','selarrrow');
	var vCols = []; var vRows = []; var vRow = [];
	
	// Get All IDs
	var mya = obj.getDataIDs();				//All displayed records
	var data = obj.getRowData(mya[0]);     //Get First row to get the labels
	var colNames = new Array();  
	var ii=0; var strTemp = "";
	
	for (var i in data){
		strTemp = GetColumn(obj, i)
		console.log(strTemp)
		if(strTemp != "~@~HIDDEN~@~"){
			colNames[ii++] = i;		// i is variable name of column			
			vCols.push(prepareValueForExcel(strTemp));
		}				
	}
	
	//Change to selected if any selected
	if(rowID.length > 0){mya = rowID}
	
	for(i=0;i<mya.length;i++){
		data = obj.getRowData(mya[i]); // get each row
		vRow = [];
		for(j=0;j<colNames.length;j++){		
			vRow.push(prepareValueForExcel(data[colNames[j]]));
		}
		vRows.push(vRow);	
	}
		var f = filename||$('.gbl_UserDocKey').html();
	Generate_ExcelFile(f + '_' + moment().format('YYMMDDHHmm'), vCols, vRows);		
}

function Grid_Export_GRID_CRD_Agency_Pending(obj){
	
	console.log("special export for agency")
	var rowID = obj.jqGrid('getGridParam','selarrrow');
	var vCols = []; var vRows = []; var vRow = [];
	
	if(rowID.length == 0){
		Notify_Warning("Please select one or more Booking Documents to export.");
		return false
	}
	
	console.log(gbl_strKey);
	console.log(gridObject);
	
	var strCol; var prop; var exportColModel = gridObject[gbl_strKey].ExportColumnModel;
			
	for(var i=0; i<exportColModel.length; i++){	
		console.log('export col model' + exportColModel[i]);
		console.log('label = ' + exportColModel[i].label);
		if(exportColModel[i].label != null){
			console.log('in if');
			vCols.push(exportColModel[i].label);
		} else {	
			console.log('in else');
			vCols.push(obj.jqGrid('getColProp', exportColModel[i].col).label);
		}			
	}
	
	cLog(vCols)
	var data; var strValue;
	for(i=0;i<rowID.length;i++){
		data = obj.getRowData(rowID[i]); // get each row
		vRow = [];
		for(var j=0; j<exportColModel.length; j++){				
			if(exportColModel[j].value != null){
				strValue = exportColModel[j].value;
			} else if (exportColModel[j].colMultiple != null){
				var cols = exportColModel[j].col; var vVal = [];					
				for(var k=0; k<cols.length; k++){
					vVal.push(prepareValueForExcel(data[cols[k]]))
				}
				strValue = vVal.join(exportColModel[j].separater);
			} else {				
				strValue = prepareValueForExcel(data[exportColModel[j].col]);
			}
			vRow.push(strValue);
			strValue = "";
		}			
		vRows.push(vRow);	
	}
	
	var strFileName = 'AgencyFile_' + $('.gbl_UserDocKey').html() + '_' + moment().format('YYYYMMDDHHmm');
	Generate_ExcelFile(strFileName, vCols, vRows);
}

function Grid_Export_Excel_ALL(obj,filename){
	
	var allJQGridData = obj.jqGrid('getGridParam', 'data');
	var vCols = []; var vRows = []; var vRow = []; var vColLabels = []; var vColNames = [];
	
	var colModel = obj.jqGrid ('getGridParam', 'colModel');
	for(var i=0; i<colModel.length; i++){
		if(colModel[i].name != "rn" && colModel[i].name != "cb" && colModel[i].hidedlg != true){
			vColLabels.push(prepareValueForExcel(colModel[i].label));
			vColNames.push(colModel[i].name)
		}	
	}
	
	for(var i=0; i<allJQGridData.length; i++){
		vRow = [];
		for(var j=0; j<vColNames.length; j++){
			vRow.push(prepareValueForExcel(allJQGridData[i][vColNames[j]]));
		}
		vRows.push(vRow);
	}
	var f = filename||$('.gbl_UserDocKey').html();
	Generate_ExcelFile( f + '_' + moment().format('YYMMDDHHmm'), vColLabels, vRows);
	
}

function prepareValueForExcel(val) {
	val = '' + val;
	if(val == '--'){return ''}
	if(val.charAt(0) == "-"){val = val.replace("-", "")}
	//remove special format for CBN cells
	val = val.replace('</li><li class="gridli">',',');
	val = val.replace('<li class="gridli">','')
	val = val.replace('</li>','')
	// need to deal with this...
	//<div style="font-weight:700">RONE-B5W628-1</div><a title="Combined Booking Link" onclick="IFrame_OpenDoc("279CC778385D718DCA258333001ABB10", "CBN")">CBN-JPOT-B5X7QH</a>
	val = val.replace('<div style="font-weight:700">','');
	var sp = val.split("</div>")
	if (sp.length>0)
		{
		val = sp[0]
		}
	
	return val;
}

function GetColumn(obj, strColName){
	var prop = obj.jqGrid('getColProp', strColName)
	if(!prop.hidden){return prop.label} else {return "~@~HIDDEN~@~"}	
}

function Grid_SaveColumnOrder_CJS(obj, strClassName){
	cLog("Grid_SaveStyle is clicked - strClassName " + strClassName)
	var mya = obj.getDataIDs();  				// Get All IDs
	var data = obj.getRowData(mya[0]);     		// Get First row to
	
	// get the labels
	var strTemp; var prop;
	var colList = [];
	for (var i in data){
		prop = obj.jqGrid('getColProp', i)
		if(!prop.hidden){colList.push(i + "~" + prop.width)}						
	}
	
	//cLog(colList)
	//cLog(strClassName)
	
	if(colList.length == 0){Notify_Warning("Error: Cannot find column data. Please contact IT Support"); return true}
	
	if(strClassName != ''){strClassName = '_' + strClassName}
	
	$('.saveColumnOrder' + strClassName).val(colList.join("|@|"));
	$('.btnSaveColumnOrder' + strClassName).click();
	
}

function Grid_GetSelected_Column(colName, fldClass, gridID){
	
	var appGrid = $("#" + gridID);
	var vColVal = [];
	var rowID = appGrid.jqGrid('getGridParam','selarrrow');
	if(rowID.length == 0){
		Notify_Warning("Please select one or more Booking Documents to update.");
		return false
	}
	for(var i=0; i<rowID.length; i++){
		vColVal.push(appGrid.jqGrid ('getCell', rowID[i], colName))
	}
	
	if(vColVal.join('~').length == 0){
		Notify_Warning("System error encounterd, Please contact IT Support.");
		return false
	}
	if(fldClass == null){return vColVal}
	$('.' + fldClass).val(vColVal.join('~'));
	//cLog('Grid_GetSelected_Column (' + colName + ', ' + fldClass + ') - returned - ' + $('.' + fldClass).val());
	return true
}

function Grid_Action_CancelBooking(){
	var gridID = "listxGrid";
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', gridID)){clearCancellationDialogFields();$('#dlg_CancelBooking').modal()}
}

function Grid_Action_UpdateStatus(strStatus){
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', "listxGrid") == false){return}
	var strFunction = "$('.scStatusChange').val('" + strStatus + "'); $('.btnStatusChange').click();"
	ConfirmAction("This action will update the status of selected booking records to '" + strStatus + "'", null, strFunction)
}

function Grid_Action_AssignAgency(){
	var gridID = "listxGrid";
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', gridID) == false){return}
	if(Grid_GetSelected_Column('bookLanguage', 'fldLanguage_Agency', gridID) == false){return}
	if(Grid_Action_IsAllLanguageSame('fldLanguage_Agency', true) == false){
		return
		//$('.fldLanguage_Agency').val("ALL"); //This is to allow to assign to Agency with all language
	}
	$('.btnUpdateAgencyList').click()	
}

function Grid_Action_AssignInterpreter(gridID, fldClass){
	if(Grid_GetSelected_Column('@unid', fldClass, gridID) == false){return}
	if(Grid_GetSelected_Column('bookLanguage', 'fldLanguage_Interpreter', gridID) == false){return}
	//In GRID_INT_Unallocated if multiple language is selected, then SJS code will check for Interpreter with all selected language
	if(gridID.indexOf("GRID_INT_Unallocated")<0){
		//if(Grid_Action_IsAllLanguageSame('fldLanguage_Interpreter', false) == false){return}
	}	
	$('.btnUpdateInterpreterList').click();	
}

function Grid_Action_SendToAgency(){
	var gridID = "listxGrid";	
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', gridID) == false){return}
	//Want to make sure we only have one agency selected.
	var vInterpVal = Grid_GetSelected_Column('bookInterpreter',null, gridID);
	if (Grid_Action_IsAllAgencyTheSame(vInterpVal) == false) {return}
	ConfirmAction('This action will mark Booking as Sent to Agency', 'btnGridSendToAgency_SJS');
}

function Grid_Action_SendToAgency_OnComplete(gridID){
	
	cLog('Grid_Action_SendToAgency_OnComplete is called now');
	
	var appGrid = $("#" + gridID);
	
	Grid_Export_Excel(appGrid);
	
	Grid_Generate_DateRange();
	Notify_Info('SUCCESS: Bookings Updated as Sent to Agency'); 
	return;		        
}

function Grid_Action_IsAllAgencyTheSame (vInterpVal)
{
	var strAgency = ''; var strVal = '';
	for(var i=0; i<vInterpVal.length; i++){		
		strVal = vInterpVal[i];		
		if(strAgency == ''){
			strAgency = strVal
		} else {
			if(strAgency != strVal){
					Notify_Warning("Please select Bookings with the same Agency to update."); return false	
			}
		}				
	}
	return true
}

function Grid_Action_IsAllLanguageSame(fldClass, bAgency){
	var fld = $('.' + fldClass);
	var vLang = fld.val().split('~');
	var strLang = ''; var strVal = '';
	for(var i=0; i<vLang.length; i++){		
		strVal = vLang[i];		
		if(strLang == ''){
			strLang = strVal
		} else {
			if(strLang != strVal){
				if(!bAgency){
					Notify_Warning("Please select Bookings with the same language to update."); return false
				} else {
					//This is to allow to assign to Agency with all language
					fld.val("ALL"); return true
				}	
			}
		}				
	}	
	fld.val(strLang);
	return true
}

function Grid_REQU_ChangeGrid(strKey){		
	Grid_KillAndReload(strKey);
	gbl_dateRangeStart = gridObject[gbl_strKey].DateRangeStart; 
	gbl_dateRangeEnd = gridObject[gbl_strKey].DateRangeEnd;
	Grid_DateRange_CallBack(moment(gbl_dateRangeStart,'YYYYMMDD'), moment(gbl_dateRangeEnd,'YYYYMMDD'), true);
}

function Grid_INT_ChangeGrid(strKey){
	//$('.scCurrentGridID').html('GRID_INT_All');
	if(strKey == 'Calendar'){
		//Show Calendar and return
		JQ_ShowHide('div_INT_Calendar', true);
		JQ_ShowHide('div_INT_GRID', false);
		return
	} else {
		JQ_ShowHide('div_INT_Calendar', false);
		JQ_ShowHide('div_INT_GRID', true);
	}
	Grid_KillAndReload(strKey);
	gbl_dateRangeStart = gridObject[gbl_strKey].DateRangeStart; 
	gbl_dateRangeEnd = gridObject[gbl_strKey].DateRangeEnd;
	Grid_DateRange_CallBack(moment(gbl_dateRangeStart,'YYYYMMDD'), moment(gbl_dateRangeEnd,'YYYYMMDD'), true);
}

function Grid_Generate(strKey){
	
	cLog("Grid_Generate is called for " + strKey + "~~~~~~~~~~~~~~~~~~")
	console.log(gridObject[strKey])
	var xGrid = $("#listx_" + strKey);
	var xPager = "#pagerx_" + strKey;	
	//appGrid = xGrid;
	if(gridObject[strKey].URL != null){
		cLog('Reloading Grid - ' + strKey + ', URL:' + gridObject[strKey].URL);
		xGrid.jqGrid('setGridParam', {url: gridObject[strKey].URL, datatype:'json', page:1});
		xGrid.trigger('reloadGrid');
        return
	}
	
	var strURL = './xJson_Data.xsp?Frm=' + gridObject[strKey].Form + '&VK=' + strKey;
	
	var viewCategory = gridObject[strKey].ViewCategory;
	if(typeof(viewCategory) != "undefined"){strURL += "&cat=" + viewCategory}
	cLog('Grid URL:' + strURL);
	
	var bInsideForm = gridObject[strKey].InsideForm;
	if(typeof(bInsideForm) == "undefined"){bInsideForm = false}
	cLog('Grid InsideForm:' + bInsideForm);
	
	var bNewDoc = gridObject[strKey].NewDocument;
	if(typeof(bNewDoc) == "undefined"){bNewDoc = true}
	
	var bMultiSelect = gridObject[strKey].MultiSelect;
	if(typeof(bMultiSelect) == "undefined"){bMultiSelect = false}
		
	//Storing strKey & URL for Grid Reload after update actions
	gridObject[strKey].URL = strURL;	
	$.jgrid.styleUI.Bootstrap.base.headerTable = "table table-bordered table-condensed";
	$.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-condensed";
	$.jgrid.styleUI.Bootstrap.base.footerTable = "table table-bordered table-condensed";
	$.jgrid.styleUI.Bootstrap.base.pagerTable = "table table-condensed";
	
	xGrid.jqGrid({
		url:strURL
		,datatype: "json"
		,colModel: gridObject[strKey].ColumnModel
		//,height: 'auto'
		,height: (bInsideForm ? 'auto' : gridObject[strKey].Height)	
		,maxheight: gridObject[strKey].Height
		,rowNum: gridObject[strKey].RowCount
		,multiselect: bMultiSelect
		,jsonReader: {
			repeatitems: false, 
			cell: "", 
			id: "@unid",
			root: function (obj) { return obj.rows; },
			records: function (obj) { return obj.rows.length; },
			beforeRequest: function() {if (pages.length === 0) {xGrid[0].p.page = 0}},
			page: function () { return 1; },			
			total: function () { return 1; }
		}
		,emptyrecords: "No records found"
		,ignoreCase: true
		,styleUI: "Bootstrap"
		,responsive: true
		,loadonce: true
		,navOptions: { reloadGridOptions: { fromServer: true } }
		,viewrecords: true
		,sortable: true	
        ,autowidth: true
		,shrinkToFit : true
        ,rowList : [20,50,100,500,100000]
		,rownumWidth : 35
		,loadComplete: function() {
			
			//Heading
			var aObj = $('.' + strKey + '_Heading');
			var strHeading = gridObject[strKey].Heading;			
			if(strHeading.indexOf('records')<0){aObj.html(strHeading + '  (' + $(this).getGridParam('records') + ' records)')}
			
			var txt_gbl_NoRecords = gbl_NoRecords;
			var newClass = 'div_grid_norecords_' + strKey;
			txt_gbl_NoRecords = txt_gbl_NoRecords.replace('div_grid_norecords', newClass);
						
			if ($(this).getGridParam('records') === 0) {
				if($('.' + newClass).length == 0){$('#listx_' + strKey).after(txt_gbl_NoRecords);}
			} else {            
				
				$('.' + newClass).remove();
				$(xPager + " option[value=100000]").text('All');
				
				var $this = $(this);
				var postfilt = $this.jqGrid('getGridParam', 'postData').filters;
	            var postsord = $this.jqGrid('getGridParam', 'postData').sord;
	            var postsort = $this.jqGrid('getGridParam', 'postData').sidx;
	            var postpage = $this.jqGrid('getGridParam', 'postData').page;
	            var postgcol = $this.jqGrid("getGridParam", "groupingView").groupField;
	            
				if ($this.jqGrid("getGridParam", "datatype") === "json") {
					setTimeout(function () {
						if ($this.jqGrid("getGridParam", "datatype") === "local"){
							$this.jqGrid("setGridParam", {
								postData: {filters: postfilt, sord: postsord, sidx: postsort},
								search: true
							});
							$this.trigger("reloadGrid", [{page: postpage}]);
							if(postgcol.length > 0){
								$this.jqGrid('groupingRemove', true);
								$this.jqGrid('groupingGroupBy',postgcol);
							}							
						}
					}, 25);
				}
			}
		} 
        ,rownumbers: true 
        ,multiSort: true
		,colMenu : (bInsideForm ? false : true)
        //,pager: xPager
		,pager: (bInsideForm ? null : xPager)
        ,groupingView : {		
			groupDataSorted : true, 
			groupCollapse : true, 
			groupText : ['<b>{0}</b> ({1})']	
		}
        ,ondblClickRow: function(ids) {
			var rowObject = xGrid.getRowData(ids);
			//Exception for RCN_Upload
			var sOpenForm = gridObject[strKey].Form
			if (rowObject['form']!=null){
				
				sOpenForm = rowObject['form']
			}
			
			if (gridObject[strKey].SubForm)
				{
				sOpenForm = gridObject[strKey].SubForm
				IFrame_OpenChildDoc(rowObject['@unid'],gridObject[strKey],rowObject['rcnBOOKUNID'])
				return
				}
			if (sOpenForm=="RCN_Upload")
				{
				if (rowObject['rcnStatus']=="Processed")
					{
					sOpenForm="RCN"
					}
				}
			
			
			
			IFrame_OpenDoc(rowObject['@unid'], sOpenForm,gridObject[strKey].forceEdit);
							
		}
        ,rowattr: function (rowId) {
			var strClass = rowId['@unid'];
			if(rowId['status'] != undefined){strClass += (' status_' + rowId['status'].toLowerCase())}
        	return { "class": strClass};
        }
	});
	if(bInsideForm == false){
		xGrid.filterToolbar({stringResult: false, defaultSearch: 'cn', searchOnEnter: false});
	}	
	xGrid.jqGrid('navGrid', xPager, {search:false, cloneToTop:false, del:false, edit:false, refresh: false, add:false});
	
	if(bInsideForm == false){
		//Refresh Option
		xGrid.jqGrid('navButtonAdd', xPager, {
	        caption: "", buttonicon: "glyphicon-refresh", title: "Reload Data",
	        onClickButton: function () {
	        	$(this).jqGrid('setGridParam', {datatype:'json'});
	            $(this).trigger('reloadGrid', [{page:1}]);
	        }
	    });
		
		//Save Column Order
		xGrid.jqGrid('navButtonAdd', xPager, {
		    caption: "", title: "Save Grid Column Order", buttonicon: 'glyphicon-floppy-disk',
		    onClickButton: function() {Grid_SaveColumnOrder_CJS(xGrid, strKey)}
		});
			
		//Reset Column Order
		xGrid.jqGrid('navButtonAdd', xPager, {
		    caption: "", title: "Reset to Default Column Order", 
		    buttonicon: 'glyphicon-flash',
		    onClickButton: function() {$('.btnResetColumnOrder_' + strKey).click()}
		});
		
		if(strKey == "GRID_INT_Unallocated"){
			//Reset Column Order
			xGrid.jqGrid('navButtonAdd', xPager, {
				caption: "", title: "Assign Interpreter", buttonicon: 'glyphicon-education',
				onClickButton: function() {Grid_Action_AssignInterpreter("listx_" + strKey, "selUNIDs_" + strKey)}
			});
		}
		
		//Excel Button
		xGrid.jqGrid('navButtonAdd',xPager,{
			caption:"", title: "Export Current Page to Spreadsheet", buttonicon: "glyphicon-download-alt",
			onClickButton: function(){
				Grid_Export_Excel(xGrid,gridObject[strKey].FileName)
			}
		});
		
		//Print Grid
		xGrid.jqGrid('navButtonAdd', xPager, {
		    caption: "", title: "Print", 
		    buttonicon: 'glyphicon-print',
		    onClickButton: function() {Grid_Print("listx_" + strKey)}
		});
		
		//New Document
		if(bNewDoc){
			var strH = gridObject[strKey].Heading.toLowerCase();
			//cLog(strH)
			var strTitle = gridObject[strKey].AddLabel || 'Add New Booking';
			if(strH.indexOf('user')>=0){strTitle = 'Add New User'}
			if(strH.indexOf('agency')>=0){strTitle = 'Add New Agency'}
			if(strH.indexOf('clinic')>=0){strTitle = 'Add New Clinic'}
			xGrid.jqGrid('navButtonAdd', xPager, {
			    caption: "", title: strTitle, position: "last",
			    buttonicon: 'glyphicon-plus',
			    onClickButton: function() {IFrame_OpenDoc('', gridObject[strKey].Form)}
			});
		}
	}	
		
	$(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
	$(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
	$(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
	$(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
	$(".ui-jqgrid-pager").removeClass("ui-state-default");
	$(".ui-jqgrid").removeClass("ui-widget-content");
}

function Grid_INT_CollapseGrid(strClass){
	
	var bCurrVisible = $('.' + strClass).is( ":visible" );
	
	//Toggle the Section
	$('.' + strClass).collapse('toggle');
	
	if(strClass == 'GRID_INT_Current'){
		$('.scCurrentGridID').html('GRID_INT_All');
	} else {
		$('.scCurrentGridID').html(strClass);
	}
	
	
	panelExpandCollapse($('.' + strClass + '_Heading').prop('id'));	
	var vList = ['GRID_INT_Current','GRID_INT_Incomplete','GRID_INT_Unallocated'];	
	for(var i=0; i<vList.length; i++){		
		if(vList[i] != strClass){
			if($('.' + vList[i]).hasClass('in')){
				$('.' + vList[i]).collapse('hide');
				$('.' + vList[i] + '_Heading').addClass('collapsed')
			}			
		}
	}
	
	if(strClass == 'GRID_INT_Current' & !bCurrVisible){
		$('.scInterpreterDDMB').removeClass('txt_Hide')
	} else {
		$('.scInterpreterDDMB').addClass('txt_Hide')
	}
}

function Grid_INT_CollapseOnLoad(strDefaultScreen){
	cLog('strDefaultScreen - ' + strDefaultScreen)
	
	JQ_ShowHide('div_INT_Calendar', strDefaultScreen == 'Calendar');
	JQ_ShowHide('div_INT_GRID', strDefaultScreen != 'Calendar');
	
	var vList = ['GRID_INT_Incomplete','GRID_INT_Unallocated'];
	for(var i=0; i<vList.length; i++){		
		$('.' + vList[i]).collapse('hide');
		$('.' + vList[i] + '_Heading').addClass('collapsed')		
	}
}

function Grid_CRD_Open(strKey){
	//cLog('strKey====' + strKey);
	Grid_KillAndReload(strKey);	//Kill and Reload New
	Grid_CRD_ViewLinks(strKey); //Fix Links and hide current
	var dtStart = gridObject[gbl_strKey].DateRangeStart; 
	var dtEnd = gridObject[gbl_strKey].DateRangeEnd;        
    if(dtStart != null){dtStart = moment(dtStart,'YYYYMMDD'); dtEnd = moment(dtEnd,'YYYYMMDD')}    
    Grid_DateRange_CallBack(dtStart, dtEnd, dtStart != null);
}

function Grid_KillAndReload(strKey){
	cLog('Grid Object Unloading .. Key: ' + strKey)
	//Need to kill grid if different structure
	$.jgrid.gridUnload("#listxGrid"); 
	gbl_strKey = strKey; gbl_FTKey = null; appGrid = null;
	
}

function Grid_CRD_ViewLinks(){	
	console.log(gridObject)
	$('.gridALL_Heading').html(gridObject[gbl_strKey].Heading);
	var linkCurrSC = 'link_' + gbl_strKey;
	if($('.' + linkCurrSC).length == 0){return}
	var vLinkList = $('.gridViewMenu');
	var strClass = 'disabled';
	for(var i=0; i<vLinkList.length; i++){
		var obj = $(vLinkList[i]); //modified
		if(obj.hasClass(linkCurrSC)){obj.addClass(strClass)} else {obj.removeClass(strClass)}
	}
	JQ_HideSC($(".btnGridActions"), (linkCurrSC != "link_GRID_CRD_Agency_Pending"));
	JQ_HideSC($(".btnSearchAll"), (linkCurrSC != "link_GRID_CRD_Agency_Pending"));
	JQ_HideSC($(".btnSendAgency"), gridObject[gbl_strKey].btnSendAgency);
	JQ_HideSC($(".btnChooseAgency"), gridObject[gbl_strKey].btnChooseAgency);

}

function Grid_Search_Common_Check(){
	
	var searchType = $('.searchType').val();
	var strSearch = strTrim($('.fldSearchValue').val());
	if(strSearch == '' || strSearch == null){
		Notify_Warning('Please enter text for search');
		return false
	}
	//Change Search Type to All if not a number
	if(isNaN(strSearch)){
		searchType = 'All'; 
		$('.searchType').val(searchType);
	}
	return true
}

function Grid_GetUNID_Object(elem,gridObject){
	if (gridObject==null){gridObject = appGrid;}
	var rowObject = gridObject.getRowData(Grid_GetRowID_Object(elem));
	return rowObject['@unid']
}

function Grid_GetRowID_Object(elem){
	return $(elem).closest('tr.jqgrow').attr('id');
}

function Grid_CRD_EditAll(){
	var grid = appGrid;
	var isInterpreterEditable = false;
	appGrid.setColProp('bookInterpreter', {
		editable: true,
		edittype: 'select',
		editoptions: {
			dataInit : function (elem) {
				//@TODO - Disable editing interpreter for booked, updated, submitted to agency status
				//console.log(elem);
				var strUNID = Grid_GetUNID_Object(elem);
				window.setTimeout(function () {						
					$.ajax({
						url: 'xGrid_SelectOptions.xsp?Id=' + strUNID,
						success: function(result){								
							var selVal = ''; //Existing Value
							$.each(result, function (i, result) {
								if(result.selected){selVal = result.value}
							    $(elem).append($('<option>', {value: result.value, text : result.text}));
							});
							if(selVal != ''){
								$(elem).val(selVal);	//Select Existing Value
							} else {
								//Add blank value and select that
								$(elem).prepend($('<option>', {value: "", text : " -select- "}));
								$(elem).val('');
							}
						},
						error: function(xhr,status,error){cLog("ERROR= " + error)}
					})
				}, 200);				
			},
			dataEvents: [{type: 'change', fn: function(e) {Grid_Edit_List(e.target, 'BOOK_Interpreter_DocKey')}}]
		}
	});
	
	appGrid.setColProp('bookIntComments', {
		editable:true, 
		edittype:'textarea',
		editoptions: {
			rows:"2",
			dataEvents: [{type: 'change', fn: function(e) {Grid_Edit_List(e.target, 'BOOK_CommentsInterpreter')}}]
		}	
	});
	
    var edit_IDs = grid.jqGrid('getDataIDs');
    var sel_IDs = grid.jqGrid('getGridParam','selarrrow');
    //Change to selected if any selected
	if(sel_IDs.length > 0){edit_IDs = sel_IDs}
    
    for (var i = 0; i < edit_IDs.length; i++) {
    	grid.jqGrid('editRow', edit_IDs[i], false);
    }
        
	var cbs = $("tr.jqgrow > td > input.cbox", grid[0]);
    cbs.attr("disabled", "disabled");
    $('#cb_listxGrid').attr("disabled", "disabled");
    
    $('.ui-jqgrid-pager').hide(); 
    $(".ui-search-toolbar").hide();
    JQ_ShowHide("gridEditAll", true);
    JQ_ShowHide("gridBar", false);
    $('.gridALL_Heading').html('Booking - Edit Mode')
}  

function Grid_RCN_EditAll(gridid){
	
	var grid =  $(gridid);
	
	console.log("before setColProp");
	console.log(grid.getColProp('rcnRemedialCost'));
	grid.setColProp('rcnRemedialCost', {
		editable:true, 
		edittype:'textarea',
		editoptions: {
			rows:"2",
			dataEvents: [{type: 'change', fn: function(e) {Grid_Edit_List(e.target, 'RemedialCost',grid)}}]
		}	
	});
	
	grid.setColProp('rcnRemComments', {
		editable:true, 
		edittype:'textarea',
		editoptions: {
			rows:"2",
			dataEvents: [{type: 'change', fn: function(e) {Grid_Edit_List(e.target, 'RemComments',grid)}}]
		}	
	});
    var edit_IDs = grid.jqGrid('getDataIDs');
    var sel_IDs = grid.jqGrid('getGridParam','selarrrow');
    //Change to selected if any selected
	if(sel_IDs.length > 0){edit_IDs = sel_IDs}
    
    for (var i = 0; i < edit_IDs.length; i++) {
    	grid.jqGrid('editRow', edit_IDs[i], false);
    }
        
	var cbs = $("tr.jqgrow > td > input.cbox", grid[0]);
    cbs.attr("disabled", "disabled");
    $('#listx_GRID_RCN_Detail').attr("disabled", "disabled");
    //"#listx_GRID_RCN_Detail"'
    $('.ui-jqgrid-pager').hide(); 
    $(".ui-search-toolbar").hide();
    JQ_ShowHide("gridEditAll", true);
    JQ_ShowHide("gridBar", false);
    $('.gridALL_Heading').html('Booking - Edit Mode')
} 

var gbl_JsonEditData = null;

function Grid_RCN_Recalc(gridid)
{
	Grid_RCN_AcceptAll(gridid,"&recalc=true")
	}

function Grid_RCN_AcceptAll(gridid,param)
{
	
	var pURL='xGrid_SaveData.xsp?DB=RCN_UPLOAD';
	if (param==null){param="&accept=true";}
		pURL+=param;
	var appGrid = $(gridid);
	var vColVal = [];
	var rowID = appGrid.jqGrid('getGridParam','selarrrow');
	if(rowID.length == 0){
		Notify_Warning("Please select one or more Booking Documents to update.");
		return false
	}
	for(var i=0; i<rowID.length; i++){
		vColVal.push({ge_rowID:rowID[i],ge_unid:appGrid.jqGrid ('getCell', rowID[i], '@unid'),ge_flds:[{ge_fldName:'Status',ge_fldVal:appGrid.jqGrid ('getCell', rowID[i], 'rcnStatus')}]})
	}
	
	$.ajax({
        type: 'POST',
        url: pURL,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(vColVal),
        success: function (response) {
        	
        	var jsonResponse = response;
        	//cLog("jsonResponse - " + jsonResponse)
        	var vMsg = []; var bError = false; var vEval = []; iSuccess = 0
        	for(var i=0 ; i<jsonResponse.length; i++){
        		
        		var rowID = jsonResponse[i].rowID;
        		
        		//Looks like some has error, restore those row
        		if(jsonResponse[i].error != null){
        			vMsg.push("Row # " + rowID + ', ' + jsonResponse[i].error);	//Push Error
        			appGrid.jqGrid('restoreRow', rowID); //Restore Row to Original, this makes row read mode
        			appGrid.jqGrid('editRow', rowID, false);	//Back to Edit Mode
        			$('#' + rowID).addClass('danger');	//Add Class Danger
        			bError = true;
        		} else {
        			//cLog(jsonResponse[i].data)
        			appGrid.jqGrid('setRowData', rowID, jsonResponse[i].data, 'success');  
        			iSuccess++;
        			
        		}
        	}
        
        	if(bError == false){
        		Notify_Info('All records updated successfully !!')
        	} else {
        		Dialog_Message("Following Records not Updated", vMsg.join('<br/>'));
        	}        	        	
        	console.log("Grid_Edit_SaveAll() - Success");
        	//Update chart counts
        	RCN_ProgressUpdate(iSuccess);
        },
        error: function (error) {
        	console.log(error)
            console.log("Grid_Edit_SaveAll() - ERROR - " + error.responseText.error);
        },
        complete: function(){
        	gbl_JsonEditData = null;
        }
        
    });	
	}

function Grid_Edit_List(elem, fieldName,gridObject){
	
	/* JSON Format Description. 'ge_', so that searching is easy
	{
		ge_unid : <<UNID>>, 
		ge_rowID: <<rowID>>,
		ge_flds : [{ge_fldName : <<FieldName>>, ge_fldVal : <<FieldValue>>}]    	
	}    				
    */
	//If null reset grid table classes
	if(gbl_JsonEditData == null){
		gbl_JsonEditData = []; 
		$('table tr.jqgrow').removeClass('danger'); 
		$('table tr.jqgrow').removeClass('success');
	}
	
	var strUNID = Grid_GetUNID_Object(elem,gridObject);
	//var strVal = XSP.toJson($(elem).val());			//if need to handle special characters
	//strVal = strVal.substring(1, strVal.length-1);	//remove first and last double quotes, added by above function
	var strVal = $(elem).val();	
	//cLog('strUNID:' + strUNID + ', - strVal:' + strVal + ', - fieldName:' + fieldName)
	
	var fldObject = {ge_fldName: fieldName, ge_fldVal: strVal}
	
	var bFieldPresent = false; var bUNIDPresent = false;
	for(var i=0; i<gbl_JsonEditData.length; i++){
		if(gbl_JsonEditData[i].ge_unid == strUNID){
			bUNIDPresent = true;
			for(var j=0; j<gbl_JsonEditData[i].ge_flds.length; j++){
				if(gbl_JsonEditData[i].ge_flds[j].ge_fldName == fieldName){
					gbl_JsonEditData[i].ge_flds[j].ge_fldVal = strVal;	//Just change value
					cLog("Field Found, Updated")
					bFieldPresent = true;
				}
			}
			//UNID is present, but new Field Entry
			if(bFieldPresent == false){gbl_JsonEditData[i].ge_flds.push(fldObject)}	
		}
	}
	if(bUNIDPresent == false){	
		gbl_JsonEditData.push({ge_unid: strUNID, ge_rowID: Grid_GetRowID_Object(elem), ge_flds: [fldObject]});
	}	
	
}

function Grid_Edit_SaveAll(sDB,gridid){
	var appGrid =$(gridid);
	if(gbl_JsonEditData == null){Notify_Warning("There is no change noticed to be Saved"); return}
	var pURL='xGrid_SaveData.xsp';
	if (sDB!=null){
		pURL+="?DB="+sDB;
	}
	$.ajax({
        type: 'POST',
        url: pURL,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: XSP.toJson(gbl_JsonEditData),
        success: function (response) {
        	
        	var jsonResponse = response;
        	//cLog("jsonResponse - " + jsonResponse)
        	var vMsg = []; var bError = false; var vEval = [];
        	for(var i=0 ; i<jsonResponse.length; i++){
        		
        		var rowID = jsonResponse[i].rowID;
        		
        		//Looks like some has error, restore those row
        		if(jsonResponse[i].error != null){
        			vMsg.push("Row # " + rowID + ', ' + jsonResponse[i].error);	//Push Error
        			appGrid.jqGrid('restoreRow', rowID); //Restore Row to Original, this makes row read mode
        			appGrid.jqGrid('editRow', rowID, false);	//Back to Edit Mode
        			$('#' + rowID).addClass('danger');	//Add Class Danger
        			bError = true;
        		} else {
        			//cLog(jsonResponse[i].data)
        			appGrid.jqGrid('setRowData', rowID, jsonResponse[i].data, 'success');        			
        		}
        	}
        	
        	if(bError == false){
        		Notify_Info('All records updated successfully !!')
        	} else {
        		Dialog_Message("Following Records not Updated", vMsg.join('<br/>'));
        	}        	        	
        	console.log("Grid_Edit_SaveAll() - Success");
        },
        error: function (error) {
            console.log("Grid_Edit_SaveAll() - ERROR - " + error.toString());
        },
        complete: function(){
        	gbl_JsonEditData = null;
        }
        
    });	
}

function Grid_Edit_Exit(sgrid){	
	if (sgrid==null){sgrid="#cb_listxGrid"}
	JQ_ShowHide("gridEditAll", false);
    JQ_ShowHide("gridBar", true);
    $('.ui-jqgrid-pager').show();
    $(".ui-search-toolbar").show();
    //appGrid[0].clearToolbar();
    $(sgrid).removeAttr("disabled");
    
   // Grid_CRD_Open('GRID_CRD_ActionRequired');
    if (sgrid=="#cb_listxGrid")
    	{
	    var dtStart = gridObject[gbl_strKey].DateRangeStart; 
		var dtEnd = gridObject[gbl_strKey].DateRangeEnd;
		if(dtStart != null){dtStart = moment(dtStart,'YYYYMMDD'); dtEnd = moment(dtEnd,'YYYYMMDD')}  
	    Grid_DateRange_CallBack(dtStart, dtEnd, dtStart != null);
    	}
    else
    	{
    	$(sgrid).trigger('reloadGrid');
    	}
}

function Grid_Search(strViewKey, strType){
	
	if(Grid_Search_Common_Check() == false){return}	
	var searchType = $('.searchType').val();
	var strSearch = strTrim($('.fldSearchValue').val());
	Grid_KillAndReload(strViewKey);
	
	if(searchType == 'URN'){
		
		gbl_ViewCategory = strSearch;
		
		//In case of INT having access to only their language booking, this
		// part restrictes only to search in their languages
		if(strViewKey == 'GRID_INT_SearchURN'){
			var langList = $('.langList').html().split('~');
			for(var i=0;i<langList.length; i++){
				langList[i] = langList[i] + "*" + strSearch;
			}			
			gbl_ViewCategory = langList.join('~');
		}
		
	} else {
		gbl_FTKey = "SearchAll~" + strSearch;
	}
	
	if(strType == 'REQU'){
		$('.GRID_REQU_Heading').html('Search Results');
	} else if(strType == 'INT'){
		$('.GRID_INT_Current_Heading').html('Search Results');
	}
	
	Grid_DateRange_CallBack(null, null);
}

function Grid_Search_Clear(strViewKey){
	
	$('.fldSearchValue').val('');
	JQ_ShowHide("gridBarSearch", false); 
	JQ_ShowHide("gridBar", true);
	
	if(strViewKey == 'GRID_CRD_ActionRequired'){
		Grid_CRD_Open(strViewKey);
		return
	}
	
	Grid_KillAndReload(strViewKey);
	var strCSS = '.' + strViewKey + '_Heading';
	if(strViewKey == 'GRID_INT_All'){strCSS = '.GRID_INT_Current_Heading'}
	
	if(strViewKey == 'GRID_REQU_ByDocKey'){
		JQ_HideSC($('.btnAllBookingRequestor'), true);
		JQ_HideSC($('.btnRequestor_ByLocation'), false);
	}
	
	$(strCSS).html(gridObject[gbl_strKey].Heading);
	
	gbl_dateRangeStart = gridObject[gbl_strKey].DateRangeStart; 
	gbl_dateRangeEnd = gridObject[gbl_strKey].DateRangeEnd;	
	Grid_DateRange_CallBack(moment(gbl_dateRangeStart,'YYYYMMDD'), moment(gbl_dateRangeEnd,'YYYYMMDD'), true);
	
}

function Grid_ResetColumnOrder(){	
	gridObject[$('.gridViewName').val()].ColumnModel = eval($('.resetColModel').html());
	$('.resetColModel').html('');
	$.jgrid.gridUnload("#listxGrid"); 
	appGrid = null;	
	Grid_Generate_DateRange();
}

function Grid_PreviousTodayNext(strAction){
	var dataDR = $('#reportrange').data('daterangepicker');
	var dtStart = dataDR.startDate;
	var dtToday = moment();
	if(strAction == 'P'){
		dtStart = dtStart.subtract(1, 'days');
	} else if(strAction == 'N') {
		dtStart = dtStart.add(1, 'days');
	} else if(strAction == 'T') {
		dtStart = dtToday;
	}
	Grid_DateRange_CallBack(dtStart, dtStart, true);	
}

function saveTextAsFile(fileNameToSaveAs, textToWrite) {	
	var ie = navigator.userAgent.match(/MSIE\s([\d.]+)/), 
	ie11 = navigator.userAgent.match(/Trident\/7.0/) && navigator.userAgent.match(/rv:11/),
	ieEDGE = navigator.userAgent.match(/Edge/g),
	ieVer=(ie ? ie[1] : (ie11 ? 11 : (ieEDGE ? 12 : -1)));
	if (ie && ieVer<10) {
		cLog("No blobs on IE ver<10");
		return;
	}
	var textFileAsBlob = new Blob([textToWrite], {type: 'text/plain'});
	if (ieVer>-1) {
		window.navigator.msSaveBlob(textFileAsBlob, fileNameToSaveAs);
	} else {
		var downloadLink = document.createElement("a");
		downloadLink.download = fileNameToSaveAs;
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = function(e) { document.body.removeChild(e.target); };
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
		downloadLink.click();
	}
}

function Alerts_Update() {
	//cLog('Alert Update is called')
	var divObj = $("#div_Alerts");
	var strHTML = '<h4><span class="label label-info">';
	strHTML += '<span class="glyphicon glyphicon-refresh spinning">';
	strHTML += '</span> Loading ...</span></h4>';
	divObj.html(strHTML);
	var strSec = $('.Alert_Refresh_Interval_Seconds').html();
	
	divObj.load( "xAlerts.xsp #loadthis", function( response, status, xhr ) {
		if( status == "error" ) {			
			divObj.html( "There was an error: " + xhr.status + " " + xhr.statusText );
		}
		if(parseInt(strSec) > 0){
			window.setTimeout(Alerts_Update, (parseInt(strSec)*1000));
		}	
	});	
}

function Alert_OnClick(Obj){	

	var title = Obj.title;
	var alertName = Obj.innerHTML.split('<span')[0];
	$('.searchType').val('All'); $('.fldSearchValue').val(alertName);
	Notify_Info(title);
	$('.btnSearchAll').click();
	
	Grid_Search('GRID_CRD_Search', 'CRD');
}

function Grid_GetSelected_URN(gridID){
	
	var strURN = ''; var appGrid = $("#" + gridID);
	if(appGrid.length == 1){
		var rowID = appGrid.jqGrid('getGridParam','selrow');
		//cLog('ROW ID - ' + rowID);
		//cLog(appGrid.jqGrid('getLocalRow', rowID));
		var rowData = appGrid.jqGrid('getLocalRow', rowID);
		if(rowData){
			if(rowData.bookPUrn){strURN = rowData.bookPUrn}
		}
		//if(rowID.length == 1){strURN = appGrid.jqGrid('getCell', rowID[0], 'urn');cLog('URN - ' + strURN)}
	}	
	return strURN	
}

function Grid_Print(tableCtrl) {
	var obj = $('#' + tableCtrl);
	var allJQGridData = obj.getDataIDs();
	
	if(allJQGridData.length == 0){
		Notify_Warning('No Records to Print');
		return
	}
	
		
	var colModel = obj.jqGrid ('getGridParam', 'colModel');
	var vColNames = [];
	var printData = '<table class="table table-bordered table-condensed"><thead><tr><th>#</th>';
		
	for(var i=0; i<colModel.length; i++){
		if(colModel[i].name != "rn" && colModel[i].name != "cb" && (colModel[i].hidden == false || colModel[i].hidden == null)){
			vColNames.push(colModel[i].name);
			printData += '<th class="sc_' + colModel[i].name + '">' + colModel[i].label + '</th>';
		}	
	}
	
	printData += '</thead></tr><tbody>';
	
	var cellValue; var rowData;
	
	for(var i=0; i<allJQGridData.length; i++){
		
		printData += '<tr><td>' + (i+1) + '.</td>' ;
		rowData = obj.getRowData(allJQGridData[i]);
		
		for(var j=0; j<vColNames.length; j++){			
			cellValue = rowData[vColNames[j]];			
			if (cellValue == null || cellValue == ''){
				cellValue += "--";
			} else {
				cellValue = cellValue.replace(/'/g, "&apos;");				
			}			
			printData += ('<td>' + cellValue + '</td>');			
		}		
		printData += '</tr>';
	}	
	printData += '</tbody></table>';
	//printData = obj.prop('outerHTML');	
	
	var hideContainer = $('.scGridPrintHide')
	if (hideContainer.length == 0){hideContainer = $('.scGridPrintHide',window.parent.document)}
	console.log(hideContainer)
	hideContainer.hide();
	var showContainer = $('#divPrint');
	if (showContainer.length==0){
		$('#divPrint',window.parent.document).show();	
		$("#divPrint_HTML",window.parent.document).html(printData);
	}
	else
		{
		$('#divPrint').show();	
		$("#divPrint_HTML" ).html(printData);
		}

	//window.print();	
}

function Grid_Print_Close(){
	$('.scGridPrintHide').show();
	$('#divPrint').hide();
    $("#divPrint_HTML" ).html('');
}

function Grid_JSON_Search(){
	
	cLog('Grid_JSON_Search - Called');
	if(appGrid == null){cLog('Error, Grid is null');return}
	
	var colModel = appGrid.jqGrid ('getGridParam', 'colModel'); var vFilters = []; 
	var bCheck_1; var bCheck_2; var bCheck_3;
	for(var i=0; i<colModel.length; i++){
		
		bCheck_1 = (colModel[i].name != "rn" && colModel[i].name != "cb");	//row number and checkbox columns
		bCheck_2 = (colModel[i].hidden == false || colModel[i].hidden == null); //Hidden Columns
		bCheck_3 = (colModel[i].qbHide == null || colModel[i].qbHide == false); 
		
		//Build Column Model
		if(bCheck_1 && bCheck_2 && bCheck_3){
			
			var colConfig =  {id: colModel[i].name, label: colModel[i].label};
			
			if(colModel[i].qbOperators){colConfig.operators = colModel[i].qbOperators}
			if(colModel[i].qbType){colConfig.type = colModel[i].qbType} else {colConfig.type = 'string'}
			if(colModel[i].qbValues){colConfig.values = colModel[i].qbValues}
			if(colModel[i].qbInput){colConfig.input = colModel[i].qbInput}
			if(colModel[i].qbValidation){colConfig.validation = colModel[i].qbValidation}
			if(colModel[i].qbPlugin){colConfig.plugin = colModel[i].qbPlugin}
			if(colModel[i].qbPlugin_config){colConfig.plugin_config = colModel[i].qbPlugin_config}
						
			vFilters.push(colConfig);
		}
	}
	
	// Fix for Bootstrap Datepicker
	$('#div_Grid_Search_Body').on('afterUpdateRuleValue.queryBuilder', function(e, rule) {
		if (rule.filter.plugin === 'datetimepicker') {
			var fldDate = rule.$el.find('.rule-value-container input');
			fldDate.on('dp.change', function() {
				fldDate.trigger('change');
			});
		}
	});
	
	$('#div_Grid_Search_Body').queryBuilder({filters: vFilters});
	
	//Reset Button
	$('#btn_Grid_Search_Reset').on('click', function() {$('#div_Grid_Search_Body').queryBuilder('reset');});
	
	//Action Search
	$('#btn_Grid_Search_Action').on('click', function() {		
	
		var result = $('#div_Grid_Search_Body').queryBuilder('getRules');
		if ($.isEmptyObject(result)){Notify_Warning("Error: No Search Rules selected !!!");return}
		
		var gridFilter = Grid_JSON_Search_GetGroup(result);		//cLog(gridFilter);		
		var gridFilter_JSON = JSON.stringify(gridFilter);		cLog(gridFilter_JSON);
		
		//Assign to Field for Saving
		$('.fld_grid_SearchQuery').val(gridFilter_JSON);
		$('.fld_qb_SearchQuery').val(JSON.stringify(result));
		
		if(gridFilter_JSON.indexOf('~ERROR~')>=0){
			Notify_Warning("Error: Issue encountered with operators, please contact IT Support !!!");return
		}
		
		JQ_ShowHide('div_qb_save', true);
		
		Grid_JSON_Search_LoadGrid(gridFilter_JSON);
		
				
	});
	
	var dialogModal = $('#dlg_GridSearch');
	dialogModal.modal({backdrop: 'static', keyboard: false, show:false});
	dialogModal.modal('show');
	
	dialogModal.on('hidden.bs.modal', function(e){
		//This means not a quick search
		if($('.fld_CB_QuickSearch').val() == ''){
			Grid_JSON_Search_DialogOnClose();
		}
	});	
}

function Grid_JSON_Search_DialogOnClose(){	
	$('#div_Grid_Search_Body').queryBuilder('destroy');
	Grid_JSON_Search_AddEditTemplates(false);	
	$('.fld_qb_SavedSearchList').val(''); $('.fld_qb_Search_Name').val('');
	$('.fld_qb_SearchQuery').val(''); $('.fld_grid_SearchQuery').val('');
}

function Grid_JSON_Search_LoadGrid(strJSONQuery, strSearchName){
	if(appGrid == null){cLog('Error, Grid is null');return}
	Grid_Unload_Load();
	$.extend(appGrid.jqGrid("getGridParam", "postData"), {filters: strJSONQuery});		
	appGrid.jqGrid("setGridParam", {search: true}).trigger('reloadGrid', [{current: true, page: 1}]);
	
	if(strSearchName){
		$('.gridALL_Heading').html(gridObject[gbl_strKey].Heading + ' - ' + strSearchName);
	}	
}

function Grid_JSON_Search_LoadRules(){	
	var intPos = $('.fld_qb_SavedSearchList').prop('selectedIndex');
	if(intPos == 0){
		$('#div_Grid_Search_Body').queryBuilder('reset');
	} else {
		var vList_SS = $('.txt_JSON_SavedSearches').html().split('~~~');
		var strQBQuery = vList_SS[intPos-1];
		$('#div_Grid_Search_Body').queryBuilder('setRules', JSON.parse(strQBQuery));
	}		
}

function Grid_JSON_Search_AddEditTemplates(bShow_AddEditArea){
	JQ_HideSC($('#btn_Grid_Search_Action'), bShow_AddEditArea);
	JQ_HideSC($('#btn_Grid_Search_Reset'), bShow_AddEditArea);
	JQ_ShowHide('div_Grid_SearchAddEdit', bShow_AddEditArea);	
	JQ_ShowHide('div_Grid_QuickSearch', !bShow_AddEditArea);
	JQ_HideSC($('.btn_Grid_QuickSearchHide'), !bShow_AddEditArea);
	if(bShow_AddEditArea){$('.fld_CB_QuickSearch').val('')}
}

function Grid_JSON_Search_QuickSearch(){
	var intPos = $('.fld_CB_QuickSearch').prop('selectedIndex');
	var strQuickSearch = $('.txt_JSON_QuickSearch').html().split('~~~')[intPos-1];
	Grid_JSON_Search_LoadGrid(strQuickSearch, $('.fld_CB_QuickSearch').val());
	$('#dlg_GridSearch').modal('hide');
}

function Grid_JSON_Search_Save_Validation(){	
	if(strTrim($('.fld_qb_SearchQuery').val()).length == 0 || strTrim($('.fld_grid_SearchQuery').val()).length == 0){
		Notify_Warning("Please Click on 'SEARCH' button before saving to make sure search criteria is working !!!");
		return;
	}
	if(strTrim($('.fld_qb_Search_Name').val()).length == 0){
		Notify_Warning("Please enter a name for search template !!");
		return;
	}
	$('.btn_qb_Search_Save_SJS').click();
}

function Grid_JSON_Search_Save_OnComplete(){	
	$('.fld_qb_SearchQuery').val(''); $('.fld_grid_SearchQuery').val(''); $('.fld_qb_Search_Name').val('');
	Notify_Info("Search Template successfully saved !!")
}

function Grid_JSON_Search_Delete(){
	$('.btn_qb_delete_procceed').click(); 
	JQ_ShowHide('div_qb_delete_confirm', false);
}

function Grid_JSON_Search_Delete_OnComplete(){
	$('#div_Grid_Search_Body').queryBuilder('reset');	
	Notify_Info("Search Template successfully deleted !!")
}

function Grid_JSON_Search_GetGroup(obj){
	
	var gridSearch_Group = {};
	gridSearch_Group.groupOp = obj.condition;
	
	var vRules = []; var ruleObj;
	
	for(var i=0; i<obj.rules.length; i++){
		var objRule = obj.rules[i];
		//this is a group
		if(objRule.condition){
			gridSearch_Group.groups = Grid_JSON_Search_GetGroup(objRule);	//Loop to Same function
			
		} else {
			ruleObj = {
				field : objRule.field,
				op : Grid_JSON_Search_GetOperator(objRule.operator),
				data : objRule.value
			};
			if(objRule.value == null){
				ruleObj.data = ""; 
				ruleObj.op = "eq";
			}
			vRules.push(ruleObj);
		}			
	}
	
	gridSearch_Group.rules = vRules;
	return gridSearch_Group;
	
}

function Grid_JSON_Search_GetOperator(strVal){	
	if(strVal == 'equal'){return 'eq'}; 			if(strVal == 'not_equal'){return 'ne'}; 		if(strVal == 'less'){return 'lt'};
	if(strVal == 'less_or_equal'){return 'le'}; 	if(strVal == 'greater'){return 'gt'}; 			if(strVal == 'greater_or_equal'){return 'ge'}
	if(strVal == 'begins_with'){return 'bw'}; 		if(strVal == 'not_begins_with'){return 'bn'}; 	if(strVal == 'in'){return 'in'}
	if(strVal == 'not_in'){return 'ni'};			if(strVal == 'ends_with'){return 'ew'}; 		if(strVal == 'not_ends_with'){return 'en'};
	if(strVal == 'contains'){return 'cn'}; 			if(strVal == 'not_contains'){return 'nc'}; 		if(strVal == 'is_null'){return 'nu'};
	if(strVal == 'is_not_null'){return 'nn'};		if(strVal == 'between'){return 'bt'};	
	return "~ERROR~"
}

function InterpreterColFormat(cellvalue, options, rowObject) {
	if(strTrim(cellvalue) == ''){return cellvalue}
	if($('.divInterpreterColors').length == 0){return cellvalue}
	var strIClass = 'i' + cellvalue.replace(/ /g,'');
	var divInterpreterColors = $('.divInterpreterColors').html();
	if(divInterpreterColors.indexOf(strIClass)<0){return cellvalue}
	return '<span class="iColorsGrid ' + strIClass + '">&#9632;</span>' + cellvalue;
	//return '<span class="iColorsGrid ' + strIClass + '">' + cellvalue + '</span>';
}

function Grid_Interpreter_EditHours_Show(vData){
	
	//cLog('IAMCALLED - ~~~~~~~~~~~~~~~~~'); cLog(vData); cLog(vData.length);
	/*
	 //Sample Value
	 (Optional will be exiting working hours start time and end time)
	 [{"iName" : "Bobby Vee""iDocKey" : "BV-1", "bDate" : "09 Feb 2018" }, 
	 {"unid" : "CA2580A300320648CA257FAA00063C23", "bTime" : "08:55", "bDuration" : "30"}, 
	 {"unid" : "CA2580A300320648CA257FAA000B6CAD", "bTime" : "09:00", "bDuration" : "45"}]
	*/
	var strInterpreterName = vData[0].iName; var strDate = vData[0].bDate;
	
	var vUNIDs = []; var dtBookStart_First; var dtBookEnd_Last;
	var dtBookStart_Compare; var dtBookEnd_Compare;
	//Check Time Range
	for(var i=1; i<vData.length; i++){
		vUNIDs.push(vData[i].unid);
		if(i == 1){
			dtBookStart_First = moment(vData[i].bTime, 'HH:mm', true);
			dtBookEnd_Last = moment(vData[i].bTime, 'HH:mm', true);
			dtBookEnd_Last.add(vData[i].bDuration, 'm');
			//cLog('dtBookStart_First ->' + dtBookStart_First.format('YYYYMMDD hhmm')); cLog('dtBookEnd_Last ->' + dtBookEnd_Last.format('YYYYMMDD hhmm'));
		} else {
		
			dtBookStart_Compare = moment(vData[i].bTime, 'HH:mm', true); 
			dtBookEnd_Compare = moment(vData[i].bTime, 'HH:mm', true);
			dtBookEnd_Compare.add(vData[i].bDuration, 'm');
			//cLog('dtBookStart_Compare ->' + dtBookStart_Compare.format('YYYYMMDD hhmm')); cLog('dtBookEnd_Compare ->' + dtBookEnd_Compare.format('YYYYMMDD hhmm'));
			
			//Compare and Change
			if(dtBookStart_Compare.isBefore(dtBookStart_First, 'minute')){
				dtBookStart_First = dtBookStart_Compare;
				dtBookStart_Compare = null;
			}
			
			if(dtBookEnd_Compare.isAfter(dtBookEnd_Last, 'minute')){
				dtBookEnd_Last = dtBookEnd_Compare;
				dtBookEnd_Compare = null;
			}
		}
	}
	
	//cLog('dtBookStart_First ->' + dtBookStart_First.format('YYYYMMDD hhmm')); cLog('dtBookEnd_Last ->' + dtBookEnd_Last.format('YYYYMMDD hhmm'));
	
	var strMsg = "Do you wish to modify working hours of <b>'" + strInterpreterName + "'</b> for the following day ?";
	strMsg += '<br/><br/><b><u>Note:</u></b> To cover all selected bookings choose between below time range: <br/>';
	strMsg += '<b><u>Start Time:</u></b> ' + dtBookStart_First.format('HH:mm') + "  <b><u>End Time:</u></b> " + dtBookEnd_Last.format('HH:mm');
	
	$('#dlg_ChangeInterpreterHours_Msg').html(strMsg);
	$('#dlg_ChangeInterpreterHours_Date').html(strDate);
	cLog(vUNIDs); cLog('~~~~~~~~~~~~~')
	$('.Grid_Interpreter_EditHours_UNIDs').html(vUNIDs.join('~'));
	$('.Grid_Interpreter_EditHours_Date').val(strDate);
	$('.Grid_Interpreter_EditHours_IDocKey').val(vData[0].iDocKey);
	
	if(vData[0].startTime_1){
		$('.fld_NewTime_Start_1').val(vData[0].startTime_1);
		$('.fld_NewTime_End_1').val(vData[0].endTime_1);
	}	
		
	//Split hours present check
	if(vData[0].startTime_2){
		$('.fld_NewTime_Start_2').val(vData[0].startTime_2);
		$('.fld_NewTime_End_2').val(vData[0].endTime_2);
		$('.fld_SplitHours').prop("checked", true);				
		JQ_ShowHide('divTime_2nd', true);
	} else {
		$('.fld_SplitHours').prop("checked", false);				
		JQ_ShowHide('divTime_2nd', false);
	}
			
	var dialogModal = $('#dlg_ChangeInterpreterHours');
	dialogModal.modal('show');
	dialogModal.on('hidden.bs.modal', function(){
		$('#dlg_ChangeInterpreterHours_Validation').html(''); 
		ClearAllFields('dlg_ChangeInterpreterHours')}
	);	
}

function Grid_Interpreter_EditHours_Validate(){
	if(ChangeInterpreterHours_Validate(true, 'DD MMM YYYY')){
		$('.btnGrid_Interpreter_EditHours_Update_SJS').click()
	}
}

function Grid_Interpreter_EditHours_OnComplete(){
	$('#dlg_ChangeInterpreterHours').modal('hide');
	$('.selUNIDs').val($('.Grid_Interpreter_EditHours_UNIDs').html());
	$('.Grid_Interpreter_EditHours_UNIDs').html('');	//Clear
	$('.Grid_Interpreter_EditHours_Date').val('');		//Clear
	$('.Grid_Interpreter_EditHours_IDocKey').val('');	//Clear
	$('.btnGridCoord_AssignInterpreter_SJS').click();	//Clear
}
