function IMSExcel(defaultFont) {

	var borderKind = [ "left", "right", "top", "bottom" ];
	var horAlign = [ "LEFT", "CENTER", "RIGHT", "NONE" ];
	var vertAlign = [ "TOP", "CENTER", "BOTTOM", "NONE" ];
	var align = { L : "left", C : "center", R : "right", T : "top", B : "bottom"};
	
	function componentToHex(c) {
	    var hex = c.toString(16);
	    return hex.length == 1 ? "0" + hex : hex;
	};
	
	this.rgbToHex = function (r, g, b) {
        if (r == undefined || g == undefined || b == undefined) return undefined;
        return (componentToHex(r) + componentToHex(g) + componentToHex(b)).toUpperCase();
    };
	
	this.toExcelUTCTime = function (date1){
		var d2=Math.floor(date1.getTime()/1000);													// Number of seconds since JS epoch
		d2=Math.floor(d2/86400)+25569;																// Days since epoch plus difference in days between Excel EPOCH and JS epoch
		
		var seconds = date1.getUTCSeconds()+60*date1.getUTCMinutes()+60*60*date1.getUTCHours();		// Number of seconds of received hour
		var SECS_DAY= 60 * 60 * 24;																	// Number of seconds of a day
		return d2+(seconds/SECS_DAY);																// Returns a local time !!
	};

	this.toExcelLocalTime = function (date1){
		var d2=Math.floor(date1.getTime()/1000);													// Number of seconds since JS epoch
		d2=Math.floor(d2/86400)+25569;																// Days since epoch plus difference in days between Excel EPOCH and JS epoch
		var seconds = date1.getUTCSeconds()+60*date1.getUTCMinutes()+60*60*date1.getUTCHours();		// Number of seconds of received hour
		seconds = seconds-60*(date1.getTimezoneOffset());											// Differences in seconds between UTC and LOCAL this depends on date becase daylight saving time
		var SECS_DAY= 60 * 60 * 24;																	// Number of seconds of a day
		return d2+(seconds/SECS_DAY);																// Returns a local time !!
	};

	var BuiltInFormats = [];
    BuiltInFormats[0] = 'General';
    BuiltInFormats[1] = '0';
    BuiltInFormats[2] = '0.00';
    BuiltInFormats[3] = '#,##0';
    BuiltInFormats[4] = '#,##0.00';

    BuiltInFormats[9] = '0%';
    BuiltInFormats[10] = '0.00%';
    BuiltInFormats[11] = '0.00E+00';
    BuiltInFormats[12] = '# ?/?';
    BuiltInFormats[13] = '# ??/??';
    BuiltInFormats[14] = 'mm-dd-yy';
    BuiltInFormats[15] = 'd-mmm-yy';
    BuiltInFormats[16] = 'd-mmm';
    BuiltInFormats[17] = 'mmm-yy';
    BuiltInFormats[18] = 'h:mm AM/PM';
    BuiltInFormats[19] = 'h:mm:ss AM/PM';
    BuiltInFormats[20] = 'h:mm';
    BuiltInFormats[21] = 'h:mm:ss';
    BuiltInFormats[22] = 'm/d/yy h:mm';

    BuiltInFormats[27] = '[$-404]e/m/d';
    BuiltInFormats[30] = 'm/d/yy';
    BuiltInFormats[36] = '[$-404]e/m/d';

    BuiltInFormats[37] = '#,##0 ;(#,##0)';
    BuiltInFormats[38] = '#,##0 ;[Red](#,##0)';
    BuiltInFormats[39] = '#,##0.00;(#,##0.00)';
    BuiltInFormats[40] = '#,##0.00;[Red](#,##0.00)';

    BuiltInFormats[44] = '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)';
    BuiltInFormats[45] = 'mm:ss';
    BuiltInFormats[46] = '[h]:mm:ss';
    BuiltInFormats[47] = 'mmss.0';
    BuiltInFormats[48] = '##0.0E+0';
    BuiltInFormats[49] = '@';

    BuiltInFormats[50] = '[$-404]e/m/d';
    BuiltInFormats[57] = '[$-404]e/m/d';
    BuiltInFormats[59] = 't0';
    BuiltInFormats[60] = 't0.00';
    BuiltInFormats[61] = 't#,##0';
    BuiltInFormats[62] = 't#,##0.00';
    BuiltInFormats[67] = 't0%';
    BuiltInFormats[68] = 't0.00%';
    BuiltInFormats[69] = 't# ?/?';
    BuiltInFormats[70] = 't# ??/??';
    BuiltInFormats[165] = '*********';              // Here we start with non hardcoded formats
    var baseFormats = 166;                              // Formats below this one are builtInt

    this.formats = BuiltInFormats;
    this.borderStyles = ["none", "thin", "medium", "dashed", "dotted", "thick", "double", "hair", "mediumDashed", "dashDot", "mediumDashDot", "dashDotDot", "mediumDashDotDot", "slantDashDot"];

    var borderStylesUpper = [];
    for (var i = 0; i < this.borderStyles.length; i++) borderStylesUpper.push(this.borderStyles[i].toUpperCase());
    
    var templateSheet = '<?xml version="1.0" ?><worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" ';
    templateSheet += 'xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:mv="urn:schemas-microsoft-com:mac:vml" ';
    templateSheet += 'xmlns:mx="http://schemas.microsoft.com/office/mac/excel/2008/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" ';
    templateSheet += 'xmlns:x14="http://schemas.microsoft.com/office/spreadsheetml/2009/9/main" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac" ';
    templateSheet += 'xmlns:xm="http://schemas.microsoft.com/office/excel/2006/main">';
    templateSheet += '{columns}<sheetData>{rows}</sheetData>{merges}</worksheet>';

    // --------------------- BEGIN of generic UTILS
    function getArray(v) {
        if (!v) return undefined;
        return (v.constructor === Array) ? v.slice() : undefined;
    };

    function findOrAdd(list, value) {
        var i = list.indexOf(value);
        if (i != -1) return i;
        list.push(value);
        return list.length - 1;
    };

    function pushV(list, value) {
        list.push(value);
        return value;
    };

    function pushI(list, value) {
        list.push(value);
        return list.length - 1;
    };

    function setV(list, index, value) {
        list[index] = value;
        return value;
    };

    // --------------------- END of generic UTILS
    
    // --------------------- BEGIN Handling of sheets 
    function toWorkBookSheet(sheet) {
        return '<sheet state="visible" name="' + sheet.name + '" sheetId="' + sheet.id + '" r:id="' + sheet.rId + '"/>';
    };

    function toWorkBookRel(sheet, i) {
        return '<Relationship Id="' + sheet.rId + '" Target="worksheets/sheet' + i + '.xml" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet"/>';
    };

    function getAsXml(sheet) {
        return templateSheet.replace('{columns}', generateColums(sheet.columns)).replace("{rows}", generateRows(sheet.rows)).replace("{merges}", generateMerges(sheet.merges));
    };

    // ------------------- BEGIN Sheet DATA Handling
    function setSheet(value, style, size) {
        this.name = value;                                                      // The only think that we can set in a sheet Is the name
    };

	function getRow(y) {
	    return (this.rows[y] ? this.rows[y] : setV(this.rows, y, { cells: [] }));                                                        // If there is a row return it, otherwise create it and return it
	};
	
	function getColumn(x) {
	    return (this.columns[x] ? this.columns[x] : setV(this.columns, x, {}));                                                          // If there is a column return it, otherwise create it and return it
	};
	
	function getCell(x, y) {
	    var row = this.getRow(y).cells;                                                                                                  // Get the row a,d its DATA component
	    return (row[x] ? row[x] : setV(row, x, {}));
	};
	
	function setCell(cell, value, style) {
	    if (value != undefined) cell.v = value;
	    if (style) cell.s = style;
	};
	
	function setColumn(column, value, style) {
	    if (value != undefined) column.wt = value;
	    if (style) column.style = style;
	};
	
	function setRow(row, value, style) {
	    if (value && !isNaN(value)) row.ht = value;
	    if (style) row.style = style;
	};
	
	// ------------------- END Sheet DATA Handling


    function createSheets() {
        var oSheets = {
            sheets: [],
            
            add: function (name) {
                var sheet = { id: this.sheets.length + 1, rId: "rId" + (3 + this.sheets.length), name: name, rows: [], columns: [], merges: [], getColumn: getColumn, set: setSheet, getRow: getRow, getCell: getCell };
                return pushI(this.sheets, sheet);
            },
            
            get: function (index) {
                var sheet = this.sheets[index];
                if (!sheet) throw "Bad sheet " + index;
                return sheet;
            },

            rows: function (i) {
                if (i < 0 || i >= this.sheets.length) throw "Bad sheet number must be [0.." + (this.sheets.length - 1) + "] and is: " + i;
                return this.sheets[i].rows;
            },
            
            setWidth: function (sheet, column, value, style) {
                // See 3.3.1.12 col (Column Width & Formatting
                if (value) this.sheets[sheet].colWidths[column] = isNaN(value) ? value.toString().toLowerCase() : value;
                if (style) this.sheets[sheet].colStyles[column] = style;
            },

            toWorkBook: function () {
                var s = '<?xml version="1.0" standalone="yes"?>' +
                    '<workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">' +
                    '<sheets>';
                for (var i = 0; i < this.sheets.length; i++) s = s + toWorkBookSheet(this.sheets[i]);
                return s + '</sheets><calcPr/></workbook>';
            },
            toWorkBookRels: function () {
                var s = '<?xml version="1.0" ?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">';
                s = s + '<Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>';                      // rId2 is hardcoded and reserved for STYLES
                for (var i = 0; i < this.sheets.length; i++) s = s + toWorkBookRel(this.sheets[i], i + 1);
                return s + '</Relationships>';
            },
            toRels: function () {
                var s = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">';
                s = s + '<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="xl/workbook.xml"/>';         // rId1 is reserverd for WorkBook
                return s + '</Relationships>';
            },
            toContentType: function () {
                var s = '<?xml version="1.0" standalone="yes" ?><Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types"><Default ContentType="application/xml" Extension="xml"/>';
                s = s + '<Default ContentType="application/vnd.openxmlformats-package.relationships+xml" Extension="rels"/>';
                s = s + '<Override ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml" PartName="/xl/workbook.xml"/>';
                s = s + '<Override ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml" PartName="/xl/styles.xml" />';
                for (var i = 1; i <= this.sheets.length; i++) s = s + '<Override ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml" PartName="/xl/worksheets/sheet' + i + '.xml"/>';
                return s + '</Types>';
            },
            fileData: function (xl) {
                for (var i = 0; i < this.sheets.length; i++) {
                    xl.file('worksheets/sheet' + (i + 1) + '.xml', getAsXml(this.sheets[i]));
                }
            }
        };
        return oSheets;
    };
    
    // --------------------- END Handling of sheets 

    // --------------------- BEGIN Handling of style

    function toFontXml(f) {
        var f = f.split(";");
        return '<font>' +
            (f[3].indexOf("B") > -1 ? '<b />' : '') +
            (f[3].indexOf("I") > -1 ? '<i />' : '') +
            (f[3].indexOf("U") > -1 ? '<u />' : '') +
            (f[1] != "_" ? '<sz val="' + f[1] + '" />' : '') +
            (f[2] != "_" ? '<color rgb="FF' + f[2] + '" />' : '') +
            (f[0] ? '<name val="' + f[0] + '" />' : '') +
            '</font>';   // <family val="2" /><scheme val="minor" />

    };

    function toFillXml(f) {
        return '<fill><patternFill patternType="solid"><fgColor rgb="FF' + f + '" /><bgColor indexed="64" /></patternFill ></fill>';
    };

    function toBorderXml(b) {
        var s = "<border>";
        b = b.split(",");
        for (var i = 0; i < 4; i++) {
            var vals = b[i].split(" ");
            s = s + "<" + borderKind[i];
            if (vals[0] == "NONE") s = s + "/>";
            else {
                var border = this.borderStyles[borderStylesUpper.indexOf(vals[0])];
                if (border)
                    s = s + ' style="' + border + '" >' + (vals[1] != "NONE" ? '<color rgb="FF' + vals[1].substring(1) + '"/>' : '');
                else
                    s = s + ">";
                s = s + "</" + borderKind[i] + ">";
            }
        }
        return s + "<diagonal/></border>";
    };

    function replaceAll(where, search, replacement) {
        return where.split(search).join(replacement);
    };

    function replaceAllMultiple(where, search, replacement) {
        while (where.indexOf(search) != -1) where = replaceAll(where, search, replacement);
        return where;
    };

    function toStyleXml(style) {
        var alignXml = "";
        if (style.align) {
            var h = align[style.align.charAt(0)];
            var v = align[style.align.charAt(1)];
            if (h || v) {
                alignXml = "<alignment ";
                if (h) alignXml = alignXml + ' horizontal="' + h + '" ';
                if (v) alignXml = alignXml + ' vertical="' + v + '" ';
                alignXml = alignXml + " />";
            }
        }
        var s = '<xf numFmtId="' + style.format + '" fontId="' + style.font + '" fillId="' + style.fill + '" borderId="' + style.border + '" xfId="0" ';
        if (style.border != 0) s = s + ' applyBorder="1" ';
        if (style.format >= baseFormats) s = s + ' applyNumberFormat="1" ';
        if (style.fill != 0) s = s + ' applyFill="1" ';
        if (alignXml != "") s = s + ' applyAlignment="1" ';
        s = s + '>';
        s = s + alignXml;
        return s + "</xf>";
    };

    function normalizeFont(fontDescription) {
    	
    	fontDescription = replaceAllMultiple(fontDescription, "  ", " ");
    	var fNormalized = ["_", "_", "_", "_"];                                 //  Name - Size - Color - Style (use NONE as placeholder) 
    	var i = 0, list = fontDescription.split(" ");                       //  Split by " "
        var name = [];
        while (list[0] && (list[0] != "none") && (isNaN(list[0])) && (list[0].charAt(0) != "#")) {
            name.push(list[0].charAt(0).toUpperCase() + list[0].substring(1).toLowerCase());
            list.splice(0, 1);
        }

        fNormalized[0] = name.join(" ");
        while (list[0] == "none") list.splice(0, 1);                        // Delete any "none" that we might have
        if (!isNaN(list[0])) {                                              // IF we have a number then this is the font size    
            fNormalized[1] = list[0];
            list.splice(0, 1);
        }
        while (list[0] == "none") list.splice(0, 1);                        // Delete any "none" that we might have
        if (list[0] && list[0].length == 7 && list[0].charAt(0) == "#") {      // IF we have a 6 digits value it must be the color
            fNormalized[2] = list[0].substring(1).toUpperCase();
            list.splice(0, 1);
        }
        while (list[0] == "none") list.splice(0, 1);                                    // Delete any "none" that we might have
        if (list[0] && list[0].length < 4) fNormalized[3] = list[0].toUpperCase();      // Finally get the STYLE
        return fNormalized.join(";");
    };

    function normalizeAlign(a) {
        if (!a) return "--";

        var a = replaceAllMultiple(a.toString(), "  ", " ").trim().toUpperCase().split(" ");
        if (a.length == 0) return "--";
        if (a.length == 1) a[1] = "-";
        return a[0].charAt(0) + a[1].charAt(0) + "--";
    };

    function normalizeBorders(b) {
        b = replaceAllMultiple(b, "  ", " ").trim();
        var l = (b + ",NONE,NONE,NONE,NONE").split(",");
        var p = "";
        for (var i = 0; i < 4; i++) {
            l[i] = l[i].trim().toUpperCase();
            l[i] = ((l[i].substring(0, 4) == "NONE" ? "NONE" : l[i]).trim() + " NONE NONE NONE").trim();
            var st = l[i].split(" ");
            if (st[0].charAt(0) == "#") {
                st[2] = st[0]; st[0] = st[1]; st[1] = st[2];
            }
            p = p + st[0] + " " + st[1] + ",";
        }
        return p;
    };

    function createStyleSheet(defaultFont) {
        var styles = [], fonts = [], formats = BuiltInFormats.slice(0), borders = [], fills = [];

        var oStyles = {
            add: function (a) {
                var style = {};
                if (a.fill && a.fill.charAt(0) == "#") style.fill = 2 + findOrAdd(fills, a.fill.toString().substring(1).toUpperCase());                  // If there is a fill color add it, with a gap of 2, because of the TWO DEFAULT HARDCODED fills
                if (a.font) style.font = findOrAdd(fonts, normalizeFont(a.font.toString().trim()));
                if (a.format) style.format = findOrAdd(formats, a.format);
                if (a.align) style.align = normalizeAlign(a.align);
                if (a.border) style.border = 1 + findOrAdd(borders, normalizeBorders(a.border.toString().trim()));                                          // There is a HARDCODED border         
                return 1 + pushI(styles, style);                                                            // Add the style and return INDEX+1 because of the DEFAULT HARDCODED style
            }
        };

        if (!defaultFont) defaultFont="Calibri 12 000000";
        oStyles.add({ font: defaultFont });


        oStyles.register = function (thisOne) {
            for (var i = 0; i < styles.length; i++) {
                if (styles[i].font == thisOne.font && styles[i].format == thisOne.format && styles[i].fill == thisOne.fill && styles[i].border == thisOne.border && styles[i].align == thisOne.align) return i;
            }
            return pushI(styles, thisOne);
        }

        oStyles.getStyle = function (a) {
            return styles[a];
        }
        oStyles.toStyleSheet = function () {
            var s = '<?xml version="1.0" encoding="utf-8"?><styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" ' +
                    'xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">';

            s = s + '<numFmts count="' + (formats.length - baseFormats) + '">';
            for (var i = baseFormats; i < formats.length; i++) s = s + '<numFmt numFmtId="' + (i) + '" formatCode="' + formats[i] + '"/>';
            s = s + '</numFmts>';


            s = s + '<fonts count="' + (fonts.length) + '" x14ac:knownFonts="1" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">';
            for (var i = 0; i < fonts.length; i++) s = s + toFontXml(fonts[i]); //'<font><sz val="8" /><name val="Calibri" /><family val="2" /><scheme val="minor" /></font>' +
            s = s + '</fonts>';

            s = s + '<fills count="' + (2 + fills.length) + '"><fill><patternFill patternType="none"/></fill><fill><patternFill patternType="gray125"/></fill>';
            for (var i = 0; i < fills.length; i++) s = s + toFillXml(fills[i]);
            s = s + '</fills>';

            s = s + '<borders count="' + (1 + borders.length) + '"><border><left /><right /><top /><bottom /><diagonal /></border>';
            for (var i = 0; i < borders.length; i++) s = s + toBorderXml(borders[i]);
            s = s + '</borders>';

            s = s + '<cellStyleXfs count="1"><xf numFmtId="0" fontId="0" fillId="0" borderId="0"/></cellStyleXfs>';

            s = s + '<cellXfs count="' + (1 + styles.length) + '"><xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" />';
            for (var i = 0; i < styles.length; i++) {
                s = s + toStyleXml(styles[i]);
            }
            s = s + '</cellXfs>';
            s = s + '<cellStyles count="1"><cellStyle name="Normal" xfId="0" builtinId="0"/></cellStyles>';
            s = s + '<dxfs count="0"/>';
            s = s + '</styleSheet>';
            return s;
        }
        return oStyles;
    };
    // --------------------- END Handling of styles

    var reUnescapedHtml = /[&<>"']/g, reHasUnescapedHtml = RegExp(reUnescapedHtml.source);
    var htmlEscapes = {'&': '&amp;', '<': '&lt;', '>': '&gt;', '"': '&quot;', "'": '&#39;'};

    function basePropertyOf(object) {
        return function (key) {
            return object == null ? undefined : object[key];
        };
    };
    
    var escapeHtmlChar = basePropertyOf(htmlEscapes);

    function escape(string) {
        if (typeof string != 'string') string = null ? '' : (string + '');

        return (string && reHasUnescapedHtml.test(string))
          ? string.replace(reUnescapedHtml, escapeHtmlChar)
          : string;
    };
	
	function cellNameH(i) {
		var rest = Math.floor(i / 26) - 1; 
		var s = (rest > -1 ? cellNameH(rest) : '');
		return  s+ "ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(i % 26); 
	};

    function cellName(colIndex, rowIndex) {
        return cellNameH(colIndex)+rowIndex;
    };

    function generateCell(cell, column, row) {
    	//cLog(cell);    	
        var s = '<c r="' + cellName(column, row) + '"';
        if (cell.s) s = s + ' s="' + cell.s + '" ';
        
		var value=cell.v;
		if (isNaN(value)) {
			if (value.charAt(0)!='=') return s + ' t="inlineStr" ><is><t>' + escape(value) + '</t></is></c>';
			return s+' ><f>'+value.substring(1)+'</f></c>';
        }
		return s + '><v>' + value + '</v></c>';
    };

    function generateMerges(merges) {
    	var oMerges = [];
        for (var index = 0; index < merges.length; index++) {
        	var merge = merges[index];
            oMerges.push('<mergeCell ref="' + cellName(merge.col_1, merge.row_1) + ':' + cellName(merge.col_2, merge.row_2) + '"/>');            
        }
        if(oMerges.join('') == ''){return ''}
        return '<mergeCells>' + oMerges.join('') + '</mergeCells>'
    };
    
    function generateRow(row, index) {
        var rowIndex = index + 1;
        var oCells = [];
        for (var i = 0; i < row.cells.length; i++) {
            if (row.cells[i]) oCells.push(generateCell(row.cells[i], i, rowIndex));
        }
        var s = '<row r="' + rowIndex + '" '
        if (row.ht) s = s + ' ht="' + row.ht + '" customHeight="1" ';
        if (row.style) s = s + 's="' + row.style + '" customFormat="1"';
        return s + ' >' + oCells.join('') + '</row>';
    };

    function generateRows(rows) {
        var oRows = [];
        for (var index = 0; index < rows.length; index++) {
            if (rows[index]) {
                oRows.push(generateRow(rows[index], index));
            }
        }
        return oRows.join('');
    };

    function generateColums(columns) {
        if (columns.length == 0) return;

        var s = '<cols>';
        for (var i = 0; i < columns.length; i++) {
            var c = columns[i];
            if (c) {
                s = s + '<col min="' + (i + 1) + '" max="' + (i + 1) + '" ';
                if (c.wt == "auto") s = s + ' width="18" bestFit="1" customWidth="1" '; else if (c.wt) s = s + ' width="' + c.wt + '" customWidth="1" ';
                if (c.style) s = s + ' style="' + c.style + '"';
                s = s + "/>";
            }
        }
        return s + "</cols>";
    };

    function isObject(v) {return (v !== null && typeof v === 'object')};

    function CombineStyles(sheets, styles) {
        // First lets do the Rows
        for (var i = 0; i < sheets.length; i++) {
            // First let's do the rows
            for (var j = 0; j < sheets[i].rows.length; j++) {
                var row = sheets[i].rows[j];
                if (row && row.style) {
                    for (var k = 0; k < row.cells.length; k++) {
                        if (row.cells[k]) AddStyleToCell(row.cells[k], styles, row.style);
                    }
                }
            }

            // Second let's do the cols
            for (var c = 0; c < sheets[i].columns.length; c++) {
                if (sheets[i].columns[c] && sheets[i].columns[c].style) {
                    var cstyle = sheets[i].columns[c].style;
                    for (var j = 0; j < sheets[i].rows.length; j++) {
                        var row = sheets[i].rows[j];
                        if (row) for (var k = 0; k < row.cells.length; k++)
                            if (row.cells[k] && k == c) AddStyleToCell(row.cells[k], styles, cstyle);
                    }
                }
            }
        }
    };

    function AddStyleToCell(cell, styles, toAdd) {
        if (!cell) return;                                      // If no cell then return
        if (!cell.s) {                                          // If cell has no style, use toAdd
            cell.s = toAdd;
            return;
        }
        var cs = styles.getStyle(cell.s - 1);
        var os = styles.getStyle(toAdd - 1);
        var ns = {}, b = false;
        for (var x in cs) ns[x] = cs[x];                        // Clone cell style
        for (var x in os) {
            if (!ns[x]) {
                ns[x] = os[x];
                b = true;
            }
        }
        if (!b) return;                                         // If the toAdd style does NOT add anything new
        cell.s = 1 + styles.register(ns);
    };

    this.newObject = function (defaultFont) {
        
    	var excel = {};
    	var sheets = createSheets();                                                                              //  Create Excel    sheets
        var styles = createStyleSheet(defaultFont);                                                                        //  Create Styles   sheet
        sheets.add("Sheet 0");                                                                                  // At least we have a [Sheet 0]

        excel.addSheet = function (name) {
            if (!name) name = "Sheet " + sheets.length;
            return sheets.add(name);
        };

        excel.addStyle = function (a) {
            return styles.add(a);
        };

        excel.set = function (s, column, row, value, style) {
            if (isObject(s)) return this.set(s.sheet, s.column, s.row, s.value, s.style);                                           // If using Object form, expand it
            if (!s) s = 0;                                                                                                          // Use default sheet
            s = sheets.get(s);
            
            if (isNaN(column) && isNaN(row)) return s.set(value, style);                                                            // If this is a sheet operation
            if (!isNaN(column)) {                                                                                                    // If this is a column operation
                if (!isNaN(row)) return setCell(s.getCell(column, row), value, style);                                                // and also a ROW operation the this is a CELL operation
                return setColumn(s.getColumn(column), value, style);                                                                // if not we confirm than this is a COLUMN operation
            }
            return setRow(s.getRow(row), value, style);                                                                             // If got here, thet this is a Row operation
        };
        
        excel.setMerge = function (s, col_1, row_1, col_2, row_2) {
        	if (!s) s = 0;                                                                                                          // Use default sheet
            s = sheets.get(s);
        	var i = s.merges.length;
        	var merge = {col_1:col_1, row_1:(row_1+1), col_2:col_2, row_2:(row_2+1)}
        	//cLog(merge)
        	s.merges.push(merge);
        	//cLog(s.merges)
        };

        excel.generate = function (filename) {
            CombineStyles(sheets.sheets, styles);
            var zip = new JSZip();                                                                              // Create a ZIP file
            zip.file('_rels/.rels', sheets.toRels());                                                           // Add WorkBook RELS   
            var xl = zip.folder('xl');                                                                          // Add a XL folder for sheets
            xl.file('workbook.xml', sheets.toWorkBook());                                                       // And a WorkBook
            xl.file('styles.xml', styles.toStyleSheet());                                                       // Add styles
            xl.file('_rels/workbook.xml.rels', sheets.toWorkBookRels());                                        // Add WorkBook RELs
            zip.file('[Content_Types].xml', sheets.toContentType());                                            // Add content types
            sheets.fileData(xl);                                                                                // Zip the rest    
            zip.generateAsync({ type: "blob" }).then(function (content) { saveAs(content, filename); });        // And generate !!!
        };
        
        return excel;
    }
    
    return this.newObject();
}

function toExcelLocalTime(date1){
	var d2=Math.floor(date1.getTime()/1000);													// Number of seconds since JS epoch
	d2=Math.floor(d2/86400)+25569;																// Days since epoch plus difference in days between Excel EPOCH and JS epoch
	var seconds = date1.getUTCSeconds()+60*date1.getUTCMinutes()+60*60*date1.getUTCHours();		// Number of seconds of received hour
	seconds = seconds-60*(date1.getTimezoneOffset());											// Differences in seconds between UTC and LOCAL this depends on date becase daylight saving time
	var SECS_DAY= 60 * 60 * 24;																	// Number of seconds of a day
	return d2+(seconds/SECS_DAY);																// Returns a local time !!
};

function Planner_ExcelFile(strFileName){
	
	var excel = IMSExcel("Calibri 12 000000");
	excel.set({sheet:0, value:"Planner"});
	
	var formatBold = excel.addStyle({font: "Calibri 11 #000000 B"});
	var formatBoldRight = excel.addStyle({font: "Calibri 11 #000000 B", align: "R"});	
	var formatBoldCenter = excel.addStyle({font: "Calibri 11 #000000 B", align: "C"});
	var formatCenter = excel.addStyle({font: "Calibri 11 #000000", align: "C"});
	var formatMerge = excel.addStyle({font: "Calibri 11 B", align: "C C"});
	var formatMergeLeft = excel.addStyle({font: "Calibri 11 B", align: "L C"});
	var formatMerge_1 = excel.addStyle({font: "Calibri 11", align: "L C"});
	
	excel.set(0, 1, 1, "#", formatMergeLeft);				excel.set(0, 1, undefined, 7);		excel.setMerge(0, 1, 1, 1, 2);	
	excel.set(0, 2, 1, "Interpreter", formatMergeLeft);		excel.set(0, 2, undefined, 25);		excel.setMerge(0, 2, 1, 2, 2);
	
	var tbl = document.getElementById('tblPlanner');
	
	var row = tbl.rows[0]; var cellLength = row.cells.length; var colCount = 3;
	for(var y=2; y<cellLength; y++){		
		
		excel.set(0, colCount, undefined, 12);
		if(y != (cellLength-1)){
			excel.set(0, colCount, 1, row.cells[y].innerHTML, formatMerge);
			excel.setMerge(0, colCount, 1, (colCount+2), 1);
			excel.set(0, colCount, 2, "Start", formatBoldRight);			
			excel.set(0, (colCount+1), 2, "End", formatBoldRight);
			excel.set(0, (colCount+2), 2, "Hours", formatBoldRight);
		} else {
			//Last Column
			excel.set(0, colCount, 1, row.cells[y].innerHTML, formatBoldRight);
			excel.setMerge(0, colCount, 1, colCount, 2);
		}
		colCount = colCount+3;
	}
	
	var rowCount = 3;
	
	for(var r=2; r<tbl.rows.length; r++){
		
		var row = tbl.rows[r]; 
		var cellLength = row.cells.length;
		var xlsRowCount = Planner_GetRowCount(row);
		var colCount = 1;
				
		excel.set(0, colCount, rowCount, row.cells[0].innerHTML, formatMerge_1);		//Counter
		excel.set(0, (colCount+1), rowCount, row.cells[1].innerHTML, formatMerge_1);	//Interpreter
		
		//Set Merging of Counter & Interpreter Name
		if(xlsRowCount > 1) {	
			excel.setMerge(0, colCount, rowCount, colCount, (rowCount+xlsRowCount-1));	
			excel.setMerge(0, (colCount+1), rowCount, (colCount+1), (rowCount+xlsRowCount-1));
		};
		
		colCount = (colCount+2);
		
		for(var c=2; c<(row.cells.length-1); c++){
			Planner_GetDayData(excel, row.cells[c], colCount, rowCount, xlsRowCount);
			colCount = (colCount+3);
		}
		
		//Total at the end
		excel.set(0, colCount, rowCount, row.cells[row.cells.length-1].innerHTML, formatBoldRight);
		//Set Merging of Total Column
		if(xlsRowCount > 1) {	
			excel.setMerge(0, colCount, rowCount, colCount, (rowCount+xlsRowCount-1));
		};
		
		rowCount = (rowCount + xlsRowCount);
	}
	
	excel.generate(strFileName + ".xlsx");		
}

function Planner_GetDayData(excel, cell, colCount, rowCount, xlsRowCount){
	
	var childNode = cell.childNodes[0];
	var formatRight = excel.addStyle({font: "Calibri 11 #000000", align: "R"});
	var formatMerge = excel.addStyle({font: "Calibri 11 B", align: "C C"});
	var newRowCount = rowCount; var newColCount = colCount;
	
	//Day Off or Leave
	if(childNode.nodeName != 'DIV'){		
		excel.set(0, newColCount, newRowCount, cell.innerHTML, formatMerge);
		excel.setMerge(0, newColCount, newRowCount, (newColCount + 2), (newRowCount + xlsRowCount - 1));
		
	} else {
		
		var tblChild = childNode.childNodes[0];
		
		for(var x=0; x<tblChild.rows.length; x++){			
			
			var rowChild = tblChild.rows[x];
			newColCount = colCount;	//Reset Column
			
			//cLog('No. of cells - ' + rowChild.cells.length);
			for(var y=0; y<rowChild.cells.length; y++){
				
				//cLog('newColCount - ' + newColCount +  ', newRowCount - ' + newRowCount + ', Value - '+ rowChild.cells[y].innerHTML);				
				excel.set(0, newColCount, newRowCount, rowChild.cells[y].innerHTML, formatRight);
				
				//2 Cells of Lunch & Day Total
				if(rowChild.cells.length == 2 && y == 0){
					excel.setMerge(0, newColCount, newRowCount, (newColCount+1), newRowCount);
					//cLog('Merge - till Column ' + (newColCount+1));
					newColCount++;
				}
				newColCount++;				
			}
			newRowCount++;			
		}		
	}	
}

function Planner_GetRowCount(row){
	var rowCount = 1;
	for(var cell=2; cell<row.cells.length; cell++){
		var childNode = row.cells[cell].childNodes[0];
		if(childNode.nodeName == 'DIV'){
			return childNode.childNodes[0].rows.length
		}
	}
	return rowCount;
}

function Report_ExcelFile(strFileName){
	
	var excel = IMSExcel("Calibri 12 000000");	
	Report_Summary(excel);
	Report_ByLanguage(excel);
	Report_ByInterpreter(excel);	
	
	var liObj = $('#location').find( "li" );
	var cntSheet = 3;
	for(var i=0; i<liObj.length; i++){
		
		var aObj = $(liObj[i]).find('a');
		var tblID = aObj.prop('href').split('#')[1];
		// var strCampusName = aObj.html();		
		var tblObj = $('#tbl' + tblID);
		if(tblObj.length > 0){
			Report_ByLocation(excel, cntSheet, tblID);
			cntSheet++;
		}
	}
	
	excel.generate(strFileName + ".xlsx");		
}

function Report_Summary(excel){
	
	excel.set({sheet:0, value:"Summary"});
	excel.set(0, 1, undefined, 40);
	excel.set(0, 2, undefined, 17);	
	
	var formatBold = excel.addStyle({font: "Calibri 12 #000000 B"});
	var formatMerge = excel.addStyle({font: "Calibri 12 #000000 B", align: "C C"});
	
	var tbl = document.getElementById('tblSummary');
	var rowLength = tbl.rows.length;
	var strVal; var intStart; var intEnd;
	
	for(var i=0; i<rowLength; i++){
	
		var row = tbl.rows[i];
		var cellLength = row.cells.length;
		
		for(var y=0; y<cellLength; y++){
			
			strVal = Report_FixValue(row.cells[y].innerHTML, y);
			
			intStart = strVal.indexOf('<b>');
			intEnd = strVal.indexOf('</b>');
			
			//Check Bold
			if(intStart>=0){				
				strVal = strVal.substring(intStart+3, intEnd);
				excel.set(0, (y+1), i, strVal, formatBold);
			} else {
				excel.set(0, (y+1), i, strVal);
			}	
		}
	}
}

function Report_ByLanguage(excel){
	
	excel.addSheet();
	excel.set({sheet:1, value:"By Language"});
	excel.set(1, 1, undefined, 40);
	excel.set(1, 2, undefined, 15);	
	excel.set(1, 3, undefined, 15);

	var formatBold = excel.addStyle({font: "Calibri 12 #000000 B"});
	var formatBoldRight = excel.addStyle({font: "Calibri 12 #000000 B", align: "R"});	
	var formatPercentage = excel.addStyle({format: "0.00%"});
	
	excel.set(1, 1, 0, "Language", formatBold);
	excel.set(1, 2, 0, "Count", formatBoldRight);
	excel.set(1, 3, 0, "Percentage", formatBoldRight);
	
	var tbl = document.getElementById('tblLanguage');
	var rowLength = tbl.rows.length; var strVal;
	
	for(var i=1; i<rowLength; i++){	
		
		var row = tbl.rows[i];
		var cellLength = row.cells.length;
		
		for(var y=0; y<cellLength; y++){
			
			strVal = Report_FixValue(row.cells[y].innerHTML, y);
			
			if(y == 2){				
				excel.set(1, (y+1), (i), "=INDIRECT(ADDRESS("+ (i+1) +", 3))/Summary!$D$1", formatPercentage);
			} else {
				excel.set(1, (y+1), (i), strVal);
			}
		}
	}	
}

function Report_ByInterpreter(excel){
	
	excel.addSheet();
	excel.set({sheet:2, value:"By Interpreter"});
	//excel.set(2, undefined, 2, 35);	//Set height

	var formatBold = excel.addStyle({font: "Calibri 11 #000000 B"});
	var formatBoldRight = excel.addStyle({font: "Calibri 11 #000000 B", align: "R"});	
	var formatBoldCenter = excel.addStyle({font: "Calibri 11 #000000 B", align: "C"});
	var formatCenter = excel.addStyle({font: "Calibri 11 #000000", align: "C"});
	var formatPercentage = excel.addStyle({format: "0.00%"});
	var formatMerge = excel.addStyle({font: "Calibri 11 B", align: "C C"});
	
	excel.set(2, 6, 1, "Statistics (/8 Hrs)", formatMerge);				excel.setMerge(2, 6, 1, 7, 1);
	excel.set(2, 9, 1, "Patient Type", formatMerge);					excel.setMerge(2, 9, 1, 11, 1);
	excel.set(2, 12, 1, "Duration", formatMerge);						excel.setMerge(2, 12, 1, 13, 1);
	excel.set(2, 15, 1, "Total Covered", formatMerge);					excel.setMerge(2, 15, 1, 17, 1);
	excel.set(2, 18, 1, "Not Covered", formatMerge);
	
	excel.set(2, 1, 2, "#", formatBoldCenter);							excel.set(2, 1, undefined, 7);
	excel.set(2, 2, 2, "Interpreter", formatBold);						excel.set(2, 2, undefined, 25);
	excel.set(2, 3, 2, "Total", formatBoldRight);						excel.set(2, 3, undefined, 10);
	excel.set(2, 4, 2, "%", formatBoldRight);							excel.set(2, 4, undefined, 10);
	excel.set(2, 5, 2, "Hours Worked", formatBoldRight);				excel.set(2, 5, undefined, 13);
	excel.set(2, 6, 2, "Days", formatBoldRight);						excel.set(2, 6, undefined, 10);
	excel.set(2, 7, 2, "Total/Day", formatBoldRight);					excel.set(2, 7, undefined, 10);
	excel.set(2, 8, 2, "Compl./Day", formatBoldRight);					excel.set(2, 8, undefined, 12);
	excel.set(2, 9, 2, "In.", formatBoldRight);							excel.set(2, 9, undefined, 8);
	excel.set(2, 10, 2, "Out.", formatBoldRight);						excel.set(2, 10, undefined, 8);
	excel.set(2, 11, 2, "HV", formatBoldRight);							excel.set(2, 11, undefined, 8);
	excel.set(2, 12, 2, "Actual", formatBoldRight);						excel.set(2, 12, undefined, 10);
	excel.set(2, 13, 2, "Waiting", formatBoldRight);					excel.set(2, 13, undefined, 10);
	excel.set(2, 14, 2, "Pending Completion", formatBoldRight);			excel.set(2, 14, undefined, 19);	//Row 1
	excel.set(2, 15, 2, "Total", formatBoldRight);						excel.set(2, 15, undefined, 10);
	excel.set(2, 16, 2, "Tel.", formatBoldRight);						excel.set(2, 16, undefined, 10);
	excel.set(2, 17, 2, "Extra", formatBoldRight);						excel.set(2, 17, undefined, 10);
	excel.set(2, 18, 2, "Total", formatBoldRight);						excel.set(2, 18, undefined, 10);
	
	var tbl = document.getElementById('tblInterpreter');
		
	//Not Covered Status, 2nd row
	var row2nd = tbl.rows[1]; var cellLength = row2nd.cells.length; var colCount = 19;
	for(var y=12; y<cellLength; y++){		
		excel.set(2, colCount, 2, row2nd.cells[y].innerHTML, formatBoldRight);
		excel.set(2, colCount, undefined, 12);
		colCount++
	}
	
	excel.setMerge(2, 18, 1, (colCount-1), 1);		//Merge for Not Covered
	
	var rowLength = tbl.rows.length; var strVal;
	
	for(var i=2; i<rowLength; i++){	
		
		var row = tbl.rows[i]; var cellLength = row.cells.length;
		
		for(var y=0; y<cellLength; y++){
			
			strVal = row.cells[y].innerHTML
			if(y == 0){
				excel.set(2, (y+1), (i+1), "=ROW()-3", formatCenter);
			} else if(y == 3){				
				excel.set(2, (y+1), (i+1), "=INDIRECT(ADDRESS("+ (i+2) +", 4))/Summary!$D$1", formatPercentage);
			} else {
				excel.set(2, (y+1), (i+1), strVal);
			}			
		}
	}	
}


function Report_ByLocation(excel, shtCount, tblID){
	
	excel.addSheet();
	excel.set({sheet:shtCount, value:"Campus - " + tblID});
	tblID = 'tbl' + tblID;

	var formatBold = excel.addStyle({font: "Calibri 11 #000000 B"});
	var formatBoldRight = excel.addStyle({font: "Calibri 11 #000000 B", align: "R"});	
	var formatBoldCenter = excel.addStyle({font: "Calibri 11 #000000 B", align: "C"});
	var formatCenter = excel.addStyle({font: "Calibri 11 #000000", align: "C"});
	var formatPercentage = excel.addStyle({format: "0.00%"});
	var formatMerge = excel.addStyle({font: "Calibri 11 B", align: "C C"});
	
	excel.set(shtCount, 10, 1, "Total Covered", formatMerge);					excel.setMerge(shtCount, 10, 1, 15, 1);
	excel.set(shtCount, 16, 1, "Not Covered", formatMerge);
	
	excel.set(shtCount, 4, 2, "Patient Type", formatMerge);						excel.setMerge(shtCount, 4, 2, 6, 2);
	excel.set(shtCount, 7, 2, "Duration (hrs.)", formatMerge);					excel.setMerge(shtCount, 7, 2, 8, 2);
	excel.set(shtCount, 10, 3, "Total", formatBoldRight);
	excel.set(shtCount, 11, 2, "In-House", formatMerge);						excel.setMerge(shtCount, 11, 2, 13, 2);
	excel.set(shtCount, 14, 2, "Agency", formatMerge);							excel.setMerge(shtCount, 14, 2, 15, 2);
	excel.set(shtCount, 16, 2, "Total", formatBoldRight);
	
	excel.set(shtCount, 1, 3, "#", formatBoldCenter);							excel.set(shtCount, 1, undefined, 7);
	excel.set(shtCount, 2, 3, "Location", formatBold);							excel.set(shtCount, 2, undefined, 50);
	excel.set(shtCount, 3, 3, "Total", formatBoldRight);						excel.set(shtCount, 3, undefined, 10);
	excel.set(shtCount, 4, 3, "In.", formatBoldRight);							excel.set(shtCount, 4, undefined, 6);
	excel.set(shtCount, 5, 3, "Out.", formatBoldRight);							excel.set(shtCount, 5, undefined, 6);
	excel.set(shtCount, 6, 3, "HV", formatBoldRight);							excel.set(shtCount, 6, undefined, 6);
	excel.set(shtCount, 7, 3, "Actual", formatBoldRight);						excel.set(shtCount, 7, undefined, 10);
	excel.set(shtCount, 8, 3, "Waiting", formatBoldRight);						excel.set(shtCount, 8, undefined, 10);
	excel.set(shtCount, 9, 3, "Pending Completion", formatBoldRight);			excel.set(shtCount, 9, undefined, 18);
	excel.set(shtCount, 11, 3, "Total", formatBoldRight);						excel.set(shtCount, 11, undefined, 10);
	excel.set(shtCount, 12, 3, "Tel.", formatBoldRight);						excel.set(shtCount, 12, undefined, 10);
	excel.set(shtCount, 13, 3, "Extra", formatBoldRight);						excel.set(shtCount, 13, undefined, 10);
	excel.set(shtCount, 14, 3, "Total", formatBoldRight);						excel.set(shtCount, 14, undefined, 10);
	excel.set(shtCount, 15, 3, "Tel.", formatBoldRight);						excel.set(shtCount, 15, undefined, 10);
	
	var tbl = document.getElementById(tblID);
	//Not Covered Status, 2nd row
	var row2nd = tbl.rows[1]; var cellLength = row2nd.cells.length; var colCount = 17;
	var cancelPos = 0; var unmetPos = 0;
	for(var y=4; y<cellLength; y++){		
		
		if(row2nd.cells[y].innerHTML == 'Cancelled'){
			excel.set(shtCount, colCount, 2, row2nd.cells[y].innerHTML, formatMerge);
			excel.setMerge(shtCount, colCount, 2, (colCount+1), 2);
			cancelPos = colCount;
			colCount++;	// Cancel covers 2 columns
		} else if(row2nd.cells[y].innerHTML == 'Unmet'){
			excel.set(shtCount, colCount, 2, row2nd.cells[y].innerHTML, formatMerge);
			excel.setMerge(shtCount, colCount, 2, (colCount+1), 2);
			unmetPos = colCount;
			colCount++;	// Unmet covers 2 columns
		} else {
			excel.set(shtCount, colCount, 2, row2nd.cells[y].innerHTML, formatBoldRight);
		}
		
		excel.set(shtCount, colCount, undefined, 12);
		colCount++;
	}
	
	excel.setMerge(shtCount, 16, 1, (colCount-2), 1);
	
	//Cancelled, 3rd row
	if(cancelPos !=0){
		excel.set(shtCount, cancelPos, 3, "Total", formatBoldRight);			excel.set(shtCount, cancelPos, undefined, 12);
		cancelPos++;
		excel.set(shtCount, cancelPos, 3, "<24hrs", formatBoldRight);			excel.set(shtCount, cancelPos, undefined, 12);
	}
	//UnMet, 3rd row
	if(unmetPos !=0){
		excel.set(shtCount, unmetPos, 3, "Total", formatBoldRight);				excel.set(shtCount, unmetPos, undefined, 12);
		unmetPos++;
		excel.set(shtCount, unmetPos, 3, "Agency", formatBoldRight);			excel.set(shtCount, unmetPos, undefined, 12);
	}
	
	
	var rowLength = tbl.rows.length; var strVal;
	
	for(var i=3; i<rowLength; i++){	
		
		var row = tbl.rows[i]; var cellLength = row.cells.length;
		
		for(var y=0; y<cellLength; y++){
			
			strVal = row.cells[y].innerHTML
			if(y == 0){
				excel.set(shtCount, (y+1), (i+1), "=ROW()-4", formatCenter);
			} else {
				excel.set(shtCount, (y+1), (i+1), strVal);
			}			
		}
	}	
}

function Report_FixValue(strVal, y){
	
	//Remove li Tag
	if(strVal.indexOf('<li>')>=0){strVal = strVal.substring((strVal.indexOf('<li>')+4), strVal.indexOf('</li>'))}
	
	//Remove comma
	if(y == 1 && strVal.indexOf(',')>=0){strVal = strVal.replace(',', '')}
	
	//HTML Character
	if(strVal.indexOf('&lt;')>=0){strVal = strVal.replace('&lt;', '<')}
	
	return strVal
}

function Generate_ExcelFile(strFileName, vCols, vRows){
	
	//cLog(strFileName); cLog(vCols); cLog(vRows);
	
	/*excel.set is the main function to generate content:
	We can use parameter notation excel.set(sheetValue,columnValue,rowValue,cellValue,styleValue) 
	Or object notation excel.set({sheet:sheetValue,column:columnValue,row:rowValue,value:cellValue,style:styleValue })
	null or 0 are used as default values for undefined entries
	
	column & row starts with 0	
	excel.set(0,8,1,15);		
	excel.set(0,8,2,13);		
	excel.set(0,7,3,"15+13");		
	excel.set(0,8,3,"=I2+I3");		
	*/		
	
	var excel = IMSExcel("Calibri 11 #000000");
	excel.set( {sheet:0,value:"IMS" } );
    
	
	/*// Style for even ROWS
	// Borders are LEFT,RIGHT,TOP,BOTTOM. Check $JExcel.borderStyles for a list of valid border styles
	var evenRow=excel.addStyle ({border: "none,none,none,thin #333333"});															
															
	// Style for odd ROWS
	// Background color, plain #RRGGBB, there is a helper $JExcel.rgbToHex(r,g,b)
	var oddRow=excel.addStyle ({fill: "#ECECEC" , border: "none,none,none,thin #333333"}); 
	
	// Set style for the first 50 rows // We want ROW 3 to be EXTRA TALL
	for (var i=1;i<50;i++) excel.set({row:i,style: i%2==0 ? evenRow: oddRow  });					
	excel.set({row:3,value: 30  });				*/													

	var vColWidth = [];
	
	var formatHeader=excel.addStyle({ 														// Format for headers
		border: "none,none,none,thin #333333", 												// Border for header
		font: "Calibri 12 #000000 B"}); 													// Font for headers

	for (var i=0; i<vCols.length; i++){														// Loop all the haders
		excel.set(0, i, 0, vCols[i], formatHeader);											// Set CELL with header text, using header format
		excel.set(0, i, undefined, "auto");													// Set COLUMN width to auto (according to the standard this is only valid for numeric columns)
		vColWidth.push(vCols[i].length);
	}
	//cLog(vColWidth)
	
	var colNum; var rowNum = 1; var strVal; var vRow; var newLength;
	for (var i=0; i<vRows.length; i++){
		
		vRow = vRows[i];
		colNum = 0;	//Start from First Column
		
		for (var j=0; j<vRow.length; j++){
			strVal = vRow[j];
			if(strVal != ""){
				strVal = Excel_FormatVal(strVal);
				excel.set(0, colNum, rowNum, strVal);
				newLength = strVal.length;
				if(newLength < 10){newLength = newLength + 4} else {newLength = newLength + 2}
				if(vColWidth[j] < newLength){vColWidth[j] = newLength}				
			}
			colNum++;
		}
		
		rowNum++; //Increase Row Count								
	}
	
	for (var i=0; i<vColWidth.length; i++){		
		excel.set(0, i, undefined, vColWidth[i]);
	}
	
	/*// Now let's write some data
	var initDate = new Date(2000, 0, 1);
	var endDate = new Date(2016, 0, 1);
	var dateStyle = excel.addStyle ( { 														// Format for date cells
			align: "R",																		// aligned to the RIGHT
			format: "yyyy.mm.dd hh:mm:ss", 													// using DATE mask, Check $JExcel.formats for built-in formats or provide your own 
			font: "#00AA00"}); 																// in color green
	
	for (var i=1;i<50;i++){																	// we will fill the 50 rows
		excel.set(0,0,i,"This is line "+i);													// This column is a TEXT
		var d=randomDate(initDate,endDate);													// Get a random date
		excel.set(0,1,i,d.toLocaleString());												// Store the random date as STRING
		excel.set(0,2,i,toExcelLocalTime(d));												// Store the previous random date as a NUMERIC (there is also $JExcel.toExcelUTCTime)
		excel.set(0,3,i,toExcelLocalTime(d),dateStyle);										// Store the previous random date as a NUMERIC,  display using dateStyle format
		excel.set(0,4,i,"Some other text");															// Some other text
	}

	excel.set(0,1,undefined,30);																	// Set COLUMN 1 to 30 chars width
	excel.set(0,3,undefined,30);																	// Set COLUMN 3 to 20 chars width
	excel.set(0,4,undefined,20, excel.addStyle( {align:"R"}));										// Align column 4 to the right
	excel.set(0,1,3,undefined,excel.addStyle( {align:"L T"}));										// Align cell 1-3  to LEFT-TOP
	excel.set(0,2,3,undefined,excel.addStyle( {align:"C C"}));										// Align cell 2-3  to CENTER-CENTER
	excel.set(0,3,3,undefined,excel.addStyle( {align:"R B"}));										// Align cell 3-3  to RIGHT-BOTTOM
*/			
    excel.generate(strFileName + ".xlsx");
	
}

function Excel_FormatVal(strVal){
	//Date Handle
	if(strVal.length == 10){
		if(strVal.indexOf('-')>0){
			if(strVal.split('-').length == 3){
				var aptDt = new Date(strVal);
				if(gbl_strKey == 'GRID_CRD_Agency_Pending'){
					strVal = moment(aptDt).format('DD-MMM-YY');
				} else {
					strVal = moment(aptDt).format('DD-MMM-YYYY');
				}							
			}
		}
	}
	//Date Handle
	if(strVal.length == 5){
		if(strVal.indexOf(':')>0){
			var vTemp = strVal.split(':');
			if(vTemp.length == 2 & vTemp[0].length == 2 & vTemp[1].length == 2){
				var aptDt = new Date();
				aptDt.setHours(vTemp[0], vTemp[1]);
				if(gbl_strKey == 'GRID_CRD_Agency_Pending'){
					strVal = moment(aptDt).format('hh:mm a');
				}							
			}
		}
	}
	
	if(strVal.indexOf('<span')>=0){
		strVal = strVal.split('</span>')[1];
	}
	return strVal
}