if(!Array.indexOf) {
   Array.prototype.indexOf = function(obj) {
      for(var i=0; i<this.length; i++) {
         if(this[i]==obj) {
            return i;
         }
      }
      return -1;
   }
}

function RCN_CostCentre_List(rcn)
{
	strClinic = rcn.getItemValueString('RCN_Clinic')
	strLocation = rcn.getItemValueString('RCN_Location')
	strSite = rcn.getItemValueString('RCN_Site')
	if (IsNullorBlank(strClinic)){return "-|"}
	var vList = @Explode (@DbLookup(GetDB_FilePath('CLN'), "CLN_Lookup_CostCentre", strSite + "~" + strLocation + "~" + strClinic, 2, "[FAILSILENT]"), "~");
	if(IsNullorBlank(vList)){return "-|"} else
	{
		//if we have one value return that if we have multiple that add a '-' to the top so it's selected
		return vList.length >1 ? vList.unshift ("-|") : vList
	}
}

function RCN_Editable()
{
	//print("RCN_Editable?");
	if (document1.isNewNote()){return true}
	//print("UPStatus="+document1.getItemValueString("UP_Status"))
	if (document1.getItemValueString("UP_Status")=="" || document1.getItemValueString("UP_Status")=="New") {return true}
	return false
}



function RCN_AgencyChange()
{
	getComponent("headerConfig").setValue(getUploadConfig())
}

function RCN_Upload_Config_JSON()
{
	var externalContext = facesContext.getExternalContext();
	var writer = facesContext.getResponseWriter();
	var response = externalContext.getResponse();
	response.setContentType("application/json");
	response.setHeader("Cache-Control", "no-cache");
	var strKey = param.RCN_Key;	
	if(IsNullorBlank(strKey)){
		writer.write("[]"); 
		writer.endDocument(); 
		print("ERROR:No KEY passed for JSON");
		return
	}
	var j = getUploadConfig(strKey);
	
	writer.write(toJson(j));
	writer.endDocument();	
}

function getUploadConfig(agency)
{
//print("got to getUpload " + agency);
if (agency==null){agency = getComponent("UP_Agency").getValue()||document1.getItemValue("UP_Agency");}
	var p = GetDB_FilePath("RCN_Upload");
	var headerConfig = @DbLookup([database.getServer(),p],"luMapping",agency,2,"[FailSilent]");
	return headerConfig
}

function RCN_Delete()
{
	document1.replaceItemValue("UP_Status","Deleted");
	document1.replaceItemValue("UP_DeleteDate",@Now());
	document1.replaceItemValue("UP_DeleteBy",session.getEffectiveUserName());
	sReportKey = document1.getItemValue("REPORTKEY");
	Global_RemoveDocument(document1.getDocument())
	//print("now get child docs");
	//loop through linked records
	var rcnDB:NotesDatabase = GetDB_Database("RCN_Upload");
	var rcnChildView:NotesView = rcnDB.getView("GRID_RCN_Detail")
	var dc:NotesDocumentCollecton = rcnChildView.getAllDocumentsByKey(sReportKey,true)
	var child:NotesDocument = dc.getFirstDocument()
	var nchild:NotesDocument = null;
	while (child!=null)
	{
		nchild = dc.getNextDocument(child);
		Global_RemoveDocument(child)
		child = nchild
	}
	rcnChildView.recycle();
	rcnDB.recycle();
	dc.recycle();
	// Now remove the RCN_BOOK (Missing stubs) from the Data db
	var dataDB:NotesDatabase = GetDB_Database("BOOK");
	var dataChildView:NotesView = dataDB.getView("luRCNReportKey")
	var datadc = dataChildView.getAllDocumentsByKey(sReportKey,true)
	child = datadc.getFirstDocument()
	nchild = null;
	while (child!=null)
	{
		nchild = datadc.getNextDocument(child);
		Global_RemoveDocument(child)
		child = nchild
	}
	dataChildView.recycle();
	dataDB.recycle();
	datadc.recycle();
	//print ("finished rcn delete")
	
}

function RCN_Rerun()
{
	document1.replaceItemValue("UP_Status","Rerun");
	document1.save()
	
}

function RCN_Upload_Save()
{
	document1.replaceItemValue("UP_Status","New");
	document1.replaceItemValue("UP_DateCreated",@Now());
	var unique = @Unique();
	//print("Unique key is: " + unique);
	document1.replaceItemValue("ReportKey",unique);
	var out = fromJson(getComponent("out").getValue());
	var rcnDB:NotesDatabase = GetDB_Database("RCN_Upload");
	var invoices = [];
	var hc = fromJson("{"+getUploadConfig().join(',')+"}")
		for (var i = 1; i<out.length;i++)
		{
			var doc:NotesDocument = rcnDB.createDocument();
			doc.replaceItemValue("Form","Recon");
			doc.replaceItemValue("ReportKey",unique);
			var jrow = out[i];

			for (col in hc)
			{
				writeRow(doc,jrow[col],hc[col]);
				if (hc[col].fld=="RCN_InvoiceNo")
				{
	
					if (jrow[col]!="" && jrow[col]!=null && jrow[col]!=undefined)
					{
						//print("found in voice column, value = " + jrow[col])
						//print("check invoices: " + invoices.indexOf[jrow[col]])
						if ((invoices.indexOf[jrow[col]]<0 || invoices.length == 0))
						{
							//print("adding to array")
							invoices.push(jrow[col])
						}
					}

				}
			}
			doc.save(true,false)
			doc.recycle()
			
		}
	//print("completed all records");
	if (invoices.length>0)
	{
		document1.replaceItemValue("Invoices",invoices)
	}

		
	RCN_Run()

}

function RCN_Run()
{
	var rcnDB:NotesDatabase = GetDB_Database("RCN_Upload");
	var agt:NotesAgent = rcnDB.getAgent("RunRCN");
	//print(agt);
	agt.runOnServer();
}

function RCN_Recalc(rcn)
{
	var nid = rcn.getNoteID();
	//print("RCN_Recalc = " + nid);
	var rcnDB:NotesDatabase = GetDB_Database("RCN_Upload");
	var agt:NotesAgent = rcnDB.getAgent("agtRecalcCost");
	//print(agt);
	agt.runOnServer(nid);
}

function writeRow(doc:NotesDocument,val,map)
{
	if (val==null||val==undefined){
		if (map.fmt=="Number")
		{
			doc.replaceItemValue(map.fld,0)
		}
		else
		{
			doc.replaceItemValue(map.fld,"")
		}
		return;
	}
	try{
		switch (map.fmt)
		{
		case "DateTime":
			var dt:NotesDateTime = session.createDateTime(val)
			doc.replaceItemValue(map.fld,dt)
			break;
		case "Number":
			if (!isNaN(val))
			{
			doc.replaceItemValue(map.fld,Number(val))
			}
			else
			{
				writeError(doc,val,map)
				doc.replaceItemValue(map.fld,0)
			}
			break;
		default:
			doc.replaceItemValue(map.fld,val)
		}
	}
	catch(err)
	{
		doc.replaceItemValue(map.fld,val)
		writeError(doc,val,map)
	}
}

function writeError(doc:NotesDocument,val, map)
{
	var sError = map.lbl + ": Error converting " + val + " to " + map.fmt
	doc.replaceItemValue(map.fld+"_Raw",val)
	doc.replaceItemValue(map.fld+"_Err",sError)
	if (doc.hasItem("LineError"))
	{
		var item:NotesItem = doc.getFirstItem("LineError")
		item.appendToTextList(sError)
	}
	else
	{
		doc.replaceItemValue("LineError",sError)
	}
	if (map.fld=="RCN_InvoiceCost"){doc.replaceItemValue("Status","Error")}
}

function RCN_Accept(ndoc)
{
	//print("start RCN_Accept")
	//sStatus = ndoc.getItemValueString("STATUS");

	var dt:NotesDateTime = session.createDateTime("Today")
	dt.setNow();
	ndoc.replaceItemValue("StatusRemediation","Accepted");
	ndoc.replaceItemValue("DateAccepted",dt);
	
	var bookdb:NotesDatabase = GetDB_Database('BOOK');
	var bookdoc:NotesDocument;
	if (ndoc.hasItem("BOOK_UNID")){	bookdoc = bookdb.getDocumentByUNID(ndoc.getItemValueString("BOOK_UNID"));}
	if (bookdoc==null) //Need to convert the temporary RCN_BOOK into a real booking
		{
		if (ndoc.hasItem("RCN_BOOK_UNID")){
			var bookdoc:NotesDocument = bookdb.getDocumentByUNID(ndoc.getItemValueString("RCN_BOOK_UNID"));
		}
			
			if (bookdoc!=null)
			{
			//print("found RCN_BOOK")	
			bookdoc.replaceItemValue("Form","BOOK")
			bookdoc.replaceItemValue("BOOK_Site",ndoc.getItemValue("RCN_SiteName"))
			bookdoc.replaceItemValue("BOOK_SiteName",ndoc.getItemValue("RCN_SiteName"))
			bookdoc.replaceItemValue("BOOK_Location",ndoc.getItemValue("RCN_Location"))
			bookdoc.replaceItemValue("BOOK_Clinic",ndoc.getItemValue("RCN_Clinic"))
			if (ndoc.getItemValueString("DocKey")=="" || ndoc.getItemValueString("DocKey")==null)
			{
				if (ndoc.getItemValueString("RCN_DocKey")=="" || ndoc.getItemValueString("RCN_DocKey")==null)
				{
					var dkey = AppForm_DocKey("BOOK");
					bookdoc.replaceItemValue("DocKey",dkey)
					ndoc.replaceItemValue("DocKey",dkey)
				}
				else
				{
					ndoc.replaceItemValue("DocKey",ndoc.getItemValueString("RCN_DocKey"))
					bookdoc.replaceItemValue("DocKey",ndoc.getItemValueString("DocKey"))
				}
			}
			else
			{
				bookdoc.replaceItemValue("DocKey",ndoc.getItemValueString("DocKey"))
			}
			
			ndoc.replaceItemValue("BOOK_UNID",bookdoc.getUniversalID())
			}
			else
			{
				print("***** RCN_Accept ERROR - can not locate RCN_BOOK or BOOK form: " + ndoc.getItemValueString("RCN_BOOK_UNID"))
			}
		}
		
		if (bookdoc!=null)
		{
			BOOK_LogAction(bookdoc, "Invoice #" + ndoc.getItemValueString("RCN_InvoiceNo") + " Accepted. (Booking Ref ID: " + ndoc.getItemValueString("RCN_AGENCY_ID") + ")", true);
			updateBookField(bookdoc,"RCN_AGENCY_ID",ndoc.getItemValueString("RCN_AGENCY_ID"))
			updateBookField(bookdoc,"BOOK_InvoiceNo",ndoc.getItemValueString("RCN_InvoiceNo"))
			//updateBookField(bookdoc,"BOOK_INVAmount",ndoc.getItemValueDouble("RemedialCost").toString())
			//updateBookField(bookdoc,"BOOK_INVStatus",ndoc.getItemValueString("Status"))
			//updateBookField(bookdoc,"BOOK_INVRem",ndoc.getItemValueString("StatusRemediation"))
			//updateBookField(bookdoc,"BOOK_INVComments",ndoc.getItemValueString("RemComments"))
			if (bookdoc.getItemValueString("Form")=="RCN_BOOK"){
				bookdoc.replaceItemValue("Form","BOOK")
			}
			bookdoc.replaceItemValue("INVOICE_Number",ndoc.getItemValueString("RCN_InvoiceNo"))
			bookdoc.replaceItemValue("INVOICE_Amount",ndoc.getItemValue("REMEDIALCOST"))
			bookdoc.replaceItemValue("INVOICE_Status",ndoc.getItemValueString("Status"))
			bookdoc.replaceItemValue("INVOICE_Agency",ndoc.getItemValueString("Agency"))
			bookdoc.replaceItemValue("INVOICE_Accepted",ndoc.getItemValueString("StatusRemediation"))
			bookdoc.replaceItemValue("ChargesXFRd","Yes")
			bookdoc.save(true,false);
			
			if (bookdoc.getItemValueString("Form")=="CBN")
			{
				//print("cbn processing)")
				//Special processing for Combined bookings
				var totalDuration = getCombinedBookingDuration(bookdoc);
			//	print (totalDuration);
				var total = ndoc.getItemValueDouble("RemedialCost");
			//	print (total);
				var bookDC:NotesDocumentCollection = CBN_BOOK_Collection(false,bookdoc);
				var child:NotesDocment = bookDC.getFirstDocument();
				var dur = 0;
				var cost = 0
				while (child!=null)
				{
					dur = getDuration(child);
				//	print("dur="+dur);
					cost = total*dur/totalDuration;
				//	print("cost="+cost)
					updateBookField(child,"RCN_AGENCY_ID",ndoc.getItemValueString("RCN_AGENCY_ID"))
					updateBookField(child,"BOOK_InvoiceNo",ndoc.getItemValueString("RCN_InvoiceNo"))
					updateBookField(bookdoc,"BOOK_INVAmount",cost.toString())
					updateBookField(bookdoc,"BOOK_INVStatus",ndoc.getItemValueString("Status"))
					updateBookField(bookdoc,"BOOK_INVRem",ndoc.getItemValueString("StatusRemediation"))
					updateBookField(bookdoc,"BOOK_INVComments",ndoc.getItemValueString("RemComments"))
					
					child.replaceItemValue("INVOICE_Number",ndoc.getItemValueString("RCN_InvoiceNo"))
					child.replaceItemValue("INVOICE_Amount",cost)
					child.replaceItemValue("INVOICE_Status",ndoc.getItemValueString("Status"))
					child.replaceItemValue("INVOICE_Agency",ndoc.getItemValueString("Agency"))
					child.replaceItemValue("INVOICE_Accepted",ndoc.getItemValueString("StatusRemediation"))
					child.replaceItemValue("ChargesXFRd","Yes")
					child.save(true,false);
					child = bookDC.getNextDocument(child);
				}
			}
			//bookdoc.replaceItemValue("FlagInvoice","true")
		}
		else
		{
			//Could find the real doc or the temp rcn doc
			
		}

	
}


function getCombinedBookingDuration(bookdoc:NotesDocument)
{
	var bookDC:NotesDocumentCollection = CBN_BOOK_Collection(false,bookdoc);
	var child:NotesDocment = bookDC.getFirstDocument();
	var dur = 0
	while (child!=null)
	{
		dur+=getDuration(child);
		child = bookDC.getNextDocument(child);
	}
	return dur;
}

function getDuration(book)
{
	if (book.getItemValueString("BOOK_ApptDuration_Actual")=="")
	{
		return book.getItemValueDouble("BOOK_ApptDuration")
	}
	else
	{
		return Number(book.getItemValueString("BOOK_ApptDuration_Actual"))
	}
}
function updateBookField(book:NotesDocument,sBook, sRCNValue,force)
{
	var rcnItem:NotesItem = null;
	try{
	if (book.hasItem(sBook))
	{
		rcnItem = book.getFirstItem(sBook)
		if (@IsNotMember(sRCNValue,rcnItem.values)||force)
		{
			rcnItem.appendToTextList(sRCNValue)

		}
	}
	else
	{
		book.replaceItemValue(sBook, sRCNValue)

	}
	}
	catch(err)
	{
	print("*** updateBookField error: "+ err.toString())
	}
}
function RCN_Check_Remediation(ndoc)
{
//	print("RCN_Check_Remediation")
	try{
	var dblInvoice = ndoc.getItemValueDouble("RCN_InvoiceCost")
	var dblRemediation = ndoc.getItemValueDouble("REMEDIALCOST")
//	print("Invoice =" + dblInvoice);
//	print("dblRemediation =" + dblRemediation);
	if (dblInvoice!=dblRemediation)
	{
		ndoc.replaceItemValue("StatusRemediation","Remediated")
		return true
	}
	return false
	}
	catch(err)
	{
		print("******* RCN_Remediation check error - " + err.toString())
	}
}

function RCN_Compare_OnSave(ndoc){
	try{
	//	print(ndoc);
	//print("RCN Compare save: " + ndoc.getItemValueString("StatusRemediation"))
	if (ndoc.getItemValueString("StatusRemediation")=="Accepted")
	{
	//	print("processesing accept")
		RCN_Accept(ndoc)
		RCN_Check_Remediation(ndoc)
	}
	
	}
	catch(err)
	{
		print("RCN_Compare_onsave error " + err.toString())
	}
}