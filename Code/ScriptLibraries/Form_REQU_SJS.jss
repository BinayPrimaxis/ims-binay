function REQU_GetFieldLabel (strFieldName)
{
	return !applicationScope.containsKey("REQU_Labels") || !applicationScope.get("REQU_Labels")[strFieldName] ? strFieldName : applicationScope.get("REQU_Labels")[strFieldName]
}

function REQU_SetFieldLabelsScope ()
{
	if (applicationScope.containsKey ("REQU_Labels")) {return}
	var labels = fromJson(GetKeyword ("REQU_Dynamic_FieldLabels"));
	applicationScope.put ("REQU_Labels", labels);
}

function REQU_BeforePageLoad(){
	
	var docX:NotesXspDocument = document1;
	
	//get the dynamic labels if we dont have them
	REQU_SetFieldLabelsScope();

	//Get User Details, if not found, redirect to no access page
	if(docX.isNewNote()){
		var strUserName = session.getEffectiveUserName();
		var strSName = session.createName(strUserName).getCommon();
		
		if(IsAuth_Domino()){
			
			var docNAB:NotesDocument = GetDB_NAB().getView('($Users)').getDocumentByKey(strUserName, true);
			if(docNAB == null){context.redirectToPage('/xNoAccess.xsp?key=NAD')}			
			docX.setValue("REQU_EffectiveUserName", strUserName);
			docX.setValue("REQU_FirstName", docNAB.getItemValueString('FirstName'));
			docX.setValue("REQU_LastName", docNAB.getItemValueString('LastName'));
			docX.setValue('REQU_FullName', @Trim(docNAB.getItemValueString('FirstName') + ' ' + docNAB.getItemValueString('LastName')));
			docX.setValue("REQU_Email", docNAB.getItemValueString('InternetAddress'));
			docX.setValue("DocKey", docNAB.getItemValueString('EmployeeID'));
						
		} else {
			
			var strSearchString = @ReplaceSubstring(GetKeyword('LDAP_User_SearchString'), 'SEARCH_KEY', strSName);
			var ldap:com.primaxis.LDAPConnectAD = new com.primaxis.LDAPConnectAD(session);
			
			//Send Distinguished Name Search itself
			strSearchString = strUserName;
			strSearchString = @ReplaceSubstring(strSearchString, ",", "\\\\,");
			strSearchString = @ReplaceSubstring(strSearchString, "/OU", ",OU");
			strSearchString = @ReplaceSubstring(strSearchString, "/DC", ",DC");
			strSearchString = @ReplaceSubstring(strSearchString, "/CN", ",CN");
			strSearchString = "(distinguishedName=" + strSearchString + ")";			
			
			var userList = ldap.SearchUser(strSearchString);					
			if(userList.length == 0){context.redirectToPage('xNoAccess.xsp?key=NAD')}
			if(userList.length > 1){context.redirectToPage('xNoAccess.xsp?key=MAD')}
			
			docX.setValue("REQU_EffectiveUserName", strUserName);
			docX.setValue("REQU_FirstName", userList[0][1]);
			docX.setValue("REQU_LastName", userList[0][2]);
			docX.setValue("REQU_FullName", userList[0][1] + " " + userList[0][2]);
			docX.setValue("REQU_Email", userList[0][3]);
			docX.setValue("DocKey", userList[0][4]);			
		}
	
	} else {
		AppForm_BeforePageLoad(docX)
	}
	
}

function REQU_PostSave(){	
	var docX:NotesXspDocument = document1;
	//Means document is not new save
	if(docX.hasItem('ModifiedBy')){
		//Requestor has saved their own document
		if(IsUser_Requestor()){
			User_ResetSessionScopes();
			view.postScript("parent.window.location.href = 'xRedirectToHome.xsp';")
		}
	}	
}

function REQU_Location_TypeaheadURL(){	
	var docX:NotesXspDocument = document1;
	var strSite = GetValue('REQU_Site');
	var strURL = './xJson_Data.xsp?Frm=BOOK&TAType=Location'
	if(strSite != ''){strURL += '&Site=' + strSite;}
	strURL += '&taQuery=';
	return strURL
}

function REQU_Location_OnChange(){
	var docX:NotesXspDocument = document1;
	var vExistVals = []; var bSetSite = true;
	if (docX.hasItem('REQU_Location')){
		vExistVals = docX.getItemValue('REQU_Location');
		bSetSite = false;
	}
	
	var fldObj:com.ibm.xsp.component.xp.XspInputText = getComponent('fld_AddValue');
	var strValue = @Trim(fldObj.getValue());
	fldObj.setValue(null);
	
	var strLoc = @Trim(@Left(strValue, '(Site:'));
	var siteCode = @Trim(@Left(@Right(strValue, '(Site:'), ')'));
	var siteName = KeywordLabelValue(siteCode, 'BOOK_Site');
	
	if(vExistVals.indexOf(strLoc)>=0){
		view.postScript("Notify_Warning('ERROR: <strong>" + strLoc + "</strong> already added to Locations')"); 
		return
	} else {
		vExistVals.push(strLoc);	
	}
	
	if(bSetSite){
		docX.replaceItemValue('REQU_SiteCode', siteCode);
		docX.replaceItemValue('REQU_Site', siteName);
	}	
	docX.replaceItemValue('REQU_Location', @Trim(vExistVals.sort()));
}






