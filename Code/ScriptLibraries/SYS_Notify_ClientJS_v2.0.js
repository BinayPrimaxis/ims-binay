var notifyLoading = null;

function Notify_Loading(bShow, strElementDisable){
	if(bShow){
		notifyLoading = $.notify({
			icon:'glyphicon glyphicon-refresh glyphicon-refresh-animate',
			message: 'Loading.. please wait'
		},{
			type: 'info',
			delay: 0,
			z_index: 1050, 
			allow_dismiss: false,
			placement: {align: 'center'}			
		});
		
		if(strElementDisable){$('.' + strElementDisable).attr('disabled','disabled')}
		
	} else {
		notifyLoading.close();
		notifyLoading = null;
		if(strElementDisable){$('.' + strElementDisable).removeAttr("disabled")}
	}	
}

function Notify_Warning(strMsg){	
	$.notify({icon: 'glyphicon glyphicon-warning-sign', message: strMsg},{type: 'danger', z_index:1050, placement: {align: 'center'}});	
}

function Notify_Info(strMsg){	
	$.notify({ icon: 'glyphicon glyphicon-ok-circle', message: strMsg}, {type: 'success', z_index:1050, placement: {align: 'center'}} );	
}

var notify_Save = null;

function Notify_Save(strMsg, iconType, strTitle, strType){
	
	this.iconType = function(iconType){
		if(iconType == "Spinner"){
			return "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>";
		} else if(iconType == "Success"){
			return "<span class='glyphicon glyphicon-ok'></span>";
		} else if(iconType == "Error"){
			return "<span class='glyphicon glyphicon-remove'></span>"
		} else {
			return "";
		}
	}
	
	this.notiType = function(iconType){
		if(iconType == "Spinner"){
			return "warning";
		} else if(iconType == "Success"){
			return "success";
		} else if(iconType == "Error"){
			return "danger"
		} else {
			return "";
		}
	}
	
	if(strMsg == 'CLOSE'){
		if(notify_Save != null){notify_Save.close()}
		notify_Save = null;
		return
	}
	
	if(!strTitle){strTitle = '<span style="font-weight:700"> SAVING</span>'}
	if(iconType == "Error"){strTitle = '<span style="font-weight:700"> ERROR</span>'}
	
	if(notify_Save == null){
		
		notify_Save = $.notify({
			title: this.iconType(iconType) + strTitle,
			message: '<br/>' + strMsg
		},
		{
			type: this.notiType(iconType),
			z_index:1050,
			placement: {align: 'center'}
		});		
		
	} else {		
		notify_Save.update("title", this.iconType(iconType) + strTitle)
		notify_Save.update('message', '<br/>' + strMsg);
		notify_Save.update('type', this.notiType(iconType));	
	}	
}

function SaveNotification_OnStart(salert){if(salert==null){salert="Saving Document"};Notify_Save(salert,'Spinner')}
function SaveNotification_OnComplete(salert){if(salert==null){salert="Successfully saved"};Notify_Save(salert,'Success')}
function SaveNotification_OnError(salert){if(salert==null){salert="This document could NOT be saved, please try again or contact IT Support"};Notify_Save(salert, 'Error')}
function SaveNotification_Close(){Notify_Save('CLOSE')}
function DelNotification_OnError(){Notify_Save('This document could NOT be deleted, please try again or contact IT Support', 'Error')}
