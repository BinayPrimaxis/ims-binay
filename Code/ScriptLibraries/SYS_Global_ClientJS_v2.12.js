var vValidateCondition = [];
if (!Array.prototype.indexOf) {	
	Array.prototype.indexOf = function(obj, start) {
	     for (var i = (start || 0), j = this.length; i < j; i++) {
	         if (this[i] === obj) { return i; }
	     }
	     return -1;
	}	
}

function cLog(strMsg){console.log(strMsg)}

function AppForm_Menu(IsDBAdmin, IsManager, IsCoordinator, IsInterpreterAlso){	
	//cLog("IsManager - " + IsManager + ', IsCoordinator - ' + IsCoordinator +', IsInterpreter - ' + IsInterpreterAlso);
	
	// Remove parent if all are false
	if(!IsDBAdmin && !IsManager && !IsCoordinator && !IsInterpreterAlso){$('.scSettingParent').remove(); return}	
	// Remove parent if just Coordinator
	//if(IsCoordinator && !(IsManager || IsInterpreterAlso || IsDBAdmin)){$('.scSettingParent').remove(); return}
	// Remove parent if just Interpreter
	if(IsInterpreterAlso && !(IsManager || IsCoordinator || IsDBAdmin)){$('.scSettingParent').remove(); return}
	
	// Remove Manage users and keywords if not manager
	if(!(IsManager | IsDBAdmin)){$('.scManager').remove();}	
	if(!IsDBAdmin){$('.scDBAdmin').remove();}
	if(IsDBAdmin){$('.scKWDMgr').remove();}
	
	// Remove Interpreter if not in role
	if(!IsInterpreterAlso){$('.scInterpreter').remove();}	
	// Remove Coordinator if not in role
	if(!(IsCoordinator | IsDBAdmin)){$('.scCoordinator').remove();}	
	//Remove ManagerCoordinator if not any of them
	if(!(IsManager | IsCoordinator | IsDBAdmin)){$('.scMgr_scCoord').remove();}
}

function ConfirmAction(strMsg, btnName, strFunction){
	
	$('.confirmMessage').html(strMsg);
	var dialogModal = $('#dlgConfirmation');
	dialogModal.modal({backdrop: 'static', keyboard: false, show:false});
		
	dialogModal.on('click', '.btn-ok-proceed', function(e) {
		console.log("test click - " + btnName)
		if(btnName == null){
			eval(strFunction)
		} else {
			console.log("preclick="+btnName);
			var btn = dojo.query('button[name$="' + btnName + '"]')[0]; 
			console.log(btn);
			btn.click();
		}	
		$('#dlgConfirmation').modal('hide');
    });
	
	dialogModal.modal('show');
	dialogModal.on('hidden.bs.modal', function () {
	    $(this).data('bs.modal', null);
	    dialogModal.off('click', '.btn-ok-proceed');
	});
}

function isNumberKey(objEvent, strClass){
	var fld = $('.' + strClass);
	var str = fld.val();
	str = str.replace(/[^0-9\.]/g, '');
	fld.val(str);
}

function strTrim(strValue){
	if (typeof(strValue) == "undefined"){return ''}
	if (strValue == null){return ''}
	return strValue.replace(/^\s*/, "").replace(/\s*$/, "")
}

function UpdateFTIndex(strURL){dojo.xhrPost({url: strURL, load: function(data) {}})}
function Names_Multiple_ClearField(strId){var fld = document.getElementById(strId); fld.focus();}

//This function is called on Close of Modal and Add Another Button
function ClearAllFields(strID){
	//cLog('ClearAllFields is called for ID - ' + strID)
	$('#' + strID).find(':input').each(function() {
		switch(this.type) {
			case 'password':
			case 'text':
			case 'textarea':
			case 'file':
			case 'select-one':
			case 'select-multiple':
				$(this).val('');
				break;
			case 'checkbox':
			case 'radio':
				this.checked = false;
		}
		if($(this).attr('class') == 'docUNID'){$(this).val('NEW')}
	});		
}

function AppForm_OnPageLoad_CJS(strForm){
	AppForm_Validation_AssignClass(strForm);
	$('[data-toggle="tooltip"]').tooltip();
	if (strForm=="RCN_Upload"){
			init_ready();
	}
}

function AppForm_Validation_AssignClass(strForm){
	
	var jsonObject = AppForm_Validation_GetJsonObj(strForm);
	
	for(var i=0; i<jsonObject.length;i++){
		
		var objJson = jsonObject[i];
		var fld = $('.fld_' + objJson.fldName);
		if(objJson.fldSearchString != null){fld = $(objJson.fldSearchString)}
		if(objJson.NotClass != null){fld = $('.fld_' + objJson.fldName).not(objJson.NotClass)}
		
		//Skip this iteration, same as next
		if(fld.length == 0){continue;}
		
		var strValCondition = objJson.validateCondition;
		if(strValCondition != null && vValidateCondition.indexOf(objJson.fldName) < 0){
			vValidateCondition.push(objJson.fldName);
			vValidateCondition.push(strValCondition);				
		}
								
		// Change Div Class based on field Value
		var divObj = $('.div_' + objJson.fldName);
		
		if(AppForm_Validation_Field(fld, strValCondition)){			
			$(divObj).removeClass('has-success'); 
			$(divObj).addClass('has-error');
		} else {
			$(divObj).removeClass('has-error'); 
			$(divObj).addClass('has-success');
		}
		
		// Add on focus and blur class for fields
		fld.focus(function(){
			var id = this.id			
			var fldName = id.substring(id.lastIndexOf(':')+1);
			var divObj = $('.div_' + fldName);
			$(divObj).removeClass('has-error');
			$(divObj).removeClass('has-success');
		});		
		
		fld.blur(function(){
			var id = this.id;
			var fldName = id.substring(id.lastIndexOf(':')+1);
			var divObj = $('.div_' + fldName);
			var fld = $(this);
			var strValCondition = null;
			
			var pos = vValidateCondition.indexOf(fldName);
			if(pos >= 0){strValCondition = vValidateCondition[pos+1]}
			
			if(AppForm_Validation_Field(fld, strValCondition)){				
				$(divObj).addClass('has-error');
			} else {				
				$(divObj).addClass('has-success');
			}
		});		
	}	
}

function AppForm_Validation_GetJsonObj(strForm){	
	var strJSON = $('.' + 'fldsMandatory_' + strForm).html();
	if(strJSON != null && strJSON != ''){return eval(strJSON)}
	
	cLog('NO JSON FieldValidation found for ' + strForm);
	
	return [];		
}

function AppForm_Validation_GetResult(strForm){	
	
	var jsonObject = AppForm_Validation_GetJsonObj(strForm);
	
	var vMsg = [];
	
	for(var i=0; i<jsonObject.length; i++){
		var objJson = jsonObject[i];
		var fld = $('.fld_' + objJson.fldName);		
		if(objJson.fldSearchString != null){fld = $(objJson.fldSearchString);}
		if(objJson.NotClass != null){fld = $('.fld_' + objJson.fldName).not(objJson.NotClass)}
		
		AppForm_Validation_Field(fld, objJson.validateCondition, vMsg, objJson.label,objJson.valType);
	//	console.log(objJson.fldName + ": ("+objJson.label+") --->" + AppForm_Validation_Field(fld, objJson.validateCondition, vMsg, objJson.label,objJson.valType))
		//if(fld.length > 0){
			//AppForm_Validation_Field(fld, objJson.validateCondition, vMsg, objJson.label,objJson.valType);			
		//}	
	}	
	return vMsg;
}

function AppForm_Validation_Field(fld, validateConditionS, vMsg, label,valType){
	// Return false means, no validation required
	//console.log('in appform validation = ');
	 
	this.IsFieldNull = function(fld, validateCondition,addLabel){
		if (fld.length == 0 && valType=="Must Exist"){vMsg.push(label);return true}
		if(fld.length == 0){return false}
		if (addLabel==null){addLabel = true}
		
		if(fld.length > 1 || fld.attr('type') == 'checkbox'){
			var strValue = this.RadioCheckedValue(fld)
		} else {
				if(fld.prop("tagName") == 'DIV' || fld.prop("tagName") == 'SPAN'){
				var strValue = fld.html();				
			} else {
				var strValue = fld.val()
			}			
		}
		
		strValue = strTrim(strValue);

		// Return value could be True or False
		var bReturn = (strValue == null || strValue == '');
		// If there is validate Condition, then need to check that also
		if(validateCondition != null){
			// if validate condition is false, then return false
			
			if(this.FieldValidateCondition(validateCondition) == false){				
				// just return 'cos validation condition not true, so no need to validate
				if(label){return false} else {return false}
			}
			
		}
		
		// Field is null, return true or append label to vMsg
		if(bReturn){if(label && addLabel){
			console.log("label added to vMsg");
			if (vMsg.indexOf(label)<0){vMsg.push(label)}
			return true
			} else {return true}}
		
		// If bReturn is false (means some value) & also has number class, then
		// check for integer value
		if(fld.hasClass('fldNumber') && isNaN(strValue)){
			if(label && addLabel){vMsg.push(label + " - only number value"); return true} else {return true}						
		}
		
		
		if(fld.hasClass('BOOK_Language_Match')){
					if(label && addLabel)
						{
							vMsg.push(label);
						}
			}
	
		
		// Field has some value, validation passed
		if(label){return false} else {return false}
	};
	
	this.FieldValidateCondition = function(validateCondition){
		var fld = $(validateCondition.fldSearchString);
		var strValue = '';
		
		if(fld.length > 1  || fld.attr('type') == 'checkbox'){
			strValue = this.RadioCheckedValue(fld)
		} else {			
			if(fld.prop("tagName") == 'DIV' || fld.prop("tagName") == 'SPAN'){
				var strValue = fld.html();
				
			} else {
				var strValue = fld.val()
			}
		}
		
		///check for language field for a false value then return validation error
		if(fld.text()=="false")
			{
			console.log("value:"+validateCondition.value);
				return true;
			}
		
		//TODOD = 
		/*if (strValue == "fldMatchLanguage") {
			strValue = $("span[id='fldMatchLanguage']").val();
		}*/
		
		
		var checkValue = validateCondition.value.split('~');
		if(strValue == null){return false}
		var compare = "EQ"
			if (validateCondition.eq){compare = validateCondition.eq}
		for(var i=0; i<checkValue.length; i++){
			if(strValue.indexOf(checkValue[i])>=0){
				if (compare=="EQ")	{return true}
				if (compare=="NOT")	{return false}	
				}
		}
		if (compare=="NOT")	{return true}	
		return false;		
	};
	
	this.RadioCheckedValue = function(Obj){		
		for(var i=0; i<Obj.length; i++){
			var fld = $(Obj[i]);
			if(fld.is(':checked')){return fld.val()}
		}
		return '';
	};
	
	if (Array.isArray(validateConditionS))
		{
		
		for (var j=0;j<validateConditionS.length;j++)
			{
			var bAns = this.IsFieldNull(fld, validateConditionS[j],false)
				if (bAns==false){return false}
	
			
			
			}

		if (vMsg!=null){
			if (vMsg.indexOf(label)<0){vMsg.push(label)}
		}
		return true
		}
	else
		{
		return this.IsFieldNull(fld, validateConditionS)	
		}
	
}

function Global_Save_Validation(strForm, strBtn, postMsg) {
	trimFieldValues();
	var vMsg = AppForm_Validation_GetResult(strForm);
		
	if (postMsg){		
	vMsg = vMsg.concat(postMsg)
	}
	if(vMsg.length == 0){
		if(!strBtn){var strBtn = 'btnSave'}
		$('.' + strBtn).click();
	} else {		
		$('.scFVMessage').html('<ul><li>' + vMsg.join('</li><li>') + '</li></ul>'); 
		$('#dlgFieldValidation').modal('show');
	}	
}

/**
 * Trim white space from fields
 * 
 * @returns
 */
function trimFieldValues () {	
	var form = $('form');
	form.each(function() {
		var formFields = $(this).find(':input');
		formFields.each(function() {
			switch(this.type) {
				case 'text':
					$(this).val($.trim($(this).val()));
					break;
				case 'textarea':
					$(this).val($.trim($(this).val()));
					break;
			}
		});
	});	
}

function JQ_ShowHide(strClass, bShow){
	var objClass = $('.' + strClass);
	// "in" classname means object is already visible
	if(bShow & objClass.hasClass('in') == false){objClass.collapse("show")}
	if(!bShow & objClass.hasClass('in')){objClass.collapse("hide")}	
}

function JQ_HideSC(obj, bShow){
	var strClass = 'txt_Hide';
	if(bShow & obj.hasClass(strClass)){obj.removeClass(strClass)}
	if(!bShow & obj.hasClass(strClass) == false){obj.addClass(strClass)}	
}

function toggleAllSections(){	
	
	var spanObj = $('.scLinkExpCollAllText');
	var hrefObj = $('.scLinkExpCollAll');
	
	if(spanObj.html() == ' Collapse All'){	
		spanObj.html(' Expand All');		
		$('.btnExpCollAll').collapse('hide');
		$('.linkCollapse').addClass('collapsed');
		hrefObj.removeClass('glyphicon-collapse-up');
		hrefObj.addClass('glyphicon-collapse-down');
		
	} else {
		spanObj.html(' Collapse All');
		$('.btnExpCollAll').collapse('show');
		$('.linkCollapse').removeClass('collapsed');
		hrefObj.addClass('glyphicon-collapse-up');
		hrefObj.removeClass('glyphicon-collapse-down');
	}	
}

function panelExpandCollapse(objID){	
	var linkObj = document.getElementById(objID);
	var vClass = linkObj.className.split(" ");
	if(vClass.indexOf('collapsed')>=0){vClass.pop('collapsed')} else {vClass.push('collapsed')}
	linkObj.className = vClass.join(" ");
}

function Duration_UpDown(strClass, intUpDown){
	var fld = $('.' + strClass);
	var fldVal = fld.val();
	if(isNaN(fldVal) || fldVal == ""){fldVal = 0} else {fldVal = parseInt(fldVal)}
	var intVal = fldVal + intUpDown;
	if(intVal < 5){return}
	fld.val(intVal.toString());	
}

function ChangePwd_Open(){
	var dialogModal = $('#dlg_ChangePassword');
	dialogModal.modal({backdrop: 'static', keyboard: false, show:false});		
	dialogModal.modal('show');
	dialogModal.on('hidden.bs.modal', function(e){ClearAllFields('dlg_ChangePassword')});	
}

function Patient_Details_Open(){
	
	//Get Patient URN Automatically
	var strURN = '';
	//Check if Booking Form
	if(window.parent.document.getElementById('divForm').style.display == ''){
		var iFrameID = document.getElementById('iframeDoc');
	    if(iFrameID) {
	    	if(iFrameID.src.indexOf('xBOOK.xsp')>=0){
	    		var fldURN = $("#iframeDoc").contents().find('.fld_BOOK_PatientUR');
	    		if(fldURN.length == 1){
	    			strURN = fldURN.val();
	    		} else {
	    			var divURN = $("#iframeDoc").contents().find('.div_BOOK_PatientUR');
		    		if(divURN.length == 1){
		    			strURN = divURN.html();
		    		}
	    		}	    		
	    	}			
	    }		
	} else if(window.document.getElementById('divView').style.display == ''){
		//If Interpreter need to check which Grid is open first
		var spanGridCurrent = $('.scCurrentGridID');
		if(spanGridCurrent.length == 1){
			var strGridID = spanGridCurrent.html();
			if(strGridID == 'GRID_INT_All'){strGridID = 'listxGrid'} else {strGridID = 'listx_' + strGridID}
			//cLog(strGridID + "~~~~~~~")
			strURN = Grid_GetSelected_URN(strGridID);
		} else{
			//Date Range Grid for Coordinators & Requestors
			strURN = Grid_GetSelected_URN('listxGrid');
		}		
	}
	
	var dialogModal = $('#dlg_Patient_Details');
	dialogModal.modal({backdrop: 'static', keyboard: false, show:false});		
	dialogModal.modal('show');
	dialogModal.on('hidden.bs.modal', function(e){$('.fldPatientURN_Details').val('')});
	
	if(strURN != ''){
		cLog(strURN);
		$('.fldPatientURN_Details').val(strURN);
		Patient_Details_Get();
	}
	
}

function Patient_Details_Clear(){	
	$('.fldPatientURN_Details').val('');
	//Show URN Field DIV
	JQ_ShowHide("divPatient_Details_URN", true);
	JQ_ShowHide("divPatient_Details_Tabs", false);
	// Hide Data Error, Data, Attendance Error, Attendance
	JQ_ShowHide("divPatient_Attendance", false);
	JQ_ShowHide("divPatient_Attendance_Error", false);
	JQ_ShowHide("divPatient_Data", false);
	JQ_ShowHide("divPatient_Data_Error", false);
	$('.hd_Patient_Details').html('Patient Details');
}

function Patient_Details_Get(){
	
	var strURN = strTrim($('.fldPatientURN_Details').val());
	if(strURN == ''){
		Notify_Warning('Patient URN is blank'); return
	} else if(isNaN(parseInt(strURN))){
		Notify_Warning('Patient URN should only be Numeric'); return
	}
	
	JQ_ShowHide("divPatient_Details_URN", false);
	JQ_ShowHide("divPatient_Details_Tabs", true);
	$('.hd_Patient_Details').html('Patient Details (URN - ' + strURN + ')')
	Patient_Data_Get(strURN);
	Patient_Attendance_Get(strURN);	
}
	
function Patient_Data_Get(strURN){
	
	var bError = false;
	
	$.ajax({
		url: "agtPatientData?openagent&URN=" + strURN,
		cache: false,
		dataType: "json",
		success: function(data) {
			//cLog(data);						
			$.each( data, function( key, val ) {			
				if(key == "ERROR"){
					$('.scError_Data').html(val);
					bError = true;
				} else {
					$('.sc' + key).html(val);
				}	
			});									
		},
		error: function (xhr,status,error){
			bError = true;
			$('.scError_Data').html(error + "<br/>Please contact IT Support");
		},
		complete: function(){
			JQ_ShowHide("divPatient_Data", (bError == false));
			JQ_ShowHide("divPatient_Data_Error", bError);
		}
	});
}

function Patient_Attendance_Get(strURN){
	
	var bError = false;
	
	$.ajax({
		url: "agtPatientAttendance?openagent&URN=" + strURN,
		cache: false,
		dataType: "json",
		success: function(data) {
			//cLog(data)
			var strHTML = ''; var strRow; var bST;			
			for(var i=0; i<data.length; i++){	
				if(data[i].ERROR != null){
					$('.scError_Attendance').html(data[i].ERROR);					
					bError = true
				} else {
					strRow = ""; bST = false;
					$.each( data[i], function( key, val ) {
						if(key == "Status"){if(val == "Cancelled"){bST = true}}
						if(key == 'row'){val += '.'}
						strRow += '<td>' + val + '</td>';					
					});
					if(bST){strHTML += '<tr class="strike-through">'} else {strHTML += '<tr>'}
					strHTML += (strRow + '</tr>');
				}
			}
			$('.divPatient_Attendance_tbody').html(strHTML);
		},
		error: function (xhr,status,error){
			bError = true;
			$('.scError_Attendance').html(error + "<br/>Please contact IT Support");
		},
		complete: function(){
			JQ_ShowHide("divPatient_Attendance", (bError == false));
			JQ_ShowHide("divPatient_Attendance_Error", bError);
		}
	});
}

function Dialog_Message(strTitle, strBody, strExecuteOnClose){
	
	var dialogModal = $('#dlgMessage');
	$('.dlgMessage_Title').html(strTitle);
	$('.dlgMessage_Body').html(strBody);
	dialogModal.modal({backdrop: 'static', keyboard: false, show:false});		
	dialogModal.modal('show');	
	if(strExecuteOnClose && strExecuteOnClose != ""){
		dialogModal.on('hidden.bs.modal', function(){
			eval(strExecuteOnClose); strExecuteOnClose = "";
		});
	}
}

function IFrame_OpenDoc(strUNID, strForm, bEdit){
	var strAction = '&action=openDocument';
	if(bEdit){strAction = '&action=editDocument'}
	if(!strForm || strForm == 'BOOK'){	
		var strURL = 'xBOOK.xsp';
		if(strUNID != ''){strURL += '?documentId=' + strUNID + strAction}
	} else if (strForm == "CBN"){
		var strURL = 'xCBN.xsp';
		if(strUNID != ''){strURL += '?documentId=' + strUNID + strAction}	
	} else if (strForm =="RCN_Upload" || strForm =="RCN" ){
		var strURL = 'xRCN.xsp?KEY=' + strForm;
		if(strUNID != ''){strURL += '&documentId=' + strUNID + strAction}
	}	else {
		var strURL = 'xForm.xsp?KEY=' + strForm;
		if(strUNID != ''){strURL += '&documentId=' + strUNID + strAction}
		//console.log('strURL = ' + strURL);
	}		
	// cLog(strURL)
	$('#iframeDoc').attr('src', strURL);
	$('#iframeDoc').iFrameResize({log:false, heightCalculationMethod: 'lowestElement'});
	
	window.parent.document.getElementById('divView').style.display = 'none';
	window.parent.document.getElementById('divForm').style.display = '';
	
	/*if(document.selection && document.selection.empty) {
		document.selection.empty();
	} else if(window.getSelection) {
		var sel = window.getSelection();
		sel.removeAllRanges();
	}*/
}

function IFrame_OpenChildDoc(strUNID, gridObject,strSecondUNID){
	//console.log("IFrame_OpenChildDoc for " + gridObject.SubForm)
	var strAction = '&action=openDocument';
	if(gridObject.forceEdit){strAction = '&action=editDocument'}
	if (gridObject.SubForm =="RCN_Compare"){
		var strURL = 'xRCN_Compare.xsp?KEY=' + gridObject.SubForm;
		if(strUNID != ''){strURL += '&documentId=' + strUNID + strAction}
		if (strSecondUNID){strURL+='&secondUNID='+strSecondUNID}
	}	else {
		var strURL = 'xForm.xsp?KEY=' + gridObject.SubForm;
		if(strUNID != ''){strURL += '&documentId=' + strUNID + strAction}
	}		
	// cLog(strURL)
	$('#iframeDocChild').attr('src', strURL);
	$('#iframeDocChild').iFrameResize({log:false, heightCalculationMethod: 'lowestElement'});
	
	document.getElementById('divViewChild').style.display = 'none';
	document.getElementById('divFormChild').style.display = '';
	
	/*if(document.selection && document.selection.empty) {
		document.selection.empty();
	} else if(window.getSelection) {
		var sel = window.getSelection();
		sel.removeAllRanges();
	}*/
}

function IFrame_Close(){
	window.parent.document.getElementById('divView').style.display = '';
    window.parent.document.getElementById('divForm').style.display = 'none';
	var iFrameID = document.getElementById('iframeDoc');
    if(iFrameID) {iFrameID.src == ''; iFrameID.height = "0px";}
}

function IFrameChild_Close(){
	window.parent.document.getElementById('divViewChild').style.display = '';
	window.parent.document.getElementById('divFormChild').style.display = 'none';
	var iFrameID = document.getElementById('iframeDocChild');
    if(iFrameID) {iFrameID.src == ''; iFrameID.height = "0px";}
}
function BOOK_UpdateAgencyList_OnComplete(){
	// $('#dlg_AssignAgency').modal(); return
	var selObj = $('.fldAgency option');	
	if(selObj.size() == 2){
		selObj.eq(1).prop('selected', true);
		$('.btnAgencyUpdate').click();
	} else {
		selObj.eq(0).prop('selected', true);
		$('#dlg_AssignAgency').modal();
	}
}

function BOOK_UpdateInterpreterList_OnComplete(){	
	var selObj = $('.fldInterpreter option');	
	// $('#dlg_AssignInterpreter').modal(); return
	if(selObj.size() == 2){
		selObj.eq(1).prop('selected', true);
		$('.btnInterpreterUpdate').click();
	} else {
		selObj.eq(0).prop('selected', true);
		$('#dlg_AssignInterpreter').modal();
	}
}

function Report_DateRange(){	
	var dtStart = moment().subtract(1, 'months').date(1);
	var dtEnd = moment().subtract(1, 'months').endOf('month');		
    $('.divReportRange').daterangepicker({
        startDate: dtStart,
        endDate: dtEnd,        
        ranges: {
           'Previous Month': [moment().subtract(1, 'months').date(1), moment().subtract(1, 'months').endOf('month')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Next Month': [moment().add(1, 'month').date(1), moment().add(1, 'month').endOf('month')]
        },
        locale: {format: "YYYY-MM-DD"}
    }, Report_DateRange_CallBack);

    Report_DateRange_CallBack(dtStart, dtEnd);
}

function Report_DateRange_CallBack(dtStart, dtEnd) {	
	$('.divReportRange input').val(dtStart.format('DD MMM YYYY') + '  -  ' + dtEnd.format('DD MMM YYYY'));	
}

function Report_Generate_OnComplete() {
	Notify_Loading(false, 'btnGenerate'); 
	var bLoad = $('#tblSummary').length > 0;
	$('.btnPrint').prop('disabled', !bLoad);
	$('.btnExport').prop('disabled', !bLoad);	
	Report_ChartGenerate(bLoad);
}

var getQueryString = function ( field, url ) {
    var href = url ? url : window.location.href;
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
};

function getDocHeight(doc) {
    doc = doc || document;
    // stackoverflow.com/questions/1145850/
    var body = doc.body, html = doc.documentElement;
    var height = Math.max( body.scrollHeight, body.offsetHeight, 
        html.clientHeight, html.scrollHeight, html.offsetHeight );
    return height;
}

function setIframeHeight(id) {
    var ifrm = document.getElementById(id);
    var doc = ifrm.contentDocument? ifrm.contentDocument:ifrm.contentWindow.document;
    ifrm.style.visibility = 'hidden';
    ifrm.style.height = "10px"; // reset to minimal height ...
    // IE opt. for bing/msn needs a bit added or scrollbar appears
    ifrm.style.height = getDocHeight( doc ) + 4 + "px";
    ifrm.style.visibility = 'visible';
}

function BtnDisable(strClass){
	$('.' + strClass).attr('disabled','disabled');
	setTimeout(function(){BtnEnable(strClass)}, 3000);	
}

function BtnEnable(strClass){$('.' + strClass).removeAttr('disabled')}

function TypeAhead_Initialize(strURL, fldClass, fldLabel, bLoadAll, bNonListItemAllowed, minChar, strChangeFunction, itemAppend){
	var fld = $("." + fldClass);	
	var bSelected = false;
	if(itemAppend != ''){strURL += '&ItemApp=' + itemAppend;}	//Not used so far, just in case
	//cLog(strChangeFunction)
	
	var TAOptions1 = {
		//hint: true,
		highlight: true,
		minLength: minChar
	};

	var btnCreateBooking = $('.btnNewBooking');
	//This is for small list and avoid sending calls to server, instead throw full list in initialize
	//Bloodhound prefetch is used here
	if(bLoadAll){
		
		var TAList = new Bloodhound({
	        datumTokenizer: Bloodhound.tokenizers.whitespace,
	        queryTokenizer: Bloodhound.tokenizers.whitespace,
	        prefetch: {url:strURL, cache:false}
	    });
		
		var TAOptions2 = {
			name: 'TAList',
			source: TAList,
			limit: 100
		};
		
	} else {
		
		var TAList = new Bloodhound({
	        datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
	        queryTokenizer: Bloodhound.tokenizers.whitespace,        
	        remote: {
	        	url: strURL + "%QUERY",
	        	wildcard: "%QUERY",
	        	filter: function(x) {
					return $.map(x, function(item) {
						//cLog(item)
						return {value: item};
					});
				}
	        }	
	    });
		
		TAList.initialize();
		
		var TAOptions2 = {
			name: 'value',
			displayKey: 'value',
			source: TAList.ttAdapter(),
			limit: 100
		};		
	}
	
	fld.typeahead(TAOptions1, TAOptions2)
		.on('typeahead:asyncrequest', function(ev, data) {
			if (fld.closest(".panel").has(".waitLoading").length == 0) {
				fld.closest(".panel").append("<div class='waitLoading'></div>");
			}
			if (fld.closest(".modal-content").has(".waitLoading").length == 0) {
				fld.closest(".modal-content").append("<div class='waitLoading'></div>");
			}
			$(".waitLoading").css("display", "block");
		})
		.on('typeahead:asynccancel typeahead:asyncreceive', function(ev, data) {
			$(".waitLoading").css("display", "none");
		})
		.bind('typeahead:select', function(ev, suggestion) {
			bSelected = true;
			//hide Create Booking button once got selected
			btnCreateBooking.hide();
			//cLog('Selection: ' + suggestion);
			if(strChangeFunction != ""){eval(strChangeFunction)}			
		})
		.change(function(){
			//cLog('change Triggered'); cLog('bSelected: ' + bSelected);
			
			if(bSelected){
				bSelected = false;	//reset flag
				return
			}
			
			//Non list items are not allowed, throw error
	  		if(bNonListItemAllowed == false){
	  			Notify_Warning('<strong>ERROR: ' + fld.val() + '</strong> not in list of ' + fldLabel);
		  		fld.typeahead('val', '');
		  		return;
	  		}
	  		
	  		//It will come here only if non list item and change function present
	  		if(strChangeFunction != ""){
	  			eval(strChangeFunction);
	  		}	  			  		
		});
}

function BOOK_New_URNSearch(){
	
	var bPMS = ($('.scIsPMS').html() == 'true');	
	
	if(bPMS){
		var dialogModal = $('#dlg_SearchURN_PMS');
		dialogModal.modal({backdrop: 'static', keyboard: false, show:false});		
		dialogModal.modal('show');
		
	} else {
		
		var strURL = './xJson_Data.xsp?TAType=URN&Frm=BOOK&VK=List_URN&taQuery=';	
		TypeAhead_Initialize(strURL, "fldPatientURN_IMS", "Patient URN", false, false, 3, '$(".btnNewBookingURN_IMS").click()', '');
		var dialogModal = $('#dlg_SearchURN_IMS');
		var btnCreateBooking = $('.btnNewBooking');
		btnCreateBooking.show();
		dialogModal.modal({backdrop: 'static', keyboard: false, show:false});		
		dialogModal.modal('show');
		
		dialogModal.on('hidden.bs.modal', function(e){
			var fld = $('.fldPatientURN_IMS');
			fld.typeahead('val', '');
			fld.typeahead('destroy');
		});
	}
}

function BOOK_New_URNSeach_Get_PMSData(){
	var strURN = strTrim($('.fldPatientURN_PMS').val());
	//Check Value if ':' is present it means coming from TypeAhead, take left of ':'
	
	if(strURN == ''){
		Notify_Warning('Patient URN is blank'); 
		return
	} else if(isNaN(parseInt(strURN))){
		Notify_Warning('Patient URN should only be Numeric'); 
		return
	}
	
	$('#dlg_SearchURN_PMS').modal('hide'); 	
	Notify_Loading(true, 'btnNewBooking');
	
	//Step 2. Get Patient Data
	var vCols = ['FirstName', 'Surname', 'Gender', 'Address', 'DOB', 'Language'];
	var strJson = null;
	var strURL = "agtPatientData?openagent&URN=" + strURN + "&Query=" + vCols.join("~");
	
	$.ajax({
		url: strURL,
		cache: false,
		dataType: "json",
		success: function(data) {
			strJson = data;
			$.each( data, function( key, val ) {			
				if(key == "ERROR"){
					Notify_Warning('Error Encountered: ' + val);
					strReturn = null;
				}	
			});									
		},
		error: function (xhr,status,error){
			Notify_Warning("Error Encountered: " + error + "<br/>Please contact IT Support"); return
		},
		complete: function(){
			$('.patientData_PMS').val(JSON.stringify(strJson));
			$('.btnNewBookingURN_PMSData').click();
		}
	});	
}

function Form_Save_OnComplete(sGridKey,salert) {
	var strMsg = $('.Form_SaveErrorMessage').html();
	//console.log("**** Form_Save_OnComplete ****")
	//console.log(strMsg);
	//console.log("parameter="+sGridKey)
	if(strMsg == ''||strMsg==undefined||strMsg==null){
		SaveNotification_OnComplete(salert); 
		var gridKey = 'GRID_' + getQueryString('KEY', parent.window.location.href);
		//console.log("gridKey="+gridKey);
		if (sGridKey!=null){
			//console.log("setting gridKey to parameter")
			gridKey = sGridKey
		}
		//console.log("gridKey="+gridKey);
		var gridObj = parent.$('#listx_' + gridKey);
		//console.log(gridObj)
		if(gridObj.length > 0){parent.Grid_Generate(gridKey)}
	} else {
		//If strMsg is not blank, SJS QuerySave returns false, by which document is not saved
		SaveNotification_Close();
		Dialog_Message("DOCUMENT NOT SAVED !!", strMsg);
	}	
}

function Form_Delete_OnComplete(){
	IFrame_Close();
	var gridKey = 'GRID_' + getQueryString('KEY', parent.window.location.href);
	//cLog(gridKey);
	var gridObj = parent.$('#listx_' + gridKey);
	if(gridObj.length != 0){
		parent.Grid_Generate(gridKey);
		return						
	}
	
	//check if its CRD Grid, mainly used by CBN for Delete
	var gridObj = parent.$("#listxGrid");
	if(gridObj.length != 0){
		parent.Grid_Generate_DateRange();
		return						
	}
	
	//cLog("No Grid Object in Parent Window for key - " + gridKey);
}

function ChangeInterpreterHours_Validate(bDateOnCC, strDateFormat, strBookTime, strDuration){
	
	if(bDateOnCC){
		var strBookDt = $('#dlg_ChangeInterpreterHours_Date').html();
		if(strBookDt.length == 0){
			ChangeInterpreterHours_ErrorControl('Date is not set properly'); 
			return false
		}
		
		var dtBook = moment(strBookDt, strDateFormat, true);
		if(dtBook.isValid() == false){
			ChangeInterpreterHours_ErrorControl('Date not valid as per moment class'); 
			return false
		}
	}
		
	var strTimeFormat = 'HH:mm';
	var bSplitTime = $('.fld_SplitHours').is(':checked');
	var strStartTime_1 = strTrim($('.fld_NewTime_Start_1').val());
	var strEndTime_1 = strTrim($('.fld_NewTime_End_1').val());
	
	if(strStartTime_1.length != 5 || strEndTime_1.length != 5 || strStartTime_1.indexOf(':') != 2 || strEndTime_1.indexOf(':') != 2){
		ChangeInterpreterHours_ErrorControl('New Hours are not set properly');
		return false;
	}
	
	if(bSplitTime){
		var strStartTime_2 = strTrim($('.fld_NewTime_Start_2').val());
		var strEndTime_2 = strTrim($('.fld_NewTime_End_2').val());
		if(strStartTime_2.length != 5 || strEndTime_2.length != 5 || strStartTime_2.indexOf(':') != 2 || strEndTime_2.indexOf(':') != 2){
			ChangeInterpreterHours_ErrorControl('New Hours are not set properly at Slot 2');
			return false
		}
	}
		
	var dtStartTime_1 = moment(strStartTime_1, strTimeFormat, true);
	var dtEndTime_1 = moment(strEndTime_1, strTimeFormat, true);
	var strFormat = 'YYYYMMDD hhmm';
	//cLog('dtStartTime_1 -> ' + dtStartTime_1.format(strFormat)); cLog('dtEndTime_1 -> ' + dtEndTime_1.format(strFormat));
	
	if(dtStartTime_1.isAfter(dtEndTime_1, 'minute')){
		ChangeInterpreterHours_ErrorControl('End Time to be greater than Start Time');
		return false
	}	
	if(dtStartTime_1.isSame(dtEndTime_1, 'minute')){
		ChangeInterpreterHours_ErrorControl('Start Time & End Time cannot be same');
		return false
	}
	
	//Exit if No Split Time, No Booking Time and No Duration
	if(bSplitTime == false & !strBookTime & !strDuration){
		ChangeInterpreterHours_ErrorControl('');
		return true
	}
	
	//Below Code if Split Time
	if(bSplitTime){
		var dtStartTime_2 = moment(strStartTime_2, strTimeFormat, true);
		var dtEndTime_2 = moment(strEndTime_2, strTimeFormat, true);
		
		if(dtStartTime_2.isAfter(dtEndTime_2, 'minute')){
			ChangeInterpreterHours_ErrorControl('End Time to be greater than Start Time at Slot 2'); 
			return false
		}
		if(dtStartTime_2.isSame(dtEndTime_2, 'minute')){
			ChangeInterpreterHours_ErrorControl('Start Time & End Time cannot be same at Slot 2');
			return false
		}
		
		//Check there is no overlapping of time between 2 time slots
		if(dtStartTime_2.isBetween(dtStartTime_1, dtEndTime_1, 'minute', '[]')){
			ChangeInterpreterHours_ErrorControl('Slot 2 Start time cannot be between Slot 1 Time range');
			return false
		}
				
		//Exit if No Split Time, No Booking Time and No Duration
		if(!strBookTime & !strDuration){
			ChangeInterpreterHours_ErrorControl('');
			return true
		}
	}
	
	//Compare with Booking Start and End
	var dtBookStart = moment(strBookTime, strTimeFormat, true); 
	var dtBookEnd = moment(strBookTime, strTimeFormat, true);
	dtBookEnd.add(strDuration, 'm');	
	//cLog('dtBookStart ->' + dtBookStart.format(strFormat)); cLog('dtBookEnd ->' + dtBookEnd.format(strFormat));
	
	//No Split, then BookingStart & BookingEnd should be within Slot 1		
	var bBookStartCheck_1 = dtBookStart.isBetween(dtStartTime_1, dtEndTime_1, 'minute', '[]');
	var bBookEndCheck_1 = dtBookEnd.isBetween(dtStartTime_1, dtEndTime_1, 'minute', '[]');
	
	if(bSplitTime == false){
		if(bBookStartCheck_1 == false){
			ChangeInterpreterHours_ErrorControl('Booking Start Time is not between New Slot of Hours');
			return false
		}	
		if(bBookEndCheck_1 == false){
			ChangeInterpreterHours_ErrorControl('Booking End Time is not between New Slot of Hours');
			return false
		}
		ChangeInterpreterHours_ErrorControl('');
		return true;
	}
	
	//Split Slots, which means Booking Start & End to be withing either of the Time Slots chosen by User		
	var bBookStartCheck_2 = dtBookStart.isBetween(dtStartTime_2, dtEndTime_2, 'minute', '[]');
	var bBookEndCheck_2 = dtBookEnd.isBetween(dtStartTime_2, dtEndTime_2, 'minute', '[]');
	
	if(bBookStartCheck_1 == false && bBookStartCheck_2 == false){
		ChangeInterpreterHours_ErrorControl('Booking Start Time is not between either of the new Time Slots');
		return false
	}		
	if(bBookEndCheck_1 == false && bBookEndCheck_2 == false){
		ChangeInterpreterHours_ErrorControl('Booking End Time is not between either of the new Time Slots');
		return false
	}
	
	ChangeInterpreterHours_ErrorControl('');
	return true;
}

function ChangeInterpreterHours_ErrorControl(strMsg){
	JQ_ShowHide('div_Error', strMsg != '');
	$('#dlg_ChangeInterpreterHours_Validation').html(((strMsg != '') ? 'Error: ' + strMsg : ''));		
}	

function reloadIframe(iframeId) {
	var iFrameID = window.parent.document.getElementById(iframeId);
    if(iFrameID) {
		iFrameID.src = iFrameID.src + '';
    }
}


/**
 * Updated by Baljit Karwal on 30 July 2019 - to add extra parameter to handle cancellation reasons in dialog box
 * Moved from BOOK client js to Global Client JS
 * To show/hide cancellation reasons
 * 
 * @param strValue
 * @param isDialogExtraString
 * @returns
 */
function BOOK_NC_Reason_OnChange(strValue, isDialogExtraString) {	
	if (isDialogExtraString == null || typeof isDialogExtraString == 'undefined') {
		isDialogExtraString = '';
	}
	clearCancellationDialogFields();
	JQ_ShowHide("div_BOOK_NC_Reason_Other" + isDialogExtraString, strValue == "other" | strValue == "error" );
	JQ_ShowHide("div_BOOK_NC_Reason_Cancelled" + isDialogExtraString, strValue == "cancelled");
	JQ_ShowHide("div_BOOK_NC_Reason_InterpreterNotRequired" + isDialogExtraString, strValue == "interpreter_nr");
	JQ_ShowHide("div_BOOK_NC_Reason_InterpreterFTA"+ isDialogExtraString, strValue == "interpreter_fta");
	JQ_ShowHide("div_BOOK_NC_Reason_InterpreterFTA_Other" + isDialogExtraString, (strValue == "interpreter_fta" && $('.fld_BOOK_NC_Reason_InterpreterFTA'+isDialogExtraString).val() == 'Other') ? true : false);	
	
}

/**
 * Updated by Baljit Karwal on 31 July 2019 - to add extra parameter to handle cancellation reasons in dialog box
 * Moved from BOOK client js to Global Client JS
 * To show/hide interpreter FTA reasons
 * 
 * @param strValue
 * @param isDialogExtraString
 * @returns
 */
function BOOK_NC_Reason_InterpreterFTA_OnChange(strValue, isDialogExtraString){	
	if (isDialogExtraString == null || typeof isDialogExtraString == 'undefined') {
		isDialogExtraString = '';
	}
	JQ_ShowHide("div_BOOK_NC_Reason_InterpreterFTA_Other" + isDialogExtraString, strValue == "Other");	
}

/**
 * Added by Baljit Karwal on 31 July 2019
 * To clear all previous values from cancellation dialog box 
 * @returns
 */
function clearCancellationDialogFields() {
	$('.fldCancelReason').val("");
	$('.fldCancelOtherReason').val("");
	$('.fldCancelNoticePeriod').val("");
	$('.fldCancelINRReason').val("");
	$('.fldCancelIFTAReason').val("");
	$('.fldCancelIFTAOtherReason').val("");
}
/**
 * Added by Baljit Karwal on 31 July 2019
 * To set field values for cancellation
 */
function setCancellationFields(strBtn, isRequest) {
	if (isRequest == null && typeof isRequest == 'undefined') {
		isRequest = false;
	}
	var fldCancelReasonDDVal = strTrim($('.fldCancelReasonDD').val()); //drop down reason value
	/*var fldCancelReasonDDName = strTrim($('.fldCancelReasonDD option:selected').html());
	var fldCancelReason = strTrim($('.fldCancelReason').val());
	var fldCancelOtherReason = strTrim($('.fldCancelOtherReason').val());
	var fldCancelNoticePeriod = strTrim($('.fldCancelNoticePeriod').val());
	var fldCancelINRReason = strTrim($('.fldCancelINRReason').val());
	var fldCancelIFTAReason = strTrim($('.fldCancelIFTAReason').val());
	var fldCancelIFTAOtherReason = strTrim($('.fldCancelIFTAOtherReason').val());*/
	if(fldCancelReasonDDName == ''){Notify_Warning('Please provide reason for the Cancellation');return true}
	if (isRequest) {
		$('#dlg_CancelReqBooking').modal('hide');
		ConfirmAction('This action will mark Booking as Cancellation Requested.<br><br><b>Cancellation Reason:</b><br>' + fldCancelReasonDDName, strBtn)
	} else {
		$('#dlg_CancelBooking').modal('hide');
		ConfirmAction('This action will mark Booking as Cancelled.<br><br><b>Cancellation Reason:</b><br>' + fldCancelReasonDDName, strBtn)
	}
}