function BOOK_AssignInterpreter_UpdateDoc(ndoc, strIDK, strInterpreter, bSave){
	if (ndoc.hasItem("CBN_DocKey")){
		//Then this is a booking linked to a Combined Booking
		ndoc = ndoc.getParentDatabase().getView('vwAllByDocKey').getDocumentByKey(ndoc.getItemValueString("CBN_DocKey"), true);
		if (ndoc==null){return ""}
	}
	var strMsg = BOOK_CheckAvailability(ndoc, strIDK);
	if(strMsg != 'OK'){return BOOK_CheckAvailability_Message(strMsg, strIDK, strInterpreter)}
	var prefix = ndoc.getItemValueString("Form")
	ndoc.replaceItemValue(prefix+'_Interpreter_Type', 'In-House');
	ndoc.replaceItemValue(prefix+'_Agency_SentDate', '');
	ndoc.replaceItemValue(prefix+'_Agency_SentBy', '');
	ndoc.replaceItemValue(prefix+'_Interpreter_DocKey', strIDK)
	ndoc.replaceItemValue(prefix+'_Interpreter_FullName', strInterpreter)

	BOOK_LogAction(ndoc, 'Appointment Booked with "' + ndoc.getItemValueString(prefix+'_Interpreter_FullName') + '"' , true);

	if(ndoc.getItemValueString(prefix+'_Status') != "booked"){
		var strSD = BOOK_Status(ndoc, 'booked');
		BOOK_LogAction(ndoc, 'Status changed to ' + strSD, true);
	}	
	if(bSave){ndoc.computeWithForm(true, false); ndoc.save(true, false)
	CBN_PropagateAgencyDetails(ndoc)
	}	
	return "UPDATED"
}

function BOOK_AssignToAgency_UpdateDoc(strADK, strAName, bSave, docX:NotesXspDocument, ndoc:NotesDocument, launch, agencyChanged, strInterpreter_E, strInterpreter){
	if(docX == null){var doc = ndoc} else {var doc = docX}
	if (doc.hasItem("CBN_DocKey")){
		//Then this is a booking linked to a Combined Booking
		doc = doc.getParentDatabase().getView('vwAllByDocKey').getDocumentByKey(ndoc.getItemValueString("CBN_DocKey"), true);
		ndoc = doc;
		if (ndoc==null){return false}
	}
	var prefix=doc.getItemValueString("Form"); //Johan - 16 Sep 09 - Compatibility with CBN
	doc.replaceItemValue(prefix+'_Interpreter_Type', 'Agency');
	doc.replaceItemValue(prefix+'_Agency_SentDate', '');//Leave these as BOOK_?No, keep prefix
	doc.replaceItemValue(prefix+'_Agency_SentBy', '');//Leave these as BOOK_?No, keep prefix

	if(strADK != null){doc.replaceItemValue(prefix+'_Interpreter_DocKey', strADK)}
	if(strAName != null){doc.replaceItemValue(prefix+'_Interpreter_FullName', strAName)}

	if (agencyChanged) {
		//If agency is not oncall and status is requested(New), reset BOOK_Pending to 0 so that it shows in Pending Bookings
		doc.removeItem(prefix+'_PendingSend');
		BOOK_LogAction(doc, 'Agency changed from "' + strInterpreter_E +'" to "' + strInterpreter + '"', true);
	} else {
		BOOK_LogAction(doc, 'Assigned to Agency "' + doc.getItemValueString(prefix+'_Interpreter_FullName') + '"' , true);
	}
	var strSendNow = GetKeyword('OnCall_SendCreateOnSave')
	if (strSendNow.equalsIgnoreCase ('Yes') && doc.getItemValueString(prefix+'_Interpreter_FullName').equalsIgnoreCase ("ONCALL")) {
		doc.replaceItemValue(prefix+'_PendingSend', 1);
	}
	if(doc.getItemValueString(prefix+'_Status') != "requested"){
		var strSD = BOOK_Status(doc, 'requested');		
		BOOK_LogAction(doc, 'Status changed to ' + strSD, true);
	}	
	ndoc = doc;
	if(bSave){
		if(ndoc != null){
			BOOK_Common_SaveOpen(ndoc,launch)
		} else {
			docX.save()
		}
		CBN_PropagateAgencyDetails(doc)
	}
	
}

//Called from the grid or from the document.
//This determins if we can cancel it straight away or we need to confirm.
//If we confirm then we will call subsequent functions form the UI 
/**
 * Updated by Baljit on 01 Aug 2019 to replace strReason with object as now we will have multiple field values
 */
function BOOK_CancelBooking (sUNID, cancellationReasonsObject) {
	//first get the document
	var isGrid = !sUNID ? false : true; //if sUNID has something in it then we're from the grid
	if(isGrid == false){
		var docX:NotesXspDocument = document1;
		var ndoc:NotesDocument = docX.getDocument();
		var sprefix = ndoc.getItemValueString("Form")||"BOOK";
		var cancellationReasonsObject = {};
		var strStatus = GetValue(sprefix+'_Status');
		//If booking cancellation is requested, get requested cancellation fields values and set to actual fields
		if (strStatus.equals("cancellation_req")) {
			cancellationReasonsObject.fldCancelReasonDD = ndoc.getItemValueString(sprefix+'_fldCancelReasonDD');
			cancellationReasonsObject.fldCancelOtherReason = ndoc.getItemValueString(sprefix+'_fldCancelOtherReason');
			cancellationReasonsObject.fldCancelNoticePeriod = ndoc.getItemValueString(sprefix+'_fldCancelNoticePeriod');
			cancellationReasonsObject.fldCancelReason = ndoc.getItemValueString(sprefix+'_fldCancelReason');
			cancellationReasonsObject.fldCancelINRReason = ndoc.getItemValueString(sprefix+'_fldCancelINRReason');
			cancellationReasonsObject.fldCancelIFTAReason = ndoc.getItemValueString(sprefix+'_fldCancelIFTAReason');
			cancellationReasonsObject.fldCancelIFTAOtherReason = ndoc.getItemValueString(sprefix+'_fldCancelIFTAOtherReason');
			//Remove cancellation requested fields
			ndoc.removeItem(sprefix+'_fldCancelReasonDD');
			ndoc.removeItem(sprefix+'_fldCancelOtherReason');
			ndoc.removeItem(sprefix+'_fldCancelNoticePeriod');
			ndoc.removeItem(sprefix+'_fldCancelReason');
			ndoc.removeItem(sprefix+'_fldCancelINRReason');
			ndoc.removeItem(sprefix+'_fldCancelIFTAReason');
			ndoc.removeItem(sprefix+'_fldCancelIFTAOtherReason');
			
		} else if (typeof cancellationReasonsObject == 'undefined' || cancellationReasonsObject == null) {
			cancellationReasonsObject.fldCancelReasonDD = getComponent('fldCancelReasonDD').getValue();
			cancellationReasonsObject.fldCancelOtherReason = getComponent('fldCancelOtherReason').getValue();
			cancellationReasonsObject.fldCancelNoticePeriod = getComponent('fldCancelNoticePeriod').getValue();
			cancellationReasonsObject.fldCancelReason = getComponent('fldCancelReason').getValue();
			cancellationReasonsObject.fldCancelINRReason = getComponent('fldCancelINRReason').getValue();
			cancellationReasonsObject.fldCancelIFTAReason = getComponent('fldCancelIFTAReason').getValue();
			cancellationReasonsObject.fldCancelIFTAOtherReason = getComponent('fldCancelIFTAOtherReason').getValue();
		}
	} else {
		var ndoc:NotesDocument = GetDB_Database('BOOK').getDocumentByUNID(sUNID);
		if(ndoc == null){print('ERROR: BOOK Document not found, UNID: ' + sUNID); return false}
		if(ndoc.getItemValueString("Form")=="CBN")	{
			view.postScript("Notify_Warning('Combined bookings must be cancelled from with the Combined Booking Form')");
			return false
		}
	}	

	var sprefix = ndoc.getItemValueString("Form")||"BOOK";
	/**
	 * Keep BOOK_ instead of prefix variable for BOOK_Agency_FlowComplete as we are not setting CBN_Agency_FlowComplete anywhere. - Change by Baljit
	 */
	if (ndoc.getItemValueString (sprefix+"_Interpreter_Type").equalsIgnoreCase("Agency") && !ndoc.hasItem("CBN_DocKey") && !ndoc.getItemValueString("BOOK_Agency_FlowComplete").equalsIgnoreCase("Yes"))
	{
		//spin off to the agency function// This might return String or objects
		var errorReason = BOOK_CheckCancelAgencyBooking (ndoc, sprefix);
		var ChargesApply = 'will';
		var errorReasonMsg = errorReason;
		var errorAgtError = '';
		if (errorReason.agtMessage != 'undefined' && errorReason.agtMessage != '') {
			errorReasonMsg = errorReason.agtMessage;
		}
		if (errorReason.agtError != 'undefined' && errorReason.agtError != '') {
			errorAgtError = errorReason.agtError;
		}
		if (errorReason.ChargesApply != 'undefined' && errorReason.ChargesApply != '') {
			ChargesApply = errorReason.ChargesApply;
		}
		if (errorReason.isTrue == true) { //if true
			if (isGrid){
				//For the grid we need to store the ids so we can display a dialog at the end. 
				getComponent('selUNIDs').setValue(getComponent('selUNIDs').getValue () + sUNID + "|" + errorReasonMsg + "~");
			} else {
				if (ndoc.getItemValueString (sprefix+"_Interpreter_FullName").equalsIgnoreCase("ONCALL") && !errorAgtError.equals("")) {
					view.postScript("Notify_Warning('ERROR: " + errorAgtError + "')");
					return false;
				}  else if (ndoc.getItemValueString (sprefix+"_Interpreter_FullName").equalsIgnoreCase("ONCALL") && !errorReasonMsg.equals("")) {
					errorReason = "Please note: The following bookings " + ChargesApply + " incur charges if you cancel them.<br><br>" + errorReasonMsg;
					view.postScript("ConfirmAction('" + errorReason +  "','btnBOOKCancelOnCallBooking_SJS')");
				} else {
					//just do the single check about charges.
					errorReason = "Please note: The following bookings " + ChargesApply + " incur charges if you cancel them.<br><br>" + errorReasonMsg;
					view.postScript("ConfirmAction('" + errorReason +  "','btnBOOKCancelOnCallBooking_SJS')");
				}
			}
			return true;
		} else if (errorReason.isTrue == false) {
			if (!isGrid){
				if (ndoc.getItemValueString (sprefix+"_Interpreter_FullName").equalsIgnoreCase("ONCALL") && !errorAgtError.equals("")) {
					view.postScript("Notify_Warning('ERROR: " + errorAgtError + "')");
				} else {
					view.postScript("Notify_Warning('ERROR: There was an error cancelling the document, please contact IT Support.')");
				}
				return false;
			} 
		} else {
			//just normal procedure
			if ( errorReason != true) {
				if (errorReason == false ){
					if (!isGrid){
						if (ndoc.getItemValueString (sprefix+"_Interpreter_FullName").equalsIgnoreCase("ONCALL")) {
							view.postScript("Notify_Warning('ERROR: There was an error requesting cancellation from ONCALL, if the problem persists, please contact IT Support')");
							return false;
						}else{
							view.postScript("Notify_Warning('ERROR: There was an error cancelling the document, please contact IT Support.')");
							return false;
						}
					} 
					return false;
				};
				if (!errorReason.equals("")) {
					if (isGrid){
						//For the grid we need to store the ids so we can display a dialog at the end. 
						getComponent('selUNIDs').setValue(getComponent('selUNIDs').getValue () + sUNID + "|" + errorReason + "~");
					} else {
						//just do the single check about charges.
						errorReason = "Please note: The following bookings " + ChargesApply + " incur charges if you cancel them.<br><br>" + errorReason;
						view.postScript("ConfirmAction('" + errorReason +  "','btnBOOKCancelOnCallBooking_SJS')");
					}
					return true;
				} else {
					return false;
				}
			}
		}
	}
	BOOK_UpdateDocument_CancelledDetails (ndoc, isGrid, cancellationReasonsObject, sprefix);
}

function BOOK_CheckAvailability(docB, strIDocKey,prefix){
	if(prefix==null){prefix=docB.getItemValueString("Form");}
	var vDt = docB.getItemValue(prefix+'_ApptStartDate_Text');
	var vTm = docB.getItemValue(prefix+'_ApptStartTime_Text');
	var vDu = docB.getItemValue(prefix+'_ApptDuration');
	if(IsNullorBlank(vDt) | IsNullorBlank(vTm)){return "ERROR"}

	var ndt:NotesDateTime = session.createDateTime(vDt[0] + " " + vTm[0]);
	var dtToday:NotesDateTime = session.createDateTime("Today");
	if (ndt.timeDifference(dtToday)<0){return "OK"}//don't bother check availability for dates in the past
	var dtBStart:Date = ndt.toJavaDate();	
	if(!IsNullorBlank(vDu)){ndt.adjustMinute(@TextToNumber(vDu[0]))}
	var dtBEnd:Date = ndt.toJavaDate();

	var strFormat = "yyyyMMddHHmm";
	var intBStart_DT = @TextToNumber(I18n.toString(dtBStart, strFormat));
	var intBEnd_DT = @TextToNumber(I18n.toString(dtBEnd, strFormat));
	var intBStart_T = @TextToNumber(@Right(@Text(intBStart_DT), 4));
	var intBEnd_T = @TextToNumber(@Right(@Text(intBEnd_DT), 4));
	var intBookingDate = @TextToNumber(@Left(@Text(intBStart_DT), 8));

	//Check Leaves first
	//Sample - [[2.017051709E11, 2.017052618E11], [2.017052709E11, 2.017052714E11]]
	//If (Booking Start is between Leave Start and Leave End) or (Booking End is between Leave Start and Leave End)
	var vHM_Leaves:java.util.HashMap = USER_Leaves();

	if(vHM_Leaves.containsKey(strIDocKey)){
		var vValues:java.util.Vector = vHM_Leaves.get(strIDocKey);
	for(var i=0; i<vValues.size(); i++){
		//print("vValues - " + vValues[i])
		if((intBStart_DT > vValues[i][0] & intBStart_DT < vValues[i][1]) | (intBEnd_DT > vValues[i][0] & intBEnd_DT < vValues[i][1])){
			var dtS = Date_ChangeFormat(vValues[i][0], "yyyyMMddhhmm", "dd MMM yyyy hh:mm");
			var dtE = Date_ChangeFormat(vValues[i][1], "yyyyMMddhhmm", "dd MMM yyyy hh:mm");
			return 'LEAVE~' + vDt[0] + '~' + dtS + '~' + dtE;
		}
	}
	}

	//Check if Interpreter Hours are changed for this day by Coordinator
	var vHM_Hours_Extra:java.util.HashMap = USER_Hours_Extra();
	if(vHM_Hours_Extra.containsKey(strIDocKey)){
		var vValues:java.util.Vector = vHM_Hours_Extra.get(strIDocKey);
	for(var i=0; i<vValues.size(); i++){
		//Date, DateTimeStart_1, DateTimeEnd_1, DateTimeStart_2, DateTimeEnd_2
		//Sample - [[20171204, 201712040000, 201712040000, 0, 0], [20171225, 201712250700, 201712251200, 201712251500, 201712251800]]
		//We have a entry of Extra hours of the booking day, now check for the time
		if(vValues[i][0] == intBookingDate){
			var strTS_2 = "0"; var strTE_2 = "0";	//Default Value
			var strReturn = 'HOURS_EXTRA~' + vDt[0];
			//Check if First Date Time
			if(intBStart_DT >= vValues[i][1] & intBStart_DT <= vValues[i][2] & intBEnd_DT >= vValues[i][1] & intBEnd_DT <= vValues[i][2]){
				return 'OK'
			}
			//Split hours Check
			if(vValues[i][3] != 0){
				if(intBStart_DT >= vValues[i][3] & intBStart_DT <= vValues[i][4] & intBEnd_DT >= vValues[i][3] & intBEnd_DT <= vValues[i][4]){
					return 'OK'						
				}
				strTS_2 = @Right(@Text(vValues[i][3]), 4); 
				strTS_2 = @Left(strTS_2, 2) + ':' + @Right(strTS_2, 2); 
				strTE_2 = @Right(@Text(vValues[i][4]), 4); 
				strTE_2 = @Left(strTE_2, 2) + ':' + @Right(strTE_2, 2);
			} 
			var strTS_1 = @Right(@Text(vValues[i][1]), 4); 
			strTS_1 = @Left(strTS_1, 2) + ':' + @Right(strTS_1, 2); 
			var strTE_1 = @Right(@Text(vValues[i][2]), 4); 
			strTE_1 = @Left(strTE_1, 2) + ':' + @Right(strTE_1, 2);
			strReturn += ('~' + strTS_1 + '~' + strTE_1);
			if(vValues[i][3] != 0){strReturn += ('~' + strTS_2 + '~' + strTE_2)}
			return strReturn
		}

	}
	}

	//Check Hours
	//If interpreter in non-Standard hours check in that otherwise check standard hours
	//Sample - [[900.0, 1600.0], [1300.0, 1800.0], 0.0, 0.0, [1300.0, 1700.0], 0.0, 0.0]
	var strHType = 'HOURS_STANDARD~' + vDt[0] + '~';
	var intWD = @Weekday(dtBStart); //WeekDay
	var vHM_Hours:java.util.HashMap = USER_Hours_Standard();
	var vValues = vHM_Hours.get(intWD);

	vHM_Hours = USER_Hours_NonStandard();
	if(vHM_Hours.containsKey(strIDocKey)){		
		strHType = 'HOURS_NON_STANDARD~' + vDt[0] + '~';
		var vValues = vHM_Hours.get(strIDocKey)[intWD-1];
	}

	//vValues could be zero or array with start & end working hours
	if(vValues == 0){return (strHType + '0')}
	if(intBStart_T < vValues[0] | intBEnd_T > vValues[1]){
		var strTS_1 = @Right('0' + @Text(vValues[0]), 4); 
		strTS_1 = @Left(strTS_1, 2) + ':' + @Right(strTS_1, 2); 
		var strTE_1 = @Right('0' + @Text(vValues[1]), 4);
		strTE_1 = @Left(strTE_1, 2) + ':' + @Right(strTE_1, 2); 
		return strHType + (strTS_1 + '~' + strTE_1);
	}

	return "OK"
}

function BOOK_CheckAvailability_Message(strMsg, strIDocKey, strIName){

	//Received strMsg is - ExceptionCode ~ BookingDateTime ~ DateTimes(different types for different Codes
	//Return strMsg - MeaningFull Message ~ strIName ~ Original above Received strMsg
	if(!strIName){strIName = @DbLookup(GetDB_FilePath('USER'), "all_byDocKey", strIDK, "USER_FullName", "[FAILSILENT]")}

	var vMsg = strMsg.split('~');
	var strAppend = '~' + strIName + '~' + strMsg;	

	if(vMsg[0] == 'LEAVE'){
		return strIName + " is on Leave from " + vMsg[2] + " until " + vMsg[3] + strAppend;
	}

	if(vMsg[0] == 'HOURS_STANDARD'){
		return "This Booking Date Time is out of Standard Working Hours (" + vMsg[2] + " - " + vMsg[3] + ")" + strAppend;
	}

	if(vMsg[0] == 'HOURS_NON_STANDARD'){
		var strReturn = strIName + " is not working on this Booking Date";
		if(vMsg[2] == "0"){
			return strReturn + strAppend
		}
		return "This Booking Date Time is out of " + strIName + " working Hours (" + vMsg[2] + " - " + vMsg[3] + ")" + strAppend;
	} 

	if(vMsg[0] == 'HOURS_EXTRA'){
		// 'HOURS_EXTRA~' + vDt[0] + '~' + Start_1 + '~' + End_1 + '~' + Start_2 + '~' + End_2
		//strMsg = strIName + '~' + strMsg;		
		//Changing of words in this  below string has impact on some functions //BOOK_GridAction_AssignInterpreter
		var strReturn = "Working Hours of " + strIName + " on " + vMsg[1] + " are from " + vMsg[2] + " untill " + vMsg[3];
		if(vMsg.length > 4){strReturn += " & " + vMsg[4] + " untill " + vMsg[5]}
		return strReturn + strAppend;	
	}

}

function BOOK_Common_SaveOpen(ndoc,launch){
	if (launch==null){launch = true}
	ndoc.computeWithForm(true, false)
	ndoc.save(true, false);	
	if (launch) {BOOK_Open(ndoc.getUniversalID());	}
}

function BOOK_Linkify(doc){
	//'xCBN.xsp?documentId="+docCBN.getUniversalID()+"&action=openDocument'
	var unid = doc.getUniversalID();
	var dockey = doc.getItemValueString("DocKey");
	var form = doc.getItemValueString("Form");
	var title = "Booking #" + dockey;
	return "<a title='"+title+"' href='x"+form+".xsp?documentId="+unid+"&action=openDocument'>"+dockey+"</a>"
}

/**
 * Updated by Baljit Karwal on 01 Aug 2019 to replace strReason with cancellationReasonsObject
 * Set cancellation details on a document
 */
function BOOK_UpdateDocument_CancelledDetails (ndoc:NotesDocument, isGrid, cancellationReasonsObject, sprefix)
{
	
	if (sprefix==null){sprefix=ndoc.getItemValueString("Form")||"BOOK"}
	//Booking is already asked for cancellation either via PMS or Requestor
	if(ndoc.getItemValueString(sprefix+'_Status') == 'cancellation_req'){
		ndoc.replaceItemValue(sprefix+'_CancellationReason', ndoc.getItemValueString(sprefix+'_CancellationReason') + '\r\n' + cancellationReasonsObject.fldCancelReason);		
		BOOK_LogAction(ndoc, 'Booking Cancellation Accepted' , true)
	} else {
		ndoc.replaceItemValue(sprefix+'_CancellationReason', cancellationReasonsObject.fldCancelReason);
		var hours = BOOK_Cancellation_TimeDifference(ndoc);
		if (hours > 24){
			ndoc.replaceItemValue(sprefix+'_CancellationNotice' , "24 hrs prior")
		} else {
			ndoc.replaceItemValue(sprefix+'_CancellationNotice' , "Same day")
		}
		BOOK_LogAction(ndoc, 'Booking Cancelled' , true);
	}
	ndoc.replaceItemValue('BOOK_Completed', 'No');
	if (cancellationReasonsObject != null || typeof cancellationReasonsObject != 'undefined'){
		var strSD = BOOK_Status(ndoc, cancellationReasonsObject.fldCancelReasonDD);	
		ndoc.replaceItemValue(sprefix+'_NC_Reason', cancellationReasonsObject.fldCancelReasonDD);
		ndoc.replaceItemValue(sprefix+'_NC_Reason_Other', cancellationReasonsObject.fldCancelOtherReason);
		ndoc.replaceItemValue(sprefix+'_CancellationNotice', cancellationReasonsObject.fldCancelNoticePeriod);
		ndoc.replaceItemValue(sprefix+'_NC_Reason_InterpreterNR', cancellationReasonsObject.fldCancelINRReason);
		ndoc.replaceItemValue(sprefix+'_NC_Reason_InterpreterFTA', cancellationReasonsObject.fldCancelIFTAReason);
		ndoc.replaceItemValue(sprefix+'_NC_Reason_InterpreterFTA_Other', cancellationReasonsObject.fldCancelIFTAOtherReason);
	} else {
		var strSD = BOOK_Status(ndoc, 'cancelled');	
		ndoc.replaceItemValue(sprefix+'_NC_Reason', 'cancelled');
	}
	
	//Now check if linked to combined document, if so set cancellation requested so it can be actioned
	if (ndoc.hasItem("CBN_DocKey")){
		var docCBN:NotesDocument = ndoc.getParentDatabase().getView('vwAllByDocKey').getDocumentByKey(ndoc.getItemValueString("CBN_DocKey"), true);
		if (docCBN!=null){
			BOOK_LogAction(docCBN, 'Booking #'+ndoc.getItemValueString("DocKey")+' was cancelled, therefore this combined booking needs to cancelled', true)
			BOOK_Status (docCBN, "cancellation_req");
			docCBN.save(true, false);
		}
	}
	//if it's an oncall document then set the end of flow field
	if (ndoc.getItemValueString("BOOK_Interpreter_FullName").equalsIgnoreCase ("OnCall") && !ndoc.hasItem("CBN_DocKey"))
	{
		ndoc.replaceItemValue ("BOOK_Agency_FlowComplete", "Yes");
		ndoc.removeItem(sprefix + '_PendingSend');
		ndoc.removeItem(sprefix + '_Agency_AmendDetails');
		ndoc.removeItem(sprefix + '_Agency_UpdateCostCentre');
	}
		
	if(isGrid == false){
		BOOK_Common_SaveOpen(ndoc)
	} else {
		ndoc.computeWithForm(true, false)
		ndoc.save(true, false);
		return true
	}
}

function BOOK_GetFieldLabel (strFieldName){
	return !applicationScope.containsKey("BOOK_Labels") || !applicationScope.get("BOOK_Labels")[strFieldName] ? strFieldName : applicationScope.get("BOOK_Labels")[strFieldName]
}

function BOOK_SetFieldLabelsScope (){
	if (applicationScope.containsKey ("BOOK_Labels")) {return}
	var labels = fromJson(GetKeyword ("BOOK_Dynamic_FieldLabels"));
	applicationScope.put ("BOOK_Labels", labels);
}

function BOOK_BeforePageLoad(){

	var docX:NotesXspDocument = document1;	
	AppForm_BeforePageLoad(document1);
	//get the dynamic labels if we dont have them
	BOOK_SetFieldLabelsScope();

	if(docX.isNewNote()){

		BOOK_Status(docX, 'draft');
		docX.setValue("BOOK_Source", "IMS"); 
		docX.setValue("BOOK_Source_Display", GetKeyword("BOOK_Source")[0]);
		docX.setValue("BOOK_ApptDuration", 15);
		docX.setValue("BOOK_DatePassed", "");	
		if (GetKeyword("AllowRequestedByDetailsForAll").equalsIgnoreCase('Yes') || IsUser_Requestor())
		{
			docX.setValue('BOOK_RequestedBy', General_GetUser_FullName());
			docX.setValue('BOOK_RequestedByContact', ssGetVariable('ssUser_Phone'));
			docX.setValue('BOOK_RequestedBy_DocKey', General_GetUser_DocKey());
		}
		if(IsUser_Requestor()){
			docX.setValue('BOOK_Site', ssGetVariable('ssUser_SiteCode'));

		} else {

			if(IsUser_Interpreter()){
				docX.setValue('BOOK_Interpreter_Type', 'In-House');
				docX.setValue('BOOK_Interpreter_DocKey', General_GetUser_DocKey());
				docX.setValue('BOOK_Interpreter_FullName', General_GetUser_FullName());
				var vLL = BOOK_LanguageList();
				if(vLL.length == 1){docX.setValue('BOOK_Language', vLL[0])}				
			}			
		}

		//DUPLICATE BOOKING CHECK
		if(sessionScope.containsKey('DuplicateBooking')){
			var strUNID = sessionScope.get('DuplicateBooking'); sessionScope.remove('DuplicateBooking');
			var docSource:NotesDocument = docX.getParentDatabase().getDocumentByUNID(strUNID)
			if(docSource != null){				
				var vFieldList = GetKeyword('BOOK_DuplicateFieldsList');
				if(docSource.getItemValueString("BOOK_Status")=="unmet_pending"){vFieldList = GetKeyword('BOOK_DuplicateFieldsList_unmet_pending')}	
				if(docSource.getItemValueString("BOOK_Status")=="cancelled"){vFieldList = GetKeyword('BOOK_DuplicateFieldsList_unmet_pending')}	
				if(IsUser_Requestor()){vFieldList = GetKeyword('BOOK_DuplicateFieldsList_Requestor')}				
				for(var i=0; i<vFieldList.length; i++){
					if (docSource.hasItem(vFieldList[i])){
						docX.setValue(vFieldList[i], docSource.getFirstItem(vFieldList[i]).getText())
					}
	
				}				
			} else {
				Global_LogError("DuplicateBooking: Cannot find document with UNID: " + strUNID)
			}

		} else if(sessionScope.containsKey('IMS_PatientData')){
			var strUNID = sessionScope.get('IMS_PatientData'); sessionScope.remove('IMS_PatientData');
			var docSource:NotesDocument = docX.getParentDatabase().getDocumentByUNID(strUNID)
			if(docSource != null){				
				var vFieldList = GetKeyword('BOOK_IMS_NewBooking_FieldsList');				
				for(var i=0; i<vFieldList.length; i++){
					if (docSource.getFirstItem (vFieldList[i]))
					{
						docX.setValue(vFieldList[i], docSource.getFirstItem(vFieldList[i]).getText())
					}
				}				
			} else {
				Global_LogError("IMS_PatientData: Cannot find document with UNID: " + strUNID)
			}
			
		} else if(sessionScope.containsKey('IMS_PatientDataFromPDB')) { 
			/**
			 * Condition added by Baljit on 21 May 2019
			 * Based on KW to fetch patient data from patient database
			 * Then loop through another KW to map patient fields with booking fields
			 */
			var strUNID = sessionScope.get('IMS_PatientDataFromPDB'); 
			sessionScope.remove('IMS_PatientDataFromPDB');
			var docSource:NotesDocument = GetDB_Database('Patient').getDocumentByUNID(strUNID);
			try {
				if(docSource != null) {				
					var vFieldList = GetKeyword('BOOK_IMS_NewBooking_PatientMappingFieldsList');		
					for(var i=0; i<vFieldList.length; i++) {
						var mapVals = vFieldList[i].split('~');		
						if (mapVals.length > 1) {
							var bookingField = mapVals[0];
							var patientField = mapVals[1].split(',');	//Check if comma exists for patient fields, save both in one booking form field for patient eg : address line 1 and address line 2						
							if (patientField[0] != 'undefined') {
								if (!docSource.getItemValueString(patientField[0]).equals('')) {
									var fieldVal = docSource.getItemValueString(patientField[0]);
									if (patientField.length > 1) {
										if (!docSource.getItemValueString(patientField[1]).equals('')) {
											fieldVal = fieldVal + '\n' + docSource.getItemValueString(patientField[1]);
										}
									}									
									if (!fieldVal.equals('')) {
										docX.setValue(bookingField, fieldVal);
									}
								}
							}
						}
					}				
				} else {
					Global_LogError("IMS_PatientData: Cannot find document with UNID: " + strUNID)
				}
			} catch (e) {
				print("Error: Unable to get patient details from patient Database");
			}
		} else if(sessionScope.containsKey('PMS_PatientData')){				
			var vJson = fromJson(sessionScope.get('PMS_PatientData')); sessionScope.remove('PMS_PatientData');				
			if(vJson.URN != null){docX.setValue('BOOK_PatientUR', vJson.URN)}
			if(vJson.FirstName != null){docX.setValue('BOOK_PatientFirstName', vJson.FirstName)}
			if(vJson.Surname != null){docX.setValue('BOOK_PatientLastName', vJson.Surname)}
			if(vJson.Gender != null){docX.setValue('BOOK_PatientGender', vJson.Gender)}
			if(vJson.Address != null){docX.setValue('BOOK_PatientAddress', vJson.Address)}
			if(vJson.DOB != null){docX.setValue('BOOK_PatientDOB', vJson.DOB)}
			if(vJson.Language != null){docX.setValue('BOOK_Language', vJson.Language)}
		} else if (sessionScope.containsKey('AmendBooking') || sessionScope.containsKey('ChangeAgency')){
			/**
			 * Updated by Baljit on 5th Sept 2018
			 * For adding new functionality to "Change Agency" which is similar to AmendBooking functionality
			 * Code moved to BOOK_Recreate_Booking() function
			 */
			if (sessionScope.containsKey('AmendBooking')) {
				BOOK_Recreate_Booking('AmendBooking');
			} else if (sessionScope.containsKey('ChangeAgency')) {
				BOOK_Recreate_Booking('ChangeAgency');
			}
			
		}
	}

	if(docX.isEditable() & !docX.isNewNote()){		
		//This will check if the post booking section is itself not allowed to edit,
		//then it means nothing is allowed to edit on this document
		if(BOOK_FieldIsEditable_Check('BOOK_Completed', 'PostBooking') == false){
			BOOK_Open(docX.getDocument().getUniversalID());
		}				
	}			
}

/**
 * Added By Baljit on 05th Sept 2018
 * New function to hold common functionality for 
 * 	Amend booking and Change Agency 
 * 	to cancel current booking and add new booking
 */
function BOOK_Recreate_Booking(scopeType) {
		
	//Amend a Rejected by Agency/Change Agency. 
	//1. Populate all the values from the original document.
	var strUNID = sessionScope.get(scopeType);
	sessionScope.remove(scopeType);
	var docSource:NotesDocument = docX.getParentDatabase().getDocumentByUNID(strUNID)
	BOOK_Create_Cloned_Booking(docX,docSource)
	/**
 * Added By Johan on 15th Sept 2018
 * Transfered below code to BOOK_Create_Cloned_Booking so that it is re-useable (from Combined Bookings)
 */
	
	/*
	if(docSource != null){				
		var vFieldList = GetKeyword('BOOK_AmendRejectedFieldsList');
		for(var i=0; i<vFieldList.length; i++){
			if (docSource.hasItem(vFieldList[i])){
				docX.setValue(vFieldList[i], docSource.getFirstItem(vFieldList[i]).getText())
			}
			
		}				
	} else {
		Global_LogError(scopeType+": Cannot find document with UNID: " + strUNID)
	}
	
	//2. Replace the DocKey with a version of the old one.
	var originalDocKey = docSource.getItemValueString ("DocKey")
	var supersededNo = docSource.getItemValueInteger("BOOK_Amended_Number") //the number of times we've amended, this updates on each new document
	
	if (supersededNo == '') {
		supersededNo = 0;
	}else{
		//remove the -N from the end of the docKey	
		originalDocKey = @LeftBack (originalDocKey, "-")
	}
	
	supersededNo ++
	
	docX.replaceItemValue ("DocKey", originalDocKey + "-" + supersededNo)
	docX.replaceItemValue ("BOOK_Amended_Number", supersededNo)
	*/
	//3. set the value on the doc so when we save it we know to update the details on both docs.
	if (scopeType == 'ChangeAgency') {
		docX.replaceItemValue ("BOOK_ChangedAgency_UNID", strUNID);
	} else {
		docX.replaceItemValue ("BOOK_Amended_UNID", strUNID);

	}
}

function BOOK_Create_Cloned_Booking(docX,docSource)
{
	if(docSource != null){				
		var vFieldList = GetKeyword('BOOK_AmendRejectedFieldsList');
		for(var i=0; i<vFieldList.length; i++){
			if (docSource.hasItem(vFieldList[i])){
					docX.replaceItemValue(vFieldList[i], docSource.getFirstItem(vFieldList[i]).getText())

				//	docX.setValue(vFieldList[i], docSource.getFirstItem(vFieldList[i]).getText())

			}
			
		}				
	} else {
		Global_LogError(scopeType+": Cannot find document with UNID: " + strUNID)
	}
	
	//2. Replace the DocKey with a version of the old one.
	var originalDocKey = docSource.getItemValueString ("DocKey")
	var supersededNo = docSource.getItemValueInteger("BOOK_Amended_Number") //the number of times we've amended, this updates on each new document
	
	if (supersededNo == '') {
		supersededNo = 0;
	}else{
		//remove the -N from the end of the docKey	
		originalDocKey = @LeftBack (originalDocKey, "-")
	}
	
	supersededNo ++
	
	docX.replaceItemValue ("DocKey", originalDocKey + "-" + supersededNo)
	docX.replaceItemValue ("BOOK_Amended_Number", supersededNo)
}

function BOOK_Increment_DocKey(oldDocKey){
	
	// CBN-JPOT-B5L2DS-1 or  CBN-JPOT-B5L2DS
	var vkey = oldDocKey.split("-");
	var returnObj={};
	var supersededNo=0
	var originalDocKey = oldDocKey
	if (vkey.length==4)
	{
		supersededNo=Number(vkey[3]);
		originalDocKey = vkey[0]+"-"+vkey[1]+"-"+vkey[2];
	}
	
	supersededNo ++;
	returnObj.Num = supersededNo;
	returnObj.DocKey = originalDocKey + "-" + supersededNo
	return returnObj
	
}
function BOOK_PatientData_PMS_NewBooking(){	
	var strJson = getComponent('patientData_PMS').getValue();
	var vJson = fromJson(strJson);
	if(vJson.Error == null){sessionScope.put('PMS_PatientData', strJson)}
}

function BOOK_HomeVisit_OnClick(){	
	var docX:NotesXspDocument = document1;
	if(docX.getItemValueString("BOOK_Source") == "IMS"){ 
		if(docX.getItemValueString('BOOK_PatientType') == 'Home Visit'){
			if(docX.hasItem('BOOK_PatientAddress')){
				docX.replaceItemValue('BOOK_Location', docX.getItemValueString('BOOK_PatientAddress'));
				docX.replaceItemValue('BOOK_Clinic', "Home Visit");
			} else {
				view.postScript("Notify_Warning('Error: Patient Address not found, please enter it manually !!')")
			}		
		}	
	}	
}

function BOOK_QuerySave(){
	
	var docX:NotesXspDocument = document1;
	var ndoc:NotesDocument = docX.getDocument();
	var bModifyBooking = (IsUser_Manager() | IsUser_Coordinator() | IsUser_DBAdmin());
	var prefix = ndoc.getItemValueString("Form");
	/**
	 * Added by Baljit on 23rd October 2018
	 * 
	 * If this is not a new document and is going to be saved as superseded document, 
	 * 	check in DB if there is already a superseded document ID exists with same booking id (DocKey) and it is not cancelled
	 * 	Do not allow to create duplicate document with same booking id for bookings
	 */
	var currentDocKey = docX.getItemValueString("DocKey");
	//get all documents with same docKey
	var ndocsWithSameKey:NotesDocumentCollection = ndoc.getParentDatabase().getView('vwAllByDocKey').getAllDocumentsByKey(currentDocKey, true);
	if (ndocsWithSameKey.getCount() > 0) {	//If multiple docs found with same docKey
		var bookDoc:NotesDocument = ndocsWithSameKey.getFirstDocument();
		var docExists = false; 
		//loop through bookings and get UNIDs
		var vUNIDs = [];
		while (bookDoc!=null)
		{
			//If duplicate booking is not cancelled and not same as current one, set true for multiple document existance
			if (bookDoc.getItemValueString("BOOK_Status") != "cancelled" && bookDoc.getUniversalID() != ndoc.getUniversalID()){
				vUNIDs.push(bookDoc.getUniversalID());
				ExistingBookDoc = bookDoc;
				docExists = true;
			}
			bookDoc = ndocsWithSameKey.getNextDocument(bookDoc);
		}

		if (docExists) {
			var existingDocKey = ExistingBookDoc.getItemValueString("DocKey");
			var existingBookingCreator = ExistingBookDoc.getItemValueString("DocKey_CreatedBy");
			view.postScript("Notify_Warning('Error: Multiple live bookings found for booking: " + currentDocKey + '. ' + existingBookingCreator + " has already created a new booking. Please do not save this booking. If you cannot save duplicate bookings, please find such bookings and cancel all similar bookings except the required one.')")
			return false;
		}
	}
	
	/********************************************************/
	var strStatus = GetValue('BOOK_Status');
	var bAllow = @IsMember(strStatus, ['draft', 'requested', 'booked', 'updated']);
	var strIType = GetValue('BOOK_Interpreter_Type');
	var strAppDate = GetValue('BOOK_ApptStartDate_Text');
	var strAppTime = GetValue('BOOK_ApptStartTime_Text');
	var strPRN = GetValue('BOOK_PatientUR');
	var strSiteCode = GetValue('BOOK_Site');
	var strGenderPref = GetValue('BOOK_PatientGenderPreference');
	var strRequestedBy = GetValue('BOOK_RequestedBy');
	var strCostCentre = GetValue('BOOK_CostCentre');

	var sAllowSameURDifSites = GetKeyword("AllowSameURDifferentSites");
	
	//Check for Duplicate Booking for the patient or overlapping booking
	var msgDbObj = getComponent('BOOK_CheckDouble');
	if(msgDbObj != null){
		if(msgDbObj.getValue() != 'IGNORE'){
			var intTime = @TextToNumber(@ReplaceSubstring(strAppTime, ":", ""));
			var strFormula = 'Form = "BOOK" & BOOK_PatientUR = "' + strPRN + '"';
			strFormula += ' & @IsMember(BOOK_Status; "draft":"requested":"booked":"updated")';
			if (sAllowSameURDifSites.equalsIgnoreCase('Yes'))
			{
				strFormula += ' & BOOK_Site="'+strSiteCode+'"';
			}
			strFormula += ' & View_Appt_StartDate = "' + I18n.toString(I18n.parseDate(strAppDate, "dd MMM yyyy"), "yyyyMMdd") + '"';
			strFormula += ' & @ToNumber(View_Appt_StartTime) <= ' + intTime;
			strFormula += ' & @ToNumber(@ReplaceSubstring(JSON_Appt_EndTime; ":"; "")) >= ' + intTime;
			if(!docX.isNewNote() || docX.getItemValueString ('BOOK_ChangedAgency_UNID') != ''){
				strFormula += ' & @Text(@DocumentUniqueID) != "' + ndoc.getUniversalID() + '"';		
			}
			var dc:NotesDocumentCollection = ndoc.getParentDatabase().search(strFormula, null, 0)
			if(dc.getCount() > 0){
				msgDbObj.setValue("ALERT: There is an existing booking for this patient within the booking date-time you have entered");
				return false				
			} else {
				msgDbObj.setValue('');
			}
		}	
	}	

	if(!IsNullorBlank(strIType)){

		var strIDK = GetValue("BOOK_Interpreter_DocKey");
		if(strIType == 'Agency'){		
			var strInterpreter = @DbLookup(GetDB_FilePath('AGN'), "all_byDocKey", strIDK, "AGN_Name", "[FAILSILENT]");
		} else {
			var strInterpreter = @DbLookup(GetDB_FilePath('USER'), "all_byDocKey", strIDK, "USER_FullName", "[FAILSILENT]");
			if(BOOK_DatePassed_Value() == ""){
				if(bAllow & getComponent('BOOK_Interpreter_DocKey').isRendered()){
					var strMsg = BOOK_CheckAvailability(docX, strIDK);
					var msgObj = getComponent('BOOK_CheckAvailability');
					if(strMsg == 'OK'){
						msgObj.setValue('');
					} else {			
						msgObj.setValue(BOOK_CheckAvailability_Message(strMsg, strIDK, strInterpreter));
						return false
					}
				}
			}	
		}
		if( docX.isNewNote() | (ndoc.getItemValueString('BOOK_Interpreter_FullName') != strInterpreter) ){
			docX.replaceItemValue('BOOK_Interpreter_FullName', strInterpreter);
		}		
	}

	var strLang = GetValue('BOOK_Language');
	var strLangCode = KeywordValue_FromLabel (strLang, "LanguageList");
	var dAppDuration = docX.getItemValueDouble('BOOK_ApptDuration');
	
	if(docX.isNewNote()){

		if(!IsNullorBlank(strIType)){		
			//New Document
			if(strIType == 'Agency'){
				BOOK_Status(docX, 'requested');			
				BOOK_LogAction(docX, 'New Appointment created for Agency: "' + strInterpreter + '"', true);
				//If we're onCall check if the keyword says to send straight away
				var strSendNow = GetKeyword('OnCall_SendCreateOnSave')
				if (strSendNow.equalsIgnoreCase ('Yes') && strInterpreter.equalsIgnoreCase ("ONCALL")) {
					docX.replaceItemValue(prefix + '_PendingSend', 1);
				}

			} else {
				var strSD = BOOK_Status(docX, 'booked');			
				BOOK_LogAction(docX, 'New Appointment ' + strSD +' with "' + strInterpreter + '"', true);
			}
		} else {
			var strSD = BOOK_Status(docX, 'requested');
			BOOK_LogAction(docX, 'New Appointment created', true);
		}

		docX.replaceItemValue('BOOK_Language_Code', strLangCode);
		
	} else {

		var strInterpreter = GetValue('BOOK_Interpreter_FullName');
		var strInterpreter_E = ndoc.getItemValueString("BOOK_Interpreter_FullName");
		var strLang_E = ndoc.getItemValueString('BOOK_Language');
		var strAppDate_E = ndoc.getItemValueString('BOOK_ApptStartDate_Text');
		var strAppTime_E = ndoc.getItemValueString('BOOK_ApptStartTime_Text')
		var strBookComments_E = ndoc.getItemValueString('BOOK_Comments');
		var strBookComments = GetValue('BOOK_Comments');
		var dAppDuration_E = ndoc.getItemValueDouble('BOOK_ApptDuration');
		var dPatientGenderPref_E = ndoc.getItemValueString('BOOK_PatientGenderPreference');
		var dRequestedBy_E = ndoc.getItemValueString('BOOK_RequestedBy');
		var dCostCentre_E = ndoc.getItemValueString('BOOK_CostCentre');
		
		var bAmmended = false;
		var bAmmendedForOncall = false;	//Store if oncall booking is amended		
		var hasCostCentreUpdated = false; //Store if oncall booking cost centre is amended
		
		//Language Changed
		if (strLang_E != strLang){
			BOOK_LogAction(docX, 'Language changed from "' + strLang_E +'" to "' + strLang + '"', true)
			docX.replaceItemValue('BOOK_Language_Code', strLangCode);
			bAmmended = true
		}

		//Appointment Date
		if (strAppDate_E != strAppDate){
			BOOK_LogAction(docX, 'Appointment Start Date changed from "' + strAppDate_E +'" to "' + strAppDate + '"', true);
			bAmmended = true
			if (strInterpreter.equalsIgnoreCase ("OnCall")) {
				bAmmendedForOncall = true;
			}
		}

		//Appointment Time
		if (strAppTime_E  != strAppTime){
			BOOK_LogAction(docX, 'Appointment Start Time changed from "' + strAppTime_E +'" to "' + strAppTime + '"', true);
			//If requestor is changing the time, then status is changed to Altered
			bAmmended = true
			if (strInterpreter.equalsIgnoreCase ("OnCall")) {
				bAmmendedForOncall = true;
			}
		}

		//Appointment Duration
		if (dAppDuration  != dAppDuration_E){
			BOOK_LogAction(docX, 'Appointment Duration changed from "' + @Text(dAppDuration_E) +'" to "' + @Text(dAppDuration) + '"', true);
			//If requestor is changing the time, then status is changed to Altered
			bAmmended = true
			if (strInterpreter.equalsIgnoreCase ("OnCall")) {
				bAmmendedForOncall = true;
			}
			
		}

		//BOOK / Interpreter Instructions
		if (strBookComments_E  != strBookComments){
			BOOK_LogAction(docX, BOOK_GetFieldLabel('Comments') + ' changed', true);
			//If requestor is changing the time, then status is changed to Altered
			bAmmended = true
		}

		//Agency Change
		if (!IsNullorBlank(strInterpreter_E) && strInterpreter_E  != strInterpreter){
			bAmmended = true
			BOOK_AssignToAgency_UpdateDoc(null, null, false, docX, null, null, true, strInterpreter_E, strInterpreter);
		}

		//Preffered gender changed
		if (strGenderPref  != dPatientGenderPref_E){
			BOOK_LogAction(docX, 'Interpreter gender preference changed from "' + dPatientGenderPref_E +'" to "' + strGenderPref + '"', true);
			if (strInterpreter.equalsIgnoreCase ("OnCall")) {
				bAmmendedForOncall = true;
			}
		}

		//Requested by contact changed
		if (strRequestedBy  != dRequestedBy_E){
			BOOK_LogAction(docX, 'Contact Name changed from "' + dRequestedBy_E +'" to "' + strRequestedBy + '"', true);
		}

		//reporting unit/cost centre changed
		if (strCostCentre  != dCostCentre_E){
			BOOK_LogAction(docX, 'Cost Centre changed from "' + dCostCentre_E +'" to "' + strCostCentre + '"', true);
			if (strInterpreter.equalsIgnoreCase ("OnCall")) {
				hasCostCentreUpdated = true; //If cost centre updated
			}
		}
		
		if(!IsNullorBlank(strIType)){

			//Interpreter Changed		
			if(strInterpreter_E != strInterpreter & strIType == 'In-House'){
				BOOK_Status(docX, 'booked');			
				BOOK_LogAction(docX, 'Appointment Booked with "' + strInterpreter + '"', true);	
				bAmmended = true
			}

			//Assigned to Agency.. this is to handle first time change in Type to Agency
			if(strIType != ndoc.getItemValueString('BOOK_Interpreter_Type') & strIType == 'Agency'){
				bAmmended = true
				BOOK_AssignToAgency_UpdateDoc(null, null, false, docX, null, null, false);
			}
		}
		//TODO - check if requestors allowed to send amendments to oncall directly.
		var requestorsAllowedtoAmend = "No";
		if (IsUser_Requestor()) {
			requestorsAllowedtoAmend = GetKeyword('AllowRequestorsToAmendViaAPI') != null ? GetKeyword('AllowRequestorsToAmendViaAPI') : "No";
		}
		
		/**
		 * If booking status is data booking error, need to reset some fields
		 */
		if (strStatus == 'api_create_error') {
			docX.removeItem(prefix + '_PendingSend');
			//clear agency sent details as well
			docX.removeItem('BOOK_Agency_SentDate');
			docX.removeItem('BOOK_Agency_SentBy');
			BOOK_LogAction(docX, 'Reset Data Booking Error' , true);
			var strSendNow = GetKeyword('OnCall_SendCreateOnSave');
			//Reset all fields if again assigned to OnCall for new and amended booking
			if (docX.getItemValueString(prefix + '_Interpreter_FullName').equalsIgnoreCase ("ONCALL")) {
				//If Amended booking
				if (isEnabledOnCallAmendAPI() && !ndoc.hasItem("CBN_DocKey")
						 && !docX.getItemValueString('BOOK_Agency_ID').equals("")
						 && (docX.getItemValueString('BOOK_api_amend_costcentre_error').equalsIgnoreCase('Yes') || docX.getItemValueString('BOOK_api_amend_error').equalsIgnoreCase('Yes'))) {
					if (docX.getItemValueString('BOOK_api_amend_costcentre_error').equalsIgnoreCase('Yes')) {
						hasCostCentreUpdated = true;
					}
					if (docX.getItemValueString('BOOK_api_amend_error').equalsIgnoreCase('Yes')) {
						bAmmendedForOncall = true;
					}
				} else if (strSendNow.equalsIgnoreCase ('Yes')) { //If new booking
					docX.replaceItemValue(prefix + '_PendingSend', 1);
				}
				docX.replaceItemValue('BOOK_Agency_FlowComplete', "");
				docX.replaceItemValue('BOOK_Agency_FlowComplete_Status', "");
				docX.replaceItemValue('BOOK_Agency_FlowComplete_Time', "");
			}
			//remove below now
			docX.removeItem('BOOK_api_amend_error');
			docX.removeItem('BOOK_api_amend_costcentre_error');
			//Reset status
			if((bAmmendedForOncall || hasCostCentreUpdated) 
				&& strStatus != 'api_amend_pending' 
				&& !docX.getItemValueString('BOOK_Agency_ID').equals("")
				&& !docX.hasItem("CBN_DocKey")) { //If it is due to amend error, set status to waiting for api.
				var strSD = BOOK_Status(docX, 'api_amend_pending');		
				BOOK_LogAction(docX, 'Status changed to ' + strSD, true);
			} else if (strStatus != 'requested') {
				 //If it is not due to amend error, set new status.
				var strSD = BOOK_Status(docX, 'requested');		
				BOOK_LogAction(docX, 'Status changed to ' + strSD, true);
			}
			
		}
		
		//Send updates to OnCall if any of these fields amended -  PreferredGender,AppointmentDate,AppointmentTime,Duration,CostCentre
		//If requestors are allowed, or if coordinator or manager or admin then only allow to send to oncall API
		if(isEnabledOnCallAmendAPI() && (bAmmendedForOncall || hasCostCentreUpdated) && !docX.hasItem("CBN_DocKey") 
				&& !ndoc.getItemValueString('BOOK_Agency_ID').equals("") 
				&& !ndoc.getItemValueString('BOOK_Agency_FlowComplete').equals("Yes")
			 	&& (ndoc.getItemValueString('BOOK_Status').equalsIgnoreCase("submitted_agency") 
			 			|| ndoc.getItemValueString('BOOK_Status').equalsIgnoreCase("api_amend_pending") 
			 			|| ndoc.getItemValueString('BOOK_Status').equalsIgnoreCase("api_create_error") 
			 			|| ndoc.getItemValueString('BOOK_Status').equalsIgnoreCase("booked"))
			 		) { 
			//Change booking status to BOOK_Status_api_amend_pending
			if ((IsUser_Requestor() && requestorsAllowedtoAmend.equalsIgnoreCase('Yes')) || bModifyBooking) {
				if (hasCostCentreUpdated) {
					ndoc.replaceItemValue('BOOK_Agency_UpdateCostCentre', 'Yes');
				} else {
					ndoc.replaceItemValue('BOOK_Agency_AmendDetails', 'Yes');
				}
				BOOK_SendUpdatesToOnCall(ndoc);
			} else if (IsUser_Requestor() && requestorsAllowedtoAmend.equalsIgnoreCase('No')) {
				if (hasCostCentreUpdated) {
					ndoc.replaceItemValue('BOOK_Agency_UpdateCostCentre', 'Amended'); //saving it as amended, as we are not yet sending to API, avoiding creating another field in form
				} else {
					ndoc.replaceItemValue('BOOK_Agency_AmendDetails', 'Amended'); //saving it as amended, as we are not yet sending to API, avoiding creating another field in form
				}
				BOOK_Status(docX, 'updated');
			}
		} else if(bAmmended && (IsUser_Requestor() || docX.hasItem("CBN_DocKey"))){
			if(strStatus == 'booked' || strStatus == 'submitted_agency' || (docX.hasItem("CBN_DocKey") && (strStatus=="requested"))){
				BOOK_Status(docX, 'updated');
				CBN_Status_Change(docX.getDocument(),"updated")
			}
		}
	}

//	Update Site Name
	var siteName = KeywordLabelValue(GetValue('BOOK_Site'), 'BOOK_Site')
	if( docX.isNewNote() | (ndoc.getItemValueString('BOOK_SiteName') != siteName) ){
		docX.replaceItemValue('BOOK_SiteName', siteName);
	}
//	If Completing the bookings
	var strCompleted = GetValue('BOOK_Completed');		
	if(strCompleted != ""){

		if(strCompleted.equalsIgnoreCase('Yes')){
			BOOK_Status(docX, 'completed');
			BOOK_LogAction(docX, 'Booking Completed', true);
		} else {
			var strSD = BOOK_Status(docX, GetValue('BOOK_NC_Reason'));
			BOOK_LogAction(docX, 'Status Changed to ' + strSD, true);
		}			
		//if it's an oncall document then set the end of flow field
		
		if (ndoc.getItemValueString("BOOK_Interpreter_FullName").equalsIgnoreCase ("OnCall") && !ndoc.hasItem("CBN_DocKey")) {
			docX.replaceItemValue ("BOOK_Agency_FlowComplete", "Yes")
		}
		CBN_Complete(docX);

	} else {
		if(!docX.isNewNote()){BOOK_LogAction(docX, 'Record Updated', true)}	
	}
	
	// if this was a amended booking from an agency then we need to do a few things.
	if (docX.getItemValueString ('BOOK_Amended_UNID') != '') {
		//get the original document.
		var strUNID = docX.getItemValueString ('BOOK_Amended_UNID');
		var docSource:NotesDocument = docX.getParentDatabase().getDocumentByUNID(strUNID)
		if(docSource != null){	
			
			//we need to replace BOOK_LOG, above we add entries we dont want so replace it from the 
			//original and add a new entry after
			docX.replaceItemValue ("BOOK_Log", docSource.getItemValue ("Book_Log"))
			
			//log to it this documents dockey
			BOOK_LogAction(docSource, 'Booking has been superseded', true)
			BOOK_LogAction(docSource, 'The new booking is: ' + docX.getItemValueString("DocKey"), true)
			
			//mark it as superseded and save it
			docSource.replaceItemValue("BOOK_Superseded", "Yes")
			docSource.replaceItemValue ("BOOK_SupersededBy_DocKey", docX.getItemValueString("DocKey"))
			BOOK_Status (docSource, "superseded");
			docSource.save(true, false)
						
			//log to this document IT's dockey
			BOOK_LogAction(docX, '-------------------------------------------------------------------------');
			BOOK_LogAction(docX, 'Booking has been superseded', true);
			BOOK_LogAction(docX, 'The previous booking was: ' + docSource.getItemValueString("DocKey"), true)
			BOOK_LogAction(docX, 'The new booking is: ' + docX.getItemValueString("DocKey"), true)
			docX.removeItem ("BOOK_Amended_UNID")
			docX.replaceItemValue ('BOOK_Superseded_DocKey', docSource.getItemValueString ("DocKey"))
		}
	}
	
	/**
	 * To update status for old booking while changing agency to cancellation requested
	 */
	if (docX.getItemValueString ('BOOK_ChangedAgency_UNID') != '') {
		//get the original document.
		var strUNID = docX.getItemValueString ('BOOK_ChangedAgency_UNID');
		var docSource:NotesDocument = docX.getParentDatabase().getDocumentByUNID(strUNID)
		if(docSource != null){	
			
			//we need to replace BOOK_LOG, above we add entries we dont want so replace it from the 
			//original and add a new entry after
			docX.replaceItemValue ("BOOK_Log", docSource.getItemValue ("Book_Log"))
			
			//log to it this documents dockey
			BOOK_LogAction(docSource, 'Booking has been reallocated and this version is to be cancelled: ', true)
			BOOK_LogAction(docSource, 'The new booking is: ' + docX.getItemValueString("DocKey"), true)
			
			BOOK_Status (docSource, "cancellation_req");
			docSource.replaceItemValue("BOOK_Reallocated", "Yes")
			docSource.save(true, false)
						
			//log to this document IT's dockey
			BOOK_LogAction(docX, '-------------------------------------------------------------------------');
			BOOK_LogAction(docX, 'Booking has been reallocated', true);
			BOOK_LogAction(docX, 'The previous booking was: ' + docSource.getItemValueString("DocKey"), true)
			BOOK_LogAction(docX, 'The new booking is: ' + docX.getItemValueString("DocKey"), true)
			docX.removeItem ("BOOK_ChangedAgency_UNID")
			docX.replaceItemValue ('BOOK_Cancelled_DocKey', docSource.getItemValueString ("DocKey"))
		}
	}
	/*******************/

	viewScope.remove("vs_BOOK_CutOffPeriod_Passed")
}

function BOOK_DatePassed_Value(prefix){	
	if(document1.isNewNote() == false){
		
		var doc:NotesDocument = document1.getDocument();
	if (prefix==null){prefix=doc.getItemValueString("Form")}
	var ndtAppt:NotesDateTime = doc.getFirstItem(prefix+'_Appt_Start').getDateTimeValue();
	if(session.createDateTime(@Now()).timeDifference(ndtAppt) >=0){return "Yes"}		
	}	
	return "";	
}

function BOOK_Open(sUNID){
	var strURL = 'xBOOK.xsp?documentId=' + sUNID + '&action=openDocument'
	context.redirectToPage(strURL);
}

function BOOK_Duplicate(){
	var docX:NotesXspDocument = document1;
	sessionScope.put("DuplicateBooking", docX.getDocument().getUniversalID())
	context.redirectToPage('xBOOK.xsp');
}

function BOOK_AmendAgencyBooking(){
	var docX:NotesXspDocument = document1;
	sessionScope.put("AmendBooking", docX.getDocument().getUniversalID())
	context.redirectToPage('xBOOK.xsp');
}

//Added by Binay String Compare ignore space and case
function CompareStringIgnoreCase(strKey1, strKey2){
	if (strKey1.toLowerCase().replace(/\s/gi,"")==strKey2.toLowerCase().replace(/\s/gi,""))
		return true;
	else
		return false;
}

//By Baljit - To change agency
function BOOK_ChangeAgencyBooking(){
	var docX:NotesXspDocument = document1;
	sessionScope.put("ChangeAgency", docX.getDocument().getUniversalID())
	context.redirectToPage('xBOOK.xsp');
}


//Clear out the interpreter and set it back to new
function BOOK_MarkAsUnallocated()
{
	var docX:NotesXspDocument = document1;
	var doc:NotesDocument = docX.getDocument();
	var strSD = BOOK_Status(doc, 'requested');
	doc.replaceItemValue('BOOK_Interpreter_Type', '');
	doc.replaceItemValue('BOOK_Interpreter_DocKey', '');
	doc.replaceItemValue('BOOK_Interpreter_FullName','');
	BOOK_LogAction(doc, 'Removed Interpreter Allocation', true);
	BOOK_LogAction(doc, 'Status changed to ' + strSD, true);
	BOOK_Common_SaveOpen(doc)
}

function BOOK_LanguageList(){

	var strKey = "vsLanguageList";
	var docX:NotesXspDocument = document1;

	if(viewScope.containsKey(strKey) == false){	
		var vListAll = GetAliasedKeyword('LanguageList');
		if(IsUser_Manager()| IsUser_Coordinator() | IsUser_Requestor()){			
			viewScope.put(strKey, vListAll)			
		} else {			
			var vList = ssGetVariable("ssUser_LanguageList");
			viewScope.put(strKey, vList);
			//if(docX.isNewNote() == false & @IsNotMember(GetValue('BOOK_Langauge'), vList)){viewScope.put(strKey, vListAll)} else {viewScope.put(strKey, vList)}			
		}	
	}
	return viewScope.get(strKey)
}
function BOOK_CostCentre_List(sform)
{
	if (sform==null){sform="BOOK"}
	strClinic = document1.getItemValueString(sform+'_Clinic')
	strLocation = document1.getItemValueString(sform+'_Location')
	strSite = document1.getItemValueString(sform+'_Site')
	if (IsNullorBlank(strClinic)){return "-|"}
	var vList = @Explode (@DbLookup(GetDB_FilePath('CLN'), "CLN_Lookup_CostCentre", strSite + "~" + strLocation + "~" + strClinic, 2, "[FAILSILENT]"), "~");
	if(IsNullorBlank(vList)){return "-|"} else
	{
		//if we have one value return that if we have multiple that add a '-' to the top so it's selected
		return vList.length >1 ? vList.unshift ("-|") : vList
	}
}
function BOOK_Interpreter_List(strType, strLang){
	if(!strLang){strLang = document1.getItemValueString('BOOK_Language')}
	if(IsNullorBlank(strLang)){return null}
	if(!strType){strType = document1.getItemValueString('BOOK_Interpreter_Type')}	
	if(IsNullorBlank(strType)){return null}	
	if(strType == 'In-House'){
		if(strLang.indexOf('~')<0){
			var vList = @DbLookup(GetDB_FilePath('USER'), "USER_byLanguage", strLang, 2, "[FAILSILENT]");
			if(IsNullorBlank(vList)){return null} else {return vList}
		} else {
			//Multiple Languages
			return InterpreterList_ByMultipleLanguage(strLang.split("~"))
		}
	} else {
		var vListAll = @DbLookup(GetDB_FilePath('AGN'), "AGN_byLanguage", "ALL", 2, "[FAILSILENT]");
		var vListAgency = "";
		if(strLang != "ALL"){vListAgency = @DbLookup(GetDB_FilePath('AGN'), "AGN_byLanguage", strLang, 2, "[FAILSILENT]")}	
		var vRet = @Trim(@Explode(@Implode(vListAgency,"~") + "~" + @Implode(vListAll,"~"), "~"));		
		if(IsNullorBlank(vRet)){return null} else {return vRet}
	}
}
/**
 * Show additional details for cancellation (Rejected by agency)
 * If a booking is unmet_pending or Amended if Rejected by Agency
 */
function BOOK_AdditionalCancelDetails_Show(){
	var docX:NotesXspDocument = document1;
	if (docX.isNewNote()) {return false}
	if (GetValue ("BOOK_Status") != "unmet_pending" && GetValue ("BOOK_Status") != "superseded") {return false;}
	if (GetValue("BOOK_Agency_AdditionalCancelDetails") == "" && GetValue("BOOK_Agency_FlowComplete_Status") == ""){return false;}
	return true;
}
/**
 * Created by Baljit Karwal on 29th October 2018
 * To display additional details for OnCall
 * If there is no additional details sent by OnCall, display status description
 */
function BOOK_Additional_Details(){
	var docX:NotesXspDocument = document1;
	if (docX.isNewNote()) {return false}
	if (GetValue("BOOK_Agency_AdditionalCancelDetails") != "") {
		docX.setValue('BOOK_Agency_AdditionalCancelDetails', GetValue("BOOK_Agency_AdditionalCancelDetails"));
	} else if(GetValue("BOOK_Agency_FlowComplete_Status") != "") {
		docX.setValue('BOOK_Agency_AdditionalCancelDetails', GetValue("BOOK_Agency_FlowComplete_Status"));
	}
}

function BOOK_Language_TextShow(){
	var docX:NotesXspDocument = document1;
	if(docX.isNewNote()){return false}
	if(BOOK_FieldIsEditable('BOOK_Language') == false){return false}
	if(docX.getItemValueString("BOOK_Source") == "PMS"){return false}
//	Show when langauge is not part of our list or when combo box is not visible
	var sLang =GetValue("BOOK_Language");
	var sLangCode = GetValue("BOOK_Language_Code")
	if (sLangCode!="")
	{
		sLang+="|"+sLangCode
	}
		
	return @IsNotMember(sLang, GetKeyword('LanguageList'));
}

function BOOK_CutOffPeriod_Passed(sprefix){

	/*
	 Returning True means no Edit Option.. this function assumes the appoint date is present
	1. If value is less than 0, then no edit is allowed after Appt.Date is passed
	2. If value is 0, then edit is allowed if Appt.Date belongs to same month
	3. If value is number greater than 0, then edit is allowed if Appt. Date uptil that date value of next month
	 */
	if (sprefix==null){sprefix="BOOK"}
	if(viewScope.containsKey('vs_BOOK_CutOffPeriod_Passed') == false){

		var docX:NotesXspDocument = document1;
	var dtBOOK:Date = docX.getItemValueDate(sprefix+'_Appt_Start');
	var fApptdate = parseFloat(I18n.toString(dtBOOK, "yyyyMMdd"));
	var fToday = parseFloat(I18n.toString(@Today(), "yyyyMMdd"));
	var bPassed = fApptdate < fToday ; // Appt Date is Passed

	//If not passed no need to check further.. (If Passed is false, no need to go further)
	if(bPassed){
		var dblCutOff = @TextToNumber(GetKeyword('BOOK_CutOffControl'));
		if(dblCutOff == 0){
			//Today YearMonth is Greater than Appt.Date year Month.. means cutoff is passed
			bPassed = (@TextToNumber(I18n.toString(dtBOOK, "yyyyMM")) > @TextToNumber(I18n.toString(@Today(), "yyyyMM")));
		}
		if(dblCutOff > 0){
			var dtCO:Date = new Date();
		dtCO = @Adjust(dtCO, 0, 1, 0, 0, 0, 0);	//Adjust Date to next month
		dtCO.setDate(dblCutOff);	//Set Date to the keyword value
		var fCO = parseFloat(I18n.toString(dtCO, "yyyyMMdd"));
		bPassed = (fApptdate > fCO)
		}
	}
	viewScope.put('vs_BOOK_CutOffPeriod_Passed', bPassed);		
	}	
	return viewScope.get('vs_BOOK_CutOffPeriod_Passed')	
}

function BOOK_FieldIsEditable(strField, strSection){	
	var docX:NotesXspDocument = document1;
	if(docX.isEditable() == false){return false}

	var bPMS = (GetValue("BOOK_Source") == "PMS");	//PMS Booking(ipm)
	//	No Edit of PMS Fields at all
	if(bPMS & BOOK_PMS_Fields(strField)){return false}

	var strStatus = GetValue('BOOK_Status');
	
	/**
	 * Added by Baljit on 6th Sept 2018
	 * Updated on 11 Sept 2018
	 * Do not allow editing locked fields for locked statuses
	 */
	var lockStatus = GetKeyword("BOOK_Locked_Status");
	var lockFields = GetKeyword("BOOK_Locked_Fields");
	if(lockStatus.indexOf(strStatus)>=0 && lockFields.indexOf(strField)>=0){
		return false;
	}
	//Do not allow to edit agency, if booking is already submitted to oncall(has oncall agency id) and is a data booking error.
	if ((strField == 'BOOK_Interpreter_Type' || strField == 'BOOK_Interpreter_DocKey') 
			&& (strStatus == 'api_create_error') 
			&& docX.getItemValueString('BOOK_Interpreter_FullName').equalsIgnoreCase ("ONCALL") 
			&& !docX.getItemValueString('BOOK_Agency_ID').equals("")) {
		return false;
	}

	/*
	 * Updated by Baljit on 13th Sept 2018
	 * To edit Post Booking fields even if booking is completed for Admin,Interpreter,manager and coordinator
	 */
	if (docX.getItemValueString('BOOK_Completed') != "" || docX.getItemValueString('BOOK_Status') == "completed") {
		if((strSection == 'PostBooking') && !IsUser_Requestor()) {
			return true;
		} else {
			return false;
		}
	}
	/*
	 * End change
	 */
	
	if(IsUser_Requestor()){
		if(docX.isNewNote()){			
			if(strSection == 'Interpreter'){return false;}
			return true;	
		} else {
			return BOOK_Requestor_FieldEditAllowed(strField, strStatus)
		}		
	} 
	
	if(IsUser_Interpreter()){
		//Interpreter can only create/edit booking assgined to them
		if(strField == 'BOOK_Interpreter_DocKey'){return false}
		if(strField == 'BOOK_Interpreter_Type'){return false}	
		if(strField == 'BOOK_Comments'){return false}
	}
	
	if(docX.isNewNote()){return true}

//	If Booking is marked as Cancelled only comments are allowed to edit any time
	if(strStatus == 'cancelled'){return strField == 'BOOK_CommentsInterpreter'}
	var bReturn = BOOK_FieldIsEditable_Check(strField, strSection);	
	return bReturn
}

function BOOK_FieldIsEditable_Check(strField, strSection,sprefix){
	
	var docX:NotesXspDocument = document1;
	if (sprefix==null){
		sprefix="BOOK"
	}
	/*	
		Manual Booking
			1. Edit anything until cut-off date is not passed.
			2. After cut-off date is passed, user can only complete post booking section
			3. Once Booking is complete, cut-off date is passed, no edit whatsoever the case may be
	
		PMS Booking
			1. Edit Interpreter Type, Patient Type, Interpreter, Post Booking Section until cut-off date is NOT passed
			2. After cut-off date is passed, user can only complete post booking section
			3. Once Booking is complete, cut-off date is passed, no edit whatsoever the case may be
	 */
	
	var bCutOffPassed = BOOK_CutOffPeriod_Passed();	
	var bIsCompleted = false;
	var bIsServiceDelivered = false;
	if(docX.isNewNote() == false){bIsCompleted = docX.getDocument().getItemValueString(sprefix+'_Completed') != ""}
	
	
	bIsServiceDelivered = GetValue (sprefix+"_Status") == "service_delivered"; //service delivered
	
	var bPMS = (GetValue(sprefix+"_Source") == "PMS");	//PMS Booking(ipm)
	if (strSection == 'ProviderDetails' && GetValue(sprefix+"_Interpreter_FullName").equalsIgnoreCase ("OnCall")) {return false}
	if (strSection == 'ProviderDetails' && docX.hasItem("CBN_DocKey")) {return false}

	//Cut-off date is not passed
	if(bCutOffPassed == false){		
	
		//if service is delivered only allow post booking section
		if (bIsServiceDelivered == true)
		{
			if(strSection == 'PostBooking'){return true}
			return false;
		}
		if(GetValue(sprefix+"_Source") == "IMS"){return true}
		//Below is for PMS Bookings (iPM)
		if(strField == sprefix+'_Interpreter_DocKey'){return true}	//For Interpreter already handled	
		if(strField == sprefix+'_CommentsInterpreter'){return true} //For Interpreter already handled
		if(strField == sprefix+'_Interpreter_Type'){return true} //For Interpreter already handled
		if(strField == sprefix+'_PatientType'){return true}
		if(strField == sprefix+'_BookingType'){return true}		
		if(strSection == 'PostBooking'){return true} 
	}

	//Booking is not marked as completed
	if(bIsCompleted == false){
		//if(strField == 'BOOK_Comments'){return true}
		if(strField == sprefix+'_CommentsInterpreter'){return true}
		if(strSection == 'PostBooking'){return true}
		if(strField == sprefix+'_PatientType'){return true}
		if(strField == sprefix+'_BookingType'){return true}
	}
	
	return false
}

function BOOK_PostBooking_Section_Show(){
	if(IsUser_Requestor()){
		var strStatus = document1.getItemValueString('BOOK_Status');
		return @IsMember(strStatus, ['cancellation_req', 'cancelled']);	
	}
	if(IsUser_Interpreter()){
		return BOOK_DatePassed_Value() == 'Yes'
	}
	return true;	
}

function BOOK_ProviderDetails_Section_Show(sprefix) {
	//If we've forwarded to a provider, then we will show the provider section
	//return true
	if (sprefix==null){sprefix="BOOK"}
	var strType = document1.getItemValueString(sprefix+'_Interpreter_Type')
	if (IsNullorBlank (strType)){
		return false
	}
	if (strType == 'In-House'){
		return false
	}
	//Check if we have the booking number filled in - if we dont dont bother showing
	if (document1.getItemValueString (sprefix+'_Agency_ID').equals('') && document1.getItemValueString(sprefix+'_Interpreter_Type').equalsIgnoreCase('OnCall')){
		return false;
	}
	return true
}

function BOOK_ShowBtn(strID){

	//Expected IDs from Form
	//btnCancelBooking btnMarkUnMet btnAssignToAgency btnAcceptCancelRequest btnEdit btnDropDown
	//added duplicate button to this too as I dont want to allow that if it's rejected				-Sean Ogden Power
	var docX:NotesXspDocument = document1;

	var bModifyBooking = (IsUser_Manager() | IsUser_Coordinator() | IsUser_DBAdmin());

	//	If Not in any role, then return false
	if(IsUser_Interpreter() == false & bModifyBooking == false & IsUser_Requestor() == false){return false}

	var strStatus = GetValue('BOOK_Status');
	
	//not for new documents
	if (docX.isNewNote()) {return false}

	//if duplicate then decide what todo with that (can be edit mode but not for rejected status)
	if (strID == 'lnkDuplicate')
	{
		return strStatus == 'unmet_pending' ? false : true; 
	}
	//If amendments sent by requestor, display accept amendment btn for valid users and for Oncall submitted bookings only except combined child bookings
	if (isEnabledOnCallAmendAPI() && strID == 'btnAcceptAmendments' 
		&& bModifyBooking && strStatus == 'updated' 
		&& docX.getItemValueString('BOOK_Interpreter_FullName').equalsIgnoreCase ("ONCALL") 
		&& !docX.hasItem("CBN_DocKey")
		&& !docX.getItemValueString('BOOK_Agency_ID').equals(""))
	{
		return true;
	}
	
	//All these buttons are for read mode only
	if(docX.isEditable()){return false}


	//	This will check if the post booking section is itself not allowed to edit,
	//	then it means nothing is allowed to edit on this document
	if(BOOK_FieldIsEditable_Check('BOOK_Completed', 'PostBooking') == false){return false}

	//	This is for dropdown buttons
	if(strID == 'btnDropDown'){
		var vChildBtns = ['btnCancelBooking', 'btnReallocate', 'btnMarkUnMet', 'btnAssignToAgency', 'btnAmendBooking', 'btnUnallocate', 'btnRefresh'];
		for(var i=0; i<vChildBtns.length; i++){if(BOOK_ShowBtn(vChildBtns[i])){return true}}
		return false
	}

	var IsPast = (BOOK_DatePassed_Value().equalsIgnoreCase('Yes'));
		
	var strType = GetValue('BOOK_Interpreter_Type');

	if(strID == 'btnRequestCancellation' & !IsPast){		
		//Added a new status check for requester to request cancellation even if booking status is "submitted_agency"
		return IsUser_Requestor() & @IsMember(strStatus, ['requested', 'booked', 'updated', 'submitted_agency']); 
	}

	//	And for Interpreters if the language is not from keywords list or its their own language
	if(strID == 'btnEdit') {
		//To display Edit button for all bookings except rejected by agency, so that users can edit post booking details
		if(!IsUser_Requestor() && @IsNotMember(strStatus, ['unmet_pending'])) {
			return true;
		}
		
		if(@IsNotMember(strStatus, ['requested', 'booked', 'updated', 'submitted_agency', 'service_delivered'])){return false}

		if(bModifyBooking){return true}
		
		if(IsUser_Interpreter()){
			return @IsMember(GetValue("BOOK_Language"), BOOK_LanguageList())				
		}
		
		//This part now handled by new buttons for Edit Control of Requestors
		if(IsUser_Requestor()){return false}
		
		return false
	}
	
	if(strID == 'btnEdit_Requestor_CJS' || strID == 'btnEdit_Requestor_SJS'){
		if(IsUser_Requestor() == false){return false}
		if(GetValue('BOOK_Source') == 'PMS'){return false}
		if(IsPast){return false} //Updated by Baljit on 21st Sept to hide edit button for past bookings
		return @Implode(GetKeyword('BOOK_Requestor_FieldEditAllowed'), '') != 'NULL'		
	}
	
	
	//	These Buttons are only available in Read Mode and following status only
	if(@IsMember(strID, ['btnCancelBooking', 'btnMarkUnMet', 'btnAssignToAgency'])){	
		//don't let them do these if we've superseded the document
		if (docX.getItemValueString("BOOK_Superseded").equalsIgnoreCase('Yes')) {return false}
		
		if (strID=='btnMarkUnMet'){
			if(@IsMember(strStatus, ['cancelled', 'unmet'])){return false}
			return IsPast
		}
		
		if (strID == 'btnCancelBooking')
		{
			//more statuses for btnCancel
			//if (docX.hasItem("CBN_DocKey")){return false}
			//if past booking, return false//Updated by Baljit on 25 Jan 19 to hide cancel button for past bookings
			if(IsPast){return false} 
			if(@IsNotMember(strStatus, ['requested', 'booked', 'updated', 'unmet_pending', 'submitted_agency', 'api_create_error','api_amend_pending'])){return false}
		} else {		
			if(@IsNotMember(strStatus, ['requested', 'booked', 'updated'])){return false}
		}
		//Hide Cancel Booking button if status is 'Cancellation Requested'
		if(strStatus == "cancellation_req" & strID == "btnCancelBooking"){return false}		
		if(bModifyBooking){return true}
		if(IsUser_Interpreter()){
			//if(strID == "btnCancelBooking"){return true}
			//if(strID == 'btnAssignToAgency'){return !IsPast}
		}
		return false
	}

	//the mark as unallocated button should only be available when the interpreter is 
	//internal and the status isn't new(requested) and we're not passed.
	if (strID == 'btnUnallocate' && @IsNotMember(strStatus, ['requested', 'cancelled', 'completed']) 
			&& (bModifyBooking || IsUser_Interpreter()) 
			&& strType == 'In-House' && IsPast != 'Yes')
	{
		if (docX.hasItem("CBN_DocKey")){return false}
		return true;
	}
	
	if (strID == 'btnRefresh' && @IsMember(strStatus, ['submitted_agency', 'booked']) 
			&& docX.getItemValueString('BOOK_Interpreter_FullName').equalsIgnoreCase ("ONCALL") 
			&& !docX.getItemValueString('BOOK_Agency_ID').equals("") && !docX.hasItem("CBN_DocKey"))
	{
		return true;
	}
	
	
	//amend booking is for 'rejected' documents, and only if it hasn't already been amended
	if (strID == 'btnAmendBooking' && strStatus == 'unmet_pending' && bModifyBooking && GetValue ('BOOK_Superseded') != 'Yes') {
		return true;		
	}
	
	//Display Reallocate button(if not already reallocated) for Submitted to agency, Booked, Updated(Amended) bookings for future bookings for a admin, manager, coordinator and interpreter
	var strAllowedStatusForReallocate = ['submitted_agency', 'booked' , 'updated'];
	if (strID == 'btnReallocate' && docX.getItemValueString("BOOK_Reallocated") != 'Yes' && @IsMember(strStatus , strAllowedStatusForReallocate) 
			&& !IsPast && !IsUser_Requestor() && !IsUser_Interpreter()) {
		return !docX.hasItem("CBN_DocKey");		
	}
	
	if(strID == 'btnAcceptCancelRequest' & strStatus == "cancellation_req"){
		if(bModifyBooking){
			return !docX.hasItem("CBN_DocKey")
		} else {
			//Northern doesn't want Interpreters to accept cancellation booking
			//return GetValue('BOOK_Interpreter_DocKey') == General_GetUser_DocKey()			
		}
	}

	return false;	
}

function BOOK_MarkAsUnmet(){
	var ndoc:NotesDocument = document1.getDocument()
	var strSD = BOOK_Status(ndoc, 'unmet');	
	ndoc.replaceItemValue('BOOK_Completed', 'No');
	ndoc.replaceItemValue('BOOK_NC_Reason', 'Unmet');	
	BOOK_LogAction(ndoc, 'Booking marked as ' + strSD , true);
	BOOK_Common_SaveOpen(ndoc);
}

function BOOK_ShowCancellationSection(){
	var docX:NotesXspDocument = document1;
	if(GetValue('BOOK_NC_Reason') == 'cancelled'){return true}
	if(GetValue('BOOK_Status') == 'cancellation_req'){return true}
	return false
}

/**
 * Updated by Baljit Karwal on 1 Aug 2019 to save multiple cancellation requested reasons
 */
function BOOK_RequestCancellation(){
	var ndoc:NotesDocument = document1.getDocument();
	var sprefix = ndoc.getItemValueString("Form")||"BOOK";
	var hours = BOOK_Cancellation_TimeDifference(ndoc);
	if (hours>24){
		ndoc.replaceItemValue(sprefix+"_fldCancelNoticePeriod" , "24 hrs prior");
	} else {
		ndoc.replaceItemValue(sprefix+ "_fldCancelNoticePeriod" , "Same day");
	}
	ndoc.replaceItemValue(sprefix+'_fldCancelReasonDD', getComponent('fldCancelReasonDD').getValue());
	ndoc.replaceItemValue(sprefix+'_fldCancelOtherReason', getComponent('fldCancelOtherReason').getValue());
	ndoc.replaceItemValue(sprefix+'_fldCancelReason', getComponent('fldCancelReason').getValue());
	ndoc.replaceItemValue(sprefix+'_fldCancelINRReason', getComponent('fldCancelINRReason').getValue());
	ndoc.replaceItemValue(sprefix+'_fldCancelIFTAReason', getComponent('fldCancelIFTAReason').getValue());
	ndoc.replaceItemValue(sprefix+'_fldCancelIFTAOtherReason', getComponent('fldCancelIFTAOtherReason').getValue());
	var strSD = BOOK_Status(ndoc, 'cancellation_req');
	BOOK_LogAction(ndoc, 'Status changed to ' + strSD, true);
	ndoc.save(true, false);
	CBN_Status_Change(ndoc);
}

function BOOK_Cancellation_TimeDifference(ndoc:NotesDocument){
	try {
		var dbSaS:NotesDatabase = sessionAsSigner.getCurrentDatabase();
	} catch (e){
		print ("Error getting Session As Signer");
		return (0);
	}
	var docTemp:NotesDocument = dbSaS.getProfileDocument("TimeDifference", ndoc.getUniversalID());
	ndoc.getFirstItem("BOOK_Appt_Start").copyItemToDocument(docTemp);
	docTemp.save(true, false);

	var strID = docTemp.getNoteID();
	docTemp.recycle();
	if (dbSaS.getAgent("agtTimeDiff").runOnServer(strID) == 0){
		docTemp = dbSaS.getDocumentByID(strID);
		if(docTemp == null){print("ERROR: Cannot Locate Time Difference document"); return 0}	
		var hours = docTemp.getItemValueDouble("Hours");
		docTemp.remove(true);
		return hours

	} else {
		print("ERROR: Agent did not run for Time Difference"); return 0
	}
}

function BOOK_AcceptCancelRequest(){
	var ndoc:NotesDocument = document1.getDocument();
	print('in accept cancellation before opening dialog');
	print('--------------' +ndoc.getItemValueString('BOOK_fldCancelReasonDD'));
	view.postScript("BOOK_AcceptCancelDialog('"+ndoc.getItemValueString('BOOK_fldCancelReasonDD')+"')");
	//BOOK_CancelBooking ();
}

//Cancel an agency assigned booking if we can with out confirming, if we need to confirm return that
function BOOK_CheckCancelAgencyBooking (ndoc:NotesDocument, sprefix)
{
	if (sprefix==null){sprefix="BOOK"}
	//lets check if it's x hours before the booking(keyword)
	var strCancelCutoff = GetKeyword ('BOOK_CancelCutOffHours');
	var ndtAppt:NotesDateTime = ndoc.getFirstItem(sprefix+'_Appt_Start').getDateTimeValue();
	ndtAppt.adjustHour (-parseInt (strCancelCutoff, 10));
	if(session.createDateTime(@Now()).timeDifference(ndtAppt) >=0){
		//we're passed the cut off so we need to do more work
		if (ndoc.getItemValueString (sprefix+"_Interpreter_FullName").equalsIgnoreCase("ONCALL") && !ndoc.getItemValueString(sprefix + '_Agency_ID').equals("")) { 
			return BOOK_CheckCancellationChargesOnCall (ndoc);
		} else {
			errorReason = ndoc.getItemValueString (sprefix+"_PatientFirstName") + " " + ndoc.getItemValueString (sprefix+"_PatientLastName") + " - ";
			errorReason += ndoc.getItemValueString (sprefix+"_ApptStartDate_Text") + " " + ndoc.getItemValueString (sprefix+"_ApptStartTime_Text"); 
			return errorReason;
		}
	} else {
		//the cut off hasn't passed so we should be ok. If we're onCall mark the doc Pending Cancel.
		if (ndoc.getItemValueString (sprefix+"_Interpreter_FullName").equalsIgnoreCase("ONCALL") && !ndoc.getItemValueString(sprefix + '_Agency_ID').equals("")) {
			return BOOK_SendCancelToOnCall (ndoc);
		}
		return true;
	}
}


function BOOK_CheckCancellationChargesOnCall (ndoc:NotesDocument){
	//Check if we have oncall cancelation charges
	try {
		var dbSaS:NotesDatabase = sessionAsSigner.getCurrentDatabase();
	} catch (e){
		print ("Error getting Session As Signer");
		return (false);
	}
	var sprefix = ndoc.getItemValueString("Form")||"BOOK";

	var docTemp:NotesDocument;
	var String:strID;
	var String:agtSuccess;
	var String:agtChargesApply;
	var String:agtMessage;
	var String:agtError;
	var String:agtErrorCode;
	docTemp = dbSaS.getProfileDocument("CheckChargesOnCallBooking", ndoc.getUniversalID());
	docTemp.replaceItemValue ("DocKey", ndoc.getItemValueString ("DocKey"));
	docTemp.save(true, false);
	strID = docTemp.getNoteID();
	docTemp.recycle();
	if (dbSaS.getAgent("agt_API_CheckCharges").runOnServer(strID) == 0){
		docTemp = dbSaS.getDocumentByID(strID);
		if(docTemp == null){print("ERROR: Cannot Locate Check Charges OnCall Agent document"); return false}	
		
		//handle the response
		agtSuccess = docTemp.getItemValueString ("Success");
		agtChargesApply = docTemp.getItemValueString ("HasCharges");
		agtMessage = docTemp.getItemValueString ("message");
		agtError = docTemp.getItemValueString ("errorMessage");
		agtErrorCode = docTemp.getItemValueString ("errorCode");
		docTemp.remove(true);
		
		//Create an object to return multiple values
		var retObject = {};
		retObject.isTrue = false;
		retObject.agtError = "";
		retObject.agtMessage = "";
		retObject.agtErrorCode = "";
		retObject.ChargesApply = "";
		if (agtSuccess.equalsIgnoreCase("Yes")){
			if (agtChargesApply.equalsIgnoreCase ("TRUE")) {
				retObject.agtMessage = agtMessage;
				retObject.isTrue = true;
			} else {
				retObject.isTrue = BOOK_SendCancelToOnCall (ndoc);
			}
		} else { 
			//Error returned by Charges Chcek OnCall API - charges might apply but bypass to return true and cancel inside IMS with warning
			errorReason = ndoc.getItemValueString (sprefix+"_PatientFirstName") + " " + ndoc.getItemValueString (sprefix+"_PatientLastName") + " - ";
			errorReason += ndoc.getItemValueString (sprefix+"_ApptStartDate_Text") + " " + ndoc.getItemValueString (sprefix+"_ApptStartTime_Text"); 
			retObject.agtMessage = errorReason; //booking details
			retObject.ChargesApply = 'might'; //we are not sure if charges apply or not
			retObject.isTrue = true; //bypass charges check and return true with booking details
		}
		return retObject;
	} else {
		print("ERROR: Agent did not run for Check Charges OnCall"); return false
	}	
}

//Called from a 'confirmation' dialog if there are charges when cancelling bookings.
//will then handle for different agencies
function BOOK_CancelMultipleAgency ()
{
	//loop through all of the entries in selUNIDs and process them
	var selEntries = getComponent ("selUNIDs").getValue().split('~');
	getComponent ("selUNIDs").setValue ("");	
	//get cancellation fields
	var cancellationReasonsObject = {};
	cancellationReasonsObject.fldCancelReasonDD = getComponent('fldCancelReasonDD').getValue();
	cancellationReasonsObject.fldCancelOtherReason = getComponent('fldCancelOtherReason').getValue();
	cancellationReasonsObject.fldCancelNoticePeriod = getComponent('fldCancelNoticePeriod').getValue();
	cancellationReasonsObject.fldCancelReason = getComponent('fldCancelReason').getValue();
	cancellationReasonsObject.fldCancelINRReason = getComponent('fldCancelINRReason').getValue();
	cancellationReasonsObject.fldCancelIFTAReason = getComponent('fldCancelIFTAReason').getValue();
	cancellationReasonsObject.fldCancelIFTAOtherReason = getComponent('fldCancelIFTAOtherReason').getValue();

	var unid;
	var ndoc:NotesDocument;
	for(var i = 0; i<selEntries.length; i++)
	{
		unid = selEntries[i].split('|')[0];
		if (!unid.equals('')) {
			ndoc = GetDB_Database('BOOK').getDocumentByUNID(unid);
			BOOK_CancelSingleAgency (ndoc, cancellationReasonsObject, true);
		}
	}
}

//Called from a 'confirmation' dialog, or the function above, if there are charges when cancelling a single (UI) booking.
//will then handle the different agencies
function BOOK_CancelSingleAgency (ndoc:NotesDocument, cancellationReasonsObject, isGrid)
{
	if (!isGrid) {isGrid = false}
	if (!ndoc){
		var docX:NotesXspDocument = document1;
		ndoc = docX.getDocument();
	}
	if (typeof cancellationReasonsObject == 'undefined' || cancellationReasonsObject == null) {
		//get cancellation fields
		var cancellationReasonsObject = {};
		cancellationReasonsObject.fldCancelReasonDD = getComponent('fldCancelReasonDD').getValue();
		cancellationReasonsObject.fldCancelOtherReason = getComponent('fldCancelOtherReason').getValue();
		cancellationReasonsObject.fldCancelNoticePeriod = getComponent('fldCancelNoticePeriod').getValue();
		cancellationReasonsObject.fldCancelReason = getComponent('fldCancelReason').getValue();
		cancellationReasonsObject.fldCancelINRReason = getComponent('fldCancelINRReason').getValue();
		cancellationReasonsObject.fldCancelIFTAReason = getComponent('fldCancelIFTAReason').getValue();
		cancellationReasonsObject.fldCancelIFTAOtherReason = getComponent('fldCancelIFTAOtherReason').getValue();
	}
	if (ndoc.getItemValueString ("BOOK_Interpreter_FullName").equalsIgnoreCase("ONCALL") && !ndoc.getItemValueString('BOOK_Agency_ID').equals("")) {
		if (BOOK_SendCancelToOnCall (ndoc))
		{
			BOOK_UpdateDocument_CancelledDetails (ndoc, isGrid, cancellationReasonsObject)
		} else {
			//there was an error cancelling through the api - right now do nothing
			return false;
		}
	} else {
		BOOK_UpdateDocument_CancelledDetails (ndoc, isGrid, cancellationReasonsObject)	
	}
}


//Call the on call cancellation API
function BOOK_SendCancelToOnCall (ndoc:NotesDocument)
{	
	try {
		var dbSaS:NotesDatabase = sessionAsSigner.getCurrentDatabase();
	} catch (e){
		print ("Error getting Session As Signer");
		return (false);
	}

	var docTemp:NotesDocument;
	var String:strID;
	var String:agtSuccess;
	var String:agtError;
	var String:agtErrorCode;
	
	docTemp = dbSaS.getProfileDocument("CancelOnCallBooking", ndoc.getUniversalID());
	docTemp.replaceItemValue ("DocKey", ndoc.getItemValueString ("DocKey"));
	docTemp.replaceItemValue ("CancelledBy", General_GetUser_FullName());
	
	docTemp.save(true, false);
	strID = docTemp.getNoteID();
	docTemp.recycle();
	if (dbSaS.getAgent("agt_API_CancelBooking").runOnServer(strID) == 0){

		docTemp = dbSaS.getDocumentByID(strID);
		if(docTemp == null){print("ERROR: Cannot Locate Cancel OnCall Agent document"); return false}	
		//handle the response
		agtSuccess = docTemp.getItemValueString ("Success");
		agtError = docTemp.getItemValueString ("errorMessage");
		agtErrorCode = docTemp.getItemValueString ("errorCode");
		docTemp.remove(true);
		if (agtSuccess.equalsIgnoreCase("Yes")){
			return true;	
		} else {
			//for some reason API returned error, check errorCode and process
			return BOOK_OnCall_CancelProcessErrors(agtErrorCode);
		}
	} else {
		print("ERROR: Agent did not run for Cancel OnCall"); return false
	}	
}
/**
 * This function verifies if ignored status codes can return to update cancellation inside IMS.
 */
function BOOK_OnCall_CancelProcessErrors(agtErrorCode) {
	if (agtErrorCode) {
		if (getOncallErrorCodeList() !== '' || getOncallErrorCodeList() !== 'undefined') {
			if (@IsMember(agtErrorCode, getOncallErrorCodeList())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	return false;
}
/**
 * Get list of ignored oncall error codes for cnacellation
 */
function getOncallErrorCodeList() {
	var strKey = "vsOnCallErrorCodeList";
	if(viewScope.containsKey(strKey) == false){	
		var vListAll = GetKeyword('OnCall_IgnoreErrorCodes');
		viewScope.put(strKey, vListAll)			
	}
	return viewScope.get(strKey)
}

function BOOK_LogAction(doc, logStr, appendUserName){
	var logFldName = 'BOOK_Log'
		if(doc.getItemValueString('Form') == 'CBN'){logFldName = 'CBN_Log'}
	var strValue = I18n.toString(@Now(), "dd-MMM-yyyy hh:mm a") + " - " + logStr;	
	if(appendUserName){strValue += ' - by ' + General_GetUser_FullName()}	
	var vLog:java.util.Vector = doc.getItemValue(logFldName);
	vLog.addElement(strValue);
	doc.replaceItemValue(logFldName, vLog);
}

function BOOK_AssignAgency(){

	var docX:NotesXspDocument = document1;
	var ndoc:NotesDocument = docX.getDocument();
	var strIDK = getComponent('fldAgency').getValue();
	var strInterpreter = @DbLookup(GetDB_FilePath('AGN'), "all_byDocKey", strIDK, "AGN_Name", "[FAILSILENT]");	
	if(IsNullorBlank(strInterpreter)){
		view.postScript("Notify_Warning('ERROR: Couldn't find Agency details, Please contact IT Support')")
		return
	}

	BOOK_AssignToAgency_UpdateDoc(strIDK, strInterpreter, true, null, ndoc, null, false);		

	view.postScript("Notify_Info('SUCCESS: Bookings assigned to \"" + strInterpreter + "\"'); UpdateFTIndex('xFTIndex_Agent.xsp')")	
}


function BOOK_FTIndex(){view.postScript("UpdateFTIndex('xFTIndex_Agent.xsp')")}
function BOOK_FTIndex_Agent(){sessionAsSigner.getCurrentDatabase().getAgent("agtFTIndex").runOnServer()}

function BOOK_CheckURN(){

	var docX:NotesXspDocument = document1;
var fldValue = GetValue('BOOK_PatientUR');

if(IsNullorBlank(fldValue)){return}

//Sample Value -  12972 : Marsh (Sam)
if(fldValue.indexOf(":")<0){return}

docX.replaceItemValue('BOOK_PatientUR', @Trim(@Left(fldValue, ":")));
var vFull = @Trim(@Right(fldValue, ":")).split(' ');
docX.replaceItemValue('BOOK_PatientFirstName', vFull[0]);
docX.replaceItemValue('BOOK_PatientLastName', vFull[1]);
if(vFull[1].indexOf("(") >=0 & vFull[1].indexOf(")")){
	docX.replaceItemValue('BOOK_PatientLastName', @Left(vFull[1], "("));
	docX.replaceItemValue('BOOK_PatientGender', @Left(@Right(vFull[1], "("), ")"));
}
}



function BOOK_Status(dataSource, strSysStatus, bNoUpdate){
	if(!bNoUpdate){bNoUpdate = false}

	var strKey = "as_BOOKStatus";
	//If strSysStatus is not in list of IMS Status Keywords then do not change to lowercase and display as it is
	if (viewScope.get('vIMSList') != null && viewScope.get('vIMSList').contains(strSysStatus)) {
		strSysStatus = @LowerCase(strSysStatus);
	}
	//Update status of a booking, if it is reallocated and now cancellation is accepted
	if (dataSource.getItemValueString('BOOK_Reallocated') == "Yes" && strSysStatus == "cancelled") {
		strSysStatus = GetKeyword('BOOK_Status_Reallocated');
	}
	var vHM:java.util.HashMap = new java.util.HashMap();
	if(applicationScope.containsKey(strKey)){vHM = applicationScope.get(strKey)}

	if(vHM.containsKey(strSysStatus) == false){
		if (GetKeyword('BOOK_Status_' + strSysStatus).equals("")) {
			vHM.put(strSysStatus, strSysStatus);
		} else {
			vHM.put(strSysStatus, GetKeyword('BOOK_Status_' + strSysStatus));
		}
		applicationScope.put(strKey, vHM);
	}

	var strStatusDisplay = vHM.get(strSysStatus);

	if(bNoUpdate == false){
		if(dataSource.getItemValueString('Form') == 'CBN'){
			dataSource.replaceItemValue('CBN_Status', strSysStatus);
			dataSource.replaceItemValue('CBN_Status_Display', strStatusDisplay);
		} else {
			dataSource.replaceItemValue('BOOK_Status', strSysStatus);
			dataSource.replaceItemValue('BOOK_Status_Display', strStatusDisplay);
		}
	}	

	return strStatusDisplay
}

function BOOK_Flexible_Show(){
	if(GetKeyword('BOOK_Field_Enable_Flexible') == '0'){return false}
	//This section is for Requestor to provide flexible time range if booking created within 48 hours (Keyword)
	var docX:NotesXspDocument = document1;
	if(docX.isNewNote()){
		return (IsUser_Requestor() || IsUser_Coordinator() || IsUser_Manager());		
	} else {
		return docX.hasItem('BOOK_Flexible');
	}
}

function BOOK_Clinic_Location_TypeaheadURL(strType,docX:NotesXspDocument){
	if (docX==null){ docX = document1;}
	var strSite = GetValue('BOOK_Site');
	var strLocation = GetValue('BOOK_Location');
	var strURL = './xJson_Data.xsp?Frm=BOOK&TAType=' + strType;
	if(strSite != ''){strURL += '&Site=' + strSite;}
	//if(strType == 'Clinic'){if(strLocation != ''){strURL += '&Loc=' + strLocation}}
	strURL += '&taQuery='
	return strURL
}

function BOOK_Location_OnChange(docX:NotesXspDocument,sPre){
	//check if value has site in it, then set site, else return
	//Sample Value: A Corridor - Outpatient Clinic 3 (Site: BHS)
	if (docX==null){ docX = document1;}
	if (sPre==null){sPre="BOOK"}
	var strValue = docX.getItemValueString(sPre+'_Location');
	if(strValue.indexOf('(Site:') >=0){
	docX.replaceItemValue(sPre+'_Location', @Left(strValue, '(Site:'));	
	docX.replaceItemValue(sPre+'_Site', @Trim(@Left(@Right(strValue, '(Site:'), ')')));
	docX.replaceItemValue(sPre+'_SiteName', @Trim(@Left(@Right(strValue, '(Site:'), ')')));
	}	
}

function BOOK_Clinic_OnChange(docX:NotesXspDocument,sPre){
	//check if value has site or location in it, then set value, else return
	//Sample Value: N - Endoscopy Outpatients - Mahindra (Site: TNH, Location: Area 18 - Outpatients E)
	if (docX==null){ docX = document1;}
	if (sPre==null){sPre="BOOK"}
	var strValue = docX.getItemValueString(sPre+'_Clinic');
	if(strValue.indexOf('(Site:') >=0 & strValue.indexOf('Location:') >=0){
		
		docX.replaceItemValue(sPre+'_Clinic', @Trim(@Left(strValue, '(Site:')));
		docX.replaceItemValue(sPre+'_Site', @Trim(@Left(@Right(strValue, '(Site:'), ', Location:')));
		docX.replaceItemValue(sPre+'_SiteName', @Trim(@Left(@Right(strValue, '(Site:'), ', Location:')));
		docX.replaceItemValue(sPre+'_Location', @Trim(@LeftBack(@Right(strValue, 'Location:'), ')')));
		
		if(sPre == "BOOK" & GetKeyword('AllowClinicAddress').equalsIgnoreCase('Yes')){
			var vwCLN:NotesView = GetDB_Database('CLN').getView("CLN_TA_Clinics");
			var docCLN:NotesDocument = vwCLN.getDocumentByKey(strValue, true);
			if(docCLN != null){
				docX.replaceItemValue('BOOK_Clinic_Address', docCLN.getItemValueString('CLN_Clinic_Address'));
				docX.replaceItemValue('BOOK_Clinic_Suburb', docCLN.getItemValueString('CLN_Clinic_Suburb'));
				docX.replaceItemValue('BOOK_Clinic_PostCode', docCLN.getItemValueString('CLN_Clinic_PostCode'));
				docX.replaceItemValue('BOOK_Clinic_State', docCLN.getItemValueString('CLN_Clinic_State'));
			}
		}	
		
		if (sPre=="RCN"){
			docX.replaceItemValue("Flag_Site_Selected", "OK");		
		}
	}
}

function BOOK_Site_OnChange(docX:NotesXspDocument,sPre){
	//clear out the clinic and location
	if (docX==null){ docX = document1;}
	if (sPre==null){sPre="BOOK"}
	docX.replaceItemValue(sPre+'_Clinic', '');
	docX.replaceItemValue(sPre+'_Location', '');
}

function BOOK_PMS_Fields(strField){
	var strKey = 'PMS_Fields';
	if(applicationScope.containsKey(strKey) == false){
		var vPMSFields = @Trim(@Right(GetKeyword('ODBC_SP_BookingData_ColumnNames'), "~"));
		applicationScope.put(strKey, vPMSFields);
	}
	var vValues = applicationScope.get(strKey);
	if(strField){
		print(strField+" "+ @IsMember(strField, vValues));
		return @IsMember(strField, vValues);	
	} else {
		return vValues;
	}	
}

function BOOK_RCN_Records(unid)
{
	if (unid==null){unid = param.documentId}
	if (unid==null){return null}
	var rcnDb:NotesDatabase = GetDB_Database("RCN_UPLOAD")
	var rcnView:NotesView = rcnDb.getView("luRCNByBOOKUID")
	var dc:NotesDocumentCollection = rcnView.getAllDocumentsByKey(unid,true)
	return dc
}

function BOOK_Requestor_FieldEditAllowed(strField, strStatus){
	var strKey = 'Requestor_FieldEditAllowed';
	if(applicationScope.containsKey(strKey) == false){
		var vValues = GetKeyword('BOOK_Requestor_FieldEditAllowed');
		if(isString(vValues)){
			if(vValues == 'NULL'){applicationScope.put(strKey, vValues)}
		} else {
			var vHM:java.util.HashMap = new java.util.HashMap(); var vTemp;
			for(var i=0;i<vValues.length; i++){
				if(vValues[i].indexOf('~')<0){
					vHM.put(vValues[i], ['ALL'])	
				} else {
					vTemp = @Right(vValues[i], '~');
					if(vTemp.indexOf(':')>=0){vTemp = vTemp.split(':')} else {vTemp = [vTemp]}
					vHM.put(@Left(vValues[i], '~'), vTemp);	
				}				
			}
		}
		applicationScope.put(strKey, vHM);
	}
	var vValues = applicationScope.get(strKey);
	if(isString(vValues)){if(vValues == 'NULL'){return false}}
	if(vValues.containsKey(strField)){
		var vStatus = vValues.get(strField);
		if(vStatus.join('') == 'ALL'){return true}
		if(vStatus.indexOf(strStatus)>=0){return true}
	}	
	return false
}

function BOOK_Interpreter_Edit_PMS_Booking_Show(){
	var docX:NotesXspDocument = document1;
	if(GetValue('BOOK_Status') == 'requested' & IsUser_Interpreter() & GetValue("BOOK_Source") == "PMS"){
		return true
	} else {
		return false
	}	
}

function BOOK_Interpreter_Edit_PMS_Booking(){
	var docX:NotesXspDocument = document1;
	docX.setValue('BOOK_Interpreter_Type', 'In-House');
	docX.setValue('BOOK_Interpreter_DocKey', General_GetUser_DocKey());
	docX.setValue('BOOK_Interpreter_FullName', General_GetUser_FullName());
}

function BOOK_ChangeInterpreterHoursUpdate(sform){
	if (sform==null){sform="BOOK"}
	var docX:NotesXspDocument = document1;	
	USER_HRS_DT_AddEdit(docX.getItemValue(sform+'_ApptStartDate_Text'), docX.getItemValueString(sform+'_Interpreter_DocKey'), 'A');	
}

function Show_RCN_Section()
{
	if (GetKeyword("EnableInvoiceRecon").equalsIgnoreCase('Yes'))
	{
		var rdc:NotesDocumentCollection = BOOK_RCN_Records();
	if (rdc==null){return false}
	if (rdc.getCount()>0){
		return IsUser_Manager() || IsUser_DBAdmin()|| IsUser_Coordinator()
	}
	}
	return false
}


function DefaultBookingType() {
	return document1.isNewNote() ? GetKeyword('BOOK_DefaultDeliveryMethod') : '';
}

function BOOK_GetLatestStatus() {
	var docX:NotesXspDocument = document1;
	if (docX.isNewNote()) {return false}
	var latestStatusDisplay = GetValue('BOOK_Status_Display');
	var latestStatus = GetValue('BOOK_Status');
	var latestSource = GetValue('BOOK_Source');
	var latestSourceDisplay = GetValue('BOOK_Source_Display');
	return latestStatusDisplay+' ' + latestSourceDisplay;
}

/**
 * Added by Baljit Karwal
 * To get status of a booking from OnCall through API call and update the status of a booking.
 * 
 * return boolean
 */
function BOOK_RefreshStatus(docX:NotesXspDocument){
	if (docX==null){ docX = document1;}
	var ndoc:NotesDocument = docX.getDocument();
	if (!ndoc.isNewNote()) {
		try {
			var dbSaS:NotesDatabase = sessionAsSigner.getCurrentDatabase();
		} catch (e){
			print ("Error getting Session As Signer");
			return (false);
		}
		var docTemp:NotesDocument;
		var String:strID;
		var String:agtSuccess;
		docTemp = dbSaS.getProfileDocument("RefreshStatusOnCall", ndoc.getUniversalID());
		docTemp.replaceItemValue ("DocKey", ndoc.getItemValueString ("DocKey"));
		docTemp.save(true, false);
		strID = docTemp.getNoteID();
		docTemp.recycle();
		var sform = ndoc.getItemValueString("Form")
		if (dbSaS.getAgent("agt_API_RefreshSingleStatusOnCallBooking").runOnServer(strID) == 0){
			docTemp = dbSaS.getDocumentByID(strID);
			if(docTemp == null){print("ERROR: Cannot Locate Refresh Status OnCall Booking Agent document"); return false}	
			//handle the response
			agtSuccess = docTemp.getItemValueString ("Success");
			docTemp.remove(true);
			//get latest values from same document
			var latestDoc:NotesDocument = ndoc.getParentDatabase().getView('vwAllByDocKey').getDocumentByKey(ndoc.getItemValueString ("DocKey"));
			var oldStatusDisplay = ndoc.getItemValueString(sform+'_Status_Display');
			if (agtSuccess.equalsIgnoreCase("Yes")){
				var latestStatusDisplay = latestDoc.getItemValueString(sform+'_Status_Display');
				if (oldStatusDisplay != latestStatusDisplay) {
					view.postScript("reloadIframe('iframeDoc')");
					view.postScript("Notify_Info('Status updated from OnCall successfully')");
					return true;
				} else if (oldStatusDisplay == latestStatusDisplay) {
				 	view.postScript("Notify_Warning('No change in status on OnCall')");
				}
			} else {
				//give post script msg that unable to refresh for some reason
				view.postScript("Notify_Warning('ERROR: There was an error refreshing status from ONCALL, if the problem persists, please contact IT Support')");
				return false;
			}
		}
	}
}


/**
 * Get full name of interpreter
 */
function BOOK_GetInterpreterFullName() {
	var docX:NotesXspDocument = document1;
	var ndoc:NotesDocument = docX.getDocument();
	var fullName = ndoc.getItemValueString("BOOK_Agency_Interpreter");
	var firstName = ndoc.getItemValueString("BOOK_Agency_Interpreter_FirstName");
	var lastName = ndoc.getItemValueString("BOOK_Agency_Interpreter_LastName");
	if (!fullName.equals("")) {
		return fullName;
	} else {
		return firstName + " " + lastName;
	}
}

function BOOK_PatientAddressFields_Show(){	
	if(!GetKeyword('AllowHidePatientAddressFields').equalsIgnoreCase('Yes')){return true;}
	var strPatientType = document1.getItemValueString('BOOK_PatientType');
	return strPatientType == 'Home Visit';
}

/**
 * Added by Baljit Karwal on 22 May 2019
 * To display patient additional details section based on keyword
 */
function BOOK_PatientAdditionalDetails_Section_Show() {
	if (GetKeyword('AllowPatientAdditionalDetails').equalsIgnoreCase('Yes')) {return true;}
	return false;
}

/**
 * Added by Baljit Karwal on 23 May 2019
 * To get full list of IMS and client Booking Non Completion Reasons
 */
/*function BOOK_GetNCReasons () {
	var strKey = "vsNCReasonsList";
	if(viewScope.containsKey(strKey) == false) {	
		var vIMSList = GetKeyword('BOOK_Status_NonCompletion');
		var vClientList = GetKeyword('BOOK_Status_NonCompletion_Extra');
		var fullList;	
		if (vIMSList != null && viewScope.containsKey('vIMSList') == false) {
			viewScope.put('vIMSList', vIMSList);
			fullList = vIMSList;
		}
		if (vClientList != null && viewScope.containsKey('vClientList') == false) {
			viewScope.put('vClientList', vClientList);
			fullList = vIMSList.concat(vClientList).sort();
		}
		viewScope.put(strKey, fullList);	
	}
	return viewScope.get(strKey);
}*/
/**
 * Added by Baljit Karwal on 23 May 2019
 * To get full list of IMS and client Booking Non Completion Reasons
 * Updated to introduce 3 keywords by Baljit Karwal on 25 July 2019
 */
function BOOK_GetNCReasons () {
	var strKey = "vsNCReasonsList";
	if(sessionScope.containsKey(strKey) == false) {
		var vIMSList = GetKeyword('IMS_Core_NC_Reasons_List');
		var vIMSReqList = GetKeyword('IMS_Requestor_Core_NC_Reasons_List');
		var vClientList = GetKeyword('Client_Core_NC_Reasons_List');
		var fullList;	
		if (vIMSList != null && vIMSList.length > 0) {
			//sessionScope.put('vIMSList', vIMSList);
			fullList = vIMSList;
		}
		if (vIMSReqList != null && vIMSReqList.length > 0) {
			sessionScope.put('vIMSReqList', vIMSReqList);
			fullList = fullList.concat(vIMSReqList).sort();
		}
		if (vClientList != null && vClientList.length > 0) {
			//sessionScope.put('vClientList', vClientList);
			fullList = fullList.concat(vClientList).sort();
		}
		sessionScope.put(strKey, fullList);	
	}
	return sessionScope.get(strKey);
}

/**
 * Added by Baljit Karwal on 26 July 2019
 * To get Requestors Core Non completion Reason list
 */
function BOOK_GetRequestorNCReasons () {
	var strKey = "vIMSReqList";
	if(sessionScope.containsKey(strKey) == false) {
		var vIMSReqList = GetKeyword('IMS_Requestor_Core_NC_Reasons_List');
		var fullList;	
		if (vIMSReqList != null && vIMSReqList.length > 0) {
			sessionScope.put('vIMSReqList', vIMSReqList);
			fullList = fullList.concat(vIMSReqList).sort();
		}
		sessionScope.put(strKey, fullList);	
	}
	return sessionScope.get(strKey);
}

/**
 * Added by Baljit Karwal on 05 July 2019
 * To send updates to OnCall
 * Update document to have flags for booking amendment or amended cost centre
 */
function BOOK_SendUpdatesToOnCall(ndoc:NotesDocument) {
	try {
		var docX:NotesXspDocument = document1;
		var strStatus = ndoc.getItemValueString("BOOK_Status");
		if (ndoc.getItemValueString('BOOK_Agency_UpdateCostCentre').equalsIgnoreCase('Amended')) {
			ndoc.replaceItemValue('BOOK_Agency_UpdateCostCentre', 'Yes');
		} else if(ndoc.getItemValueString('BOOK_Agency_AmendDetails').equalsIgnoreCase('Amended')) {
			ndoc.replaceItemValue('BOOK_Agency_AmendDetails', 'Yes');
		}
		if (strStatus != 'api_amend_pending') {
			var strSD = BOOK_Status(ndoc, 'api_amend_pending');
			BOOK_LogAction(docX, 'Status changed to ' + strSD, true);
		}
	} catch (e) {
		view.postScript("Notify_Warning('ERROR: There was an error sending amendments to ONCALL, if the problem persists, please contact IT Support')");
		print("Error in BOOK_SendUpdatesToOnCall. Message: " + e);
	}
}

/**
 * Added by Baljit Karwal on 12 July 2019
 * To accept amendments for oncall bookings
 */
function BOOK_AcceptAmendmentRequest() {
	var docX:NotesXspDocument = document1;
	var ndoc:NotesDocument = docX.getDocument();
	BOOK_LogAction(docX, 'Booking Amendment Accepted' , true)
	BOOK_SendUpdatesToOnCall(ndoc);
	BOOK_Common_SaveOpen(ndoc);
}



