function Planner_DateRange(){
	var intWeekBack = 1;
	var dtStart = moment().subtract(intWeekBack, 'weeks').startOf('isoWeek');
	var dtEnd = moment().subtract(intWeekBack, 'weeks').endOf('isoWeek');
	
    $('.divPlannerRange').daterangepicker({
        startDate: dtStart,
        endDate: dtEnd,        
        ranges: {
           'Previous Month': [moment().subtract(1, 'months').date(1), moment().subtract(1, 'months').endOf('month')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Next Month': [moment().add(1, 'month').date(1), moment().add(1, 'month').endOf('month')]
        },
        locale: {format: "YYYY-MM-DD"}
    }, Planner_DateRange_CallBack);

    Planner_DateRange_CallBack(dtStart, dtEnd);
}

function Planner_DateRange_CallBack(dtStart, dtEnd) {	
	$('.divPlannerRange input').val(dtStart.format('DD MMM YYYY') + '  -  ' + dtEnd.format('DD MMM YYYY'));	
}

function Planner_Generate_OnComplete() {		
	
	Notify_Loading(false, 'btnGeneratePlanner');
	
	var bCheck = $('#tblPlanner').length <= 0;	
	$('.btnPrint').prop('disabled', bCheck); 
	$('.btnExport').prop('disabled', bCheck);
	
	if($('.btnPlannerAgent').length > 0){
		//if bCheck is true, planner is not generated
		//if bCheck is false, check if any dates lies within last month to update data
		if(bCheck == false){			
			var intYM_Start = parseInt($('.divPlannerRange').data('daterangepicker').startDate.format('YYYYMM'));
			var intYM_End = parseInt($('.divPlannerRange').data('daterangepicker').endDate.format('YYYYMM'));
			var intYM_LastMonth = parseInt(moment().subtract(1, 'months').format('YYYYMM'));
			//cLog(intYM_Start); cLog(intYM_End); cLog(intYM_LastMonth);
			//if any date NOT within last month, then button should be disabled, which means true 
			if(intYM_LastMonth != intYM_Start && intYM_LastMonth != intYM_End){
				bCheck = true	
			}			
		}
		
		$('.btnPlannerAgent').prop('disabled', bCheck);
	}	
	
	Planner_Activate_Table();
}

function Planner_Activate_Table(){
	
	if($('#tblPlanner').length > 0 & $('.scPlanner_EditAllowed').html() == 'true'){
		
		var allTDS = $("td[id^='TDH~']");
		
		//Add Click Event and onMouseOver pointer
		allTDS.bind({
			click: function() {Planner_EditHours_Show($(this).attr('id'), $(this).attr('class'))},
			mouseover: function() {$(this).css( "cursor", "pointer")}			
		});
		
		//Add Title
		allTDS.each(function(){
			var strID = $(this).attr('id');			
			var strInterpreter = $(this).attr("class");			
			$(this).attr("title", "Modify hours of '" + strInterpreter + "' on '" + strID.split('~')[2] + "'");			
        });
	}
	
}

function Planner_EditHours_Show(strID, strInterpreterName){
	
	var vCodes = strID.split('~');
	//cLog(vCodes); cLog(strID);
	//TDH~DocKey~Date~Action~Start1~End1~LunchFlag~Start2~End2
	
	var strMsg = "Do you wish to modify working hours of <b>'" + strInterpreterName + "'</b> for the following day ?"	
	$('#dlg_ChangeInterpreterHours_Date').html(vCodes[2]);
	$('#dlg_ChangeInterpreterHours_Msg').html(strMsg);
	
	$('.scPlanner_Action').val(vCodes[3]);	
	$('.scPlanner_InterperterName').val(strInterpreterName);
	$('.scPlanner_Date').val(vCodes[2]);
	$('.scPlanner_InterperterDocKey').val(vCodes[1]);
	
	if(vCodes[3] == 'Add' || vCodes[3] == 'Edit'){
		if(vCodes.length > 4){
			$('.fld_NewTime_Start_1').val(vCodes[4]);
			$('.fld_NewTime_End_1').val(vCodes[5]);
			var rbFldObj = $("input[type=radio][name*='fld_Lunch']");
			for(var i=0; i<rbFldObj.length; i++){
				var fld = $(rbFldObj[i]);
				if(fld.val() == vCodes[6]){
					fld.prop("checked", true);
				}			
			}		
			//Split hours present check
			$('.fld_NewTime_Start_2').val(((vCodes.length > 7) ? vCodes[7] : ''));
			$('.fld_NewTime_End_2').val(((vCodes.length > 7) ? vCodes[8] : ''));
			$('.fld_SplitHours').prop("checked", vCodes.length > 7);				
		} else {
			//Check the Auto flag, which gets cleared on close of previous dialog box in case
			$("input[type=radio][name*='fld_Lunch'][value='A']").prop("checked", true);
		}
	}
	
	JQ_ShowHide('divTime_2nd', vCodes.length > 7);
	JQ_HideSC($('.btnPlanner_Hours_Delete'), (vCodes[3] == 'Edit'));
	
	var dialogModal = $('#dlg_ChangeInterpreterHours');
	dialogModal.modal('show');
	dialogModal.on('hidden.bs.modal', function(){$('#dlg_ChangeInterpreterHours_Validation').html(''); ClearAllFields('dlg_ChangeInterpreterHours')});
}

function Planner_EditHours_Validate(){	
	if(ChangeInterpreterHours_Validate(true, 'DD-MMM-YYYY')){
		$('#dlg_ChangeInterpreterHours').modal('hide');
		$('.btnPlanner_Hours_AddEdit_SJS').click()
	};
}

function Planner_EditHours_Delete(){
	$('.scPlanner_Action').val('Delete');
	$('#dlg_ChangeInterpreterHours').modal('hide');
	ConfirmAction("This Action will Delete modified hours of <b>'" + $('.scPlanner_InterperterName').val() + "'</b> for " + $('.scPlanner_Date').val(), 'btnPlanner_Hours_Delete_SJS')
}
